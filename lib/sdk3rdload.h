/*****************************************************************************************

  Copyright (C), 2001-2015, Xinguodu Co., Ltd.

******************************************************************************************
  File Name     : chartobmp.h
  Version       : Initial Draft
  Author        : pxp
  Created       : 2015/10/08
  Last Modified :
  Description   :  This file contains char to bmp related function prototypes
  History       :
  1.Date        : 2015/10/08 
    Author	    : pxp
    Modification: Created file
  
********************************************************************************************/

#ifndef _SDK3RDLOAD_H_
#define _SDK3RDLOAD_H_


/*******************************************************************************************
Note:
1��Must call sdkSysLoad3rdDll function before using the following API

2�� Before exiting  main, If call sdkSysLoad3rdDll function you must call sdkSysUnLoad3rdDll function
*********************************************************************************************/
extern s32 sdkSysLoad3rdDll(u8 *pasErr, s32 siSize);    //must call this function before using the following API
extern s32 sdkSysUnLoad3rdDll(void);


/************************************ Biometric API **************************************/
typedef enum
{
    SDK_FPC_ISO = 1,    //ISO type
    SDK_FPC_MIAXIS      //MIAXIS type
}SDK_FPC_TYPE;


typedef enum
{
    SDK_FPC_LV1 = 1,    //Level 1
    SDK_FPC_LV2,
    SDK_FPC_LV3,
    SDK_FPC_LV4,
    SDK_FPC_LV5
}SDK_SAFE_LEVEL;

typedef enum
{
    SDK_CNT_2 = 2,
    SDK_CNT_3
}SDK_FEATURE_CNT;

//extern void sdkFpcReset(s32 iDelayMS);
//extern s32 sdkFpcInit(void);
extern s32 sdkFpcGetImage(u8 *pheImgData);
extern s32 sdkFpcEnroll(const u8 *pheImgData, SDK_FPC_TYPE eFpcType, u8 *pheFeature);
extern s32 sdkFpcMix(const u8 *pheFeatureA, const u8 *pheFeatureB, const u8 *pheFeatureC, u8 *pheTemplate, SDK_FPC_TYPE eFpcType, SDK_FEATURE_CNT eFeatureCnt);
extern s32 sdkFpcMatch(const u8 *pheFeature, const u8 *pheTemplate, SDK_SAFE_LEVEL eFpcLv, SDK_FPC_TYPE eFpcType);
extern s32 sdkFpcCheckImgData(const u8 *pheImgData);
/**************************************** END ********************************************/

/********************************** PIN & PUK API *************************************/
typedef enum
{
    SDK_SIM_UNKNOWN = -2,
    SDK_SIM_NO_SIM,         //no SIM
    SDK_SIM_NO_PIN,         //no PIN
    SDK_SIM_PIN,            //has PIN
    SDK_SIM_PIN2,           //has PIN2
    SDK_SIM_PUK,            //has PUK
    SDK_SIM_PUK2,           //has PUK2
}SDK_SIM_PINPUK_TYPE;

extern s32 sdkSimEnterIdle(void);
extern s32 sdkSimOpenPin(const u8 *pasPin);
extern s32 sdkSimClosePin(const u8 *pasPin);
extern s32 sdkSimGetStatus(void);
extern s32 sdkSimGetPinPukRemainTimes(SDK_SIM_PINPUK_TYPE ePinType);
extern s32 sdkSimInputPinPuk(SDK_SIM_PINPUK_TYPE ePinType, const u8 *pasPin, const u8 *pasPuk);
extern s32 sdkSimChangePin(const u8 *pasOldPin, const u8 *pasNewPin);
/**************************************** END ********************************************/

/********************************** FTP API *************************************/
extern s32 sdkFtpDownload(bool bisAnonymous, const u8 *pasUser, const u8 *pasPassword, const u8 *pasFtpFileURL,const u8 *pasLocalFilePath, bool bisUseBreakpoint);
extern s32 sdkFtpUpload(bool bisAnonymous, const u8 *pasUser, const u8 *pasPassword, const u8 *pasFtpFileURL, const u8 *pasLocalFilePath, bool bisUseBreakpoint);
/**************************************** END ********************************************/


/********************************** Image API *************************************/
extern s32 sdkShowImage(const u8 *pheImage, s32 siWidth, s32 siHeight, s32 siTop, s32 siLeft, u16 usDispColor);
extern s32 sdkPrintImage(const u8 *pheImage, s32 siWidth, s32 siHeight, s32 siLeft);
/**************************************** END ********************************************/


/**********************************  Tsptkey API ******************************************/
extern s32 sdkLoadTransportkey(u8 *pbcCheckValue);
extern s32 sdkUpdateEncryptTMK(s32 siTmkIndex, const u8 *pbcEncryptTmk, s32 silen, SDK_PED_DES_TYPE eKeyType,s32 siTimeout, u8 *pbcCheckValue);
/**************************************** END *********************************************/ 

#endif

