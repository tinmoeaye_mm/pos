/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : Billpay.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/3/17
   Last Modified :
   Description   : agent module-related functions are defined
                  here
   History       :
   1.Date        : 2016/3/17
    Author      : Thu Kha, Tin Moe Aye
    Modification: Created file

******************************************************************************/
#include "../inc/global.h"

const u8 asPathBillPay[] = "/mtd0/res/billpaymainfirst.bmp";
u8 agreementno[64] = {0};
char Invoice[20] = {0};
char Busnum[128]={0};
char bill [128] = {0};
int len = 0;
void CPPhNo(void);
void CPAmount(void);
void CPNum(void);
void CPConfirm(void);
void CPSendData(void);
void HelloCabVhNo(void);
void HelloCabDriverID(void);
void HelloCabAmount(void);
void HelloCabConfirm(void);
void HelloCabSendData(void);
void BnfBusBookingID(void);
void BnfTrip(void);
void ShowTrip(void);
void GoBusBookingID(void);
void GoBusTrip(void);
void GoBusTripInfo(void);
void BnfTripInfo(void);
void Select_AeonPay(void);
void AeonConfirm(void);
void AeonPhone(void);
void AeonAmount(void);
void AeonName(void);
void AeonAgreement(void);
void AeonPay_Cash(void);
void YescBillRefNum(void);
void YcdcBillRefNum(void);
void YcdcPhNum(void);
void TicketMenu(void);
void YescBillConfirm(void);
void YcdcConfirm(void);
void MptBillAmount(void);
void MptBillCharge(void);
void MptBillRefNum(void);
void MptBillPhNum(void);
void YescPhNum(void);
void CnpRefund(void);
void CnpRefundNum(void);
void RefundConfirm(void);
void TicketBoName(void);
void TicketBoPhone(void);
void TicketBoBookingID(void);
void TicketBoPayType(void);
void TicketBoPay_Cash(void);
void TicketBoAmount(void);
void TicketBoConfirm(void);
void YescTitanConfirm(void);
void YescTitanSendData(void);
void YescTitanPhNum(void);
void YescTitanAmt(void);
void YESCMainMenu(void);
void YescTitanBillNum(void);
void MescBillRefNum(void);
void MescPhNum(void);
void MescBillConfrim(void);
char *ForSendData();
void TicketConfirm(void);
void TicketPhone(void);
void TicketAmount(void);
void AirTicketMainMenu(void);
void TicketSendData(void);
 void MemBillPayLoginCheck(int i);
 void MemBillPayLoginFace(void);
 void ModuleCustomerReadCardForBillPay(void);
 void Select_TitanPay(void);
 void SelectHelloCabPayment(void);
 void SelectCNPCardPayment(void);
 void YESCSendData(void);
void WMSelectPayment(void);
void TicketCardSendData(void);
void NewMainMenu(void);
void CPInvoice(void);
void IhomeConfirm(void);
void IHomeref(void);
void IhomeLastData(void);
void BillPayMainMenu(void)
{
	s32 key = 0;
	u8 page;
	step1:
	page= 0;
	while(1)
	{	
		step2:
		if(page==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, asPathBillPay);
			sdkDispBrushScreen();
		}
		else if(page==1)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320,"/mtd0/res/billpaymenusecond.bmp");
			sdkDispBrushScreen();
		}
		
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | 
							   SDK_KEY_MASK_5 |SDK_KEY_MASK_6|SDK_KEY_MASK_7|SDK_KEY_MASK_8|SDK_KEY_MASK_9|SDK_KEY_MASK_UP | SDK_KEY_MASK_DOWN|SDK_KEY_MASK_ESC,KbWaitTime);
			
	switch (key)
	{
		 case SDK_KEY_1:
		 	
			{
				if(page==0)
				{
					printtype.BillMainMenu=Bill_Old_Menu;//For  mainmen
					AeonAgreement();
				}
				else if(page==1)
				{
					IHomeref();
				}
				
															
		 	}
			break;

		 case SDK_KEY_2:
			{
				if(page==0)
				{
					printtype.BillMainMenu=Bill_Old_Menu;
					TicketMenu();
				}
				
			}
			break;

		 case SDK_KEY_3:
			{
				if(page==0)
				{
					printtype.BillMainMenu=Bill_Old_Menu;
					HelloCabDriverID();
			
				}
					
			}
			break;

		 case SDK_KEY_4:
			{
				if (page==0)
				{
					printtype.BillMainMenu=Bill_Old_Menu;
					YESCMainMenu();
				}
				
			}
			break;

		 case SDK_KEY_5:
			{
				if(page==0)
				{
					printtype.BillMainMenu=Bill_Old_Menu;
			 		 YcdcBillRefNum();
				}
				
			}
			break;
		case SDK_KEY_6:
			{
				if(page==0)
				{
					printtype.BillMainMenu=Bill_Old_Menu;
					MptBillRefNum();
				}
				
			}
			break;	
		case SDK_KEY_7:
			{
				if(page==0)
				{
					printtype.BillMainMenu=Bill_Old_Menu;
					CPNum();
				}
					
			}
			break;
		case SDK_KEY_8:
			{
				if(page==0)
				{
					printtype.BillMainMenu=Bill_Old_Menu;
					AirTicketMainMenu();
					
				}
				
			}	
			break;
			 case SDK_KEY_UP:
                {
                    if (page == 1)
                    {
                        page=page-1;
                    }
                     goto step2;
                }
                break;

             case SDK_KEY_DOWN:
                {
                	formenu.billservice=pageflagon;
                    if (page == 0)
                    {
                        page=page+1;
                    }
                }
                break;
	
		 case SDK_KEY_ESC:
			{
				if(page==0)
				{
					MainMenu();
				}
				else if(page==1)
				{
					goto step1;
				}
			   
			}
			break;
		 default:
			{
				
			}
	
		}
		if(formenu.billservice==pageflagon)
		    {
		    	formenu.billservice=pageflagoff;
		    	goto step2;
			}
			else
			{
				break; 	
			}	
	}
	MainMenu();
}
void TicketMenu(void)
{
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/TicketMenu.bmp");
    sdkDispBrushScreen();

	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 |SDK_KEY_MASK_3| SDK_KEY_MASK_ESC, 60000);
	switch(key)
	{
		case SDK_KEY_1:
			{
				BnfBusBookingID();
			}
			break;
		case SDK_KEY_2:
			{
			
				TicketBoBookingID();
			}
			break;
		case SDK_KEY_3:
			{
				FunUndDev(3);
			}	
			break;	
		case SDK_KEY_ESC:
			{
				BillPayMainMenu();
			}
			break;
			default:
			{
				BillPayMainMenu();
			}
			
	}
	BillPayMainMenu();
}

void AeonAgreement(void)             // Aeon  agreement Start 
{
	u8 buf[16] = {0};
	s32 res;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/aeonagreenonew.bmp");
	sdkDispBrushScreen();
	secondtransactionstart=false;  // for pageagentcheckbalance
	memset(buf,0,sizeof(buf));
	res = GetFormatNumber(30*1000,buf,SDK_DISP_LINE3,16,16);
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("AeonAgreement", "buf= %s\r\n", buf);
				memset(sendData.asAeonAgreeNo,0,sizeof(sendData.asAeonAgreeNo));
				memcpy(sendData.asAeonAgreeNo,&buf[1],buf[0]);
				Trace("AeonAgreement", "sendData.asAeonAgreeNo= %s\r\n",sendData.asAeonAgreeNo);
				AeonName();
			}
			break;
		case SDK_KEY_ESC:        //for shortcutmenu & old menu
			{
				if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
			}
			break;
		default:
			{
				
			}	
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)   //for shortcutmenu & old menu
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
	
}

void AeonName(void)
{		
	u8 buf[16] = {0};
	s32 res;
	char src[32] = {0};

	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/AeonName.bmp");
	sdkDispBrushScreen();

	memset(buf,0,sizeof(buf));
	res = sdkKbGetScanf(60000, buf, 1, 20, SDK_MMI_LETTER ,SDK_DISP_LINE4);
	switch(res)
	 {
 		 case SDK_KEY_ENTER:
  			 {
				    Trace("AeonName", "buf= %s\r\n", buf);
				    Trace("AeonName","len=%d\r\n",strlen(buf));
				    memset(sendData.asName,0,sizeof(sendData.asName));
				
				    memcpy(sendData.asName,&buf[1],buf[0]);
				
				    memcpy(src,sendData.asName,strlen(sendData.asName));
				
				    Trace("AeonName","src=%s\r\n",src);
				
				    int len = 0, spaces = 0;
				
				    // Scan through src counting spaces and length at the same time //
				    while (src[len]) {
				     if (src[len] == ' ')
				       ++spaces;
				     ++len;
				    }
				
				    // Figure out how much space the new string needs (including 0-term) and allocate it //
				    int newLen = len + spaces*2 + 1;
				    char * dst = malloc(newLen);
				    // Scan through src and either copy chars or insert %20 in dst //
				    int srcIndex=0,dstIndex=0;
				    while (src[srcIndex]) {
				    if (src[srcIndex] == ' ') {
				      dst[dstIndex++]='%';
				      dst[dstIndex++]='2';
				      dst[dstIndex++]='0';
				      ++srcIndex;
				    } else {
				      dst[dstIndex++] = src[srcIndex++];
				    }
				    }
				    dst[dstIndex] = '\0';
				
				    Trace("AeonName","dst=%s\r\n",dst);
				
				    memset(sendData.asAeonName,0,sizeof(sendData.asAeonName));
				    memcpy(sendData.asAeonName,dst,strlen(dst));
				
				    Trace("AeonName","sendData.asAeonName=%s\r\n",sendData.asAeonName);
				    Trace("AeonName","sendData.asName=%s\r\n",sendData.asName);
				 
				    free(dst);
				  
				   
				    AeonAmount();
  			 }
			break;
		case SDK_KEY_ESC:
			{
				AeonAgreement();
			}
			break;
			default:
			{
				
			}	
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)             //for shortcutmenu & old menu
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
}

void AeonAmount(void)
{
	u8 buf[16] ={0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/aeonamountnew.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetAmountNumber(30*1000,buf, SDK_DISP_LINE3,3,8, NULL," KS");
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("AeonAmount", "buf= %s\r\n", buf);
				memset(sendData.asAmount,0,sizeof(sendData.asAmount));
				memcpy(sendData.asAmount,&buf[1],buf[0]);
				Trace("AeonAmount", "sendData.asAmount= %s\r\n", sendData.asAmount);
				AeonPhone();
			}
			break;
		case SDK_KEY_ESC:
			{
				AeonName();
			}
			break;	
			default:
			{
					
			}	
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)   //for shortcutmenu & old menu
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
}

void AeonPhone(void)
{
	u8 buf[16] = {0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/AeonPhone.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	switch(res)
	{	
		case SDK_KEY_ENTER:
			{
				Trace("AeonPhone","buf=%s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("AeonPhone","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				AeonConfirm();
			}
			break;
		case SDK_KEY_ESC:
			{
				AeonAmount();
			}
			break;
			default:
			{
				
			}	
		
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)          //for shortcutmenu & old menu
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
}

void AeonConfirm(void)
{
	 u8 buf[64] = {0};
	 s32 key=0;
	
	 sdkDispClearScreen();
	 sdkDispShowBmp(0,0,240,320,"/mtd0/res/aeonconfirm1.bmp");
	
	 sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	
	 memset(buf,0,sizeof(buf));
	
	 memset(agreementno,0,sizeof(agreementno));
	 strcpy(agreementno,sendData.asAeonAgreeNo);
	
	 memset(sendData.asAeonAgreeNo,0,sizeof(sendData.asAeonAgreeNo));
	 strcpy(sendData.asAeonAgreeNo,AgreementNumFormat(agreementno));
	 sprintf(buf, "%s", AgreementNumFormat(agreementno));
	Trace("AeonConfirm", "sendData.asAeonAgreeNo = %s\r\n", sendData.asAeonAgreeNo);
	 sdkDispAt(12, 75, buf);    
	       
	 memset(buf,0,sizeof(buf));
	 sprintf(buf, "%s", sendData.asName);
	 sdkDispAt(12, 140, buf);
	          
	 memset(buf,0,sizeof(buf));
	 sprintf(buf, "%sKs",sendData.asAmount);
	 sdkDispAt(12, 200, buf);
	          
	 memset(buf,0,sizeof(buf));
	 sprintf(buf, "09%s",sendData.asPhoneNum );
	 sdkDispAt(12, 270, buf);
	 
	 sdkDispBrushScreen();
	 key = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);
			
	 switch(key)
	 {
	  case SDK_KEY_ENTER:
	  {
	   Select_AeonPay();
	  } 
	  break;
	  case SDK_KEY_ESC:
	  {
	   
	  
	   AeonAgreement();
	  }
	  break;
	  default:
	  {
	   
	  } 
	 }
	
	if(printtype.BillMainMenu==Bill_Old_Menu)   //for shortcutmenu & old menu
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
}
void Select_AeonPay(void)
{	
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cashorTap.bmp");
	sdkDispBrushScreen();
	key = sdkKbWaitKey(SDK_KEY_MASK_1|SDK_KEY_MASK_2|SDK_KEY_MASK_ESC,KbWaitTime);
	printtype.BillPrint=AEON;
	switch(key)
	{
		case SDK_KEY_1:
			{
				printtype.BillPayment=MEMCARD;
				ModuleCustomerReadCardForBillPay();
			}
			break;
		case SDK_KEY_2:
			{
				
				printtype.BillPayment=CASH;
				AeonPay_Cash();	
			}
			break;
			default:
			{
				
			}	
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)  //for shortcutmenu & old menu
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
}
void AeonPay_Cash(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	ForRecall=true;

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	ForSendData();               //checktrx
	Trace("AeonPay_Cash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	memset(message,0,sizeof(message));
	if(printtype.BillPayment==CASH)
	{
		sprintf(message,"%s%s%s%s%s%s%s",sendData.asName,sendData.asAeonAgreeNo,sendData.asPhoneNum,sendData.asAmount,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
		Trace("AeonPay_Cash","message=%s\r\n",message);
		
	}
	else if(printtype.BillPayment==MEMCARD)
	{
		sprintf(message,"%s%s%s%s%s%s%s%s%s%s",sendData.asName,sendData.asAeonAgreeNo,sendData.asPhoneNum,sendData.asAmount,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
		Trace("AeonPay_Cash","message=%s\r\n",message);
	}
	
	
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("AeonPay_Cash","tmp = %s\r\n",sendData.asHashData);
	
if(printtype.BillPayment==CASH)
	{
		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf,"%s%s/aeonPayCash/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAeonName,sendData.asAeonAgreeNo,sendData.asPhoneNum,sendData.asAmount,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
	
	}
	else if(printtype.BillPayment==MEMCARD)
	{
		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf,"%s%s/aeonPayCard/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAeonName,sendData.asAeonAgreeNo,sendData.asPhoneNum,sendData.asAmount,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
	
	}


	ret = SendReceivedData(sendData.asSendBuf,false);
	
	memset(&receivedData,0,sizeof(receivedData));
	if(ret!=0)
	{
	
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			
			transaction=true;              //  for pageagentcheckbalance
			secondtransactionstart=true;   //  for pageagentcheckbalance
			
	
			js_tok(receivedData.Amount,"modifiedAmount",0);

		
			js_tok(receivedData.AeonPhoneNo,"phoneNo",0);
			
		
			js_tok(receivedData.MemberRef,"customerCardRef",0);
			
		
			js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
			
	
			js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
			
		
			js_tok(receivedData.AeonName,"name",0);
			
		
			js_tok(receivedData.AeonAgreeNo,"agreementNo",0);
			
		
			js_tok(receivedData.CardRef,"agentCardRef",0);

			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);

			
			js_tok(receivedData.TRX,"trx",0);
			
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);

			
			js_tok(receivedData.commission,"feeCharges",0);
			
					
			foragent = false;
			BillPrint(); 
			foragent = true;									
			BillPrint();
			CheckPaperRollStatus();
			if(printtype.BillMainMenu==Bill_Old_Menu)
			{
				MainMenu();
			}
			else if(printtype.BillMainMenu==Bill_New_Menu)
			{
				NewMainMenu();
			}
		}
 			
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;     //for pageagentcheckbalance
		
			js_tok(receivedData.ErrorMessage,"message",0);
			js_tok(receivedData.ErrorCode,"code",0);
			Trace("AeonPay_Cash","receiveddata.errorcode=%s\r\n",receivedData.ErrorCode);
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}
			else
			{
				DisplayErrorMessage("AEON",receivedData.ErrorMessage,false);
			}
			
		}
			
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	
	if(printtype.BillMainMenu==Bill_Old_Menu)  // for shortcut menu & old menu
	{
		MainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
		

}


void BnfBusBookingID(void)
{	
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/BnfBok.bmp");
	sdkDispBrushScreen();
	secondtransactionstart=false;
	memset(Busnum,0,sizeof(Busnum));
	key=sdkKbGetScanf(60000,Busnum,4,10,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				memset(sendData.asBnfBusNo,0,sizeof(sendData.asBnfBusNo));
				memcpy(sendData.asBnfBusNo,&Busnum[1],6);
				Trace("BnfBusBookingID","sendData.asBnfBusNo=%s\r\n",sendData.asBnfBusNo);
				printtype.BillPrint=BNF;
				BnfTripInfo();	
			}
			break;
		    
		 case SDK_KEY_ESC:
		 	{
		 		BillPayMainMenu();	
			}
		 break;
	   default:
	   		{
	   			BillPayMainMenu();
		  	}
	    
	}
	BillPayMainMenu();
}

void BnfTripInfo(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%sCash",sendData.asBnfBusNo,sendData.asAgentCardNo,sendData.asTerminalSN);
	Trace("BnfTripInfo","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out,32);
	Trace("BnfTripInfo","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/requestInfoForBnf/%s/%s/%s/%s/Cash/%s",uri,IPAddress,SvcVersion,sendData.asBnfBusNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	
	Trace("BnfTripInfo","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			
			js_tok(receivedData.Amount,"amount",0);
			
			js_tok(receivedData.BookingId,"bookingId",0);
			
			js_tok(receivedData.commission,"feeCharges",0);
		
			js_tok(receivedData.Trip,"trip",0);
		
			ShowTrip();
			
		}
		else
		{
			
			js_tok(receivedData.ErrorMessage,"message",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}
			else
			{
				DisplayErrorMessage("BNF",receivedData.ErrorMessage,false);
			}
			
			
		}
		
	}
	else
	{
		DisplayErrorMessage("","",true);
    }
	
	BillPayMainMenu();
	
}

void ShowTrip(void)
{
	s32 key;
	
	sdkDispClearScreen();
	
	sdkDispFillRowRam(SDK_DISP_LINE1,0,receivedData.Trip,SDK_DISP_LEFT_DEFAULT);
	
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"BID:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,receivedData.BookingId,SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"Amount:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,receivedData.Amount,SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispFillRowRam(SDK_DISP_LINE4,0,"Fees:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4,0,receivedData.commission,SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	sdkDispBrushScreen();
	
	key=sdkKbWaitKey(SDK_KEY_MASK_ESC|SDK_KEY_MASK_ENTER,60000);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				if(	printtype.BillPrint==BNF)
				{
					BnfTrip();
				}
				else if(printtype.BillPrint==GOBUS)
				{
					//GoBusTrip();
				}
			}
			break;
		case SDK_KEY_ESC:
			{
				BillPayMainMenu();
			}
			break;
			default:
			{
				BillPayMainMenu();
			}
		
	}
	BillPayMainMenu();
}

void BnfTrip(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	printtype.BillPayment=CASH;
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s",sendData.asBnfBusNo,sendData.asAgentCardNo,sendData.asTerminalSN);
	Trace("BnfTrip","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	Trace("BnfTrip","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/bnfCash/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asBnfBusNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	
	Trace("BnfTrip","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);			
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			memset(receivedData.TRUEREF,0,sizeof(receivedData.TRUEREF));
			js_tok(receivedData.TRUEREF,"tmmRefNo",0);
			
			memset(receivedData.bnf,0,sizeof(receivedData.bnf));
			js_tok(receivedData.bnf,"bookingId",0);
			
			memset(receivedData.BookingDate,0,sizeof(receivedData.BookingDate));
			js_tok(receivedData.BookingDate,"bookingDate",0);
			
			memset(receivedData.Amount,0,sizeof(receivedData.Amount));
			js_tok(receivedData.Amount,"amount",0);
			
			memset(receivedData.Trip,0,sizeof(receivedData.Trip));
			js_tok(receivedData.Trip,"trip",0);
			
			memset(receivedData.TripDate,0,sizeof(receivedData.TripDate));
			js_tok(receivedData.TripDate,"tDate",0);
			
			memset(receivedData.TripTime,0,sizeof(receivedData.TripTime));
			js_tok(receivedData.TripTime,"tTime",0);
			
			memset(receivedData.Bus,0,sizeof(receivedData.Bus));
			js_tok(receivedData.Bus,"bus",0);
			
			memset(receivedData.BusClass,0,sizeof(receivedData.BusClass));
			js_tok(receivedData.BusClass,"bClass",0);
			
			memset(receivedData.commission,0,sizeof(receivedData.commission));
			js_tok(receivedData.commission,"feeCharges",0);
		
			memset(receivedData.TotalAmount,0,sizeof(receivedData.TotalAmount));
			js_tok(receivedData.TotalAmount,"totalAmount",0);
		
			memset(receivedData.Passanger,0,sizeof(receivedData.Passanger));
			js_tok(receivedData.Passanger,"passenger1",0);
			
			memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
			js_tok(receivedData.CardRef,"agentCardRef",0);

			memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);


			memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			memset(receivedData.TRX,0,sizeof(receivedData.TRX));
			js_tok(receivedData.TRX,"trx",0);
			
			Trace("BnfTrip","receivedData.Amount = %s\r\n",receivedData.Amount);
			
			BillPrint();
			CheckPaperRollStatus();
			 	
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;   // for pageagentcheckbalance
		
			memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
			js_tok(receivedData.ErrorMessage,"message",0);
			
			DisplayErrorMessage("BNF",receivedData.ErrorMessage,false);
		}
	    
	
	}
	else 
	{
			DisplayErrorMessage("","",true);	
	}
	

	MainMenu();
}


 void HelloCabDriverID(void)
 {	
 	u8  buf[128]={0};
 	s32 key=0;
 	
 	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/driverid.bmp");
	sdkDispBrushScreen();
	secondtransactionstart=false;
	memset(buf,0,sizeof(buf));
	key=sdkKbGetScanf(60000,buf,4, 10,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				memset(sendData.asHelloCabDriverID,0,sizeof(sendData.asHelloCabDriverID));
        		memcpy(sendData.asHelloCabDriverID,&buf[1],buf[0]);
        		Trace("HelloCabDriverID","sendData.asHelloCabDriverID=%s\r\n",sendData.asHelloCabDriverID);
				HelloCabAmount();
			}
			
			break;
			case SDK_KEY_ESC:
			{
				BillPayMainMenu();
					
			}
			break;
			default:
			{
				BillPayMainMenu();
			}	
	}

	BillPayMainMenu();
	
}

void HelloCabAmount(void)
{	
	u8 buf[16] = {0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/purchase.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,3,8, NULL, " KS");
	switch(res)
	{
		case SDK_KEY_ENTER:
		{	
			Trace("HelloCabAmount", "buf= %s\r\n", buf);
			memset(sendData.asAmount,0,sizeof(sendData.asAmount));
			memcpy(sendData.asAmount,&buf[1],buf[0]);
			Trace("HelloCabAmount", "sendData.asAmount= %s\r\n", sendData.asAmount);
			HelloCabConfirm();
		}
		break;
		case SDK_KEY_ESC:
		{
			HelloCabDriverID();
		}
		break;
		default:
		{
			BillPayMainMenu();
		}	
	}
	BillPayMainMenu();
}
void HelloCabConfirm(void)
{						
	s32 key=0;
	u8 buf[64] = {0};
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS%s",sendData.asAmount);
	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"HELLOCABS ",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"AMOUNT   :",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,buf,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"DRIVER ID:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,sendData.asHelloCabDriverID,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	sdkDispBrushScreen();

	key = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);

	switch(key)
	{
		case  SDK_KEY_ENTER:
		{
		
			SelectHelloCabPayment();
		}
		break;
		case SDK_KEY_ESC:
		{
			HelloCabAmount();
		}
		break;
		default:
		{
			BillPayMainMenu();	
		}	
		
	}
	BillPayMainMenu();
}
void SelectHelloCabPayment(void)
{
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cashorTap.bmp");
	sdkDispBrushScreen();
	key = sdkKbWaitKey(SDK_KEY_MASK_1|SDK_KEY_MASK_2|SDK_KEY_MASK_ESC,KbWaitTime);
	printtype.BillPrint=HELLOCAB;
	switch(key)
	{
		case SDK_KEY_1:
			{
				printtype.BillPayment=MEMCARD;
				ModuleCustomerReadCardForBillPay();
			}
			break;
		case SDK_KEY_2:
			{
				
				printtype.BillPayment=CASH;
				HelloCabSendData();
			}
			break;
			default:
			{
				BillPayMainMenu();
			}	
	}
	BillPayMainMenu();	
}
void HelloCabSendData(void)
{	
	u8 out[128] = {0};
	u8 message[128] ={0};
	sdkDispBrushScreen();
	s32 ret;
	ForRecall=true;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	ForSendData();
	Trace("HelloCabSendData","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	if(printtype.BillPayment == CASH)
	{
			memset(message,0,sizeof(message));
			sprintf(message,"%s%s%s%s%s%s",sendData.asHelloCabDriverID,sendData.asAmount,"0",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
			Trace("HelloCabSendData","message=%s\r\n",message);
	}
	else if(printtype.BillPayment == MEMCARD)
	{
		memset(message,0,sizeof(message));
		sprintf(message,"%s%s%s%s%s%s%s%s%s",sendData.asHelloCabDriverID,sendData.asAmount,"0",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
		Trace("HelloCabSendData","message=%s\r\n",message);
	}

	
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("HelloCabSendData","tmp = %s\r\n",sendData.asHashData);
	
	if(printtype.BillPayment == CASH)
	{
		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf,"%s%s/HCabsCash/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asHelloCabDriverID,sendData.asAmount,"0",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
		
	}
	else if(printtype.BillPayment == MEMCARD)
	{
		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf,"%s%s/HCabsCard/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asHelloCabDriverID,sendData.asAmount,"0",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
	
	}
	
	Trace("HelloCabSendData","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
	
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			js_tok(receivedData.Amount,"amount",0);
			
			
			js_tok(receivedData.DriverId,"driverId",0);
			
			
			js_tok(receivedData.DriverName,"driverName",0);
			
		
			js_tok(receivedData.CardRef,"agentCardRef",0);

		
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			
			js_tok(receivedData.TRX,"trx",0);

			js_tok (receivedData.commission,"feeCharges",0);
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);
			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
			
			js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
			
			js_tok(receivedData.MemberRef,"customerCardRef",0);
			
			printtype.BillPrint=HELLOCAB;
			BillPrint();
			CheckPaperRollStatus();	
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;   // for pageagentcheckbalance
			    
			js_tok(receivedData.ErrorMessage,"message",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}
			else
			{
				DisplayErrorMessage("HELLOCABS",receivedData.ErrorMessage,false);
			}
			
			
		}
	}
	else
	
	{
		DisplayErrorMessage("","",true);
	}
	
	MainMenu();
}

void CPNum(void)
{	
	char CPNum[10]={0};
	int num=0;
	s32 key=0;

	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cpcv.bmp");
	sdkDispBrushScreen();

	secondtransactionstart=false;     // for pageagentcheckbalance
	memset(CPNum,0,sizeof(CPNum));
	//key=sdkKbGetScanf(60000,CPNum,4, 9,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	key=sdkKbGetScanf(60000,CPNum,4, 10,SDK_MMI_NUMBER|SDK_MMI_LETTER,SDK_DISP_LINE3);

	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				num=strlen(CPNum)-1;
				memset(sendData.asCPNum,0,sizeof(sendData.asCPNum));
				memcpy(sendData.asCPNum,&CPNum[1],num);
				Trace("xgd","sendData.asCPNum=%s\r\n",sendData.asCPNum);
				//CPPhNo();
				CPInvoice();
			}
			break;
		case SDK_KEY_ESC:
			{
				BillPayMainMenu();
			}
			break;
			default:
			{
				BillPayMainMenu();
			}	
	}
	BillPayMainMenu();
}
void CPInvoice(void)
{

	int num = 0;
	s32 key = 0;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cpinvoice.bmp");
	sdkDispBrushScreen();
//	int count=1;
	memset(Invoice,0,sizeof(Invoice));
	key=sdkKbGetScanf(60000,Invoice,0, 10,SDK_MMI_NUMBER,SDK_DISP_LINE3);


	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				num=strlen(Invoice)-1;
				memset(sendData.InvoiceNumber,0,sizeof(sendData.InvoiceNumber));
				memcpy(sendData.InvoiceNumber,&Invoice[1],num);
				Trace("CPInvoice","sendData.asCPInvoice=%s\r\n",sendData.InvoiceNumber);				
				CPPhNo();
			}
			break;
		case SDK_KEY_ESC:
			{
				CPNum();
			}
			break;
			default:
			{
				BillPayMainMenu();
			}	
	}
	BillPayMainMenu();

}
void CPPhNo(void)
{
	u8 buf[16] = {0};
	s32 res;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cpph.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(60000,buf, SDK_DISP_LINE3,7,10, "09", NULL,"ALL");

	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("CPPhNo", "buf= %s\r\n", buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("CPPhNo","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				CPAmount();
			}
		break;
		case SDK_KEY_ESC:
			{
				CPInvoice();
			}
		break;
		default:
		{
			BillPayMainMenu();
		}	
	}
	BillPayMainMenu();
	
}
void CPAmount(void)
{	
	u8 buf[16] = {0};
	s32 res;

	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cpamount.bmp");
	sdkDispBrushScreen();
	memset(buf,0,sizeof(buf));
	res = GetAmountNumber(60000, buf, SDK_DISP_LINE3,5,8, NULL, " KS");

	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("CPAmount", "buf= %s\r\n", buf);
				memset(sendData.asAmount,0,sizeof(sendData.asAmount));
				memcpy(sendData.asAmount,&buf[1],buf[0]);
				Trace("CPAmount","sendData.asAmount=%s\r\n",sendData.asAmount);
				
				CPConfirm();
			}
		break;
		case SDK_KEY_ESC:
			{
				CPPhNo();
			}
		break;
		default:
			{
				BillPayMainMenu();
			}
			
	}
	BillPayMainMenu();
}
void CPConfirm(void)
{
	u8 buf[32] = {0};
	s32 key=0;
	
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
//	s32 len = 0, len1 = 0, i;
//	u8 dispbuf[32+1] = {0};

	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	ForSendData();               //checktrx
	Trace("CPConfirm","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	
	if(strlen(sendData.InvoiceNumber) == 0)
	{
		strcpy(sendData.InvoiceNumber,"0");
		Trace("CPConfirm","sendData.asCPInvoice=%s\r\n",sendData.InvoiceNumber);	
	}
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s%s%s%s",sendData.asCPNum,sendData.InvoiceNumber,sendData.asPhoneNum,sendData.asAmount,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asAgentEnterPin,sendData.asEDCserial);
	Trace("CPConfirm","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("CPConfirm","tmp = %s\r\n",sendData.asHashData);

	
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/CPEnquiry/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCPNum,sendData.InvoiceNumber,sendData.asPhoneNum,sendData.asAmount,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);
	Trace("CPConfirm","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			memset(receivedData.commission,0,sizeof(receivedData.commission));		
			js_tok(receivedData.commission,"feeCharges",0);	
			
			
			memset(receivedData.ShopName,0,sizeof(receivedData.ShopName));
			js_tok(receivedData.ShopName,"shopName",0);			
		
			//sdkDispBrushScreen();
			
		
			Trace("CPConfirm","FeeCharges=%s\r\n",receivedData.commission);
			sdkDispClearScreen();
			
			sdkDispFillRowRam(SDK_DISP_LINE1,0,"CP FOOD PAYMENT",SDK_DISP_DEFAULT);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "PH NO  :09%s", sendData.asPhoneNum);
			sdkDispAt(12, 50, buf);			
				          
			memset(buf,0,sizeof(buf));
			sprintf(buf, "CV CODE:%s",sendData.asCPNum);
			sdkDispAt(12, 85, buf);
				          
			memset(buf,0,sizeof(buf));
			sprintf(buf, "AMOUNT :%sKS",sendData.asAmount);
			sdkDispAt(12, 120, buf);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "FEES   :%sKS",receivedData.commission);
			sdkDispAt(12, 155, buf);
			
			memset(buf,0,sizeof(buf));
			strcpy(receivedData.ShopAddress,"SHOP:");
			sprintf(buf,"%s",receivedData.ShopAddress);				
			sdkDispAt(12,190,buf);
			
			memset(buf,0,sizeof(buf));
			memset(receivedData.ShopAddress,0,sizeof(receivedData.ShopAddress));		
	
			/*
			memset(&st_font, 0, sizeof(st_font));
    		st_font.uiAscFont = SDK_PRN_ASCII8X16B;
    		sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
			*/
			s32 i,len,j=0;
			len = 32;
			for (i = 0; i != strlen(receivedData.ShopName); i++)
			{
			
					
					if(i<=13)
					{
						memset(buf,0,sizeof(buf));
						receivedData.ShopAddress[i] = receivedData.ShopName[i];
						sprintf(buf,"%s",receivedData.ShopAddress);
						sdkDispAt(75, 190, buf);
						Trace("CPConfirm","FirstShopName = %s\r\n",receivedData.ShopAddress);
					}
					else
					{
						memset(buf,0,sizeof(buf));
						receivedData.ShopNameEx[j++] = receivedData.ShopName[i];
						sprintf(buf,"%s",receivedData.ShopNameEx);
						sdkDispAt(12, 225, buf);
						Trace("CPConfirm","SecondShopName = %s\r\n",receivedData.ShopNameEx);		
					}			
					
				/*
				else				
				{
					memset(buf,0,sizeof(buf));
					receivedData.ShopNameEx[i] = receivedData.ShopName[i];
					Trace("xgd","i value = %s\r\n",i);
						sprintf(buf,"%s",receivedData.ShopNameEx);
						sdkDispAt(12, 225, buf);
						Trace("xgd","SecondShopName = %s\r\n",receivedData.ShopNameEx);
						
					
				
				}*/
				
			}
			
		/*	
			if( strlen(receivedData.ShopName) >= 14)
			{
				memset(buf,0,sizeof(buf));
				strcpy(receivedData.ShopAddress,"SHOP:");
				sprintf(buf,"%s",receivedData.ShopAddress);
				sdkDispAt(12,190,buf);
				memset(buf,0,sizeof(buf));
				sprintf(buf, "%s",receivedData.ShopName);
				sdkDispAt(12,225,buf);
			}
			else
			{
				
				memset(buf,0,sizeof(buf));
				sprintf(buf, "SHOP:%s",receivedData.ShopName);
				sdkDispAt(12, 190, buf);	
			}
			*/
			
			/*
			sdkDispFillRowRam(SDK_DISP_LINE2,0,"PH NO:",SDK_DISP_LEFT_DEFAULT);
			memset(buf,0,sizeof(buf));
			sprintf(buf,"09%s",sendData.asPhoneNum);
			sdkDispFillRowRam(SDK_DISP_LINE2,0,buf,SDK_DISP_RIGHT_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3,0,"CV NO   :",SDK_DISP_LEFT_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3,0,sendData.asCPNum,SDK_DISP_RIGHT_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE4,0,"AMOUNT  :",SDK_DISP_LEFT_DEFAULT);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf,"KS%s",sendData.asAmount);
			sdkDispFillRowRam(SDK_DISP_LINE4,0,buf,SDK_DISP_RIGHT_DEFAULT);
			
			sdkDispFillRowRam(SDK_DISP_LINE5,0,"FEES    :",SDK_DISP_LEFT_DEFAULT);
			memset(buf,0,sizeof(buf));
			sprintf(buf,"KS%s",receivedData.commission);
			sdkDispFillRowRam(SDK_DISP_LINE4,0,buf,SDK_DISP_RIGHT_DEFAULT);
			*/
			
			
			sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
			sdkDispBrushScreen();
			key=sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);
			switch(key)
			{	
				case SDK_KEY_ENTER:
					{
						
						CPSendData();
					}
					break;
				case SDK_KEY_ESC:
					{
					
						BillPayMainMenu();
					}
					break;
				default:
					{
						BillPayMainMenu();
					}	
			}
			BillPayMainMenu();		
					
						
		}
		else
		{
			sdkDispClearScreen();	
		
			js_tok(receivedData.ErrorMessage,"message",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			
			DisplayErrorMessage("CP",receivedData.ErrorMessage,false);
			
			sdkDispBrushScreen();
			
			sdkmSleep(500);
		
						
		}
		
	}
	else 
	{
		DisplayErrorMessage("","",true);
		
	}
	
	
}
void CPSendData(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	ForRecall=true;
	printtype.BillPayment=CASH;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	
	printtype.BillPrint=CP;
	

	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s%s%s%s",sendData.asCPNum,sendData.InvoiceNumber,sendData.asPhoneNum,sendData.asAmount,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asAgentEnterPin,sendData.asEDCserial);
	Trace("xgd","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);

	
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/CPCash/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCPNum,sendData.InvoiceNumber,sendData.asPhoneNum,sendData.asAmount,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
		
			js_tok(receivedData.Company,"company",0);
			
			js_tok(receivedData.Amount,"amount",0);
			//Trace("xgd","receivedData.Amount=%s\r\n",receivedData.Amount);
		
			js_tok(receivedData.commission,"feeCharges",0);
			
			js_tok(receivedData.SerialNo,"cv_code",0);
			
			js_tok(receivedData.InvoiceNo,"invoiceNo",0);
			
			js_tok(receivedData.ShopName,"shopName",0);
			
			
	
			
			js_tok(receivedData.PhoneNo,"phoneNo",0);
			Trace("xgd","receivedData.phno=%s\r\n",receivedData.PhoneNo);
			
			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);
			
			foragent = false;
			BillPrint(); 
			foragent = true;									
			BillPrint();
			CheckPaperRollStatus();
						
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;   // for pageagentcheckbalance
		
			js_tok(receivedData.ErrorMessage,"message",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
				{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}
			else
			{
				DisplayErrorMessage("CP",receivedData.ErrorMessage,false);
			}
						
		}
		
	}
	else 
	{
		DisplayErrorMessage("","",true);
		
	}

		MainMenu();		
}

void YescBillRefNum(void)
{	
	int num={0};
	s32 key;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpyesc.bmp");
	sdkDispBrushScreen();
	
	memset(bill,0,sizeof(bill));
	key =  sdkKbGetScanf(60000,bill,4, 25,SDK_MMI_LETTER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "bill= %s\r\n", bill);
				num=strlen(bill)-1;
				memset(sendData.asBill,0,sizeof(sendData.asBill));
				memcpy(sendData.asBill,&bill[1],num);
				Trace("xgd","sendData.asBill=%s\r\n",sendData.asBill);
				YescPhNum();
			}
			break;
			case SDK_KEY_ESC:
			{
			
				YESCMainMenu();	
			}
			default:
			{
			
			}	
	}

	YESCMainMenu();
	
}
	
void YescPhNum(void)
{
	s32 res;
	u8 out[128] = {0};
	u8 message[128] ={0};
	u8 buf[32] = {0};
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpmptphone.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd","buf=%s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("xgd","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				
				sdkDispClearScreen();	 
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
				sdkDispBrushScreen();
				
				memset(message,0,sizeof(message));
				sprintf(message,"%s%s%s%s%s",sendData.asBill,"YESC",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
				Trace("xgd","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asHashData, 0, 128);
				sdkBcdToAsc(sendData.asHashData, out, 32);
	
				Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf, "%s%s/billamountapi/%s/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,"YESC",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
				memset(&receivedData,0,sizeof(receivedData));
				res = SendReceivedData(sendData.asSendBuf,false);
				if(res!=0)
				{
					js_tok(receivedData.Status,"status",0);
					
					if(strcmp(receivedData.Status,"Success")==0)
					{
						js_tok(receivedData.Amount,"amount",0);
						
						js_tok(receivedData.commission,"charge",0);
						
						js_tok(receivedData.CustRef,"custRefNo",0);
						
						js_tok(receivedData.BillRefNo,"billRefNo",0);
						
						js_tok(receivedData.DueDate,"paymentDueDate",0);
						
						if (strcmp(receivedData.CustRef,"null")!=0) 
						{
							strcpy(receivedData.CopyCustRef,receivedData.CustRef);
						}
						 else 
						{
							strcpy(receivedData.CopyCustRef,receivedData.CustRef);
					 	}
						
						YescBillConfirm();
					
					}
					
					else
					{
						js_tok(receivedData.Status,"status",0);
						
						if (strcmp(receivedData.Status,"Success")!=0)
						{
							js_tok(receivedData.Message,"message",0);
							
							js_tok(receivedData.ErrorCode,"code",0);
							if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							else
							{
								DisplayErrorMessage("CNP",receivedData.Message,false);
							}
					
						}
					
					}
		
				}
				else 
				{
						DisplayErrorMessage("","",true);
				}
		
			}
			case SDK_KEY_ESC:
				{
					YescBillRefNum();
						
				}
				break;
			default:
				{
				
				}
					
	}
	YESCMainMenu();
	
}
void YescBillConfirm(void)
{

	s32 key=0;
	ForRecall=true;
	u8 buf[128]={0};
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "CNP PAYMENT");
	sdkDispAt(72, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",receivedData.Amount);
	sdkDispAt(108,60,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","CHARGE:");
	sdkDispAt(12,90,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",receivedData.commission);
	sdkDispAt(108,90,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","REF NUM:");
	sdkDispAt(12,120,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",receivedData.CopyCustRef);
	sdkDispAt(12,145,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","PAYMENT DUEDATE:");
	sdkDispAt(12,175,buf);
	memset(buf,0,sizeof(buf));
	strcpy(receivedData.CopyDueDate,receivedData.DueDate);
	sprintf(buf,"%s",date(receivedData.CopyDueDate));
	sdkDispAt(12,200,buf);
	
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);

	sdkDispBrushScreen();
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
		switch(key)
		{
			case SDK_KEY_ENTER:
				{
				
				SelectCNPCardPayment();	
				}
				break;
			case SDK_KEY_ESC:
				{
					if(printtype.BillMainMenu==Bill_Old_Menu)
					{
						BillPayMainMenu();
					}
					else if(printtype.BillMainMenu==Bill_New_Menu)
					{
						NewMainMenu();
					}		
				}
				break;
			default:
				{
				
				}
					
				
		}
	if(printtype.BillMainMenu==Bill_Old_Menu)
			{
				BillPayMainMenu();
			}
			else if(printtype.BillMainMenu==Bill_New_Menu)
			{
				NewMainMenu();
			}	
												
}
void SelectCNPCardPayment(void)
{
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cashorTap.bmp");
	sdkDispBrushScreen();
	key = sdkKbWaitKey(SDK_KEY_MASK_1|SDK_KEY_MASK_2|SDK_KEY_MASK_ESC,KbWaitTime);
	printtype.BillPrint=YESC;
	switch(key)
	{
		case SDK_KEY_1:
			{
				printtype.BillPayment=MEMCARD;
				ModuleCustomerReadCardForBillPay();
			}
			break;
		case SDK_KEY_2:
			{
				
				printtype.BillPayment=CASH;
				YESCSendData();
			}
			break;
			default:
			{
				
			}	
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
								
}
void YESCSendData(void)
{
	s32 ret;
	u8 out[128] = {0};
	u8 message[128] ={0};	
				
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	ForSendData();
	Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	if(printtype.BillPayment == CASH)
		
		{
			memset(message,0,sizeof(message));
			sprintf(message,"%s09%s%s%s%s%s%s%s%s",receivedData.CopyCustRef,sendData.asPhoneNum,"YESC",receivedData.Amount,receivedData.commission,receivedData.DueDate,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
			Trace("xgd","message=%s\r\n",message);
		
		}
		else if(printtype.BillPayment == MEMCARD)
		{
			memset(message,0,sizeof(message));
			sprintf(message,"%s09%s%s%s%s%s%s%s%s%s%s%s",receivedData.CopyCustRef,sendData.asPhoneNum,"YESC",receivedData.Amount,receivedData.commission,receivedData.DueDate,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
			Trace("xgd","message=%s\r\n",message);
		}			
		
		sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

		memset(sendData.asHashData, 0, 128);
		sdkBcdToAsc(sendData.asHashData, out, 32);
					
		Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
		if(printtype.BillPayment == CASH)
		
		{
			memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
			sprintf(sendData.asSendBuf, "%s%s/paymentbycash/%s/09%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,receivedData.CopyCustRef,sendData.asPhoneNum,"YESC",receivedData.Amount,receivedData.commission,receivedData.DueDate,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
				
		}
		else if(printtype.BillPayment==MEMCARD)
		{
			memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
			sprintf(sendData.asSendBuf, "%s%s/paymentbycard/%s/09%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,receivedData.CopyCustRef,sendData.asPhoneNum,"YESC",receivedData.Amount,receivedData.commission,receivedData.DueDate,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
		}
		ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			memset(receivedData.Status,0,sizeof(receivedData.Status));
			js_tok(receivedData.Status,"status",0);
						
				if(strcmp(receivedData.Status,"Success")==0)
				{
					
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					transaction=true;              //  for pageagentcheckbalance
					secondtransactionstart=true;   //  for pageagentcheckbalance
					memset(receivedData.TotalAmount,0,sizeof(receivedData.TotalAmount));
					js_tok(receivedData.TotalAmount,"totalBillAmount",0);
							
					memset(receivedData.BillAmount,0,sizeof(receivedData.BillAmount));
					js_tok(receivedData.BillAmount,"billAmount",0);
							
					memset(receivedData.BillRefNo,0,sizeof(receivedData.BillRefNo));
					js_tok(receivedData.BillRefNo,"refNo",0);
							
					memset(receivedData.commission,0,sizeof(receivedData.commission));
					js_tok(receivedData.commission,"charge",0);
							
					memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
					js_tok(receivedData.CardRef,"agentCardRef",0);
							
					memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));
					js_tok(receivedData.TerminalSerialNo,"terminalSNo",0);
							
					memset(receivedData.TRX,0,sizeof(receivedData.TRX));
					js_tok(receivedData.TRX,"txnId",0);

					memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
					js_tok(receivedData.TransactionLogDate,"txnDate",0);
					
					memset(receivedData.CustomerCardNum,0,sizeof(receivedData.CustomerCardNum));
					js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
					
					memset(receivedData.BalanceAmount,0,sizeof(receivedData.BalanceAmount));
					js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
					
					memset(receivedData.MemberRef,0,sizeof(receivedData.MemberRef));
					js_tok(receivedData.MemberRef,"customerCardRef",0);
							
					foragent = false;
					BillPrint(); 
					foragent = true;									
					BillPrint();
					CheckPaperRollStatus();
					if(printtype.BillMainMenu==Bill_Old_Menu)
					{
						MainMenu();
					}
					else if(printtype.BillMainMenu==Bill_New_Menu)
					{
						NewMainMenu();
					}
				
				}
							
				else
				{	
					if (strcmp(receivedData.Status,"Success")!=0)
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						transaction=false;		
						memset(receivedData.Message,0,sizeof(receivedData.Message));
						memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
						js_tok(receivedData.ErrorMessage,"errorMessage",0);
						js_tok(receivedData.Message,"message",0);
								
						if(strlen(receivedData.ErrorMessage)!=0)
						{
							DisplayErrorMessage("YESC",receivedData.ErrorMessage,false);
						}
						else 
						{
							DisplayErrorMessage("YESC",receivedData.Message,false);
						}
								
					}
				}
		}
					
		else
		{
			DisplayErrorMessage("","",true);
		}
		
		if(printtype.BillMainMenu==Bill_Old_Menu)
		{
			MainMenu();
		}
		else if(printtype.BillMainMenu==Bill_New_Menu)
		{
			NewMainMenu();
		}	
		
}
void MescBillRefNum(void)
{
	int num={0};
	s32 key;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpmesc.bmp");
	sdkDispBrushScreen();
	
	memset(bill,0,sizeof(bill));
	key =  sdkKbGetScanf(60000,bill,4, 25,SDK_MMI_LETTER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "bill= %s\r\n", bill);
				num=strlen(bill)-1;
				memset(sendData.asBill,0,sizeof(sendData.asBill));
				memcpy(sendData.asBill,&bill[1],num);
				Trace("xgd","sendData.asBill=%s\r\n",sendData.asBill);
				MescPhNum();
			}
			break;
			case SDK_KEY_ESC:
			{
			
				YESCMainMenu();	
			}
			default:
			{
				YESCMainMenu();	
			}	
	}

	YESCMainMenu();
}
void MescPhNum(void)
{
	s32 res;
	u8 out[128] = {0};
	u8 message[128] ={0};
	u8 buf[32] = {0};
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpmptphone.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd","buf=%s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("xgd","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				
				
				sdkDispClearScreen();	 
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
				sdkDispBrushScreen();
				
				memset(message,0,sizeof(message));
				sprintf(message,"%s%s%s%s%s",sendData.asBill,"YESC",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
				Trace("xgd","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asHashData, 0, 128);
				sdkBcdToAsc(sendData.asHashData, out, 32);
	
				Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf, "%s%s/billamountapi/%s/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,"YESC",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
				memset(&receivedData,0,sizeof(receivedData));
				res = SendReceivedData(sendData.asSendBuf,false);
				if(res!=0)
				{
					
					js_tok(receivedData.Status,"status",0);
					
					if(strcmp(receivedData.Status,"Success")==0)
					{
						
						
						js_tok(receivedData.Amount,"amount",0);
						
						js_tok(receivedData.commission,"charge",0);
						
						js_tok(receivedData.CustRef,"custRefNo",0);
						
						js_tok(receivedData.BillRefNo,"billRefNo",0);
						
						js_tok(receivedData.DueDate,"paymentDueDate",0);
						
						if (strcmp(receivedData.CustRef,"null")!=0) 
						{
							strcpy(receivedData.CopyCustRef,receivedData.CustRef);
						}
						 else 
						{
							strcpy(receivedData.CopyCustRef,receivedData.CustRef);
					 	}
						
						MescBillConfrim();
					
					}
					
					else
					{
						
						js_tok(receivedData.Status,"status",0);
						
						if (strcmp(receivedData.Status,"Success")!=0)
						{
							
							
							js_tok(receivedData.Message,"message",0);
							js_tok(receivedData.ErrorCode,"code",0);
							js_tok(receivedData.ErrorCode,"code",0);
							if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							else
							{
								//DisplayErrorMessage("CNP",receivedData.Message,false);
								DisplayErrorMessage("MESC",receivedData.Message,false);
							}
							
						}
					
					}
		
				}
				else 
				{
						DisplayErrorMessage("","",true);
				}
		
			}
			case SDK_KEY_ESC:
				{
					MescBillRefNum();
						
				}
				break;
			default:
				{
					YESCMainMenu();	
				}
	}	
}
void MescBillConfrim(void)
{
	s32 ret;
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key=0;
	u8 buf[128]={0};
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "MESC PAYMENT");
	sdkDispAt(72, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS%s",receivedData.Amount);
	sdkDispAt(108,60,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","CHARGE:");
	sdkDispAt(12,90,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",receivedData.commission);
	sdkDispAt(108,90,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","REF NUM:");
	sdkDispAt(12,120,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",receivedData.CopyCustRef);
	sdkDispAt(12,145,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","PAYMENT DUEDATE:");
	sdkDispAt(12,175,buf);
	memset(buf,0,sizeof(buf));
	strcpy(receivedData.CopyDueDate,receivedData.DueDate);
	sprintf(buf,"%s",date(receivedData.CopyDueDate));
	sdkDispAt(12,200,buf);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);

	sdkDispBrushScreen();
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
		switch(key)
		{
			case SDK_KEY_ENTER:
				{
					printtype.BillPrint=MESC;
					
					sdkDispClearScreen();	 
					sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
					sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
					sdkDispBrushScreen();
					
					memset(message,0,sizeof(message));
					sprintf(message,"%s09%s%s%s%s%s%s%s%s",receivedData.CopyCustRef,sendData.asPhoneNum,"YESC",receivedData.Amount,receivedData.commission,receivedData.DueDate,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
					Trace("xgd","message=%s\r\n",message);
					sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

					memset(sendData.asHashData, 0, 128);
					sdkBcdToAsc(sendData.asHashData, out, 32);
					
					Trace("xgd","tmp = %s\r\n",sendData.asHashData);
					
					memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
					sprintf(sendData.asSendBuf, "%s%s/paymentbycash/%s/09%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,receivedData.CopyCustRef,sendData.asPhoneNum,"YESC",receivedData.Amount,receivedData.commission,receivedData.DueDate,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
				
					ret = SendReceivedData(sendData.asSendBuf,false);
					if(ret!=0)
					{
						memset(receivedData.Status,0,sizeof(receivedData.Status));
						js_tok(receivedData.Status,"status",0);
						
						if(strcmp(receivedData.Status,"Success")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							memset(receivedData.TotalAmount,0,sizeof(receivedData.TotalAmount));
							js_tok(receivedData.TotalAmount,"totalBillAmount",0);
							
							memset(receivedData.BillAmount,0,sizeof(receivedData.BillAmount));
							js_tok(receivedData.BillAmount,"billAmount",0);
							
							memset(receivedData.BillRefNo,0,sizeof(receivedData.BillRefNo));
							js_tok(receivedData.BillRefNo,"refNo",0);
							
							memset(receivedData.commission,0,sizeof(receivedData.commission));
							js_tok(receivedData.commission,"charge",0);
							
							memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
							js_tok(receivedData.CardRef,"agentCardRef",0);
							
							memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));
							js_tok(receivedData.TerminalSerialNo,"terminalSNo",0);
							
							memset(receivedData.TRX,0,sizeof(receivedData.TRX));
							js_tok(receivedData.TRX,"txnId",0);

							memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
							js_tok(receivedData.TransactionLogDate,"txnDate",0);
							
							foragent = false;
							BillPrint(); 
							foragent = true;									
							BillPrint();
							CheckPaperRollStatus();	
 						
						}
							
						else
						{	
							if (strcmp(receivedData.Status,"Success")!=0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								memset(receivedData.Message,0,sizeof(receivedData.Message));
								memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
								js_tok(receivedData.ErrorMessage,"errorMessage",0);
								js_tok(receivedData.Message,"message",0);
								
								if(strlen(receivedData.ErrorMessage)!=0)
								{
									DisplayErrorMessage("MESC",receivedData.ErrorMessage,false);
								}
								else 
								{
									DisplayErrorMessage("MESC",receivedData.Message,false);
								}
								
							}
						}
					}
					
					else
					{
						DisplayErrorMessage("","",true);
					}
					
				}
			case SDK_KEY_ESC:
				{
					YESCMainMenu();
						
				}
				break;
			default:
				{
					YESCMainMenu();
				}
					
				
		}

	if(printtype.BillMainMenu==Bill_Old_Menu)  // for shortcut menu & old menu
			{
				MainMenu();
			}
	else if(printtype.BillMainMenu==Bill_New_Menu)
			{
				NewMainMenu();
			}
}	
void YcdcBillRefNum(void)
{
	int num={0};
	
	s32 key;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpycdc.bmp");
	sdkDispBrushScreen();
	secondtransactionstart=false;
	memset(bill,0,sizeof(bill));
	key =  sdkKbGetScanf(60000,bill,4,25,SDK_MMI_LETTER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "bill= %s\r\n", bill);
				num=strlen(bill)-1;
				memset(sendData.asBill,0,sizeof(sendData.asBill));
				memcpy(sendData.asBill,&bill[1],num);
				Trace("xgd","sendData.asBill=%s\r\n",sendData.asBill);
				YcdcPhNum();
			}
			break;
			case SDK_KEY_ESC:
			{
				BillPayMainMenu();	
			}
			default:
			{
				BillPayMainMenu();
			}	
	}
	BillPayMainMenu();
}
void YcdcPhNum(void)
{
	s32 res;
	u8 out[128] = {0};
	u8 message[128] ={0};
	u8 buf[128]={0};
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpmptphone.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd","buf=%s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("xgd","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				
				sdkDispClearScreen();	 
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
				sdkDispBrushScreen();
				
				memset(message,0,sizeof(message));
				sprintf(message,"%s%s%s%s%s",sendData.asBill,"YCDC",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
				Trace("xgd","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asHashData, 0, 128);
				sdkBcdToAsc(sendData.asHashData, out, 32);
			
				Trace("xgd","tmp = %s\r\n",sendData.asHashData);
				
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf, "%s%s/billamountapi/%s/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,"YCDC",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
				
				Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
				memset(&receivedData,0,sizeof(receivedData));
				res = SendReceivedData(sendData.asSendBuf,false);
				if(res!=0)
				{
					
					js_tok(receivedData.Status,"status",0);
					
					
					if(strcmp(receivedData.Status,"Success")==0)
					{
						js_tok(receivedData.Amount,"amount",0);
						
						js_tok(receivedData.commission,"charge",0);
						
						js_tok(receivedData.BillRefNo,"billRefNo",0);
						
						js_tok(receivedData.CustRef,"custRefNo",0);
						
						js_tok(receivedData.DueDate,"paymentDueDate",0);
						
						if (strcmp(receivedData.BillRefNo,"null")!=0) 
						{
							strcpy(receivedData.CopyBillRefNo,receivedData.BillRefNo);
						}
						 else 
						 {
						 	strcpy(receivedData.CopyBillRefNo,receivedData.CustRef);
						 }
						
						YcdcConfirm();
					}
					else
					{
						
						
						js_tok(receivedData.ErrorMessage,"errorMessage",0);
						
					
						js_tok(receivedData.Message,"message",0);	
						js_tok(receivedData.ErrorCode,"code",0);
						js_tok(receivedData.ErrorCode,"code",0);
						if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
						{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
						
						if (strcmp(receivedData.ErrorMessage,"null")!=0) 
						{
							DisplayErrorMessage("YCDC",receivedData.ErrorMessage,false);
						}
						else
						{
							DisplayErrorMessage("YCDC",receivedData.Message,false);
						}
					}
						
						
				}
				else
				{
					DisplayErrorMessage("","",true);
				}
										
						 	 		
			}
			break;
			case  SDK_KEY_ESC:
			{
				
				YcdcBillRefNum();
			}
			break;
			default:
			{
				BillPayMainMenu();						
			}
			
	}
	BillPayMainMenu();
}


void YcdcConfirm(void)
{
	s32 ret;
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key=0;
	ForRecall=true;
	u8 buf[128]={0};
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "YCDC PAYMENT");
	sdkDispAt(72, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",receivedData.Amount);
	sdkDispAt(108,60,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","CHARGE:");
	sdkDispAt(12,90,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",receivedData.commission);
	sdkDispAt(108,90,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","REF NUM:");
	sdkDispAt(12,120,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",receivedData.CopyBillRefNo);
	sdkDispAt(12,145,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","PAYMENT DUEDATE:");
	sdkDispAt(12,175,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",receivedData.DueDate);
	sdkDispAt(12,200,buf);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);

	sdkDispBrushScreen();
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
	 switch(key)
	 {
	 	case SDK_KEY_ENTER:
		{
			printtype.BillPrint=YCDC;
			printtype.BillPayment=CASH;
			
			sdkDispClearScreen();	 
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
			
			memset(message,0,sizeof(message));
			sprintf(message,"%s09%s%s%s%s%s%s%s%s",receivedData.CopyBillRefNo,sendData.asPhoneNum,"YCDC",receivedData.Amount,receivedData.commission,receivedData.DueDate,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
			Trace("xgd","message=%s\r\n",message);
			sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
			
			memset(sendData.asHashData, 0, 128);
			sdkBcdToAsc(sendData.asHashData, out, 32);
			
			Trace("xgd","tmp = %s\r\n",sendData.asHashData);
			
			
			memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
			sprintf(sendData.asSendBuf, "%s%s/paymentbycash/%s/09%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,receivedData.CopyBillRefNo,sendData.asPhoneNum,"YCDC",receivedData.Amount,receivedData.commission,receivedData.DueDate,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
			ret = SendReceivedData(sendData.asSendBuf,false);
			
			if(ret!=0)
			{
				memset(receivedData.Status,0,sizeof(receivedData.Status));
				js_tok(receivedData.Status,"status",0);
				
										
				if(strcmp(receivedData.Status,"Success")==0)
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					
					transaction=true;   // for pageagentcheckbalance
					secondtransactionstart=true;     // for pageagentcheckbalance
					memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));
					js_tok(receivedData.TerminalSerialNo,"terminalSNo",0);
					
					memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
					js_tok(receivedData.TransactionLogDate,"txnDate",0);

					memset(receivedData.TRX,0,sizeof(receivedData.TRX));
					js_tok(receivedData.TRX,"txnId",0);
														
					memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
					js_tok(receivedData.CardRef,"agentCardRef",0);
														
					memset(receivedData.BillRefNo,0,sizeof(receivedData.BillRefNo));
					js_tok(receivedData.BillRefNo,"refNo",0);
														
					memset(receivedData.Amount,0,sizeof(receivedData.Amount));
					js_tok(receivedData.Amount,"billAmount",0);
														
					memset(receivedData.TotalAmount,0,sizeof(receivedData.TotalAmount));
					js_tok(receivedData.TotalAmount,"totalBillAmount",0);
														
					memset(receivedData.commission,0,sizeof(receivedData.commission));
					js_tok(receivedData.commission,"charge",0);
							
							
					foragent = false;							
					BillPrint(); 
					foragent = true;									
					BillPrint();
					CheckPaperRollStatus();
 					
														
				}
				else
				{	
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					
					transaction=false;   // for pageagentcheckbalance
		
					memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
					js_tok(receivedData.ErrorMessage,"errorMessage",0);
					
					memset(receivedData.Message,0,sizeof(receivedData.Message));
					js_tok(receivedData.Message,"message",0);	
					
					if (strcmp(receivedData.ErrorMessage,"null")!=0) 
					{
						DisplayErrorMessage("YCDC",receivedData.ErrorMessage,false);
					}
					else
					{
						DisplayErrorMessage("YCDC",receivedData.Message,false);
					}
				}
				
			}
			else
			{
				DisplayErrorMessage("","",true);
			}
			
		}
		break;
		case  SDK_KEY_ESC:
		{
			BillPayMainMenu();
		}
		break;
		default:
		{
										
		}
		
	}

	MainMenu();
}
void MptBillRefNum(void)
{
	s32 key=0;
	int num={0};
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpmpt.bmp");
	sdkDispBrushScreen();
	transaction=false;   // for pageagentcheckbalance
		
	memset(bill,0,sizeof(bill));
	key =  sdkKbGetScanf(60000,bill,4, 25,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
					Trace("xgd", "bill= %s\r\n", bill);
					num=strlen(bill)-1;
					memset(sendData.asBill,0,sizeof(sendData.asBill));
					memcpy(sendData.asBill,&bill[1],num);
					Trace("xgd","sendData.asBill=%s\r\n",sendData.asBill);
					MptBillPhNum();
			}
			break;
		case SDK_KEY_ESC:
			{
				BillPayMainMenu();
			}
			break;
			default:
			{
				BillPayMainMenu();	
			}
			
	}
	BillPayMainMenu();
}
void MptBillPhNum(void)
{
	u8 buf[16] = {0};
	s32 res;
		
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpmptphone.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	switch(res)
	{
		case SDK_KEY_ENTER:
		{
			Trace("xgd","buf=%s\r\n",buf);
			memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
			memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
			Trace("xgd","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
			MptBillAmount();
		}
		break;
		case SDK_KEY_ESC:
		{
			MptBillRefNum();
					
		}
		break;
		default:
		{
			BillPayMainMenu();
		}	
	
	}
	BillPayMainMenu();
	
}

void MptBillAmount(void)
{
	s32 key=0;
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	ForRecall=true;
	int num={0};
	u8 buf[128]={0};
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cnpmpttotalamount.bmp");
	sdkDispBrushScreen();
	
	memset(bill,0,sizeof(bill));
	key =  sdkKbGetScanf(60000,bill,3, 25,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				printtype.BillPrint=MPTBILL;
				Trace("xgd","bill= %s\r\n",bill);
				num=strlen(bill)-1;
				memset(sendData.asBillAmount,0,sizeof(sendData.asBillAmount));
				memcpy(sendData.asBillAmount,&bill[1],num);
				Trace("xgd","sendData.asBillAmount=%s\r\n",sendData.asBillAmount);
				
				sdkDispSetFontSize(SDK_DISP_FONT_BIG);
				sdkDispClearScreen();
				
				memset(buf,0,sizeof(buf));
				sprintf(buf, "%s", "MPTBILL PAYMENT");
				sdkDispAt(63, 20, buf);
				
				memset(buf,0,sizeof(buf));
				sprintf(buf,"%s","MPT Phone No:");
				sdkDispAt(12,60,buf);
				memset(buf,0,sizeof(buf));
				sprintf(buf,"%s",sendData.asBill);
				sdkDispAt(12,85,buf);
				
				
				memset(buf,0,sizeof(buf));
				sprintf(buf,"%s","AMOUNT:");
				sdkDispAt(12,115,buf);
				memset(buf,0,sizeof(buf));
				sprintf(buf,"%s",sendData.asBillAmount);
				sdkDispAt(108,115,buf);
				
				
				sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
				sdkDispBrushScreen();
				
				key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
				switch(key)
				{
					case SDK_KEY_ENTER:
						
						{
							printtype.BillPayment=CASH;
							sdkDispClearScreen();	 
							sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
							sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
							sdkDispBrushScreen();
				
							sprintf(message,"%s09%s%s%s%s%s%s%s%s",sendData.asBill,sendData.asPhoneNum,"MPT",sendData.asBillAmount,"0","0",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
							sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
							
							memset(sendData.asHashData, 0, 128);
							sdkBcdToAsc(sendData.asHashData, out, 32);
				
							Trace("xgd","tmp = %s\r\n",sendData.asHashData);
							memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
							sprintf(sendData.asSendBuf,"%s%s/paymentbycash/%s/09%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,sendData.asPhoneNum,"MPT",sendData.asBillAmount,"0","0",sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
							
							Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
							memset(&receivedData,0,sizeof(receivedData));
							ret = SendReceivedData(sendData.asSendBuf,false);
				
							if(ret!=0)
							
							{
						
								js_tok(receivedData.Status,"status",0);
					
								
					
								if(strcmp(receivedData.Status,"Success")==0)
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
									transaction=true;   // for pageagentcheckbalance
									secondtransactionstart=true;     // for pageagentcheckbalance
									js_tok(receivedData.commission,"charge",0);
									
									
									js_tok(receivedData.TotalAmount,"totalBillAmount",0);
									
									
									js_tok(receivedData.Amount,"billAmount",0);
									
								
									js_tok(receivedData.BillRefNo,"refNo",0);
									
									
									js_tok(receivedData.TerminalSerialNo,"terminalSNo",0);
									
								
									js_tok(receivedData.TransactionLogDate,"txnDate",0);
				
									
									js_tok(receivedData.TRX,"txnId",0);
																		
								
									js_tok(receivedData.CardRef,"agentCardRef",0);
									
									
									js_tok(receivedData.TotalAmount,"totalBillAmount",0);
										
									foragent = false;									
									BillPrint(); 
									foragent = true;									
									BillPrint();
									CheckPaperRollStatus();
									MainMenu();	
			 										
								}
								else
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
										transaction=false;   // for pageagentcheckbalance
									js_tok(receivedData.ErrorMessage,"message",0);
									js_tok(receivedData.ErrorCode,"code",0);
								if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
								{
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
										sdkDispBrushScreen();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}
									else
									{
										DisplayErrorMessage("MptBillAmount",receivedData.ErrorMessage,false);
									}
								
								}
							
							}
							else
							{
								DisplayErrorMessage("","",true);
							}
					
						}
					break;
					case SDK_KEY_ESC:
					{
						BillPayMainMenu();
					}			
					
				}
			}
			case  SDK_KEY_ESC:
				{
					BillPayMainMenu();
				}
			break;
				default:
				{
												
				}
	}
	
		MainMenu();
}
void CnpRefundNum(void)
{
	int num={0};
	s32 key;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/refundbillnumber.bmp");
	sdkDispBrushScreen();
	
	memset(bill,0,sizeof(bill));
	key =  sdkKbGetScanf(60000,bill,4, 25,SDK_MMI_LETTER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "bill= %s\r\n", bill);
				num=strlen(bill)-1;
				memset(sendData.asBill,0,sizeof(sendData.asBill));
				memcpy(sendData.asBill,&bill[1],num);
				Trace("xgd","sendData.asBill=%s\r\n",sendData.asBill);
				CnpRefund();
			}
			break;
			case SDK_KEY_ESC:
			{
				BillPayMainMenu();	
			}
			break;
			default:
			{
				BillPayMainMenu();
			}	
	}
	BillPayMainMenu();
}
void CnpRefund(void)
{
	s32 ret;
	u8 out[128] = {0};
	u8 message[128] ={0};
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s",sendData.asBill,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);

	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/refund/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0) 
	{
			
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
			
				
				js_tok(receivedData.Amount,"billAmount",0);
				RefundConfirm();									
													
			}
			else
			{
				
				js_tok(receivedData.ErrorMessage,"message",0);
		
				DisplayErrorMessage("CNP REFUND",receivedData.ErrorMessage,false);
			}
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	BillPayMainMenu();							
			
}
void RefundConfirm(void)
{		
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	u8 buf[128]={0};
	ForRecall=true;
	
	s32 key=0;	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "REFUND PAYMENT");
	sdkDispAt(72, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","REFUND NUM:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asBill);
	sdkDispAt(12,85,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT :");
	sdkDispAt(12,115,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",receivedData.Amount);
	sdkDispAt(108,115,buf);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispBrushScreen();
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
	switch(key)
		{
			case SDK_KEY_ENTER:
				{
					
					sdkDispClearScreen();	 
					sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
					sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
					sdkDispBrushScreen();
					
					sprintf(message,"%s%s%s%s%s",sendData.asBill,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.Amount);
					sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
			
					memset(sendData.asHashData, 0, 128);
					sdkBcdToAsc(sendData.asHashData, out, 32);
					
					Trace("xgd","tmp = %s\r\n",sendData.asHashData);
					
					memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
					sprintf(sendData.asSendBuf, "%s%s/refund/%s/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.Amount,sendData.asHashData);
					memset(&receivedData,0,sizeof(receivedData));
					ret = SendReceivedData(sendData.asSendBuf,false);
					
					if(ret!=0)
					{
							
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
									
									js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

									
									js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
							
								
									js_tok(receivedData.TRX,"trxID",0);
									
								
									js_tok(receivedData.BillType,"type",0);
									
								
									js_tok(receivedData.CardRef,"agentCardRef",0);
									
									
									js_tok(receivedData.Amount,"billAmount",0);
									
									PrintRefund(); 
									foragent = true;									
									PrintRefund();
									
							}
							else
							{
								
								js_tok(receivedData.ErrorMessage,"message",0);
						
								DisplayErrorMessage("BILL REFUND",receivedData.ErrorMessage,false);
							}
					}
					else
					{
						DisplayErrorMessage("","",true);	
					}
						BillPayMainMenu();
				}
				case SDK_KEY_ESC:
				{
					BillPayMainMenu();	
				}
				break;
				default:
				{
					BillPayMainMenu();
				}	
		}
		BillPayMainMenu();
}

void TicketBoBookingID(void)
{
	s32 key=0;
	u8 buf[16] = {0};
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/TickeBoBookingId.bmp");
	sdkDispBrushScreen();
	secondtransactionstart=false;
	memset(buf,0,sizeof(buf));
	key=sdkKbGetScanf(60000,buf,1,10,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				memset(sendData.asTicketBookingID,0,sizeof(sendData.asTicketBookingID));
				memcpy(sendData.asTicketBookingID,&buf[1],10);
				Trace("xgd","sendData.asTicketBookingID=%s\r\n",sendData.asTicketBookingID);
				TicketBoPayType();	
			}
			break;
		    
		 case SDK_KEY_ESC:
		 	{
		 		BillPayMainMenu();	
			}
		 break;
	   default:
	   		{
	   			BillPayMainMenu();
		  	}
	    
	}
	BillPayMainMenu();
}

void TicketBoPayType(void)
{
	s32 key=0;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cashorTap.bmp");
	sdkDispBrushScreen();
	
	key=sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 |SDK_KEY_MASK_ESC, KbWaitTime);
	switch(key)
	{
		case SDK_KEY_1:
		{
			FunUndDev(3);
		}
		break;
		case SDK_KEY_2:
		{
			
			TicketBoPay_Cash();	
				
		}
		break;
		case SDK_KEY_ESC:
		{
			BillPayMainMenu();	
		}
		break;
		default:
		{
			BillPayMainMenu();
		}	
		break;
	}
	BillPayMainMenu();
		
}
void TicketBoPay_Cash(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	ForRecall=true;
	printtype.BillPrint=TICKETBO;
	printtype.BillPayment=CASH;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sdkDispBrushScreen();
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s",sendData.asTicketBookingID,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/ticketbo/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asTicketBookingID,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
	
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{	
			
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			
			js_tok(receivedData.BookingId,"booking_number",0);
			
			js_tok(receivedData.Amount,"amount",0);
			
			
			js_tok(receivedData.departcity,"depart_city",0);
			
			js_tok(receivedData.arrivalcity,"arrival_city",0);
			
			
			js_tok(receivedData.TripDate,"depart_date",0);
			
			
			js_tok(receivedData.TripTime,"depart_time",0);
			
			
			js_tok(receivedData.Bus,"bus_company",0);
			
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

		
			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
			
			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			
			js_tok(receivedData.commission,"charge",0);
			
			js_tok(receivedData.Passanger,"passenger_name",0);
		
		
			js_tok(receivedData.ShopName,"seat_name",0);
		
			js_tok(receivedData.TotalAmount,"totalAmount",0);
	
			
			TicketBoConfirm();
		}
		else
		{
			
			
			transaction=false;   // for pageagentcheckbalance
			
			js_tok(receivedData.ErrorMessage,"errorMessage",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			if(strcmp(receivedData.ErrorCode,"Err00077")==0||strcmp(receivedData.ErrorCode,"Err00023")||strcmp(receivedData.ErrorCode,"Err00086")||strcmp(receivedData.ErrorCode,"Err00110")||strcmp(receivedData.ErrorCode,"Err00136")||strcmp(receivedData.ErrorCode,"Err00167")||strcmp(receivedData.ErrorCode,"Err00245"))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}	
			else
			{
				DisplayErrorMessage("TIKKETBO",receivedData.ErrorMessage,false);
			}
			
			
		
		}
		
			
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	MainMenu();
}
void TicketBoConfirm(void)
{
	s32 key=0;
	sdkDispClearScreen();
	
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"DEPART CITY:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE1,0,receivedData.departcity,SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"ARRIVAL CITY:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,receivedData.arrivalcity,SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"AMOUNT:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,receivedData.Amount,SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispFillRowRam(SDK_DISP_LINE4,0,"FEES:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4,0,receivedData.commission,SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"PRESS [ENTER] TO CONTINUE",SDK_DISP_LEFT_DEFAULT);
	
	
	sdkDispBrushScreen();
	key=sdkKbWaitKey(SDK_KEY_MASK_ENTER,60000);
	{
		switch(key)
		{
			case SDK_KEY_ENTER:
				{
					BillPrint();
					CheckPaperRollStatus();
						
				}
				break;
			case SDK_KEY_ESC:
				{
					MainMenu();	
				}
				break;
			default:
				{
						MainMenu();	
				}	
				break;
					
		}
	}

	MainMenu();
	

}

void YESCMainMenu(void)
{
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/TitanMainMenu.bmp");
    sdkDispBrushScreen();
	secondtransactionstart=false;  // for pageagentcheckbalance
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);
	switch(key)
	{
		case SDK_KEY_1:
			{
				YescBillRefNum();	
			}
			break;
		case SDK_KEY_2:
			{
			
				YescTitanBillNum();
			
			}
			break;
		
		case SDK_KEY_ESC:
			{
					if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
			}
			break;
			default:
			{
				
			}
			
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)
		{
			BillPayMainMenu();
		}
	else if(printtype.BillMainMenu==Bill_New_Menu)
		{
			NewMainMenu();
		}
}
void YescTitanBillNum(void)
{
	
	s32 key;
	sdkDispClearScreen();

	sdkDispShowBmp(0,0,240,300,"/mtd0/res/Titanpayment.bmp");
	sdkDispShowBmp(0, 60, 240, 150,"/mtd0/res/Titanbillref.bmp");
	sdkDispBrushScreen();
	
	memset(bill,0,sizeof(bill));
	key =  sdkKbGetScanf(60000,bill,4, 25,SDK_MMI_LETTER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "bill= %s\r\n",bill);
				
				memset(sendData.asBill,0,sizeof(sendData.asBill));
				memcpy(sendData.asBill,&bill[1],bill[0]);
				Trace("xgd","sendData.asBill=%s\r\n",sendData.asBill);
				YescTitanAmt();
			}
			break;
			case SDK_KEY_ESC:
			{
				YESCMainMenu();	
			}
			default:
			{
				
			}	
	}
	
	YESCMainMenu();
}
void YescTitanAmt(void)
{
	u8 buf[16] = {0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,300,"/mtd0/res/Titanpayment.bmp");
	sdkDispShowBmp(0, 60, 240, 150,"/mtd0/res/TitanAmt.bmp");
	
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,3,8, NULL, " KS");
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "buf= %s\r\n", buf);
				memset(sendData.asAmount,0,sizeof(sendData.asAmount));
				memcpy(sendData.asAmount,&buf[1],buf[0]);
				Trace("xgd", "sendData.asAmount= %s\r\n",sendData.asAmount);
				YescTitanPhNum();
			}
			break;
		case SDK_KEY_ESC:
			{
				YescTitanBillNum();
			}
			break;	
			default:
			{
					
			}	
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}	
}
void YescTitanPhNum(void)
{
	u8 buf[16] = {0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,300,"/mtd0/res/Titanpayment.bmp");
	sdkDispShowBmp(0, 60, 240,150,"/mtd0/res/Titanphno.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	switch(res)
	{	
		case SDK_KEY_ENTER:
			{
				Trace("xgd","buf=%s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("xgd","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				YescTitanConfirm();
			}
			break;
		case SDK_KEY_ESC:
			{
				YescTitanAmt();
			}
			break;
			default:
			{
				
			}	
		
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
}
void YescTitanConfirm(void)
{
	
	s32 key=0;
	u8 buf[128]={0};
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "TITAN PAYMENT");
	sdkDispAt(72, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","REF NUM:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asBill);
	sdkDispAt(12,100,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT :");
	sdkDispAt(12,140,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",sendData.asAmount);
	sdkDispAt(108,140,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","PhNo:");
	sdkDispAt(12,180,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"09%s",sendData.asPhoneNum);
	sdkDispAt(12,220,buf);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);

	sdkDispBrushScreen();
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
	switch(key)
		{
			case SDK_KEY_ENTER:
			{
				//YescTitanSendData();
				Select_TitanPay();
			}
			break;
		case SDK_KEY_ESC:
			{
				
				YESCMainMenu();
			}
			break;
			default:
			{
			
			}	
		}
		if(printtype.BillMainMenu==Bill_Old_Menu)
		{
			BillPayMainMenu();
		}
		else if(printtype.BillMainMenu==Bill_New_Menu)
		{
			NewMainMenu();
		}
		
}
void Select_TitanPay(void)
{
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cashorTap.bmp");
	sdkDispBrushScreen();
	key = sdkKbWaitKey(SDK_KEY_MASK_1|SDK_KEY_MASK_2|SDK_KEY_MASK_ESC,KbWaitTime);
	printtype.BillPrint=TITAN;
	switch(key)
	{
		case SDK_KEY_1:
			{
				printtype.BillPayment=MEMCARD;
				ModuleCustomerReadCardForBillPay();
			}
			break;
		case SDK_KEY_2:
			{
				
				printtype.BillPayment=CASH;
				YescTitanSendData();
			}
			break;
			default:
			{
				
			}	
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}
	
}
void YescTitanSendData(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	printtype.BillPrint=TITAN;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	ForSendData();
	Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	if(printtype.BillPayment == CASH)
	{
		memset(message,0,sizeof(message));
		sprintf(message,"%s%s%s%s%s%s",sendData.asBill,sendData.asAmount,sendData.asPhoneNum,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
		Trace("xgd","message=%s\r\n",message);
	
		
	}
	else if (printtype.BillPayment == MEMCARD)
	{
		memset(message,0,sizeof(message));
		sprintf(message,"%s%s%s%s%s%s%s%s%s",sendData.asBill,sendData.asAmount,sendData.asPhoneNum,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
		Trace("xgd","message=%s\r\n",message);
	}
	
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);

	if(printtype.BillPayment == CASH)
	{
		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf,"%s%s/titan/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,sendData.asAmount,sendData.asPhoneNum,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	}
	else if(printtype.BillPayment == MEMCARD)
	{
		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf,"%s%s/titanPayCard/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,sendData.asAmount,sendData.asPhoneNum,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
		
	}
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	memset(&receivedData,0,sizeof(receivedData));
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			
			transaction=true;              //  for pageagentcheckbalance
			secondtransactionstart=true;   //  for pageagentcheckbalance
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);

			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.Amount,"amount",0);
			
			js_tok(receivedData.commission,"charge",0);
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);
			
			js_tok(receivedData.BillRefNo,"consumerReferenceNo",0);
			
			js_tok(receivedData.DueDate,"dueDate",0);
			
			js_tok(receivedData.Company,"townshipCode",0);
			
			js_tok(receivedData.MemberRef,"customerCardRef",0);	
				
			js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
			
			js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
			
			js_tok(receivedData.PhoneNo,"phoneNo",0);
			
			foragent = false;
			BillPrint(); 
			foragent = true;									
			BillPrint();
			CheckPaperRollStatus();
			if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					MainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
		}
 			
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;              //  for pageagentcheckbalance
			
			js_tok(receivedData.ErrorMessage,"message",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}
			else
			{
			
				DisplayErrorMessage("TITAN",receivedData.ErrorMessage,false);
			}
		
		}
			
	}
	else
	{
		DisplayErrorMessage("","",true);
		
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)
		{
			MainMenu();
		}
		else if(printtype.BillMainMenu==Bill_New_Menu)
		{
			NewMainMenu();
		}
		
}
void AirTicketMainMenu(void)
{
	s32 key=0;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/worldmyanmarmenu.bmp");
    sdkDispBrushScreen();
	transaction=false;   // for pageagentcheckbalance
		  
    key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 |SDK_KEY_MASK_3| SDK_KEY_MASK_ESC, 60000);
	switch(key)
	{
		case SDK_KEY_1:
			{
				printtype.BillPrint=AIRDOMESTIC;
				
				TicketPhone();
			}
			break;
		case SDK_KEY_2:
			{
				printtype.BillPrint=AIROVERSEA;
			
				TicketPhone();
			}
			break;
		case SDK_KEY_3:
			{
				printtype.BillPrint=BUSTICKET;
			
				TicketPhone();
			}	
			break;	
		case SDK_KEY_ESC:
			{
				BillPayMainMenu();
			}
			break;
			default:
			{
				BillPayMainMenu();
			}
			
	}
	BillPayMainMenu();
}

void TicketPhone(void)
{
	u8 buf[16] = {0};
	s32 res;
	
	
	sdkDispClearScreen();
	if(printtype.BillPrint==AIRDOMESTIC)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0,0,240,300,"/mtd0/res/airticketlocal.bmp");
		sdkDispShowBmp(0, 60, 240, 150,"/mtd0/res/phoneno.bmp");
	
		sdkDispBrushScreen();
		
		
	}
	else if(printtype.BillPrint==AIROVERSEA)
	{
		
		
		sdkDispClearScreen();
		sdkDispShowBmp(0,0,240,300,"/mtd0/res/airticketoversea.bmp");
		sdkDispShowBmp(0, 60, 240, 150,"/mtd0/res/phoneno.bmp");
	
		sdkDispBrushScreen();
		
	
	}
	else if(printtype.BillPrint==BUSTICKET)
	{
		
		sdkDispClearScreen();
		sdkDispShowBmp(0,0,240,300,"/mtd0/res/busticket.bmp");
		sdkDispShowBmp(0, 60, 240, 150,"/mtd0/res/phoneno.bmp");
	
		sdkDispBrushScreen();
		
	
	}
		

	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	switch(res)
	{	
		case SDK_KEY_ENTER:
			{
				Trace("xgd","buf=%s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("xgd","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				TicketAmount();
			}
			break;
		case SDK_KEY_ESC:
			{
				AirTicketMainMenu();
			}
			break;
			default:
			{
				AirTicketMainMenu();
			}	
		
	}
	AirTicketMainMenu();
}

void TicketAmount(void)
{
	u8 buf[16] ={0};
	s32 res;
	
	
	sdkDispClearScreen();
	if(printtype.BillPrint==AIRDOMESTIC)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0,0,240,300,"/mtd0/res/airticketlocal.bmp");
		sdkDispShowBmp(0, 60, 240, 150,"/mtd0/res/TitanAmt.bmp");
		sdkDispBrushScreen();
	}
	else if(printtype.BillPrint==AIROVERSEA)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0,0,240,300,"/mtd0/res/airticketoversea.bmp");
		sdkDispShowBmp(0, 60, 240, 150,"/mtd0/res/TitanAmt.bmp");
		sdkDispBrushScreen();
	}
	else if(printtype.BillPrint==BUSTICKET)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0,0,240,300,"/mtd0/res/busticket.bmp");
		sdkDispShowBmp(0, 60, 240, 150,"/mtd0/res/TitanAmt.bmp");
		sdkDispBrushScreen();
	
	}
	memset(buf,0,sizeof(buf));
	res = GetAmountNumber(30*1000,buf, SDK_DISP_LINE3,3,8, NULL," KS");
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "buf= %s\r\n", buf);
				memset(sendData.asAmount,0,sizeof(sendData.asAmount));
				memcpy(sendData.asAmount,&buf[1],buf[0]);
				Trace("xgd", "sendData.asAmount= %s\r\n", sendData.asAmount);
				TicketConfirm();
			}
			break;
		case SDK_KEY_ESC:
			{
				AirTicketMainMenu();
			}
			break;	
			default:
			{
				AirTicketMainMenu();	
			}	
	}
	AirTicketMainMenu();
}
void TicketConfirm(void)
{
	s32 key=0;
	u8 buf[128]={0};
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "TICKET PAYMENT");
	sdkDispAt(60, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","Ph No:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"09%s",sendData.asPhoneNum);
	sdkDispAt(12,100,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT :");
	sdkDispAt(12,140,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",sendData.asAmount);
	sdkDispAt(108,140,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","TICKET TYPE:");
	sdkDispAt(12,180,buf);
	memset(buf,0,sizeof(buf));
	
	if(printtype.BillPrint==AIRDOMESTIC)
	{
		sprintf(buf,"AIRTICKET_LOCAL");
		
		printtype.BillPrint=AIRDOMESTIC;
	}
	else if(printtype.BillPrint==AIROVERSEA)
	{
		sprintf(buf,"AIRTICKET_OVERSEA");
		printtype.BillPrint=AIROVERSEA;
	}
	else if(printtype.BillPrint==BUSTICKET)
	{
		sprintf(buf,"BUSTICKET");
		printtype.BillPrint=BUSTICKET;
	}
	
	sdkDispAt(12,220,buf);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);

	sdkDispBrushScreen();
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
	switch(key)
		{
			case SDK_KEY_ENTER:
			{
		
				WMSelectPayment();
			}
			break;
		case SDK_KEY_ESC:
			{
				
				AirTicketMainMenu();
			}
			break;
			default:
			{
				AirTicketMainMenu();
			}	
		}
		AirTicketMainMenu();
		
}
void WMSelectPayment(void)
{
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/cashorTap.bmp");
	sdkDispBrushScreen();
	key = sdkKbWaitKey(SDK_KEY_MASK_1|SDK_KEY_MASK_2|SDK_KEY_MASK_ESC,KbWaitTime);

	switch(key)
	{
		case SDK_KEY_1:
			{
				printtype.BillPayment=MEMCARD;
				ModuleCustomerReadCardForBillPay();
			}
			break;
		case SDK_KEY_2:
			{
				
				printtype.BillPayment=CASH;
				TicketSendData();
			}
			break;
			default:
			{
				BillPayMainMenu();
			}	
	}
	BillPayMainMenu();
}
void TicketSendData(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};

	u8 buf[128] = {0};

	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	
	sdkDispBrushScreen();
	
	ForSendData();
	Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	memset(buf,0,sizeof(buf));
	if(printtype.BillPrint==AIRDOMESTIC)
	{
		sprintf(buf,"air_domestic");
	}
	else if(printtype.BillPrint==AIROVERSEA)
	{
		sprintf(buf,"air_oversea");
	}
	else if(printtype.BillPrint==BUSTICKET)
	{
		sprintf(buf,"busticket");
	}
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s%s%s",buf,sendData.asPhoneNum,sendData.asAmount,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
	Trace("xgd","message=%s\r\n",message);
	
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/wmtraveltour/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,buf,sendData.asPhoneNum,sendData.asAmount,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
	
	
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	memset(&receivedData,0,sizeof(receivedData));
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			
			js_tok(receivedData.Amount,"amount",0);
			
			
			js_tok(receivedData.commission,"charge",0);
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);
			
			
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			js_tok(receivedData.PhoneNo,"mobileNo",0);
			
			js_tok(receivedData.TRX,"trx",0);
			
			BillPrint(); 
			CheckPaperRollStatus();		
		}
 			
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false; 
			js_tok(receivedData.ErrorMessage,"message",0);
			js_tok(receivedData.ErrorCode,"code",0);
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
				{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}	
			else
			{
			
				DisplayErrorMessage("TICKET",receivedData.ErrorMessage,false);
			}
			
		}
			
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	
	
MainMenu();
		

}
void TicketCardSendData(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};

	u8 buf[128] = {0};

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	if(printtype.BillPrint==AIRDOMESTIC)
	{
		sprintf(buf,"air_domestic");
	}
	else if(printtype.BillPrint==AIROVERSEA)
	{
		sprintf(buf,"air_oversea");
	}
	else if(printtype.BillPrint==BUSTICKET)
	{
		sprintf(buf,"busticket");
	}
	
	ForSendData();
	Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s%s%s%s%s",buf,sendData.asPhoneNum,sendData.asAmount,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
	Trace("xgd","message=%s\r\n",message);
	
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/wmtraveltourCardPayment/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,buf,sendData.asPhoneNum,sendData.asAmount,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
	
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	memset(&receivedData,0,sizeof(receivedData));
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			
			js_tok(receivedData.Amount,"amount",0);
			
			js_tok(receivedData.commission,"charge",0);
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			js_tok(receivedData.PhoneNo,"mobileNo",0);
			
			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.CustomerCardNum,"customerCardNo;",0);
			
			js_tok(receivedData.MemberRef,"customerCardRef",0);
			
			js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
			
			BillPrint(); 
			CheckPaperRollStatus();
		}
 			
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;   // for pageagentcheckbalance
			js_tok(receivedData.ErrorMessage,"message",0);
			DisplayErrorMessage("TICKET",receivedData.ErrorMessage,false);
		}
			
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	
	
	MainMenu();
	
}
void ModuleCustomerReadCardForBillPay(void)
{
	step1:
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/membercard.bmp");
	sdkDispBrushScreen();
	sdkmSleep(1500);
	//Trace("xgd", "Start ModuleReadCard= \r\n");
	

   	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;

	
   	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	
	  	
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asCustomerCardNo,0,sizeof(sendData.asCustomerCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("ModuleCustomerReadCardForBillPay", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	if(strcmp(sendData.asFirstDigit,"2")==0)
	{
		memcpy(sendData.asCustomerCardNo,&buf1[0],9);
		Trace("ModuleCustomerReadCardForBillPay","Customer Card No=%s\r\n",sendData.asCustomerCardNo);
		MemBillPayLoginFace();
		
	}
	else 
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "MEMBER CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		goto step1;
	}	
		
}
void MemBillPayLoginCheck(int i)
{
	u32 Temp_timer;
	Temp_timer = sdkTimerGetId();
	s32 rslt;
	if(printtype.BillMainMenu==Bill_Old_Menu)
	{
		fingerprint.Error=TOPUP;
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		fingerprint.Error=NEWMAINMENU;
	}
	
	if(i==1)
	{	
		
		while(1)
		{	
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT...", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();

			if (sdkTimerIsEnd(Temp_timer, 120 * 1000))     //Timer-out 60s
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				//return BillPayMainMenu();
				if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
			}
		
			rslt = MemberFPCheck();

			if (rslt == SDK_OK)                                                               
			{
				rslt = VerifyFPC(receivedData.asFingerTemplate,3);

				if (rslt == SDK_OK)                                                               
				{
					memset(sendData.asCustomerEnterPin,0,sizeof(sendData.asCustomerEnterPin));
					strcpy(sendData.asCustomerEnterPin,"0");
					Trace("xgd", "Customer Enter Pin= %s\r\n",sendData.asCustomerEnterPin);	
				
					if(printtype.BillPrint==AEON)
					{
						AeonPay_Cash();
					}
					else if(printtype.BillPrint == TITAN)
					{
						YescTitanSendData();
					}
					else if(printtype.BillPrint == HELLOCAB)
					{
						HelloCabSendData();
					}
					else if(printtype.BillPrint == YESC)
					{
						YESCSendData();
					}
					else if(printtype.BillPrint == AIRDOMESTIC)
					{
						TicketCardSendData();
					}
					else if(printtype.BillPrint == AIROVERSEA)
					{
						TicketCardSendData();
					}
					else if(printtype.BillPrint == BUSTICKET )
					{
						TicketCardSendData();
					}
				}
				else			
				{
					if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
				}
			}
			else
			{
				if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
			}
		}
	}
	else
	{
		s32 key;
		u8 buf[64] = {0};
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/enterpincode.bmp");
		sdkDispBrushScreen();
		
	
		memset(sendData.asCustomerEnterPin, 0, sizeof(sendData.asCustomerEnterPin));
		
  		key = sdkKbGetScanf(60000, buf, 6, 6, SDK_MMI_PWD ,SDK_DISP_LINE4);
  		
  			if(key == SDK_OK)
  		{
  			memcpy(sendData.asCustomerEnterPin,&buf[1],6);
			Trace("xgd", "Customer Enter Pin= %s\r\n", sendData.asCustomerEnterPin);
			memset(receivedData.AccessFinger,0,sizeof(receivedData.AccessFinger));
			strcpy(receivedData.AccessFinger,"0");
			Trace("xgd", "Customer Finger= %s\r\n",receivedData.AccessFinger);			
			if(printtype.BillPrint==AEON)
					{
						AeonPay_Cash();
					}
			else if(printtype.BillPrint == TITAN)
					{
						YescTitanSendData();
					}
			else if(printtype.BillPrint == HELLOCAB)
					{
						HelloCabSendData();
					}
			else if(printtype.BillPrint == YESC)
					{
						YESCSendData();
					}
			else if(printtype.BillPrint == AIRDOMESTIC)
					{
						TicketCardSendData();
					}
			else if(printtype.BillPrint == AIROVERSEA)
					{
						TicketCardSendData();
					}
			else if(printtype.BillPrint == BUSTICKET )
					{
						TicketCardSendData();
					}
		}
		else
		{
				if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
		}
  			
  		if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
	
	
	}
	if(printtype.BillMainMenu==Bill_Old_Menu)
	{
		BillPayMainMenu();
	}
	else if(printtype.BillMainMenu==Bill_New_Menu)
	{
		NewMainMenu();
	}

		
			
}
void MemBillPayLoginFace(void)
{
	s32 res;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/auth.bmp");
	sdkDispBrushScreen();
		
	 res = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, KbWaitTime);

	if (res == SDK_KEY_1)
    {
		
		MemBillPayLoginCheck(1);

    }
    else if (res == SDK_KEY_2)
    {
    	
    	MemBillPayLoginCheck(2);
    }
    else if (res == SDK_KEY_ESC)
    {
        	if(printtype.BillMainMenu==Bill_Old_Menu)
				{
					BillPayMainMenu();
				}
				else if(printtype.BillMainMenu==Bill_New_Menu)
				{
					NewMainMenu();
				}
    }

	if(printtype.BillMainMenu==Bill_Old_Menu)
		{
			BillPayMainMenu();
		}
		else if(printtype.BillMainMenu==Bill_New_Menu)
		{
			NewMainMenu();
		}

}

void IHomeref(void)             // Aeon Start 
{
	int num={0};
	s32 key;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/ihomeref.bmp");
	sdkDispBrushScreen();
	
	memset(bill,0,sizeof(bill));
	key =  sdkKbGetScanf(60000,bill,4, 25,SDK_MMI_LETTER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "bill= %s\r\n", bill);
				num=strlen(bill)-1;
				memset(sendData.asBill,0,sizeof(sendData.asBill));
				memcpy(sendData.asBill,&bill[1],num);
				Trace("xgd","sendData.asBill=%s\r\n",sendData.asBill);
				IHomeSentData();
			}
			break;
			case SDK_KEY_ESC:
			{
			
				BillPayMainMenu();	
			}
			default:
			{
			
			}	
	}

	BillPayMainMenu();
	
}

void IHomeSentData(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	ForRecall=true;
	printtype.BillPayment=CASH;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	
	printtype.BillPrint=IHOME;
	ForSendData();               //checktrx
	Trace("AeonPay_Cash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	

	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s",sendData.asBill,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
	Trace("xgd","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);

	
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/ihomebillamountreq/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asBill,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
		
			js_tok (receivedData.RefNo,"tru_ID ",0);
			
			js_tok(receivedData.Amount,"tru_Amount",0);
			
			js_tok(receivedData.commission,"charge",0);
			//Trace("xgd","receivedData.Amount=%s\r\n",receivedData.Amount);
		
			
			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);
			
			IhomeConfirm();
						
		}
		else
		{
			
			transaction=false;   // for pageagentcheckbalance
		
			js_tok(receivedData.ErrorMessage,"message",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
				{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}
			else
			{
				DisplayErrorMessage("IHOME",receivedData.ErrorMessage,false);
			}
						
		}
		
	}
	else 
	{
		DisplayErrorMessage("","",true);
		
	}

	BillPayMainMenu();	
}

void IhomeConfirm(void)
{						
	s32 key=0;
	u8 buf[64] = {0};
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS%s",receivedData.Amount);
	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"IHOME PAYMENT ",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"AMOUNT:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,buf,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"CHARGE:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,receivedData.commission,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4,0,"REF NUM:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4,0,receivedData.RefNo,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	sdkDispBrushScreen();

	key = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);

	switch(key)
	{
		case  SDK_KEY_ENTER:
		{
		
			IhomeLastData();
		}
		break;
		case SDK_KEY_ESC:
		{
			BillPayMainMenu();
		}
		break;
		default:
		{
			BillPayMainMenu();	
		}	
		
	}
	BillPayMainMenu();
}
void IhomeLastData(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	ForRecall=true;
	printtype.BillPayment=CASH;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	
	printtype.BillPrint=IHOME;
	

	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s%s%s",receivedData.RefNo,receivedData.Amount,receivedData.commission,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
	Trace("xgd","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);

	
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/ihomepaymentbycash/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,receivedData.RefNo,receivedData.Amount,receivedData.commission,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
		
			js_tok (receivedData.RefNo,"refNo ",0);
			
			js_tok(receivedData.Amount,"billAmount",0);
			//Trace("xgd","receivedData.Amount=%s\r\n",receivedData.Amount);
		
			
			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.TotalAmount,"totalBillAmount",0);
			
			js_tok(receivedData.commission,"charge",0);
			
			foragent = false;
			BillPrint(); 
			foragent = true;									
			BillPrint();
			CheckPaperRollStatus();

						
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;   // for pageagentcheckbalance
		
			js_tok(receivedData.ErrorMessage,"message",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
				{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}
			else
			{
				DisplayErrorMessage("IHOME",receivedData.ErrorMessage,false);
			}
						
		}
		
	}
	else 
	{
		DisplayErrorMessage("","",true);
		
	}

		BillPayMainMenu();		
}











	








