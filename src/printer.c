/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : printer.c
   Version       : Initial Draft
   Author        : Cody
   Created       : 2014/6/26
   Last Modified :
   Description   : Printer module related functions are defined here.
   History       :
   1.Date        : 2016/5/5
    Author      : Tin Moe Aye
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"

static void PrintText(void);
static void PrintBmp(void);
void PrintAgentCheckBalance(void);
void ReprintMainMenu(void);
void NewMainMenu(void);
//s32 printresult;

u8 tmnLogo[] = "/mtd0/res/tmnprintlogo.bmp";
u8 GoBusLogo[] = "/mtd0/res/GoBusLogo.bmp";
u8 BNFLogo[]="/mtd0/res/logobnf.bmp";
u8 HelloCabLogo[]="/mtd0/res/logohello.bmp";
u8 AeonLogo[]="/mtd0/res/aeonLogo.bmp";
u8 asPathCP[] = "/mtd0/res/logocp.bmp";
u8 asPathPrtAGDlogo[] = "/mtd0/res/agdlogo.bmp";

u8 asPathYCDCLogo[]="/mtd0/res/logoycdc.bmp";


/*******************************************************************************
** Description :  Printer module submenu
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-09
** Remark      :
*******************************************************************************/
void ModulePrinter(void)
{
    s32 key;

    while (1)
    {
        sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "PRINTER", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "1.PRINT TEXT", SDK_DISP_LEFT_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "2.PRINT BMP", SDK_DISP_LEFT_DEFAULT);
        sdkDispBrushScreen();

        // Wait for key input
        key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 30000);

        switch (key)
        {
             case SDK_KEY_1:
                {
                    PrintText();
                }
                break;

             case SDK_KEY_2:
                {
                    PrintBmp();
                }
                break;

             case SDK_KEY_ESC:
                {
                    return;
                }

             default:
                {
                }
                break;
        }
    }
}

/*******************************************************************************
** Description :  Print a receipt template and a report
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-21
** Remark      :  1. initialize printer
                  2. pack string to print buffer
                  3. start printing data on buffer

                  CAUTION:
                  A maximum of 32 lines of packed data is recommended before you
                  call sdkPrintStr to print them out. Because additional dynamic
                  memory has to be allocated if the size of data exceeds certain
                  limits.
*******************************************************************************/
static void PrintText(void)
{
    SDK_PRINT_FONT st_font;
    SDK_PRINT_DATA pPrintAmount;
    SDK_PRINT_DATA pPrintRmb;
    SDK_PRINT_DATA pPrintNumber;

    u8 buf[64] = {0};
    s32 spacing = 4;

    // Display printing slip
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING SLIP...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();

    // Initialize the printer
    sdkPrintInit();

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    st_font.uiChFont = SDK_PRN_CH24X24;
    st_font.uiChZoom = SDK_PRN_ZOOM_AB;

    // Pack title
    sdkPrintStr("RECEIPT TEMPLATE", st_font, SDK_PRINT_MIDDLEALIGN, 0, 50);

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
    st_font.uiChFont = SDK_PRN_CH24X24;
    st_font.uiChZoom = SDK_PRN_ZOOM_N;

    // Pack receipt contents
    sdkPrintStr("DATE/TIME  : 2014/07/21 12:24:45", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("MERCHANT ID: 1586000000008", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("TERMINAL ID: 21400001", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("BATCH NUM  : 000001", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("INVOICE NUM: 000006", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    st_font.uiChFont = SDK_PRN_CH24X24;
    st_font.uiChZoom = SDK_PRN_ZOOM_AB;

    // Pack transaction name
    sdkPrintStr("SALE", st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);


    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
    st_font.uiChFont = SDK_PRN_CH24X24;
    st_font.uiChZoom = SDK_PRN_ZOOM_AB;

    // Pack card number
    sdkPrintStr("CARD NUM: ", st_font, SDK_PRINT_LEFTALIGN, 0, 0);
    sdkPrintStr("4902********7061", st_font, SDK_PRINT_RIGHTALIGN, 0, spacing);

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
    st_font.uiChFont = SDK_PRN_CH24X24;
    st_font.uiChZoom = SDK_PRN_ZOOM_N;

    // Pack contents
    sdkPrintStr("EXP DATE   : 12/16", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("REFER NUM  : 417007057890", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    memset(buf, 0, sizeof(buf));
    strcpy(buf, "AMOUNT:        RMB         12.00");
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    // begin 20150818 xiaokai added
    //set multistr print

    memset(&pPrintAmount, 0, sizeof(pPrintAmount));
    memset(&pPrintRmb, 0, sizeof(pPrintRmb));
    memset(&pPrintNumber, 0, sizeof(pPrintNumber));
    pPrintAmount.uiFont.uiAscFont = SDK_PRN_ASCII12X24;
    pPrintAmount.uiFont.uiChFont = SDK_PRN_CH12X12;
    pPrintAmount.uiFont.uiAscZoom = SDK_PRN_ZOOM_N;
    pPrintAmount.uiFont.uiChZoom = SDK_PRN_ZOOM_AB;
    pPrintAmount.siOffset = 0;
    memcpy(pPrintAmount.pasText, "AMOUNT", 6);


    pPrintRmb.uiFont.uiAscFont = SDK_PRN_ASCII16X32YB;
    pPrintRmb.uiFont.uiChFont = SDK_PRN_CH16X16;
    pPrintRmb.uiFont.uiAscZoom = SDK_PRN_ZOOM_N;
    pPrintRmb.uiFont.uiChZoom = SDK_PRN_ZOOM_A;
    pPrintRmb.siOffset = 120;
    memcpy(pPrintRmb.pasText, "RMB", 3);


    pPrintNumber.uiFont.uiAscFont = SDK_PRN_ASCII28X48B;
    pPrintNumber.uiFont.uiChFont = SDK_PRN_CH12X12;
    pPrintNumber.uiFont.uiAscZoom = SDK_PRN_ZOOM_N;
    pPrintNumber.uiFont.uiChZoom = SDK_PRN_ZOOM_B;

    pPrintNumber.siOffset = 200;
    memcpy(pPrintNumber.pasText, "12.00", 5);

    // the function sdkPrintMultStr  has the max params of three ,the last  must set for null;
    sdkPrintMultStr(SDK_PRINT_RIGHTALIGN, &pPrintAmount, &pPrintRmb, &pPrintNumber, NULL);
    sdkPrintStartWithMultFont();

    //end  20150818 xiaokai added


    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
    st_font.uiChFont = SDK_PRN_CH12X12;
    st_font.uiChZoom = SDK_PRN_ZOOM_N;

    // Pack signature
    memset(buf, 0, sizeof(buf));
    memset(buf, '_', 48);
    memcpy(buf, "SIGNATURE:", 10);
    sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 50);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    // Start printing slip
    sdkPrintStart();


    // Display printing settlement report
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING SETTLEMENT...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    st_font.uiChFont = SDK_PRN_CH24X24;
    st_font.uiChZoom = SDK_PRN_ZOOM_AB;

    // print title
    sdkPrintStr("SETTLEMENT", st_font, SDK_PRINT_MIDDLEALIGN, 0, 50);


    // Set text font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
    st_font.uiChFont = SDK_PRN_CH24X24;
    st_font.uiChZoom = SDK_PRN_ZOOM_N;

    // Pack contents
    memset(buf, 0, sizeof(buf));
    strcpy(buf, "CARD NAME            CARD NUMBER");
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    memset(buf, 0, sizeof(buf));
    strcpy(buf, "EXP DATE             INVOICE NUM");
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    memset(buf, 0, sizeof(buf));
    strcpy(buf, "TRANSACTION               AMOUNT");
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    memset(buf, 0, sizeof(buf));
    memset(buf, '_', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    /*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
    sdkPrintStartNoRollBack();

    // Pack contents
    memset(buf, 0, sizeof(buf));
    strcpy(buf, "VISA            4902********7061");
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    memset(buf, 0, sizeof(buf));
    strcpy(buf, "12/16                     000001");
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    memset(buf, 0, sizeof(buf));
    strcpy(buf, "SALE                       10.00");
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    // Print without feeding paper
    sdkPrintStartNoRollBack();

    sdkPrintStr("CREDIT:", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("CARDTYPE      COUNT       AMOUNT", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("VISA          1 RMB        10.00", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("________________________________", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sdkPrintStr("TOTALS        1 RMB        10.00", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    // Add some space to the end of slip
    sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 150);

    // Print withour feeding paper
    sdkPrintStartNoRollBack();
}

/*******************************************************************************
** Description :  Print bitmap
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-22
** Remark      :  1. Bitmap files are usually put in folder /mtd0/res/
                  2. The bitmap file must be 1 bit-per-pixel
*******************************************************************************/
static void PrintBmp(void)
{
    u8 path[] = "/mtd0/res/xgdprintlogo.bmp";


    // Display printing bmp
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING BMP...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();

    // Initialize the printer
    sdkPrintInit();

    // Print bitmap if file exists
    if (sdkAccessFile(path) == true)
    {
        // Left aligned
        sdkPrintBitMap(path, SDK_PRINT_LEFTALIGN, 0);
        // Middle aligned
        sdkPrintBitMap(path, SDK_PRINT_MIDDLEALIGN, 0);
        // Right aligned
        sdkPrintBitMap(path, SDK_PRINT_RIGHTALIGN, 0);

        // Start printing
        sdkPrintStart();
    }
    else
    {
        sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, path, SDK_DISP_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE4, 0, "DOESN'T EXIST!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();

        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
    }
}


void PrintAgentCheckBalance(void)
{
	// Initialize the printer
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	u8 name[32] = "BALANCE SLIP";
	
	
	
	// Display printing slip
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"AGENT CHECK BALANCE",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);

	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	// Set font
   	memset(&st_font, 0, sizeof(st_font));
   	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
   	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

   	// Pack receipt contents
   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "WALLET LEFT   :KS %s ",receivedData.BalanceAmount);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TMN CARD NO.  : %s",receivedData.CardNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);	
	
	// Add some space to the end of slip
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0,80);

	/*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
   sdkPrintStart();
   CheckPaperRollStatus();
    NewMainMenu();
		
}


void PrintAgentReport(void)
{ 
	SDK_PRINT_FONT st_font;
	u8 buf[64] = {0};
	s32 spacing = 4;
	u8 name[32] = "SUMMARY REPORT";
	u8 oneday[64] = "TOPUP";

	u8 onedaybill[64]="BILLPAYMENT";
	u8 remittance[64]="REMITTANCE";
	 
	 sdkDispClearScreen();
	 sdkDispFillRowRam(SDK_DISP_LINE1,0,"SUMMARY REPORT",SDK_DISP_DEFAULT);
	 sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	 sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	 sdkDispBrushScreen();
	 
	 sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;
	    
	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
	
	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	 
	 memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_AB;
	 
	 memset(buf,0,sizeof(buf));
	 sprintf(buf,"%s to  %s",receivedData.CopyDueDate,receivedData.DueDate);
	 sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing); 
	 
	 memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	 
	    
	memset(buf, 0, sizeof(buf));
	 memset(buf, '-', 32);
	 sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	    
	memset(buf,0,sizeof(buf));
	sprintf(buf, "OPENING BALANCE:KS %s",receivedData.openingBalance);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	    
	 memset(buf,0,sizeof(buf));
	sprintf(buf, "CLOSING BALANCE:KS %s" ,receivedData.closingBalance);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	    
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	//sdkPrintStr(agent,st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "AGENT COMMISSION :KS %s" ,receivedData.commission);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "AGENT REFILL     :KS %s" ,receivedData.RefillAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "AGENT WITHDRAW   :KS %s" ,receivedData.WithdrawAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "AGENT REFUND     :KS %s" ,receivedData.BankName);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "CASH BACK        :KS %s\n(Promotion)" ,receivedData.AeonPhoneNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
     memset(buf, 0, sizeof(buf));
 	memset(buf, '-', 32);
 	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
 	
 	
    /*memset(buf,0,sizeof(buf));
    sprintf(buf, "REMIT(CASHOUT-LOCAL):KS %s" ,receivedData.Amount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "REMIT(CASHOUT-INTL) :KS %s" ,receivedData.BillAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);*/
    
   
	    
	sdkPrintStr(oneday, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "MPT         :KS %s" ,receivedData.mpt);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "OOREDOO     :KS %s" ,receivedData.ooredoo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "MECTEL      :KS %s" ,receivedData.mectel);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    
    
   memset(buf,0,sizeof(buf));
   sprintf(buf,"TELENOR     :KS %s",receivedData.TelenorEpin);
   sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   
   memset(buf, 0, sizeof(buf));
 	memset(buf, '-', 32);
 	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
 	
 	sdkPrintStr(onedaybill,st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
 	
   
	 memset(buf,0,sizeof(buf));
    sprintf(buf, "BNF         :KS %s" ,receivedData.bnf);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
  
    memset(buf,0,sizeof(buf));
    sprintf(buf,"HELLOCABS   :KS %s",receivedData.HelloCab);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"AEON        :KS %s",receivedData.AeonName);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"CP          :KS %s",receivedData.cp);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"TITAN       :KS %s",receivedData.Trip);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"TIKKETBO    :KS %s",receivedData.TripDate);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
 	sprintf(buf,"CNP         :KS %s",receivedData.CnpAmount);
 	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
 	
 	memset(buf, 0, sizeof(buf));
 	memset(buf, '-', 32);
 	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
 	
 	sdkPrintStr(remittance,st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
 	
 	
 	memset(buf,0,sizeof(buf));
    sprintf(buf, "CASH IN     :KS %s",receivedData.Status);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
 	
    
    memset(buf,0,sizeof(buf));
    sprintf(buf, "CASH OUT    :KS %s",receivedData.Amount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
 	sprintf(buf, "IR(CASH OUT):KS %s",receivedData.BillAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
     
    
 	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
    
    memset(buf, 0, sizeof(buf));
	 memset(buf, '-', 32);
 	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
 
 	memset(buf, 0, sizeof(buf));
 	sprintf(buf, "AID       : %s",receivedData.AgentCardNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    memset(buf, 0, sizeof(buf));
 	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf, 0, sizeof(buf));
 	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf, 0, sizeof(buf));
 	memset(buf, '-', 32);
 	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
 	
 	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
 // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

 sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
 sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
 sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing); 


 // Add some space to the end of slip
 sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

 /*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
    sdkPrintStartNoRollBack();
    CheckPaperRollStatus();
 //PrintSlipConfirm();
 AgentMenu();
  
 
}


void PrintBankInfo(void)
{
	 SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
    u8 name[32] = "BANK INFO";
    
    u8 truemoeny[32]="True Money Myanmar Co.Ltd";
    
    sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"BANK INFO",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sdkPrintInit();

	// Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;

    sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
    
   	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);

  
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	sdkPrintStr(truemoeny,st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
    memset(buf, 0, sizeof(buf));
    memset(buf, '-', 32);

    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    sprintf(buf, "%s  %s", "ACC NO :",receivedData.AccNum);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	sprintf(buf, "%s  %s", "BRANCH :",receivedData.BankName);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   
    sdkPrintStartNoRollBack();
			
    
    
}

void PrintSaleCheckBalance(void)
{
	// Initialize the printer
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	u8 name[32] = "BALANCE SLIP";
	
	// Display printing slip
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"SALE CHECK BALANCE",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);

	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	// Set font
   	memset(&st_font, 0, sizeof(st_font));
   	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
   	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

   	// Pack receipt contents
   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "WALLET LEFT   :KS %s ",receivedData.BalanceAmount);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TMN CARD NO.  : %s",receivedData.CardNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "SID       : %s",receivedData.SalesCardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	// Add some space to the end of slip
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
    sdkPrintStartNoRollBack();
	CheckPaperRollStatus();
	SalesMenu();
		
}

void BillPrint(void)
{
	u8 name[32] = {0};
	SDK_PRINT_FONT st_font;
	u8 buf[128] = {0};
	u8 buf1[128] = {0};
		u8 buf2[128] = {0};
		
		u8 shopname1[128]={0};
		u8 shopname2[128]={0};

	s32 spacing = 4;
	
	//s32 ret;
		
	memset(name,0,sizeof(name));
	sdkSysBeep(SDK_SYS_BEEP_OK);
		
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"PAYMENT",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
		
	sdkPrintInit();
		
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
   	st_font.uiAscZoom = SDK_PRN_ZOOM_B;
   	
   	if(printtype.BillPrint==GOBUS||printtype.BillPrint==BNF||printtype.BillPrint==HELLOCAB||printtype.BillPrint==AEON||printtype.BillPrint==CP||printtype.BillPrint==TICKETBO||printtype.BillPrint==TITAN||printtype.BillPrint==AIRDOMESTIC||printtype.BillPrint==AIROVERSEA||printtype.BillPrint==BUSTICKET||printtype.BillPrint==YESC||printtype.BillPrint==MPTBILL||printtype.BillPrint==YCDC)	
    {
	   	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
	}
   	if(printtype.BillPrint==GOBUS)
   	{
   		strcpy(name, "GOBUS PAYMENT");
   		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
					
		sdkPrintBitMap(GoBusLogo,SDK_PRINT_MIDDLEALIGN,0);
					
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
					
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
					
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRUE TX ID    :",receivedData.TRUEREF);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","BOOKING ID   :",receivedData.gobus);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","BOOKING DATE :",receivedData.BookingDate);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","AMOUNT      :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
					
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP      :",receivedData.Trip);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP DATE  :",receivedData.TripDate);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP TIME :",receivedData.TripTime);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","BUS      :",receivedData.Bus);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","BUS CLASS :",receivedData.BusClass);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s KS %s","FEES      :",receivedData.commission);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
					
		memset(buf,0,sizeof(buf));
		memset(buf,'-',32);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s : %s","SEAT",receivedData.GobusSeat);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
					
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s : %s","PASSANGER:\n",receivedData.Passanger);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);	
   				
	  }
	else if(printtype.BillPrint==BNF)
 	 {
 	 	strcpy(name, "BNF PAYMENT");
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintBitMap(BNFLogo,SDK_PRINT_MIDDLEALIGN,0);
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRUE TX ID   :",receivedData.TRUEREF);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","BOOKING ID   :",receivedData.bnf);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","BOOKING DATE :",receivedData.BookingDate);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
				
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP      :",receivedData.Trip);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP DATE :",receivedData.TripDate);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP TIME :",receivedData.TripTime);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","BUS       :",receivedData.Bus);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","BUS CLASS :",receivedData.BusClass);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","AMOUNT     :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s KS %s","FEES      		 :",(receivedData.commission));
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s KS %s","TOTAL AMOUNT:",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);			
				
		memset(buf,0,sizeof(buf));
		memset(buf,'-',32);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s : %s","PASSANGER:",receivedData.Passanger);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);	
	  }
	  else if(printtype.BillPrint==HELLOCAB)
	  {
	  	//strcpy(name, "HELLOCABS PAYMENT");
	  	if(printtype.BillPayment==CASH)
		{
			strcpy(name, "HELLOCABS PAYMENT");
		}
		else if(printtype.BillPayment==MEMCARD)
		{
			strcpy(name,"HELLOCABS PAYMENT\n BY MEMBER CARD");
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintBitMap(HelloCabLogo,SDK_PRINT_MIDDLEALIGN,0);
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","DRIVERID   :",receivedData.DriverId);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","NAME       :",receivedData.DriverName);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		if(printtype.BillPayment==MEMCARD)
		{
			
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32); 
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s %s", "MEMBER CARD No.:",receivedData.CustomerCardNum);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","MEMBER CARD BALANCE:",receivedData.BalanceAmount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		
		}
		
		memset(buf,0,sizeof(buf));
		memset(buf,'-',32);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
				
        memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","Amount      :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES        :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s KS %s","TOTAL AMOUNT:",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	         					
	  }
	else if(printtype.BillPrint==AEON)	
	{
		if(printtype.BillPayment==CASH)
		{
			strcpy(name, "AEON PAYMENT");
		}
		else if(printtype.BillPayment==MEMCARD)
		{
			strcpy(name,"AEON PAYMENT\n BY MEMBER CARD");
		}
		
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintBitMap(AeonLogo,SDK_PRINT_MIDDLEALIGN,0);
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		
		if (!foragent)
			{
				sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
					
			}	 
			else 
			{
				sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
			} 
				
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		if(printtype.BillPayment==MEMCARD)
		{
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s %s", "MEMBER CARD No.:",receivedData.CustomerCardNum);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","MEMBER CARD BALANCE:",receivedData.BalanceAmount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32); 
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		}
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s", "NAME       :",receivedData.AeonName);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s", "AGREE NO   :",receivedData.AeonAgreeNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s 09%s","PHONE      :",receivedData.AeonPhoneNo);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","AMOUNT      :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES        :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s KS %s","TOTAL AMOUNT:",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);		
				
	 } 
	 else if(printtype.BillPrint==CP)
	 {
	 	strcpy(name, "CP FOOD PAYMENT");
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintBitMap(asPathCP,SDK_PRINT_MIDDLEALIGN,0);
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		if (!foragent)
			{
				sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
			}	 
			else 
			{
				sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
			} 
				 
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","COMPANY  :",receivedData.Company);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","CV NO    :",receivedData.SerialNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			
			memset(shopname1,0,sizeof(shopname1));
			memset(shopname2,0,sizeof(shopname2));
		
			if(strlen(receivedData.ShopName)>= 20)
			{
				strncpy(shopname1,receivedData.ShopName,20);
				
				int len=strlen(receivedData.ShopName);
				int	len1=strlen(shopname1);
				int	len2=strlen(receivedData.ShopName)-strlen(shopname1);
				Trace("billprint","len1=%d\r\n",len1);
				Trace("billprint","len2=%d\r\n",len2);
				Trace("billprint","len=%d\r\n",len);
				strncpy(shopname2,&receivedData.ShopName[21],len2);
				Trace("billprint","shopname1=%s\r\n",shopname1);
				Trace("billprint","shopname2=%s\r\n",shopname2);
				
				memset(buf,0,sizeof(buf));
				sprintf(buf, "%s %s","SHOP NAME:",shopname1);
				sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
				memset(buf,0,sizeof(buf));
				sprintf(buf,"          %s",shopname2);
				sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
					
					
		
			
		
			}
			else
			{
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s %s","SHOP NAME:",receivedData.ShopName);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			}
		
	
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","INVOICE  :",receivedData.InvoiceNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
				
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
						
		memset(buf,0,sizeof(buf));
		Trace("xgd","receivedData.Amount=%s\r\n",receivedData.Amount);
		sprintf(buf, "%s KS %s","AMOUNT      :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES        :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s KS %s","TOTAL AMOUNT:",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	 }	
	 else if(printtype.BillPrint==YCDC)
	 {
	 	strcpy(name, "YCDC PAYMENT");
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS" ,receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0,20);
		if (!foragent)	
		{
			sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		} 
		else  
		{
			sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","REF NO:",receivedData.BillRefNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","PAYMENT DUEDATE  :",receivedData.DueDate);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","BILL AMOUNT      :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES              :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","TOTAL AMOUNT     :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		
	 }
	else if(printtype.BillPrint==YESC) 
	{
		//strcpy(name, "CNP PAYMENT");
		if(printtype.BillPayment==CASH)
		{
			strcpy(name, "CNP PAYMENT");
		}
		else if(printtype.BillPayment==MEMCARD)
		{
			strcpy(name,"CNP PAYMENT\n BY MEMBER CARD");
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS" ,receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0,20);
				
		if (!foragent)	
		{
		sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		} 
		else  
		{
			sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","REF NO:",receivedData.BillRefNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","PAYMENT DUEDATE  :",date(receivedData.CopyDueDate));
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
			
		if(printtype.BillPayment==MEMCARD)
		{
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32); 
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s %s", "MEMBER CARD No.:",receivedData.CustomerCardNum);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","MEMBER CARD BALANCE:",receivedData.BalanceAmount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
			
		}
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","BILL AMOUNT  :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES         :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","TOTAL AMOUNT :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		
	}
	else if(printtype.BillPrint==MESC) 
	{
		strcpy(name, "MESC TRIPLE\n CIRCLE PAYMENT");
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS" ,receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0,20);
				
		if (!foragent)	
		{
			sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		} 
		else  
		{
			sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","REF NO:",receivedData.BillRefNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","PAYMENT DUEDATE  :",date(receivedData.CopyDueDate));
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","BILL AMOUNT  :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES         :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","TOTAL AMOUNT :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		
	}
	else if(printtype.BillPrint==MPTBILL)
	{
		strcpy(name, "MPTBILL PAYMENT");
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS" ,receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0,20);
				
		if (!foragent)	
		{
		sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		} 
		else  
		{
		sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		}
				
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","MPT PHONE No.:",receivedData.BillRefNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		/*memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","PAYMENT DUEDATE:",receivedData.DueDate);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);*/
				
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","BILL AMOUNT    :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES           :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","TOTAL AMOUNT   :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		
	}
	else if(printtype.BillPrint==TICKETBO)
	{
		strcpy(name, "TIKKETBO PAYMENT");
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","BOOKING No   :",receivedData.BookingId);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","AMOUNT       :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		memset(buf,0,sizeof(buf));
		
		sprintf(buf,"%s KS %s","FEES         :",(receivedData.commission));
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);		
		memset(buf,0,sizeof(buf));
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","TOTAL AMOUNT :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		memset(buf,'-',32);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf1,0,sizeof(buf1));
		strcpy(buf1,receivedData.departcity);
		strcat (buf1,"-");
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP         :",strcat(buf1,receivedData.arrivalcity));
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP Date    :",receivedData.TripDate);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","TRIP TIME    :",receivedData.TripTime);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","BUS          :",receivedData.Bus);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","SEAT         :",receivedData.ShopName);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		memset(buf,'-',32);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s : %s","PASSANGER:",receivedData.Passanger);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);	
	}
	else if(printtype.BillPrint==TITAN) 
	{
		
		if(printtype.BillPayment==CASH)
		{
			strcpy(name, "TITAN PAYMENT");
		}
		else if(printtype.BillPayment==MEMCARD)
		{
			strcpy(name,"TITAN PAYMENT\n BY MEMBER CARD");
		}
		
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS" ,receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0,20);
				
		if (!foragent)	
		{
		sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		} 
		else  
		{
			sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s%s","REF NO          :",receivedData.Company,receivedData.BillRefNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","PAYMENT DUEDATE :",receivedData.DueDate);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","MOBILE No.      :",receivedData.PhoneNo);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		if(printtype.BillPayment==MEMCARD)
		{
			
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32); 
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s %s", "MEMBER CARD No.    :",receivedData.CustomerCardNum);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","MEMBER CARD BALANCE:",receivedData.BalanceAmount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
			
		}
		
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","BILL AMOUNT  :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES         :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","TOTAL AMOUNT :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	}
	else if(printtype.BillPrint==AIRDOMESTIC||printtype.BillPrint==AIROVERSEA||printtype.BillPrint==BUSTICKET)
	{
	//	strcpy(name, "WORLD MYANMAR  PAYMENT");
		if(printtype.BillPayment==CASH)
		{
			strcpy(name, "WORLD MYANMAR PAYMENT");
		}
		else if(printtype.BillPayment==MEMCARD)
		{
			strcpy(name,"WORLD MYANMAR PAYMENT\n BY MEMBER CARD");
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
		
		if(printtype.BillPrint==AIRDOMESTIC)
		{
			sprintf(buf2,"AIRTICKET_LOCAL");
		}
	else if(printtype.BillPrint==AIROVERSEA)
		{
			sprintf(buf2,"AIRTICKET_OVERSEA");
		}
	else if(printtype.BillPrint==BUSTICKET)
		{
		sprintf(buf2,"BUSTICKET");
		}
				
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s","TICKET TYPE :",buf2);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","MOBILE No.   :",receivedData.PhoneNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		memset(buf,0,sizeof(buf));
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
		if(printtype.BillPayment==MEMCARD)
		{		
			
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s %s", "MEMBER CARD No.    :",receivedData.CustomerCardNum);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","MEMBER CARD BALANCE:",receivedData.BalanceAmount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32); 
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
			
		}
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","AMOUNT       :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		memset(buf,0,sizeof(buf));
		
		sprintf(buf,"%s KS %s","FEES         :",(receivedData.commission));
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);		
		memset(buf,0,sizeof(buf));
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","TOTAL AMOUNT :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		
	}
	
	else if(printtype.BillPrint==IHOME)
	{
		if(printtype.BillPayment==CASH)
		{
			strcpy(name, "IHOME PAYMENT");
		}
		else if(printtype.BillPayment==MEMCARD)
		{
			strcpy(name,"IHOME PAYMENT\n BY MEMBER CARD");
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
		sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
				
		/*memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS" ,receivedData.BillAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0,20);*/
				
		if (!foragent)	
		{
		sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		} 
		else  
		{
			sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
		}
		memset(&st_font, 0, sizeof(st_font));
		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","REF NO:",receivedData.RefNo);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		//memset(buf,0,sizeof(buf));
		//sprintf(buf,"%s %s","PAYMENT DUEDATE  :",date(receivedData.CopyDueDate));
		//(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
			
		if(printtype.BillPayment==MEMCARD)
		{
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32); 
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s %s", "MEMBER CARD No.:",receivedData.CustomerCardNum);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","MEMBER CARD BALANCE:",receivedData.BalanceAmount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
			
		}
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32); 
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","BILL AMOUNT  :",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEES         :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","TOTAL AMOUNT :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	}
	
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
    
    memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	if(printtype.BillPayment==MEMCARD)
	{
		memset(buf,0,sizeof(buf));
		sprintf(buf,"MID        : %s",receivedData.MemberRef);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		 
		
	}
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID        : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS        : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX        : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME  : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	
	if(printtype.BillPrint==GOBUS)
	{
			sdkPrintStr("GOBUS HOT LINE : 09785909001",st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	}
	else if(printtype.BillPrint==BNF)
	{
		 sdkPrintStr("BNF HOT LINE : 09254898255",st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	}
	
	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0,80);

	foragent = false;
    sdkPrintStart();
 
   
   				
}

void PrintCashTransfer(void)
{
	SDK_PRINT_FONT st_font;
 	char  buf[64] = {0};
 	s32 spacing = 4;
	u8 name[32] = {0};
	char starnum[64] = {0};
	char copystarnum[64]={0};

	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"CASH TRANSFER",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    
	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);

	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);

	strcpy(name, "TRANSFER");
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s KS %s", name,receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	sdkPrintStr("CUSTOMER COPY", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
 	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

    memset(buf, 0, sizeof(buf));
    memset(buf, '-', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    
	sdkPrintStr("SENDER    [NON-MEMBER]", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    memset(buf,0,sizeof(buf));
    sprintf(buf, "%s", receivedData.SenderPhone);
    Trace("printCashTransfer","buf=%s\r\n",buf);
    
    memset(starnum,0,sizeof(starnum));
    strcpy(starnum,valuestar(buf));
    
    memset(copystarnum,0,sizeof(copystarnum));
   	sprintf(copystarnum, "%s %s", "MOBILE NO :",starnum);
    sdkPrintStr(copystarnum, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);


	sdkPrintStr("RECEIVER  [NON-MEMBER]", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s", receivedData.ReceiverPhone);
    
    memset(starnum, 0, sizeof(starnum));
    strcpy(starnum,valuestar(buf));
    
    memset(copystarnum,0,sizeof(copystarnum));
    sprintf(copystarnum,"%s %s","MOBILE NO :",starnum);
    sdkPrintStr(copystarnum, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s", "PASSCODE 	:",receivedData.Passcode);
    sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
    
    memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s ","EXPIRED DATE:",receivedData.ExpDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
 	
 	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s %s", "TRANSFER AMOUNT  :KS",receivedData.TransferAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s", "TRANSFER FEES    :KS",receivedData.TransferFee);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s","TOTAL CHARGE AMT :KS",receivedData.Amount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
   	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	// Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    
	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 80);


	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);

	strcpy(name, "TRANSFER");
		
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s KS %s", name,receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	sdkPrintStr("AGENT COPY", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
 	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

    memset(buf, 0, sizeof(buf));
    memset(buf, '-', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	sdkPrintStr("SENDER    [NON-MEMBER]", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf,0,sizeof(buf));
    sprintf(buf, "%s", receivedData.SenderPhone);
    
    memset(starnum,0,sizeof(starnum));
    strcpy(starnum,valuestar(buf));
    
    memset(copystarnum,0,sizeof(copystarnum));
   	sprintf(copystarnum, "%s %s", "MOBILE NO :",starnum);
    sdkPrintStr(copystarnum, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	 sdkPrintStr("RECEIVER  [NON-MEMBER]", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	 memset(buf,0,sizeof(buf));
    sprintf(buf,"%s", receivedData.ReceiverPhone);
    
    memset(starnum, 0, sizeof(starnum));
    strcpy(starnum,valuestar(buf));
    
    memset(copystarnum,0,sizeof(copystarnum));
    sprintf(copystarnum,"%s %s","MOBILE NO :",starnum);
    sdkPrintStr(copystarnum, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

    memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
 	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s %s", "TRANSFER AMOUNT  :KS",receivedData.TransferAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s", "TRANSFER FEES    :KS",receivedData.TransferFee);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s","TOTAL CHARGE AMT :KS",receivedData.Amount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
   	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	
   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	


   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	// Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0,80);

	/*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
    sdkPrintStartNoRollBack();
    CheckPaperRollStatus();
  	MainMenu();
							    
}
char * valuestar(char * phonenum)
{	
	static	char  star[16] = {'\0'};
	int d= 0;int c=0;
	

	for (d=strlen(phonenum); d > 0 ;d--)
	{
		
		phonenum[d-1]='*';
		c++;
		if (c==4) break;
		
	}
	
	memset(star,0,sizeof(star));
	strcpy(star,phonenum);
	Trace("xgd", "phonenum= %s\r\n", phonenum);
	
	
	return (char *)star;
	

}
char * date(char *datenum)
{
	static char date[64]={0};
	int d;int c=0;
	for (d=strlen(datenum);d>=0;d--)
	{
		datenum[d]=' ';
		c++;
		if(c==15) break;
	}
	memset(date,0,sizeof(date));
	strcpy(date,datenum);
	return (char *)date;
}

void PrintDomesticCashOut(void)
{
	SDK_PRINT_FONT st_font;
	u8 buf[64] = {0};
	s32 spacing = 4;
	u8 name[32] = {0};
	u8 phnum[64]={0};
	//ForRecall=true;
	
	//printtype.CashPrint=CASHOUT;
	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"DOMESTIC CASHOUT",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);

	strcpy(name, "CASHOUT:KS");

	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s",name,receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);

	sdkPrintStr("CUSTOMER COPY", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	

		
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr("RECEIPIENT: [NON-MEMBER]", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", receivedData.PhoneNo);

	memset(phnum,0,sizeof(phnum));
	sprintf(phnum,"%s %s","MOBILE NO : ",buf);
	sdkPrintStr(phnum, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	/*memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","CASHOUT MONEY:","KS",receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	*/
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","TRANSFER AMOUNT   :","KS",receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	if(strcmp(receivedData.RegenerateCount,"0") != 0)
	{
					
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s %s","PCODE FEES        :","KS",receivedData.RePassCodeFee);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			
		/*memset(buf,0, sizeof(buf));
		sprintf(buf,"%s %s %s","TOTAL CASHOUT AMT :","KS",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);*/
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s %s","TOTAL CASHOUT AMT :","KS",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	}

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);	
	

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 80);

	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);

	strcpy(name, "CASHOUT:KS");
		
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s",name,receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);

	sdkPrintStr("AGENT COPY", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr("RECEIPIENT: [NON-MEMBER]", st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", receivedData.PhoneNo);

	memset(phnum,0,sizeof(phnum));
	sprintf(phnum,"%s %s","MOBILE NO : ",buf);
	sdkPrintStr(phnum, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","TRANSFER AMOUNT   :","KS",receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	if(strcmp(receivedData.RegenerateCount,"0") != 0)
	{
					
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s %s %s","PCODE FEES        :","KS",receivedData.RePassCodeFee);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			
		memset(buf,0, sizeof(buf));
		sprintf(buf,"%s %s %s","TOTAL CASHOUT AMT :","KS",receivedData.Amount);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
	}

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	

	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	sdkPrintStart();
	
	    	
}


void PrintTopUp(void)
{
	SDK_PRINT_FONT st_font;
 	u8 buf[64] = {0};
 	s32 spacing = 4;

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "TOPUP", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SUCCESS.", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PRINTING...", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sdkPrintInit();
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    
    sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
    
    if(topup.Operator == MPT)
    {
    	sdkPrintBitMap("/mtd0/res/logo-mpt-prt.bmp", SDK_PRINT_MIDDLEALIGN, 0);	
	}
	else if(topup.Operator==MECTEL)
	{
		sdkPrintBitMap("/mtd0/res/logo-mectel-prt.bmp", SDK_PRINT_MIDDLEALIGN, 0);	
	}
	else if(topup.Operator==OOREDOO)
	{
		sdkPrintBitMap("/mtd0/res/logo-ooredoo-prt.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	}
	else if(topup.Operator==TELENOR)
	{
		sdkPrintBitMap("/mtd0/res/logo-telenor-prt.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	}
		
	if (topup.TopUpType ==TOPUP_ELOAD)
	{
		memset(&st_font, 0, sizeof(st_font));
	    st_font.uiAscFont = SDK_PRN_ASCII16X16B;
	    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
	    sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	    
	    memset(buf,0,sizeof(buf));
	    sprintf(buf, "%s","RECEIPT SLIP");
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"SUCCESSFULLY TOP-UP\nKS.%s to 09%s",receivedData.Amount,receivedData.PhoneNo);
	    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	    
	    
	    memset(&st_font, 0, sizeof(st_font));
	    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	    
	    memset(buf, 0, sizeof(buf));
	    memset(buf, '-', 32);
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
		if(topup.PaymentType==PAY_TCARD)
		{
			memset(buf,0,sizeof(buf));
			sprintf(buf,"MEMBER CARD No     :%s",receivedData.CustomerCardNum);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf,"MEMBER CARD BALANCE:KS %s",receivedData.BalanceAmount);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			
			memset(buf, 0, sizeof(buf));
		    memset(buf, '-', 32);
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf,"MID      	: %s",receivedData.CustRef);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		}
		 if(topup.Operator==OOREDOO)
 	 {
   		memset(buf, 0, sizeof(buf));
   		sprintf(buf, "REF NO    : %s",receivedData.RefNo);
   		Trace("Printtopup","ooredoo refno=%s\r\n",receivedData.RefNo);
      	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
 	 }
	
		
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(&buf, 0, sizeof(buf));
		sprintf(buf, "TRX       : %s",receivedData.TRX);
	   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	   	memset(buf, 0, sizeof(buf));
		sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	   	
	   	memset(buf,0,sizeof(buf));
	   	sprintf(buf,"%s","THANK YOU");
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	    
	}
	else if(topup.TopUpType ==TOPUP_EPIN)
	{
			memset(&st_font, 0, sizeof(st_font));
		    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
		    
		    memset(buf, 0, sizeof(buf));
		    memset(buf, '-', 32);
		    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		    
		    memset(buf,0,sizeof(buf));
	    	sprintf(buf, "%s","RECEIPT SLIP");
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
		    
		    memset(&st_font,0,sizeof(st_font));
			st_font.uiAscFont = SDK_PRN_ASCII12X24B;
			st_font.uiAscZoom = SDK_PRN_ZOOM_N;
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s", " PIN ");
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
			
			memset(&st_font, 0, sizeof(st_font));
		    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
		    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
		    
		    memset(buf,0,sizeof(buf));
		    sprintf(buf,"%s",receivedData.PhPinCode);
			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
			
			memset(&st_font, 0, sizeof(st_font));
		    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
		    
		    memset(buf, 0, sizeof(buf));
		    memset(buf, '-', 32);
		    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		    
		   	if(topup.PaymentType==PAY_TCARD)
		   	{
		   		memset(buf,0,sizeof(buf));
				sprintf(buf,"MEMBER CARD No     :%s",receivedData.CustomerCardNum);
				sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
				memset(buf,0,sizeof(buf));
				sprintf(buf,"MEMBER CARD BALANCE:KS %s",receivedData.BalanceAmount);
				sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
				
				memset(buf, 0, sizeof(buf));
			    memset(buf, '-', 32);
				sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				 }
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s KS %s","AMOUNT    :",receivedData.Amount);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			
			memset(buf,0,sizeof(buf));
		    sprintf(buf,"%s %s","SERIAL NO :", receivedData.SerialNo);
		    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		    
		    memset(buf,0,sizeof(buf));
		    sprintf(buf, "%s %s","EXP DATE  :",receivedData.ExpDate);
			sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			
			memset(buf, 0, sizeof(buf));
		    memset(buf, '-', 32);
		    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		    if(topup.Operator == OOREDOO)
		    
		    {
		    	sdkPrintBitMap("/mtd0/res/topupGuideord.bmp", SDK_PRINT_MIDDLEALIGN, 0);
			}
			else if (topup.Operator==MPT)
			{
				sdkPrintBitMap("/mtd0/res/topupGuidempt.bmp", SDK_PRINT_MIDDLEALIGN, 0);
			}
			else if(topup.Operator==MECTEL)
			{
				if(MecNewPin == true)	
				{
					sdkPrintBitMap("/mtd0/res/mectopupguide.bmp", SDK_PRINT_MIDDLEALIGN, 0);	
				}
				else
				{
					sdkPrintBitMap("/mtd0/res/nmectopupguide.bmp", SDK_PRINT_MIDDLEALIGN, 0);
				}
			}
			else if(topup.Operator==TELENOR)
			{
				sdkPrintBitMap("/mtd0/res/topupGuideTelenor.bmp", SDK_PRINT_MIDDLEALIGN, 0);
			}
			memset(&st_font, 0, sizeof(st_font));
		    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
		    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

		    memset(buf, 0, sizeof(buf));
		    memset(buf, '-', 32);
		    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		    
		    if(topup.PaymentType==PAY_TCARD)
		    {
		    	 memset(buf,0,sizeof(buf));
				sprintf(buf,"MID       : %s",receivedData.CustRef);
				sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
			}
		   
			
			
	   		 memset(buf, 0, sizeof(buf));
			sprintf(buf, "AID       : %s",receivedData.AgentCardRef);
		   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		   	
		   	memset(buf, 0, sizeof(buf));
			sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
		   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		   	
		   	memset(&buf, 0, sizeof(buf));
			sprintf(buf, "TRX       : %s",receivedData.TRX);
		   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
		
		   	memset(buf, 0, sizeof(buf));
			sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
		   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	}
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0,80);

		/*
	     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	     * The differences between them are as follows:
	     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	     */
	sdkPrintStartNoRollBack();
	CheckPaperRollStatus();
	if(topup.MAINMenu==New_Menu)
	      	{
	        		 
	           	NewMainMenu();
			}
	else
			{
					
				
				MainMenu();
			}
	
			    	
}
/*void PrintSlipConfirm(s32 rtmp)
{
	s32 ret;
	sdkDispBrushScreen();
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/printSlip/%s/%s",uri,IPAddress,SvcVersion,receivedData.TRX);
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
			if(rtmp==1||rtmp==2||rtmp==3||rtmp==4||rtmp==6)
			{
				
				
					MainMenu();	
				
			}
			
			else if(rtmp==5)
			
			{
				SalesMenu();
 			}
 			
 			
			
			
			else if(rtmp==7)
			{
				ReprintMainMenu();
			}
		
			
			 
		}
		else
		{
			memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
			js_tok(receivedData.ErrorMessage,"message",0);
			
			DisplayErrorMessage("PRINT SLIP CONFIRM",receivedData.ErrorMessage,false);
		}
	    
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	MainMenu();
}*/
void PrintRefund(void)
{
	SDK_PRINT_FONT st_font;
 	u8 buf[64] = {0};
 	s32 spacing = 4;
 	u8 name[32];

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "REFUND", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SUCCESS", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PRINTING...", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sdkPrintInit();
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    
    sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
    
	strcpy(name, " BILL REFUND");
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;
				
	sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN,0,20);
	
	if (!foragent)	
	{
		sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
	} 
	else  
	{
		sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
	}
				
	memset(&st_font, 0, sizeof(st_font));
   	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
   	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"BILL REF  : %s",sendData.asBill);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"REFUND AMT: KS %s",receivedData.Amount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"BILL TYPE : %s",receivedData.BillType);
	sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0, spacing);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	 	foragent = false;	
	sdkPrintStartNoRollBack();
	    	
}
void PrintCustomerRegister(void)
{
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	u8 name[32] = "CUSTOMER REGISTER\n SUCCESS";
	
	// Display printing slip
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"CUSTOMER REGISTER",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);

	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	
	// Set font
   	memset(&st_font, 0, sizeof(st_font));
   	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
   	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
   	
   	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","NAME      :",sendData.asName);
	sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s 09%s","PHONE No. :",sendData.asPhoneNum);
	sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","CARD No.  :",receivedData.CustomerCardNum);
	sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
	
	memset(buf,0,sizeof(buf));
	memset(buf,'-',32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "MID       : %s",receivedData.MemberRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	//sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);

	// Add some space to the end of slip
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);
	sdkPrintStart();


	/*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
    sdkPrintStartNoRollBack();
    CheckPaperRollStatus();
	MainMenu();			

}
/*void PrintRecall(s32 rtmp)
{
	s32 key;
	key = sdkKbWaitKey(SDK_KEY_MASK_F2 | SDK_KEY_MASK_ESC,20000);
	switch(key)
    	{
			case SDK_KEY_F2:
    			{	
    				if(rtmp==1)
    				{
    					if(printtype.BillPrint==AEON||printtype.BillPrint==CP||printtype.BillPrint==YCDC||printtype.BillPrint==YESC||printtype.BillPrint==MPTBILL)
						{
							BillPrint();
							foragent = true;
							BillPrint();
							ForRecall=false;
							PrintSlipConfirm(1);
							
 					
						}	
					}
    				else if(rtmp==2)
    				{
    					 if(printtype.BillPrint==BNF||printtype.BillPrint==HELLOCAB||printtype.BillPrint==TICKETBO)
						{
							BillPrint();
							ForRecall=false;
							PrintSlipConfirm(2);
						}	
					}
					
    				else if(rtmp==3)
    				{
    					 if(printtype.CashPrint==CASHOUT)
						{
							PrintDomesticCashOut();
							CashOutRecall=false;
							PrintSlipConfirm(3);
						}
					}
					 
				}
					
			break;
			case SDK_KEY_ESC:
				{
				
					if(printtype.CashPrint==CASHOUT)
					{
						MainMenu(); 
					}
					else if(printtype.BillPrint==AEON||printtype.BillPrint==CP||printtype.BillPrint==YCDC||printtype.BillPrint==YESC||printtype.BillPrint==MPTBILL||printtype.BillPrint==BNF||printtype.BillPrint==HELLOCAB)
					{
						BillPayMainMenu();
					}
				}
				break;
				default:
				{
						
				}	
		}
	
		  	
}*/
 
 
 

void PrintAgentRefill(void)
{
	//SDK_PRINT_FONT st_font;
 	//u8 buf[64] = {0};
 	//s32 spacing = 4;

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "AGENT REFILL", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SUCCESS.", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PRINTING...", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	u8 name[32] = {0};
    SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
    
    strcpy(name, " REFILL SLIP");
	
  	sdkPrintInit();
  	 
  	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
    
    sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
    
   	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
   	
   	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf,"FREE OF CHARGE");
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	memset(buf, 0, sizeof(buf));
    memset(buf, '-', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
   	
   	memset(buf,0,sizeof(buf));
   	sprintf(buf,"%s Ks %s","AGENT REFILL AMT :",receivedData.RefillAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf,"%s Ks %s","AGENT WALLET AMT :",receivedData.BalanceAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf,"%s %s","AGENT CARD NO    :" ,sendData.asAgentCardNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
    
    memset(buf, 0, sizeof(buf));
    memset(buf, '-', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    
    memset(&buf, 0, sizeof(buf));
	sprintf(buf, "SID       : %s",receivedData.SalesCardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		   	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
		
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

		/*
	     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	     * The differences between them are as follows:
	     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	     */
	sdkPrintStartNoRollBack();
	CheckPaperRollStatus();
	SalesMenu();
	
}
void PrintAgentWithdraw(void)
{
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "AGENT WITHDRAW", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SUCCESS.", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PRINTING...", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	u8 name[32] = {0};
    SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	
	strcpy(name, " WITHDRAW SLIP");
	
	sdkPrintInit();
  	 
  	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
    
    sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
    
   	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
   	
   	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
   	
   	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%sKS %s","AGENT WITHDRAW AMT:",receivedData.WithdrawAmount);
   sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
    
	memset(buf,0,sizeof(buf));
    sprintf(buf,"%sKS %s","AGENT WALLET LEFT :",receivedData.BalanceAmount);
	sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","AGENT CARD No.    :",receivedData.CardNo);
	sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    
    memset(&buf, 0, sizeof(buf));
	sprintf(buf, "SID       : %s",receivedData.SalesCardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		   	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
		
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		   	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

		/*
	     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	     * The differences between them are as follows:
	     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	     */
	sdkPrintStartNoRollBack();
	CheckPaperRollStatus();
	SalesMenu();
	
}
void PrintMemToMemTransfer(void)
{
	SDK_PRINT_FONT st_font;
 	char  buf[64] = {0};
 	s32 spacing = 4;
	u8 name[32] = {0};

	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"CASH TRANSFER",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    
	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);

	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);
	
	
	strcpy(name, "MEMBER\nTRANSFER");
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s KS %s", name,receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	sdkPrintStr("CUSTOMER COPY", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
 	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

    memset(buf, 0, sizeof(buf));
    memset(buf, '-', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "SENDER CARD No   :",receivedData.CardNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
	
	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "RECEIVER CARD No :",receivedData.CustomerCardNum);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s KS %s", "WALLET LEFT      :",receivedData.BalanceAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   
    memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s %s", "TRANSFER AMOUNT :KS",receivedData.TransferAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s", "TRANSFER FEES   :KS",receivedData.TransferFee);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s","TOTAL AMOUNT    :KS",receivedData.TotalAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
   	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "MID       : %s",receivedData.MemberRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	// Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);
	
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    
	//sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);


	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);

	
	
	

	strcpy(name, "MEMEBER\nTRANSFER");
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s KS %s", name,receivedData.TransferAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	sdkPrintStr("AGENT COPY", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
 	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

    memset(buf, 0, sizeof(buf));
    memset(buf, '-', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    
   memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "SENDER CARD No   :",receivedData.CardNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
	
	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "RECEIVER CARD No :",receivedData.CustomerCardNum);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   
    memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s %s", "TRANSFER AMOUNT :KS",receivedData.TransferAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s", "TRANSFER FEES   :KS",receivedData.TransferFee);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s %s","TOTAL AMOUNT    :KS",receivedData.TotalAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
   	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "MID       : %s",receivedData.MemberRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	// Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    
	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 80);


	
	/*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
    sdkPrintStartNoRollBack();
    CheckPaperRollStatus();
  	MainMenu();
}
void printMemtoMemCashOut(void)
{
	SDK_PRINT_FONT st_font;
	u8 buf[64] = {0};
	s32 spacing = 4;
	u8 name[32] = {0};
	
	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"DOMESTIC CASHOUT",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);

	strcpy(name, "MEMBER\nCASHOUT:KS");
		
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s",name,receivedData.Amount);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	sdkPrintStr("CUSTOMER COPY", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
		
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "RECEIVER CARD No :",receivedData.CustomerCardNum);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s KS %s", "WALLET LEFT      :",receivedData.BalanceAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","CASHOUT MONEY:","KS",receivedData.Amount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","CASHOUT FEES :","KS",receivedData.commission);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","( FEES INCLUDE IN MEMBERCARD.)");
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	/*memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","TOTAL CASHOUT AMOUNT:","KS",receivedData.TotalAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);*/
	
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "MID       : %s",receivedData.MemberRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 80);

	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);

	strcpy(name, "MEMBER\nCASHOUT:KS");
		
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s",name,receivedData.Amount);
	Trace("xgd","for Amount=%d",receivedData.Amount);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	sdkPrintStr("AGENT COPY", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	
	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "RECEIVER CARD No :",receivedData.CustomerCardNum);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);


	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","CASHOUT MONEY:","KS",receivedData.Amount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);


	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","CASHOUT FEES :","KS",receivedData.commission);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","( FEES INCLUDE IN MEMBERCARD.)");
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "MID       : %s",receivedData.MemberRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);	
	

	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	sdkPrintStart();
	CheckPaperRollStatus();
	MainMenu();
	    	
}
void PrintMemToMemCashIn(void)
{
	SDK_PRINT_FONT st_font;
	u8 buf[64] = {0};
	s32 spacing = 4;
	u8 name[32] = {0};
	strcpy(name,"MEMBER\nCASHIN SLIP");
		
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"DOMESTIC CASHIN",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
	sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "MEMBER CARD No :",receivedData.CustomerCardNum);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);


	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","CASHIN AMOUNT :","KS",receivedData.Amount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","TOTAL WALLET AMOUNT:KS",receivedData.BalanceAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "MID       : %s",receivedData.MemberRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);	
	
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	 
	sdkPrintStart();
	CheckPaperRollStatus();
	MainMenu();

}

void PrintMemCheckBalance(void)
{
	// Initialize the printer
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	u8 name[32] = "MEMBER\nBALANCE SLIP";
	
	
	
	// Display printing slip
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"MEMBER CHECK BALANCE",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);

	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	// Set font
   	memset(&st_font, 0, sizeof(st_font));
   	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
   	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

   	// Pack receipt contents
   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "WALLET LEFT   : KS %s",receivedData.BalanceAmount);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	
   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "CHECK BALANCE FEES: KS %s",receivedData.commission);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "MEMBER CARD NO.: %s",receivedData.CustomerCardNum);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "MID       : %s",receivedData.MemberRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
   	
   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	

	// Add some space to the end of slip
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
   sdkPrintStart();
   CheckPaperRollStatus();
    MainMenu();
		
}

void PrintIntCashout(void)
{
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	//u8 Header[32] = "INTERNATIONAL CASHOUT";
	u8 name[64]={0};
	u8 buf2[64] = {0};
	u8 buf3[64] = {0};
	u8 SenderCountry[64] = {0};
	u8 ReceiverCountry[64] = {0};
	
	
	
	// Display printing slip
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"INT CASHOUT",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCESS",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sprintf(name, "%s %s %s","INT'L CASHOUT\n","KS",receivedData.Amount);
	sdkPrintInit();
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(asPathPrtAGDlogo, SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",name);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	if (!foragent)
			{
				sdkPrintStr("CUSTOMER COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
					
			}	 
			else 
			{
				sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
			} 
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","RECIPIENT:","NON MEMBER");
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","MOBILE NO: ",receivedData.PhoneNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","RECIPIENT ID:",receivedData.BookingId);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	if (strcmp(receivedData.SenderCountryCode,"66")==0)
	{
		
		memset(buf2,0,sizeof(buf2));
		sprintf(buf2,"TO  : %s","MYANMAR");
	}
	else 
	{
		  
		memset(buf2,0,sizeof(buf2));
		sprintf(buf2,"TO  : %s","THAILAND");
	 }				
	sprintf(SenderCountry,"%s",buf2);
	sdkPrintStr(SenderCountry, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	if (strcmp(receivedData.ReceiverCountryCode,"95")==0)
	{
		memset(buf3,0,sizeof(buf3));
		sprintf(buf3,"FROM: %s","THAILAND");
					
	}
	else 
	{
		memset(buf3,0,sizeof(buf3));
		sprintf(buf3,"FROM: %s","MYANMAR");
	}
	sprintf(ReceiverCountry,"%s",buf3);
	sdkPrintStr(ReceiverCountry, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf,"%s %s %s","CASHOUT MONEY:","KS",receivedData.Amount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","NOTIFY CASHOUT STATUS:",receivedData.ServiceStatus);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN,0,spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

   	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

    // Set font
    memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII8X16;
    st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	

	// Add some space to the end of slip
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
     * The differences between them are as follows:
     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
     */
   sdkPrintStart();
	
	
	
}
void PrintMasterAgentRefill(void)
{
	// Initialize the printer
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	u8 name[32] = "MASTER AGENT REFILL";
	
	
	
	// Display printing slip
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"MASTERREFILL",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
	sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "MASTER CARD NO:",receivedData.CustRef);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "AGENT CARD NO :",receivedData.AgentCardNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);


	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","REFILL AMOUNT :","KS",receivedData.RefillAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","BALANCE AMOUNT:","KS",receivedData.BalanceAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","TOTAL AMOUNT:KS",receivedData.TotalAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "MAS       : %s",receivedData.MasterCardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.AgentCardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0,80);	
	

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	 
	sdkPrintStart();
	CheckPaperRollStatus();
	MainMenu();
}
void PrintMasterAgentWithDraw(void)
{
	// Initialize the printer
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	u8 name[32] = "MASTER AGENT WITHDRAW";
	
	
	
	// Display printing slip
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"MASTEAGENT WITHDRAW",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
	sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "MASTER CARD NO:",receivedData.CustRef);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "AGENT CARD NO :",receivedData.AgentCardNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);


	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","TOTAL AMOUNT   :KS",receivedData.TotalAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","WITHDRAW AMOUNT:","KS",receivedData.RefillAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s %s","REMAING BALANCE:","KS",receivedData.BalanceAmount);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "MAS       : %s",receivedData.MasterCardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.AgentCardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	 
	sdkPrintStart();
	CheckPaperRollStatus();
	MainMenu();
}
void PrintAgentActivation(void)
{
		// Initialize the printer
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	u8 name[32] = "AGENT ACTIVATION\nSUCCESS(step4).";
	
	
	sdkDispClearScreen();    
	sdkDispFillRowRam(SDK_DISP_LINE1, 0, "AGENT ACTIVATION", SDK_DISP_DEFAULT);		
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING...", SDK_DISP_DEFAULT);		
	sdkDispBrushScreen();
	
	sdkPrintInit();
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
	sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	
	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s  KS %s", "POS DEPOSIT:",receivedData.Amount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "AGENT CARD NO :",receivedData.AgentCardNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);


	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);	
	
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	 
	sdkPrintStart();
	CheckPaperRollStatus();
	SalesMenu();
}
void CheckPaperRollStatus(void)
{
	Trace("CheckPaperRollStatus","Paper roll \r\n");
	s32 ret = 0;  
	ret = sdkPrintGetState(300);
	if(ret==SDK_PRINT_OUTOF_PAPER)
	{
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();	 
		sdkDispShowBmp(0,0,240,320,"/mtd0/res/outofpaper.bmp");
		sdkDispBrushScreen();
		sdkmSleep(1500);	 		 
	}
	
	
}
void PrintSaleRequest(void)
{
			// Initialize the printer
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
    u8 name[32] = {0};
    u8 name1[32] = {0};
    memset(buf,0,sizeof(buf));
   	
	 memset(name,0,sizeof(name));
    if(forsale.servicetype==agentContractEndRequest)
    {
    	
    	strcpy(name,"AGENT CONTRACT END\n");
    	strcpy(name1,"REQUEST SUCCESS.");
		sdkDispClearScreen();    
		sdkDispFillRowRam(SDK_DISP_LINE1, 0, "AGENT CONTRACT END", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "REQUEST SUCCESS.", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING...", SDK_DISP_DEFAULT);		
		sdkDispBrushScreen();	
	}
	
	else if(forsale.servicetype==terminalChangeRequest)
	{
		
		 strcpy(name,"TERMINAL CHANGE\n REQUEST SUCCESS.");
		sdkDispClearScreen();    
		sdkDispFillRowRam(SDK_DISP_LINE1, 0, "TERMINAL CHANGE", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "REQUEST SUCCESS.", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING...", SDK_DISP_DEFAULT);		
		sdkDispBrushScreen();
	}
	else if (forsale.servicetype==terminalSwitchRequest)
	{
		strcpy(name,"TERMINAL SWITCH\n REQUEST SUCCESS.");
		sdkDispClearScreen();    
		sdkDispFillRowRam(SDK_DISP_LINE1, 0, "TERMINAL SWITCH", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "REQUEST SUCCESS.", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING...", SDK_DISP_DEFAULT);		
		sdkDispBrushScreen();
	}
	else if(forsale.servicetype==reactivateAgentRequest)
	{
		strcpy(name,"REACTIVE AGENT\n SUCCESS.");
		sdkDispClearScreen();    
		sdkDispFillRowRam(SDK_DISP_LINE1, 0, "REACTIVE AGENT", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING...", SDK_DISP_DEFAULT);		
		sdkDispBrushScreen();
	}
	else if(forsale.servicetype==posHandoverRequest)
	{
		strcpy(name,"POS HAND OVER\n SUCCESS(Step3).");
		sdkDispClearScreen();    
		sdkDispFillRowRam(SDK_DISP_LINE1, 0, "POS HAND OVER", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING...", SDK_DISP_DEFAULT);		
		sdkDispBrushScreen();
	}
	sdkPrintInit();
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
	sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN, 0, 20);
	 if(forsale.servicetype==agentContractEndRequest)
	 {
	 	sdkPrintStr(name1,st_font,SDK_PRINT_MIDDLEALIGN, 0, 20);
	 }
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	
	
	 if(forsale.servicetype==posHandoverRequest)
	{
		memset(buf,0,sizeof(buf));
	   	sprintf(buf, "%s %s", "SALE CARD NO :",receivedData.SaleCardRef);
	    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	}
	else
	{
		memset(buf,0,sizeof(buf));
	   	sprintf(buf, "%s %s", "AGENT CARD NO :",receivedData.AgentCardNo);
	    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	}
    


	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	if(forsale.servicetype==agentContractEndRequest|| forsale.servicetype==reactivateAgentRequest||forsale.servicetype==terminalChangeRequest||forsale.servicetype==terminalSwitchRequest)
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);	
	
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 80);

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	 
	sdkPrintStart();
	CheckPaperRollStatus();
	SalesMenu();
}
void PrintMasterSettlement(void)
{
	// Initialize the printer
	SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
	u8 name[32] = "MASTER AGENT \n SETTLEMENT";
	
	
	
	// Display printing slip
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"MASTE AGENT ",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"SETTLEMENT",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"SUCCED",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4,0,"PRINTING...",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkPrintInit();
	 
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII16X24B;
	st_font.uiAscZoom = SDK_PRN_ZOOM_B;

	sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	sdkPrintBitMap(tmnLogo, SDK_PRINT_MIDDLEALIGN, 0);
	sdkPrintStr(name,st_font,SDK_PRINT_MIDDLEALIGN, 0, 20);
	
	if (!foragent)
			{
				sdkPrintStr("AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
					
			}	 
			else 
			{
				sdkPrintStr(" MASTER AGENT COPY",st_font,SDK_PRINT_MIDDLEALIGN,0,20);
			} 
				
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "MASTER CARD NO   :",receivedData.AgentCardNo);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "MASTER BALANCE   :KS",receivedData.BalanceAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "SELLTMENT BAL    :KS",receivedData.settotalamt);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "%s %s", "MASTER NEW BAL   :KS",receivedData.TotalAmount);
    sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);


	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","AGENT CARD N0   :",receivedData.OrderNumber);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","AGENT OPENING BAL:KS",receivedData.setopeningbalnce);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s %s","AGENT CLOSING BAL:KS",receivedData.setclosingbalance);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "MAS       : %s",receivedData.MasterCardRef);
   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	
	
	sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
	
	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
	
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0,50);

	/*
	 * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	 * The differences between them are as follows:
	 * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	 * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	 */
	 
	sdkPrintStart();
	
}


