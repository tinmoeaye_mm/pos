/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : Cashin.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/3/17
   Last Modified :
   Description   :CashIn module-related functions are defined
                  here
   History       :
   1.Date        : 2016/3/17
    Author      :  Tin Moe Aye
    Modification: Created file

******************************************************************************/



#include "../inc/global.h"

char copyagentcardno[10] ;

char agentcardno[10];
void TapMasterAgentCard(void);
void OTHERAgentCardNum(void);
void OTHERAgentCardRead(void);
void MasterAgentConfrim(void);
void TapOtherAgentCard(void);
void MasterAgentWithDrawSendData(void);
void MasterAgentCashInSendData(void);
void MasterAgentWithdrawConfrim(void);
void MasterAgentWithDrawAmt(void);
void MasterAgentCashIn(void);
void MasterAgentWithdraw(void);
void MasterAgentPinCode(void);
void MemToMemCashInConfirm(void);
void MemToMemCashIn(void);
void ForSendData(void);
void NewMainMenu(void);
void MasterAgentCard(void);
void MasterAgentSettlement(void);
void MasterSettlementSecurl(void);
void MasterAgentSettlementSendData(void);
void MasterAgentPinCode(void);
void MasterSettlementConfirm(void);
void MemToMemCashInMainMenu(void)
{
	s32 res;
	u8 buf[16] = {0};
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320,"/mtd0/res/CashinMainMeun.bmp");
	sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
	sdkDispFillRowRam(SDK_DISP_LINE5, 0, "PRESS [ENTER] TO CONTINUE", SDK_DISP_DEFAULT);
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispBrushScreen();;
	
	secondtransactionstart=false;     // for pageagentcheckbalance
	
	memset(buf,0,sizeof(buf));
	res= GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,4,8, NULL, " KS");
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				printtype.CashPrint=CASHIN;
				Trace("cashin", "buf= %s\r\n", buf);
				memset(sendData.asAmount,0,sizeof(sendData.asAmount));
				memcpy(sendData.asAmount,&buf[1],buf[0]);
				Trace("cashin", "sendData.asCashinAmount= %s\r\n",sendData.asAmount);
				ModuleTransferCustomerReadCard();
				 //ReadMemoryCard();
			}
			break;
		
		case SDK_KEY_ESC:
			{
				MainMenu();	
			}
			break;
		default:
			{
			
			}				
	}

	NewMainMenu();
		
}

void MemToMemCashInConfirm(void)
{
	s32 key;
	
	u8 buf[128]={0};
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "MEMBER CASHIN");
	sdkDispAt(60, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","MEMBER CARDNo:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asCustomerCardNo);
	sdkDispAt(12,100,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT:");
	sdkDispAt(12,140,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",sendData.asAmount);
	sdkDispAt(12,180,buf);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);

	sdkDispBrushScreen();
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				MemToMemCashIn();
			}
			break;
		case SDK_KEY_ESC:
			{
				MemToMemCashInMainMenu();
			}
			break;
		default:
			{
				
			}		
	}
	MemToMemCashInMainMenu();
}
void MemToMemCashIn(void)
{
	
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	ForSendData();
	Trace("memtomemcashin","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	

	
	sprintf(message,"%s%s%s%s%s",sendData.asAmount,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("memtomemcashin","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/cusCashIn/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAmount,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
	
	Trace("memtomemcashin","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0)
	{
	
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);

		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			js_tok(receivedData.Amount,"cashInAmount",0);
			
			js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
			
			js_tok(receivedData.BalanceAmount,"cusBalanceAmount",0);
			
			js_tok(receivedData.MemberRef,"customerCardRef",0);
			
			PrintMemToMemCashIn();
			
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;   // for pageagentcheckbalance
			
			
			js_tok(receivedData.ErrorMessage,"errorMessage",0);
			
			DisplayErrorMessage("Member CashIn",receivedData.ErrorMessage,false);
		}
		
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	NewMainMenu();
	
}
void MemberCheckBalance(void)
{
	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	
	
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key;

	
	s32 ret;
	
	step1:
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/membercard.bmp");
	sdkDispBrushScreen();
	sdkmSleep(1500);
	//Trace("xgd", "Start ModuleReadCard= \r\n");
	
	printtype.CashPrint=CASHBALANCE;
   	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	
	  	
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asCustomerCardNo,0,sizeof(sendData.asCustomerCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("MemberCheckBalance", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	if(strcmp(sendData.asFirstDigit,"2")==0)
	{
		memcpy(sendData.asCustomerCardNo,&buf1[0],9);
		Trace("MemberCheckBalance","Customer Card No=%s\r\n",sendData.asCustomerCardNo);
		
	}	
	else 
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "MEMBER CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		goto step1;
	}
	
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sprintf(message,"%s%s%d%d%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,0,0,sendData.asTerminalSN);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("MemberCheckBalance","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/cusChkBalance/%s/%s/%s/%d/%d/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,0,0,sendData.asTerminalSN,sendData.asHashData);
	
	Trace("MemberCheckBalance","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		//Trace("xgd","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			js_tok(receivedData.MemberRef,"customerCardRef",0);
			
			js_tok(receivedData.BalanceAmount,"cusBalanceAmount",0);
			
			js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
			
			js_tok(receivedData.commission,"checkBalanceFee",0);
			
			js_tok(receivedData.MemberRef,"customerCardRef",0);
			
			sdkDispSetFontSize(SDK_DISP_FONT_BIG);
			sdkDispClearScreen();

			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s", "MEM CHECK BALANCE");
			sdkDispAt(12, 20, buf);
		
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s","BALANCE:");
			sdkDispAt(12,80,buf);
			memset(buf,0,sizeof(buf));
			sprintf(buf,"KS %s",receivedData.BalanceAmount);
			sdkDispAt(12,120,buf);
			
			sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
			sdkDispFillRowRam(SDK_DISP_LINE5, 0, "PRESS [ENTER] TO PRINT SLIP", SDK_DISP_DEFAULT);
			sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	        sdkDispBrushScreen();

			key = sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);

			switch (key)
	        {
	             case SDK_KEY_ENTER:
	                {
	                
	                    PrintMemCheckBalance();
	                }
	                break;
	             case SDK_KEY_ESC:
	                {
	                    MainMenu();
	                }
	             default:
	                {
	                }
	                break;
	        }
					
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			
			js_tok(receivedData.ErrorMessage,"errorMessage",0);
			
			DisplayErrorMessage("Member CheckBalance",receivedData.ErrorMessage,false);
		}
		
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	
	MainMenu();
	
}
void MasterAgentCashIn(void)
{
	s32 res = 0 ,key = 0;
	u8 buf[128] = {0};
	printtype.CashPrint=MASTERREFILL;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/mastercashin.bmp");
	sdkDispBrushScreen();
	
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | SDK_KEY_MASK_5 | SDK_KEY_MASK_ESC , KbWaitTime);
 	
 	memset(sendData.asAmount,0,sizeof(sendData.asAmount));			
    
	switch (key)
    {
         case SDK_KEY_1:
            {
                sprintf(sendData.asAmount,"100000");
                Trace("MasterAgentCashIn", "sendData.asAmount= %s\r\n", sendData.asAmount);
                TapOtherAgentCard();
            }
            break;

         case SDK_KEY_2:
            {
				sprintf(sendData.asAmount,"200000");
				Trace("MasterAgentCashIn", "sendData.asAmount= %s\r\n", sendData.asAmount);
				TapOtherAgentCard();
            }
            break;

         case SDK_KEY_3:
            {
				sprintf(sendData.asAmount,"300000");
				Trace("MasterAgentCashIn", "sendData.asAmount= %s\r\n", sendData.asAmount);
				TapOtherAgentCard();
            }
            break;

         case SDK_KEY_4:
            {
                sprintf(sendData.asAmount,"400000");
                Trace("MasterAgentCashIn", "sendData.asAmount= %s\r\n", sendData.asAmount);
                TapOtherAgentCard();
            }
            break;
        case SDK_KEY_5:
        	{
        		sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/masteragentamount.bmp");
				sdkDispBrushScreen();
			    memset(buf,0,sizeof(buf));
				res= GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,4,8, NULL, " KS");
				switch(res)
				{
					case SDK_KEY_ENTER:	
					{
						
						Trace("MasterAgentCashIn", "buf= %s\r\n", buf);
						memset(sendData.asAmount,0,sizeof(sendData.asAmount));
						memcpy(sendData.asAmount,&buf[1],8);
						Trace("MasterAgentCashIn", "sendData.asMasteragentCashinAmount= %s\r\n",sendData.asAmount);
						TapOtherAgentCard();						
					}
					break;
					case SDK_KEY_ESC:
					{
						AgentMenu();	
					}
					break;
					default:
					{
											
					}	
			
	      			MainMenu();
				}
				break;
        	}
        	

	}
	
	
	
}

void TapOtherAgentCard(void)
{

	s32 key;
	s32 timid;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/cardmenu.bmp");
	sdkDispBrushScreen();
	//sdkmSleep(1500);

	timid = sdkTimerGetId(); 
 	while (!sdkTimerIsEnd(timid, 30000))
    {
        // Check for valid key
        if ((key = sdkKbGetKey()) > 0)        
         { 
         
    		switch (key)
                {
                    // Finish inputting
                    case SDK_KEY_ENTER:
                    	{
                    		OTHERAgentCardRead();
						}
                            
               		 break;
						                            
                    case SDK_KEY_ESC:
                    	{
                    		AgentMenu();
						}
        			break;

           			case 15:
           				{
						   OTHERAgentCardNum();
						}
          			break;
          			case SDK_KEY_F1:
          				{
          					OTHERAgentCardNum();
						 }
          

                    default:
                        sdkSysBeep(SDK_SYS_BEEP_ERR);
                        continue;
                }
        }

  }
  
   AgentMenu();
	
}
void OTHERAgentCardRead(void)
{
	
	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
    
	if(printtype.CashPrint==MASTERREFILL)
	{
		step1:
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/showagentcard.bmp");
		sdkDispBrushScreen();
		sdkmSleep(1500);
	}
	
	else if(printtype.CashPrint==MASTERWITHDRAW)
	{
		step2:
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/withdrawotheragentcardread.bmp");
		sdkDispBrushScreen();
		sdkmSleep(1500);
	}
	else if(printtype.CashPrint==MASTERSETTLEMENT)
	{
		step3:
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/setcardread.bmp");
		sdkDispBrushScreen();
		sdkmSleep(1500);
	}
	
	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	
	  	
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asOtherAgentCardNo,0,sizeof(sendData.asOtherAgentCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("xgd", "Card Start Digit = %s\r\n",sendData.asFirstDigit);
	
	if(strcmp(sendData.asFirstDigit,"1")==0)
	{
		memcpy(sendData.asOtherAgentCardNo,&buf1[0],9);
		Trace("xgd","OtherAgent Card No=%s\r\n",sendData.asOtherAgentCardNo);
		
	}
	
	else 
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "AGENT CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		if(printtype.CashPrint==MASTERREFILL)
		{
			goto step1;
		}
		else if(printtype.CashPrint==MASTERWITHDRAW)
		{
			goto step2;
		}
		else if(printtype.CashPrint==MASTERSETTLEMENT)
		{
			goto step3;
		}
	}
		
	
	if(printtype.CashPrint==MASTERREFILL)
	{
		
		MasterAgentConfrim();
	}
	else if(printtype.CashPrint==MASTERWITHDRAW||printtype.CashPrint==MASTERSETTLEMENT)
	{
	
		MasterAgentPinCode();
	}
	
	
}
void OTHERAgentCardNum(void)
{
	
	s32 key = 0;
	if(printtype.CashPrint==MASTERREFILL)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/otheragentcardmanul.bmp");
		sdkDispBrushScreen();
	}
	else if(printtype.CashPrint==MASTERWITHDRAW)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/withdrawcardno.bmp");
		sdkDispBrushScreen();
	}
	else if(printtype.CashPrint==MASTERSETTLEMENT)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/settlementcardnum.bmp");
		sdkDispBrushScreen();
	}
	
	memset(copyagentcardno,0,sizeof(copyagentcardno));
	key = sdkKbGetScanf(30000,copyagentcardno,9,9,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	Trace("OTHERAgentCardNum","Intpasskey=%s\r\n",copyagentcardno);
	memset(sendData.asOtherAgentCardNo,0,sizeof(sendData.asOtherAgentCardNo));
	memcpy(sendData.asOtherAgentCardNo,&copyagentcardno[1],9);
	Trace("OTHERAGENTCARDNUM","sendData.asOtherAgentcardNo=%s\r\n",sendData.asOtherAgentCardNo);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				if(printtype.CashPrint==MASTERREFILL)
				{
					
					MasterAgentConfrim();
				}
				 else if((printtype.CashPrint==MASTERWITHDRAW)||(printtype.CashPrint==MASTERSETTLEMENT))
				{
					
					
					MasterAgentPinCode();
				}
				
			}
			break;
		case SDK_KEY_ESC:
			{
				if(printtype.CashPrint==MASTERREFILL)
				{
					MasterAgentCashIn();
				}
				 else if(printtype.CashPrint==MASTERWITHDRAW)
				 {
				 	MasterAgentWithdraw();
				 }
				 else if(printtype.CashPrint==	MASTERSETTLEMENT)
				 {
				 	MasterAgentSettlement();
				 }
			}
	}
	AgentMenu();
}
void MasterAgentConfrim(void)
{
	
	u8 buf[32] = {0};
	s32 key=0;
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "MAS REFILL");
	sdkDispAt(60, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",sendData.asAmount);
	sdkDispAt(108,60,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","MASTER CARDNo:");
	sdkDispAt(12,100,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asAgentCardNo);
	sdkDispAt(12,140,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AGENT CARDNo:");
	sdkDispAt(12,180,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asOtherAgentCardNo);
	sdkDispAt(12,220,buf);
	
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	sdkDispBrushScreen();
	key=sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);
	switch(key)
	{	
		case SDK_KEY_ENTER:
			{
		
				MasterAgentCashInSendData();	
				
			}
			break;
		case SDK_KEY_ESC:
			{
			
				 MasterAgentCashIn();
			}
			break;
		default:
			{
				
			}
		
	}

	AgentMenu();
	
}
void MasterAgentCashInSendData(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s%s",sendData.asAgentCardNo,sendData.asOtherAgentCardNo,"0",sendData.asAgentEnterPin,sendData.asTerminalSN,sendData.asAmount);
	Trace("MasterAgentCashInSendData","message=%s\r\n",message);
	
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("MasterAgentCashInSendData","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/masterAgentRefill/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asOtherAgentCardNo,"0",sendData.asAgentEnterPin,sendData.asTerminalSN,sendData.asAmount,sendData.asHashData);

	Trace("MasterAgentCashInSendData","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);

	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0)
		{

				js_tok(receivedData.ServiceStatus,"serviceStatus",0);

				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				
				{
					
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					transaction=true;              //  for pageagentcheckbalance
					secondtransactionstart=true;   //  for pageagentcheckbalance
			
					js_tok(receivedData.RefillAmount,"refillAmount",0);

					js_tok(receivedData.TotalAmount,"totalAmount",0);
					
					js_tok(receivedData.BalanceAmount,"agentBalanceAmount",0);
					
					js_tok(receivedData.AgentCardNo,"agentCardNo",0);
					
					js_tok(receivedData.AgentCardRef,"agentCardRef",0);
					
					js_tok(receivedData.MasterCardRef,"masterCardRef",0);
		
					js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
					
					js_tok(receivedData.TRX,"trx",0);	
		
					js_tok(receivedData.CustRef,"masterAgentCardNo",0);
					
					js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);	
					
					
					
					PrintMasterAgentRefill();				
				}
				else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					transaction=false;              //  for pageagentcheckbalance
				
			
				js_tok(receivedData.ErrorMessage,"errorMessage",0);
		
				DisplayErrorMessage("Master Agent CashIn",receivedData.ErrorMessage,false);
			}

		}
	
	else
	{
		DisplayErrorMessage("","",true);
	}
	AgentMenu();
	
}

void MasterAgentWithdraw(void)
{
	s32 key;
	printtype.CashPrint=MASTERWITHDRAW;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/WithdrawMeun.bmp");
	sdkDispBrushScreen();
	s32 timid;
	
	timid = sdkTimerGetId(); 
 	while (!sdkTimerIsEnd(timid, 30000))
    {
        // Check for valid key
        if ((key = sdkKbGetKey()) > 0)        
         { 
         
    		switch (key)
                {
                    // Finish inputting
                    case SDK_KEY_ENTER:
                    	{
                    		OTHERAgentCardRead();
						}
                            
               		 break;
						                            
                    case SDK_KEY_ESC:
                    	{
                    		AgentMenu();
						}
        			break;

           			case 15:
           				{
						   OTHERAgentCardNum();
						}
          			break;
          			case SDK_KEY_F1:
          				{
          					OTHERAgentCardNum();
						 }
          

                    default:
                        sdkSysBeep(SDK_SYS_BEEP_ERR);
                        continue;
                }
        }
      
	}
        AgentMenu();
}
void MasterAgentPinCode(void)
{	
	s32 key = 0;
	u8 buf[128] = {0};
	if(printtype.CashPrint==MASTERWITHDRAW)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/OtherAgentWithDrawCardNo.bmp");
		sdkDispBrushScreen();
	}
	else if(printtype.CashPrint==MASTERSETTLEMENT)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/settlementpincode.bmp");
		sdkDispBrushScreen();
	}

	
	memset(buf,0,sizeof(buf));
	 key = sdkKbGetScanf(60000, buf, 6, 6, SDK_MMI_PWD ,SDK_DISP_LINE4);
	switch(key)
	{
		case SDK_KEY_ENTER:	
		{
			
			Trace("MasterAgentCashInSendData", "buf= %s\r\n",buf);
			memset(sendData.asOtherPin,0,sizeof(sendData.asOtherPin));
			memcpy(sendData.asOtherPin,&buf[1],6);
			Trace("xgd", "sendData.asOtherPin= %s\r\n",sendData.asOtherPin);
			if(printtype.CashPrint==MASTERWITHDRAW)
			{
				MasterAgentWithDrawAmt();
			}
			else if(printtype.CashPrint==MASTERSETTLEMENT)
			{
				MasterAgentSettlementSendData();
			}
			
		}
		break;
		case SDK_KEY_ESC:
		{
			if(printtype.CashPrint==MASTERWITHDRAW)
			{
				MasterAgentWithdraw();
			}
			else if(printtype.CashPrint==MASTERSETTLEMENT)
			{
				MasterAgentSettlement();
			}
			
		}
		break;
		default:
		{
								
		}
						
			
	}
	AgentMenu();
	
	
}
void MasterAgentWithDrawAmt(void)
{
	s32 res = 0;
	u8 buf[128] = {0};
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/withdrawamt.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res= GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,4,8, NULL, " KS");
	switch(res)
	{
		case SDK_KEY_ENTER:	
		{
			printtype.CashPrint=MASTERWITHDRAW;
			Trace("xgd", "buf= %s\r\n",buf);
			memset(sendData.asAmount,0,sizeof(sendData.asAmount));
			memcpy(sendData.asAmount,&buf[1],buf[0]);
			Trace("xgd", "sendData.asMasteragentWithDreawAmount= %s\r\n",sendData.asAmount);
			MasterAgentWithdrawConfrim();
		}
		break;
		case SDK_KEY_ESC:
		{
			MasterAgentWithdraw();	
		}
		break;
		default:
		{
								
		}
						
			
	}
	AgentMenu();
}
void MasterAgentWithdrawConfrim(void)
{
	u8 buf[32] = {0};
	s32 key=0;
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "MAS WITHDRAW");
	sdkDispAt(40, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",sendData.asAmount);
	sdkDispAt(108,60,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","MASTER CARDNo:");
	sdkDispAt(12,100,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asAgentCardNo);
	sdkDispAt(12,140,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AGENT CARDNo:");
	sdkDispAt(12,180,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asOtherAgentCardNo);
	sdkDispAt(12,220,buf);
	
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	sdkDispBrushScreen();
	key=sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);
	switch(key)
	{	
		case SDK_KEY_ENTER:
			{
			
				MasterAgentWithDrawSendData();	
				
			}
			break;
		case SDK_KEY_ESC:
			{
				
				 //MasterAgentCashIn();
				 MasterAgentWithdraw();
			}
			break;
		default:
			{
				
			}
		
	}

	AgentMenu();
}

void MasterAgentWithDrawSendData(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s%s%s",sendData.asAgentCardNo,sendData.asOtherAgentCardNo,"0",sendData.asAgentEnterPin,sendData.asOtherPin,sendData.asTerminalSN,sendData.asAmount);
	Trace("xgd","message=%s\r\n",message);
	
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/masterAgentWithdraw/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asOtherAgentCardNo,"0",sendData.asAgentEnterPin,sendData.asOtherPin,sendData.asTerminalSN,sendData.asAmount,sendData.asHashData);

	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);

	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0)
		{

				js_tok(receivedData.ServiceStatus,"serviceStatus",0);

				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					transaction=true;              //  for pageagentcheckbalance
					secondtransactionstart=true;   //  for pageagentcheckbalance
			
		
					js_tok(receivedData.RefillAmount,"withdrawAmount",0);

					js_tok(receivedData.TotalAmount,"totalAmount",0);
					
					js_tok(receivedData.BalanceAmount,"remainingBalance",0);
					
					js_tok(receivedData.AgentCardNo,"agentCardNo",0);
					
					js_tok(receivedData.AgentCardRef,"agentCardRef",0);
					
					js_tok(receivedData.MasterCardRef,"masterAgentCardRef",0);
		
					js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
					
					js_tok(receivedData.TRX,"trx",0);	
		
					js_tok(receivedData.CustRef,"masterAgentCardNo",0);
					
					js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);	
					
					
					PrintMasterAgentWithDraw();				
				}
				else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				transaction=false;              //  for pageagentcheckbalance
		
			

				js_tok(receivedData.ErrorMessage,"errorMessage",0);
		
				DisplayErrorMessage("MasterAgent WithDraw",receivedData.ErrorMessage,false);
			}

		}
	
	else
	{
		DisplayErrorMessage("","",true);
	}
	MainMenu();
	
}
void MasterAgentSettlement(void)
{
	s32 key;
	printtype.CashPrint=MASTERSETTLEMENT;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/settlementchoosecard.bmp");
	sdkDispBrushScreen();
	s32 timid;
	
	timid = sdkTimerGetId(); 
 	while (!sdkTimerIsEnd(timid, 30000))
    {
        // Check for valid key
        if ((key = sdkKbGetKey()) > 0)        
         { 
         
    		switch (key)
                {
                    // Finish inputting
                    case SDK_KEY_ENTER:
                    	{
                    		OTHERAgentCardRead();
						}
                            
               		 break;
						                            
                    case SDK_KEY_ESC:
                    	{
                    		AgentMenu();
						}
        			break;

           			case 15:
           				{
						   OTHERAgentCardNum();
						}
          			break;
          			case SDK_KEY_F1:
          				{
          					OTHERAgentCardNum();
						 }
          

                    default:
                        sdkSysBeep(SDK_SYS_BEEP_ERR);
                        continue;
                }
        }
      
	}
        AgentMenu();
}

void MasterAgentSettlementSendData(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(out,0,sizeof(out));
	sdkMD5(out,sendData.asOtherPin,6);	
	memset(sendData.asHashEnterPin,0,sizeof(sendData.asHashEnterPin));
	sdkBcdToAsc(sendData.asHashEnterPin,out,16);
	Trace("settlement","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
	memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
	strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
	Trace("settlement","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
	
	memset(message,0,sizeof(message));
	memset(out,0,sizeof(out));
	sprintf(message,"%s%s%s%s",sendData.asAgentCardNo,sendData.asOtherAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN);
	Trace("settlement","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
	sdkBcdToAsc(sendData.asSignature, out, 32);
	Trace("settlement","signature = %s\r\n",sendData.asSignature);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"--data &masterAgentCardNo=%s&agentCardNo=%s&agentPinNo=%s&terminalSerialNo=%s&hashValue=%s",sendData.asAgentCardNo,sendData.asOtherAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asSignature);
	Trace("settlement","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	
	memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
	sprintf(sendData.asSendURL, "%s%s/masterAgentSettlement/",uri,IPAddress);
	Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
	

	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
	if(ret!=0)
		{

				js_tok(receivedData.ServiceStatus,"serviceStatus",0);

				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					transaction=true;              //  for pageagentcheckbalance
					secondtransactionstart=true;   //  for pageagentcheckbalance
			
					js_tok(receivedData.setamount,"masterBal",0);
					
					js_tok(receivedData.settotalamt,"masterNewBal",0);
					
					js_tok(receivedData.setopeningbalnce,"agentOpeningBal",0);
					
					js_tok(receivedData.setclosingbalance,"agentClosingBal",0);
					
					Trace("settlement","recivedData.closingbalance=%d\r\n",receivedData.closingBalance);
					
					
				
					MasterSettlementConfirm();
									
				}
				else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				transaction=false;              //  for pageagentcheckbalance
		
			

				js_tok(receivedData.ErrorMessage,"errorMessage",0);
		
				DisplayErrorMessage("MasterAgent WithDraw",receivedData.ErrorMessage,false);
			}

		}
	
	else
	{
		DisplayErrorMessage("","",true);
	}
	AgentMenu();
}
void MasterSettlementSecurl(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	ForSendData();               //checktrx
	Trace("settlement second","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	memset(out,0,sizeof(out));
	sdkMD5(out,sendData.asOtherPin,6);	
	memset(sendData.asHashEnterPin,0,sizeof(sendData.asHashEnterPin));
	sdkBcdToAsc(sendData.asHashEnterPin,out,16);
	Trace("settlement second","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
	memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
	strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
	Trace("settlement second","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
	
	memset(message,0,sizeof(message));
	memset(out,0,sizeof(out));
	sprintf(message,"%s%s%s%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.setamount,sendData.asOtherAgentCardNo,sendData.asPostAgentEnterPin,receivedData.setopeningbalnce,receivedData.setclosingbalance,sendData.asEDCserial);
	Trace("settlement second","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
	sdkBcdToAsc(sendData.asSignature, out, 32);
	Trace("settlement second","signature = %s\r\n",sendData.asSignature);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"--data &masterAgentCardNo=%s&terminalSerialNo=%s&masterBal=%s&agentCardNo=%s&agentPinNo=%s&agentOpeningBal=%s&agentClosingBal=%s&trx=%s&hashValue=%s",sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.setamount,sendData.asOtherAgentCardNo,sendData.asPostAgentEnterPin,receivedData.setopeningbalnce,receivedData.setclosingbalance,sendData.asEDCserial,sendData.asSignature);
	Trace("settlement","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	
	memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
	sprintf(sendData.asSendURL, "%s%s/masterAgentSettlementConfirm/",uri,IPAddress);
	Trace("settlement","sendData.asSendURL=%s\r\n",sendData.asSendURL);
	

	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
	if(ret!=0)
		{

				js_tok(receivedData.ServiceStatus,"serviceStatus",0);

				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					transaction=true;              //  for pageagentcheckbalance
					secondtransactionstart=true;   //  for pageagentcheckbalance
			
					js_tok(receivedData.Amount,"masterBal",0);
					
					js_tok(receivedData.TotalAmount,"masterNewBal",0);
					
					js_tok(receivedData.CardRef,"agentCardRef",0);

			
					js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			
					js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);

			
					js_tok(receivedData.TRX,"trx",0);
					
					js_tok(receivedData.AgentCardNo,"masterAgentCardNo",0);
					
					js_tok(receivedData.OrderNumber,"agentCardNo",0);
					
					js_tok(receivedData.BalanceAmount,"masterBal",0);
					
					js_tok(receivedData.settotalamt,"settlementBal",0);
					
					js_tok(receivedData.TotalAmount,"masterNewBal",0);
					
					js_tok(receivedData.setclosingbalance,"agentClosingBal",0);
					
					js_tok(receivedData.setopeningbalnce,"agentOpeningBal",0);
					
					js_tok(receivedData.MasterCardRef,"masterAgentCardRef",0);
					
					
					foragent = false;
					PrintMasterSettlement(); 
					foragent = true;									
					PrintMasterSettlement();
					CheckPaperRollStatus();
	
				
									
				}
				else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				transaction=false;              //  for pageagentcheckbalance
		
			

				js_tok(receivedData.ErrorMessage,"errorMessage",0);
		
				DisplayErrorMessage("MasterAgent WithDraw",receivedData.ErrorMessage,false);
			}

		}
	
	else
	{
		DisplayErrorMessage("","",true);
	}
	MainMenu();
}

void MasterSettlementConfirm(void)
{
	//Trace("Mastersettlementconfirm","confirm");
	u8 buf[32] = {0};
	s32 key=0;
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "MA SETTLEMENT");
	sdkDispAt(40, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","MA BAL:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS%s",receivedData.setamount);
	sdkDispAt(94,60,buf);
	//Trace("Settlementconfirm","test");
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AGENT OPENING BAL:");
	sdkDispAt(12,100,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",receivedData.setopeningbalnce);
	sdkDispAt(12,140,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AGENT CLOSING BAL:");
	sdkDispAt(12,180,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",receivedData.setclosingbalance);
	sdkDispAt(12,220,buf);
	
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	sdkDispBrushScreen();
	key=sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);
	switch(key)
	{	
		case SDK_KEY_ENTER:
			{
			
				MasterSettlementSecurl();	
				
			}
			break;
		case SDK_KEY_ESC:
			{
				MasterAgentSettlement();
				 
			}
			break;
		default:
			{
				
			}
		
	}

	AgentMenu();
}

