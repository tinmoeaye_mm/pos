/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : CustomerRegister.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/3/17
   Last Modified :
   Description   : agent module-related functions are defined
                  here
   History       :
   1.Date        : 2016/3/17
    Author      :  Tin Moe Aye
    Modification: Created file

******************************************************************************/
#include "../inc/global.h"

const u8 asPathSelectFinger1[] = "/mtd0/res/fpselect.bmp";

void CustomerRegisterFingerPrint(void);
void CustomerRegisterPhone(void);
void CustomerRegisterEnterPin(void);
void CustomerRegisterTapCard(void);
void CustomerRegisterConfirm(void);
void ModuleCustomerReadCard(void);
void CustomerRegisterName(void);

void CustomerRegisterName(void)
{		
	u8 buf[16] = {0};
	s32 res;

	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/Customerregistername.bmp");
	sdkDispBrushScreen();

	memset(buf,0,sizeof(buf));
	res = sdkKbGetScanf(60000, buf, 0, 20, SDK_MMI_LETTER ,SDK_DISP_LINE4);
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("Customerregistername", "buf= %s\r\n", buf);
				Trace("Customerregistername","len=%d\r\n",strlen(buf));
				memset(sendData.asName,0,sizeof(sendData.asName));
				if(strlen(buf)==0)
				{
					sprintf(sendData.asName,"null");
				}
				memcpy(sendData.asName,&buf[1],buf[0]);
				Trace("customerregistername","sendData.asName=%s\r\n",sendData.asName);
				CustomerRegisterPhone();
			}
			break;
		case SDK_KEY_ESC:
			{
				AgentMenu();
			}
			break;
			default:
			{
				AgentMenu();
			}	
	}
	AgentMenu();
}
void CustomerRegisterPhone(void)
{
	u8 buf[16] = {0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/CustomerRegisterPhone.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	switch(res)
	{	
		case SDK_KEY_ENTER:
			{
				Trace("customerregisterphone","buf=%s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("customerregisterphone","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				CustomerRegisterFingerPrint();
			}
			break;
		case SDK_KEY_ESC:
			{
				AgentMenu();
			}
			break;
			default:
			{
				AgentMenu();
			}	
		
	}
	AgentMenu();
}
void CustomerRegisterFingerPrint(void)
{
	s32 key=0;
	s32 ret = 0;
	u8 temp[1024];
	fingerprint.Error=AGENT;
	sdkDispClearScreen();
	sdkDispShowBmp(0,5,240,150,"/mtd0/res/Hdcustomer.bmp");
	sdkDispShowBmp(0, 0, 240, 320, asPathSelectFinger1);
	sdkDispBrushScreen();
	
	memset(sendData.asFingerImage,0,sizeof(sendData.asFingerImage));
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);

	if (key == SDK_KEY_1)
	{
		strcpy(sendData.asFingerImage, "lindex");
	}
	else if (key == SDK_KEY_2)
	{
		strcpy(sendData.asFingerImage, "rthumb");
	}
	else if(key==SDK_KEY_ESC)
	{
		CustomerRegisterPhone();
	}

	Trace("xgd","sendData.asFingerimage = %s\r\n",sendData.asFingerImage);
	
	ret = EnrollFPC(2,true,temp);

	s32 len2;
	u8 *pBufLen = &temp[8];
	len2 = ((pBufLen[0] << 24) 
		  | (pBufLen[1] << 16)
		  | (pBufLen[2] << 8)
		  | pBufLen[3]);
	
	u8 buf2[1024] = {0};
	sdkBcdToAsc(buf2, temp, len2);
	TraceHex("customerregisterfingerprint", "Fingerprint Template", temp, len2);
	
	Trace("customerregisterfingerprint","buf2 = %s\r\n",buf2);

	if (ret != SDK_OK)
	{
		AgentMenu();
	}
	memset(sendData.asFingerTemplate,0,sizeof(sendData.asFingerTemplate));

	memcpy(sendData.asFingerTemplate,buf2,len2*2); //cody for pst data
	
	//memcpy(sendData.asFingerTemplate,buf2,512);
	ret = VerifyFPC(temp,SafeLEVEL_3);
	if (ret == SDK_OK)									  //cancel key to exit
	{
		CustomerRegisterEnterPin();
	}
	else
	{
		AgentMenu();
	}
	
	
}
void CustomerRegisterEnterPin(void)
{
	s32 key=0;
	u8 temp2[128];
	s32 ret=0;
	memset(sendData.asCustomerEnterPin, 0, sizeof(sendData.asCustomerEnterPin));
	memset(temp2, 0, sizeof(temp2));
	u8 memberflag = 0;

	sdkDispClearScreen();
	sdkDispShowBmp(0,5,240,150,"/mtd0/res/Hdcustomer.bmp");
	sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/sxipin.bmp");
	sdkDispBrushScreen();				  

	key = sdkKbGetScanf(30000, temp2, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE5);
	
	switch(key)
	{
			case SDK_KEY_ENTER:
				{
					Trace("customerregisterenterpin", "temp2= %s\r\n", temp2);
					memcpy(sendData.asCustomerEnterPin, &temp2[1], 6);
					Trace("customerregisterenterpin","sendData.asCustomerEnterPin=%s\r\n",sendData.asCustomerEnterPin);
					memset(temp2, 0, sizeof(temp2));
						while (1)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0,5,240,150,"/mtd0/res/Hdcustomer.bmp");
							sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/rentersixpin.bmp");
							sdkDispBrushScreen();
						
							key = sdkKbGetScanf(30000, temp2, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE5);
							if (key == SDK_KEY_ESC || key == SDK_TIME_OUT)
							{
								AgentMenu();
							}
						
							ret = memcmp(sendData.asCustomerEnterPin, &temp2[1], sizeof(sendData.asCustomerEnterPin));
						
							if (SDK_EQU != ret)
							{
								sdkDispClearRow(SDK_DISP_LINE3);
								sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PIN NOT MATCH", SDK_DISP_NOFDISP | SDK_DISP_CDISP | SDK_DISP_INCOL);
								sdkDispBrushScreen();
								sdkmSleep(1000);
								memberflag++;
						
								if (memberflag == 3)
								{
									AgentMenu();
								}
							}
							
							else if(SDK_EQU==ret)
							{
								ModuleCustomerReadCard();
							}
							
						}
						
				}
				break;
			case SDK_KEY_ESC:
				{
					CustomerRegisterFingerPrint();
				}
				break;
			default:
			{
				
			}	
	}
	AgentMenu();
	
}

void ModuleCustomerReadCard(void)
{
	sdkDispClearScreen();
	sdkDispShowBmp(0,5,240,150,"/mtd0/res/Hdcustomer.bmp");
	//sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/trueagent.bmp");
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/membercard.bmp");
	sdkDispBrushScreen();
	sdkmSleep(1500);
	//Trace("xgd", "Start ModuleReadCard= \r\n");

   	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	//u8 buf5[256] = {0};
	
	
   	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asCustomerCardNo,0,sizeof(sendData.asCustomerCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("modulereadcard", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	if(strcmp(sendData.asFirstDigit,"2")==0)
	{
		memcpy(sendData.asCustomerCardNo,&buf1[0],9);
	}	
	Trace("xgd","Customer Card No=%s\r\n",sendData.asCustomerCardNo);
	CustomerRegisterConfirm();
	
}

void CustomerRegisterConfirm(void)
{
	u8 buf[128]={0};
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	s32 key=0;
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "CUSTOMER REGISTER");
	sdkDispAt(30, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","NAME:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asName);
	sdkDispAt(108,60,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","PHONE NUM:");
	sdkDispAt(12,90,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"09%s",sendData.asPhoneNum);
	sdkDispAt(12,115,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","MEMBER CARD NO:");
	sdkDispAt(12,145,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asCustomerCardNo);
	sdkDispAt(12,165,buf);
	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	sdkDispBrushScreen();
	key=sdkKbWaitKey(SDK_KEY_MASK_ESC|SDK_KEY_MASK_ENTER,60000);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				sdkDispClearScreen();	 
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
				sdkDispBrushScreen();
					
				sprintf(message,"%s%s%s%s%s%s%s%s",sendData.asPhoneNum,"0","0",sendData.asFingerTemplate,sendData.asCustomerEnterPin,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN);

				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
			
				memset(sendData.asHashData, 0, 128);
				sdkBcdToAsc(sendData.asHashData, out, 32);
			
				Trace("xgd","tmp = %s\r\n",sendData.asHashData);
			
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf, "%s%s/memberReg/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asPhoneNum,"0","0",sendData.asFingerTemplate,sendData.asCustomerEnterPin,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
			
				Trace("customerregistersenddata","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
			
				ret = SendReceivedData(sendData.asSendBuf,false);
			
			
				if(ret!=0)
				{
					memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
					Trace("xgd","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
					
					if(strcmp(receivedData.ServiceStatus,"Success")==0)
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						sdkDispClearScreen();    
				        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "CUSTOMER REGISTER", SDK_DISP_DEFAULT);		
				        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
						sdkDispBrushScreen();
						
						memset(receivedData.MemberRef,0,sizeof(receivedData.MemberRef));
						js_tok(receivedData.MemberRef,"customerCardRef",0);
						
						memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));
						js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
			
						memset(receivedData.TRX,0,sizeof(receivedData.TRX));
						js_tok(receivedData.TRX,"trx",0);
			
						memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
						js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
						
						memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
						js_tok(receivedData.CardRef,"agentCardRef",0);
						
						memset(receivedData.CustomerCardNum,0,sizeof(receivedData.CustomerCardNum));
						js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
						
						PrintCustomerRegister();
						
						//sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
					}
					else
					{
						sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
						memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
						js_tok(receivedData.ErrorMessage,"message",0);
						
						DisplayErrorMessage("CUSTOMER REGISTER",receivedData.ErrorMessage,false);
					}
				
				}
				else
				{
					DisplayErrorMessage("","",true);
				}
				
			}
			break;
			case  SDK_KEY_ESC:
			{
				AgentMenu();
			}
			break;
			default:
			{
										
			}	
		
	}

	//AgentMenu();
	MainMenu();
		
}




