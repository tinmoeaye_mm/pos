/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : wifi.c
   Version       : Initial Draft
   Author        : Steven
   Created       : 2014/11/12
   Last Modified :
   Description   : This file shows user how to communicate with WIFI.
   History       :
   1.Date        : 2014/11/12
    Author      : Steven
    Modification: Created file

******************************************************************************/
#include "../inc/global.h"

static void TestWifi(void);

static void ConnectWifi(void);

/*******************************************************************************
** Description :  wifi module
** Parameter   :  void
** Return      :
** Author      :  Steven   2014-11-12
** Remark      : libxgdwifi.so and wifiapp.so must be exist in path /mtd0/dll.
*******************************************************************************/
void ModuleWifi(void)
{
    s32 key;

    while(1)
    {
        sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "WIFI", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "1.OPEN WIFI", SDK_DISP_LEFT_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "2.TEST WIFI", SDK_DISP_LEFT_DEFAULT);
        sdkDispBrushScreen();

        key = sdkKbWaitKey(SDK_KEY_MASK_12 | SDK_KEY_MASK_ESC, 5000);
        
        /* Connect WIFI */
        if (key == '1')
        {  
           ConnectWifi();
        }
        /* communicate with WIFI */
        else if (key == '2')
        {
            TestWifi();
        }
        else if (key == SDK_KEY_ESC)
        {
            return;
        }
    }
}

/*******************************************************************************
** Description :  communicate with wifi
** Parameter   :  void
** Return      :
** Author      :  Steven   2014-11-12
** Remark      :
*******************************************************************************/
static void TestWifi(void)
{
    u8 sbuf[128] = "GET /index.html HTTP/1.0\r\n\r\n";
    u8 rbuf[1024] = {0};
    s32 len;
    SDK_COMM_STCOMMPARAM st_comm_param;
    s32 ret;
    u8 dispbuf[32+1] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len1;
    s32 i;

    sdkDispClearScreen();  
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "WIFI", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
    sdkDispBrushScreen();

    /* configurate communication parameters */
    memset(&st_comm_param, 0, sizeof(st_comm_param));
    st_comm_param.eMode = SDK_COMM_WIFI;
    st_comm_param.ucDialTime = 15 * 1000;  //15s
    st_comm_param.stCommInfo.stLanInfo.bIsDhcp = true;
   // strcpy(st_comm_param.stServerInfo.asServerIP, "219.133.170.86");        // can be any valid IP
    strcpy(st_comm_param.stServerInfo.asPort, "80");
    strcpy(st_comm_param.stServerInfo.asServerIP, "93.184.216.34");           // example.com

    if (sdkCommConfig(&st_comm_param) != SDK_OK)
    {
        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CONFIGURATION ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return;
    }

    // check it connect wifi ?
    ret = sdkWIFICheck();
    Trace("xgd", "[TestWifi] sdkWIFICheck ret = %d\r\n", ret);
    if (SDK_WIFI_LINK_OK != ret)
    {    
        if (SDK_WIFI_SUCCESS  != sdkWIFIReConnect())
        {
            DispClearContent();
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CONNECTION FAILED", SDK_DISP_DEFAULT);
            sdkDispBrushScreen();
            sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
            return;
        }
    }
    
    /* connect to server */
    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CONNECTING...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();

    ret = sdkCommCreateLink();
    Trace("xgd", "sdkCommCreateLink ret = %d\r\n", ret);

    if (SDK_OK != ret)
    {
        sdkCommDestoryLink();
        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "LINK ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return;
    }
    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SENDING DATA...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    sdkmSleep(1000);

    /* send data to server */
    if (SDK_OK != sdkCommSendData(sbuf, strlen(sbuf), SDK_COMM_TRANSPARENT))
    {
        sdkCommDestoryLink();
        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SEND DATA ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return;
    }
    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "RECEIVING DATA...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);

    /* Receive data */
    len = sdkCommRecvData(rbuf, sizeof(rbuf), 30000, NULL);

    if (len <= 0)
    {
        sdkCommDestoryLink();

        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "RECEIVE DATA ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return;
    }
    
    // Output debug info
    len = len > 128 ? 128 : len;
    memcpy(tracebuf, rbuf, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);

    // Display response message
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESPONSE:", SDK_DISP_LEFT_DEFAULT);
    for (i = 0; i < 4 && len > 0; i++)
    {
        len1 = len > 20 ? 20 : len;
        len -= len1;
        memcpy(dispbuf, &tracebuf[20*i], len1);
        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
    }
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);

    sdkCommDestoryLink();
}


/*******************************************************************************
** Description :  Connect wifi
** Parameter   :  void
** Return      :
** Author      :  xiaokai  20150720
** Remark      :
*******************************************************************************/
static void ConnectWifi(void)
{
    s32 ret  = SDK_WIFI_FAIL;     
    s32 i = 0; 
    s32 list_num = 0;
    s32 pnNumList = 20;
    SDK_EXT_WIFI_INFO stinfo;
    SDKWIFIINFO wifiList[20];
    char buf[1024] = {0};
    char temp[64] = {0};


    // Open wifi device 
    ret = sdkWIFIOpen();
    if(ret != SDK_WIFI_SUCCESS)
    {
        Trace("xgd", "ConnectWifi sdkWIFIOpen failed ret = %d\r\n", ret);

        sdkDispClearScreen();  
        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "WIFI", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "OPEN FAILED", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return ;
    }

    sdkDispClearScreen();  
    sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Scan Hotspots...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();

    memset(wifiList,0,sizeof(wifiList));
    memset(buf,0,sizeof(buf));
    // It is slow when you search for the first time and it may return error
    // So you can search the second time after 5s.
    for(i = 0; i < 2; i++)
    {
        pnNumList = 20;
        // Get wifi SSID list
        ret = sdkWIFIGetSSIDList(wifiList,&pnNumList);
        if (ret == SDK_WIFI_SUCCESS && pnNumList > 0)
        {
            for(i = 0; i < pnNumList;i++)
            {
                memset(temp,0,sizeof(temp));
                if (wifiList[i].SecMode < SDK_WIFI_PSK_WEP)
                    sprintf(temp, "%d. !%s\r", i+1, wifiList[i].SSID);  // no encryption
                else
                    sprintf(temp, "%d. %s\r", i+1, wifiList[i].SSID);   
                strcat(buf, temp);
            }
            
            break;
            
        }
        else
        {
            sdkmSleep(5000);
        }
        
    }
    
    if (ret != SDK_WIFI_SUCCESS)
    {
        Trace("xgd", "ConnectWifi sdkWIFIGetSSIDList firstly get failed ret = %d\r\n", ret);
        return ;
    }

    // Display SSID list
    list_num = sdkDispListBox("Select a SSID", buf, 0);
    if(list_num < 0 || ret > pnNumList)
    {
        Trace("xgd","[ConnectWifi]Select a ssid failed \r\n");
        return ;
    }

    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "Wifi Setting", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    
    memset(&stinfo,0,sizeof(SDK_EXT_WIFI_INFO));
    if(wifiList[list_num].SecMode > SDK_WIFI_PSK_OPEN)
    {        
        memset(temp,0,sizeof(temp));
        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Input PWD", SDK_DISP_LDISP);
        sdkDispBrushScreen();
        sdkKbGetScanf(0, temp, 0, 63,  SDK_MMI_NUMBER | SDK_MMI_LETTER | SDK_MMI_SYMBOL, SDK_DISP_LINE4);
        if (strlen(temp) != 0)
        {
            memcpy(stinfo.pwd,&temp[1],temp[0]);
        }
    }

    memcpy(stinfo.ssid,wifiList[list_num].SSID,strlen(wifiList[list_num].SSID));
    
    ret = sdkDispMsgBox("WIFI Setting ","1.DHCP \r2.Manual Setting ", 0, SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC);
    if( ret == SDK_KEY_2)
    {
        stinfo.iDhcp = false;
        DispClearContent();
        
        memset(temp,0,sizeof(temp));
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Input IP:", SDK_DISP_LDISP);
        sdkDispBrushScreen();
        ret = sdkKbGetScanf(0, temp, 0, 15,  SDK_MMI_NUMBER | SDK_MMI_SYMBOL, SDK_DISP_LINE4);
        Trace("xgd","ConnectWifi stinfo.cLocalIp = %s\r\n",stinfo.cLocalIp);
        if(ret != SDK_KEY_ENTER)
        {
            return ;
        }
        
        memcpy(stinfo.cLocalIp,&temp[1],temp[0]);
        

        DispClearContent();
        memset(temp,0,sizeof(temp));
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Input GateWay:", SDK_DISP_LDISP);
        sdkDispBrushScreen();
        ret = sdkKbGetScanf(0, stinfo.cGateWay, 0, 15,  SDK_MMI_NUMBER | SDK_MMI_SYMBOL, SDK_DISP_LINE4);
        Trace("xgd","ConnectWifi stinfo.cGateWay = %s\r\n",stinfo.cGateWay);
        if(ret != SDK_KEY_ENTER)
        {
            return ;
        }
        
        memcpy(stinfo.cMask,&temp[1],temp[0]);
        

        DispClearContent();
        memset(temp,0,sizeof(temp));
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Input Mask:", SDK_DISP_LDISP);
        sdkDispBrushScreen();
        ret = sdkKbGetScanf(0, temp, 0, 15,  SDK_MMI_NUMBER | SDK_MMI_SYMBOL, SDK_DISP_LINE4);
        Trace("xgd","ConnectWifi stinfo.cMask = %s\r\n",stinfo.cMask);
        if(ret != SDK_KEY_ENTER)
        {
            return ;
        }
        
        memcpy(stinfo.cMask,&temp[1],temp[0]);
        

        DispClearContent();
        memset(temp,0,sizeof(temp));
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Input DNS1:", SDK_DISP_LDISP);
        sdkDispBrushScreen();
        ret = sdkKbGetScanf(0, temp, 0, 15,  SDK_MMI_NUMBER | SDK_MMI_SYMBOL, SDK_DISP_LINE4);
        Trace("xgd","ConnectWifi stinfo.cDns1 = %s\r\n",stinfo.cDns1);
        if(ret != SDK_KEY_ENTER)
        {
            return ;
        }
        
        memcpy(stinfo.cDns1,&temp[1],temp[0]);
            

        DispClearContent();
        memset(temp,0,sizeof(temp));
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Input DNS2:", SDK_DISP_LDISP);
        sdkDispBrushScreen();
        ret = sdkKbGetScanf(0, temp, 0, 15,  SDK_MMI_NUMBER | SDK_MMI_SYMBOL, SDK_DISP_LINE4);
        Trace("xgd","ConnectWifi stinfo.cDns2 = %s\r\n",stinfo.cDns2);
        if(ret != SDK_KEY_ENTER)
        {
            return ;
        }
        memcpy(stinfo.cDns2,&temp[1],temp[0]);        
  
    }
    else if (ret == SDK_KEY_1)
    {
        stinfo.iDhcp = true;
    }
    else
    {
        sdkWIFIClose();
        return ;
    }

    stinfo.iAuth = wifiList[list_num].SecMode;

    switch(stinfo.iAuth)
    {
        case SDK_WIFI_PSK_OPEN:			       /**no passwd */
            stinfo.iCipher = 0;
            break;
        case SDK_WIFI_PSK_WEP:                 /**Open key  WEP*//**Share secret WEP*/         
            stinfo.iCipher = SDK_WIFI_CIPHER_WEP|SDK_WIFI_CIPHER_WEP128;
            break;
        case SDK_WIFI_PSK_WPAPSK:			   /**WPA-PSK*/
        case SDK_WIFI_PSK_WPA2PSK:			   /**WPA2-PSK*/
            stinfo.iCipher = SDK_WIFI_CIPHER_DEFAULT;
            break;
        case SDK_WIFI_PSK_WPACCKM:              /**WPA*/
            stinfo.iCipher = SDK_WIFI_CIPHER_DEFAULT;
            break;
        default :
            
            break;

    
    }

    Trace("xgd", "[ConnectWifi] stinfo.ssid=%s,stinfo.pwd = %s, iAuth=%d, iCipher=%d\n", stinfo.ssid,stinfo.pwd, stinfo.iAuth, stinfo.iCipher);
    
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Connecting...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();

    // Making connection
    memset(temp,0,sizeof(temp));
    ret = sdkWIFIConnect(&stinfo);
    if(ret != SDK_WIFI_SUCCESS)
    {
        Trace("xgd","ConnectWifi sdkWIFIConnect ret = %d\r\n",ret);
        sprintf(temp, "Connect WIFI Failed");
    }
    else
    {
        sprintf(temp, "Connect WIFI Successed");
    }

    sdkDispMsgBox("WIFI Setting", temp, 0, SDK_KEY_MASK_ALL); 

    return ;  
}

