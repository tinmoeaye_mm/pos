/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : https.c
  Version       : Initial Draft
  Author        : Cody
  Created       : 2015/9/29
  Last Modified :
  Description   : Do message exchange through HTTP.
  History       :
  1.Date        : 2015/9/29
    Author      : Cody
    Modification: Created file

******************************************************************************/


#include "../inc/global.h"


/*******************************************************************************
** Description :  A demo on how to exchange message through HTTPS
** Parameter   :  void
** Return      :  
** Author      :  Cody   2015-10-08 
** Remark      :  CA file ca_cert.crt.cer should be downloaded to /mtd0/res/
*******************************************************************************/
void ModuleHttps(void)
{
    s32 key;
    s32 ret = SDK_ERR;
    u8 recbuf[50 *1024] = {0};
    u8 dispbuf[32+1] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len, len1;
    s32 i;

    SDK_EXT_CURL_DATA curl_data = {
          .bMethod = true,                                    // GET method
          .psURL = "https://www.example.com",                 // URL
          .psCaFile = "/mtd0/res/ca_cert.crt.cer",            // Path to CA certificate
          .nPort = 443,                                       // HTTPS default port 
          .psSend = NULL,                                     // No data to be sent
          .nSendLen = 0,
          .psRecv = recbuf,                                   // Receive buffer, make sure the size of this buffer exceeds that of response message
          .nRecvLen = sizeof(recbuf),                         // Size of receive buffer
          .nTimeout = 30*1000,                                // Time out          
    };

    
    // Select network interface
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "HTTPS", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
    sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SELECT A COMM:", SDK_DISP_LEFT_DEFAULT);
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "1.ETHERNET", SDK_DISP_LEFT_DEFAULT);
    sdkDispFillRowRam(SDK_DISP_LINE4, 0, "2.GPRS/WCDMA", SDK_DISP_LEFT_DEFAULT);
    sdkDispBrushScreen();
    key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 30*1000);
    if (key == SDK_KEY_1)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CREATING ETH0...", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        
        if (OpenEth0(true) != true)
        {
            DispClearContent();    
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CREATE ETH0 FAILED!", SDK_DISP_DEFAULT);
            sdkDispBrushScreen();
            sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
            return;
        }
    }
    else if (key == SDK_KEY_2)
    {
        SDK_COMM_STCOMMPARAM st_comm_param;
        s32 res;

        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CREATING PPP...", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        
        memset(&st_comm_param, 0, sizeof(st_comm_param));
        st_comm_param.eMode = SDK_COMM_GPRS;
        strcpy(st_comm_param.stCommInfo.stPppWireLessInfo.asGprsApn, "CMNET");
        strcpy(st_comm_param.stServerInfo.asServerIP, "219.133.170.86");        //can be any valid IP
        strcpy(st_comm_param.stServerInfo.asPort, "8616");
        sdkCommConfig(&st_comm_param);
        res = sdkCommCreatePPP(20*1000);
        Trace("xgd", "sdkCommCreatePPP() = %d\r\n", res);

        if (res != SDK_OK)
        {
            DispClearContent();    
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CREATE PPP FAILED!", SDK_DISP_DEFAULT);
            sdkDispBrushScreen();
            sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
            return;
        }
    }
    else
    {
        return;
    }
    
    
    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "EXCHANGING MSG...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen(); 
    
    memset(recbuf, 0, sizeof(recbuf));   
    Trace("xgd","sdkExtCurl before curl_data.nRecvLen = %d\r\n", curl_data.nRecvLen);
    // Start exchanging data through HTTPS protocol
    ret =  sdkExtCurl(&curl_data);
    Trace("xgd","sdkExtCurl ret = %d\r\n",ret);
    if (ret != SDK_OK)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
        return;
    }

    // Output debug info
    Trace("xgd", ">>>> Total length = %d\r\n", curl_data.nRecvLen);
    len = curl_data.nRecvLen > 128 ? 128 : curl_data.nRecvLen;
    memcpy(tracebuf, curl_data.psRecv, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);

    // Display response message
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESPONSE:", SDK_DISP_LEFT_DEFAULT);
    for (i = 0; i < 4 && len > 0; i++)
    {
        len1 = len > 20 ? 20 : len;
        len -= len1;
        memcpy(dispbuf, &tracebuf[20*i], len1);
        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
    }
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);
}


