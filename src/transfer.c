/******************************************************************************

  Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

 ******************************************************************************
  File Name     : transfer.h
  Version       : Initial Draft
  Author        : Thu Kha Aung
  Created       : 2014/11/4
  Last Modified :
  Description   : This file contains prototypes for Transfer Money
  Function List :
  History       :
  1.Date        : 2016/11/3
    Author      : Thu Kha Aung,Tin Moe Aye
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"

const u8 asPathTransfer[] = "/mtd0/res/dotransferamount.bmp";
const u8 asPathPin1[] = "/mtd0/res/enterpincode.bmp";

void SendNT2NT(void);	
void TransferConfirm(void);
void TransferMemToMemLogin(void);
void TransferMainMenu(void);
void TransferLoginCheck(int i);
void TransferMemToMemReceiver(void);
void TransferMemToMemConfirm(void);
void ModuleTransferCustomerReadCard(void);
void TransferModule(void);
void TransferPhone(void);
void TransferMemtoMem(void);
void CashOutMemToMem(void);
void CashOutMemToMemConfirm(void);
void ModuleTransferCustomerReadCard(void);
void MemToMemCashIn(void);
void MemToMemCashInConfirm(void);
void MemberCheckBalance(void);
void NewMainMenu(void);
void TransferMainMenu(void)
{
	s32 res;
	u8 buf[16] = {0};
	Step1 :
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320,"/mtd0/res/TransfMainMeun.bmp");
	sdkDispBrushScreen();
	
	secondtransactionstart=false;     // for pageagentcheckbalance
	fingerprint.Error=NEWMAINMENU;   
    transfer.Transfer_MainMenu = Transfer_New_Menu;     //for shortcut menu & old menu
	memset(buf,0,sizeof(buf));
	res= GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,4,8, NULL, " KS");
	
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
					printtype.CashPrint=CASHTRANSFER;
					Trace("xgd", "buf= %s\r\n", buf);
					memset(sendData.asTransferAmount,0,sizeof(sendData.asTransferAmount));
					memcpy(sendData.asTransferAmount,&buf[1],buf[0]);
					Trace("xgd", "sendData.asTransferAmount= %s\r\n",sendData.asTransferAmount);
					ModuleTransferCustomerReadCard();
				
			}
			break;
		case 15:
			{
				if(strlen(buf)==0)
				{
					Trace("xgd","15");
					Trace("xgd", "buf= %s\r\n", buf);
					sdkDispClearScreen();
					sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Please Enter Amount.", SDK_DISP_LEFT_DEFAULT);
					sdkDispBrushScreen();
					sdkmSleep(1500);
					goto Step1;
				}
				else if(strlen(buf)>=4)
				{
					Trace("xgd","15");
					Trace("xgd", "buf= %s\r\n", buf);
					memset(sendData.asTransferAmount,0,sizeof(sendData.asTransferAmount));
					memcpy(sendData.asTransferAmount,&buf[1],buf[0]);
					Trace("xgd", "sendData.asTransferAmount= %s\r\n",sendData.asTransferAmount);
					TransferModule();
				}
				
			}
			break;
		case SDK_KEY_F1:
			{
				if(strlen(buf)==0)
				{
					Trace("xgd","F1");
					Trace("xgd", "buf= %s\r\n", buf);
					sdkDispClearScreen();
					sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Please Enter Amount.", SDK_DISP_LEFT_DEFAULT);
					sdkDispBrushScreen();
					sdkmSleep(1500);
					goto Step1;
				}
				else if(strlen(buf)>=4)
				{
					Trace("transfer","F1");
					Trace("transfer", "buf= %s\r\n", buf);
					memset(sendData.asTransferAmount,0,sizeof(sendData.asTransferAmount));
					memcpy(sendData.asTransferAmount,&buf[1],buf[0]);
					Trace("transfer", "sendData.asTransferAmount= %s\r\n",sendData.asTransferAmount);
					TransferModule();
				}
				
			}
			break;
	
			
		case SDK_KEY_ESC:
			{
				MainMenu();	
			}
			break;
		default:
			{
			
			}				
	}

	MainMenu();
	
	
}
void ModuleTransferCustomerReadCard(void)
{
	
	step1:
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/membercard.bmp");
	sdkDispBrushScreen();
	sdkmSleep(1500);
	//Trace("xgd", "Start ModuleReadCard= \r\n");
	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	//open RF device
	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	
	  	
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asCustomerCardNo,0,sizeof(sendData.asCustomerCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("xgd", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	if(strcmp(sendData.asFirstDigit,"2")==0)
	{
		memcpy(sendData.asCustomerCardNo,&buf1[0],9);
		
		Trace("xgd","Customer Card No=%s\r\n",sendData.asCustomerCardNo);
		if(printtype.CashPrint==CASHIN)
		{
			MemToMemCashInConfirm();
		}
		else if(printtype.CashPrint==CASHTRANSFER||printtype.CashPrint==CASHOUT)
		{
				if(cashout.CashOutMAINMenu==CashOut_New_Menu)
				{
						fingerprint.Error=NEWMAINMENU;
						TransferMemToMemLogin();
					}
				else if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					fingerprint.Error=MEMCASHOUTCASHIN;
					TransferMemToMemLogin();
				}	
		
		}
		
	}
	else 
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "MEMBER CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		goto step1;
	}	
	
}
void TransferMemToMemLogin(void)
{
	s32 res;
	if(printtype.CashPrint==CASHOUT)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/DomesticCashOutLogin.bmp");
		sdkDispBrushScreen();
	}
	else if(printtype.CashPrint==CASHTRANSFER)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/domestictransfer4.bmp");
		sdkDispBrushScreen();
	
	}
	
    res = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, KbWaitTime);

	if (res == SDK_KEY_1)
    {
		TransferLoginCheck(1);
	 }
    else if (res == SDK_KEY_2)
    {
    	TransferLoginCheck(2);
    }
    else if (res == SDK_KEY_ESC)
    {
    	if(printtype.CashPrint==CASHTRANSFER)
        
			{
				TransferMainMenu();
		 	} 
		 else if(printtype.CashPrint==CASHOUT)
		 	{
			 
		 			if(cashout.CashOutMAINMenu==CashOut_New_Menu)
				{
					NewMainMenu();
				}
				else if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
						CashOutModule();
				}	
			 }	
    }
         
	if(printtype.CashPrint==CASHTRANSFER)
        
	{
		TransferMainMenu();
	} 
	else if(printtype.CashPrint==CASHOUT)
	{
			 
		if(cashout.CashOutMAINMenu==CashOut_New_Menu)
		{
			NewMainMenu();
		}
		else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
		{
				CashOutModule();
		}	
	 }	
         
}
void TransferLoginCheck(int i)
{
	u32 Temp_timer;
	Temp_timer = sdkTimerGetId();
 	u8 buf[64] = {0};
	s32 key = 0;
	s32 rslt;
	if(i==1)
	{	
		memset(sendData.asAgentEnterPin, 0, sizeof(sendData.asAgentEnterPin));
		sprintf(sendData.asAgentEnterPin,"0");
		while(1)
		{	
			if (sdkTimerIsEnd(Temp_timer, 120 * 1000))     //Timer-out 60s
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				if(cashout.CashOutMAINMenu==CashOut_New_Menu)
				{
					NewMainMenu();
				}
				else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					MainMenu();
				}	
				
			}
			
			rslt = MemberFPCheck();
			if (rslt == SDK_OK)  
			{
				rslt = VerifyFPC(receivedData.asFingerTemplate,3);

				if (rslt == SDK_OK)                                                               
				{
					if(printtype.CashPrint==CASHTRANSFER)
					{
						TransferMemToMemReceiver();
						
					}
					else if(printtype.CashPrint==CASHOUT)
					{
						CashOutMemToMem();
						
					}
				}
				
				else
							
				{
					if(printtype.CashPrint==CASHTRANSFER)
        
					{
						TransferMainMenu();
		 			} 
		 		else if(printtype.CashPrint==CASHOUT)
		 			{
			 			if(cashout.CashOutMAINMenu==CashOut_New_Menu)
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
		 			
			 		}	
				}
			}
			else
			{
				if(printtype.CashPrint==CASHTRANSFER)
        			{
						TransferMainMenu();
		 			} 
		 		else if(printtype.CashPrint==CASHOUT)
		 			{
			 			if(cashout.CashOutMAINMenu==CashOut_New_Menu)
						{
							NewMainMenu();
						}
						else if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
			 		}	
			}

			
			
		}
	}
	else
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, asPathPin1);
		sdkDispBrushScreen();
		key = sdkKbGetScanf(60000, buf, 6, 6, SDK_MMI_PWD ,SDK_DISP_LINE4);

		if (key == SDK_KEY_ENTER)
	  	{
	  		
				memset(sendData.asCustomerEnterPin,0,sizeof(sendData.asCustomerEnterPin));
				memcpy(sendData.asCustomerEnterPin,&buf[1],6);
				Trace("xgd","sendData.asCustomerEnterPin= %s\r\n",sendData.asCustomerEnterPin);
				if(printtype.CashPrint==CASHTRANSFER)
				{
					TransferMemToMemReceiver();
				}
				else if(printtype.CashPrint==CASHOUT)
				{
				
					CashOutMemToMemConfirm();
				}
				
		
	  }
		else if(key==SDK_KEY_ESC)
		{
			if(printtype.CashPrint==CASHTRANSFER)
        
			{
				TransferMainMenu();
		 	} 
		 else if(printtype.CashPrint==CASHOUT)
		 	{
			 
		 		if(cashout.CashOutMAINMenu==CashOut_New_Menu)
				{
					NewMainMenu();
				}
				else if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					CashOutModule();
				}	
			 }	
		}
		
	}

	MainMenu();
	
}

void TransferMemToMemReceiver()
{
	
	 u8 receivercardno[32]={0};

	u8 ret;

	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/TransferReceivecardno.bmp");
	sdkDispBrushScreen();
	
	ret= sdkKbGetScanf(30000, receivercardno, 9,9 ,SDK_MMI_NUMBER, SDK_DISP_LINE4);
	switch(ret)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd", "receivercardno= %s\r\n", receivercardno);
				memset(sendData.asReceiverCardNo,0,sizeof(sendData.asReceiverCardNo));
				memcpy(sendData.asReceiverCardNo,& receivercardno[1], receivercardno[0]);
				Trace("xgd", "sendData.asReceiverCardNo= %s\r\n", sendData.asReceiverCardNo);
				TransferMemToMemConfirm();
			}
			break;
		case SDK_KEY_ESC:
			{
				TransferMainMenu();
			}
			break;
		default:
			{
				
			}		
		
	}
	TransferMainMenu();
}
void TransferMemToMemConfirm(void)
{
	s32 key;
	u8 buf[64] = {0};
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/domestictransfer6.bmp");
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s",sendData.asCustomerCardNo);
	sdkDispAt(12, 81, buf);    
							
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", sendData.asReceiverCardNo);
	sdkDispAt(12, 155, buf);
						    
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%sKs",sendData.asTransferAmount);
	sdkDispAt(12, 235, buf);
	
	
	sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s","PRESS [ENTER] TO CONTINUE");
	sdkDispAt(12, 285, buf);
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	
					    
	sdkDispBrushScreen();
	sdkmSleep(1500);
	key=sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,30000);
	{
		switch(key)
		{
			case SDK_KEY_ENTER:
				{
					TransferMemtoMem();	
				}
				
				break;
			case SDK_KEY_ESC:
				{
					TransferMainMenu();	
				}	
				break;
				default:
				{
					
				}	
		}
	}
	TransferMainMenu();
		
}
void TransferMemtoMem(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sprintf(message,"%s%s%s%s%s%d%s",sendData.asCustomerCardNo,sendData.asReceiverCardNo,sendData.asAgentCardNo,sendData.asTransferAmount,sendData.asTerminalSN,0,sendData.asCustomerEnterPin);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/t2t/%s/%s/%s/%s/%s/%s/%d/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asReceiverCardNo,sendData.asAgentCardNo,sendData.asTransferAmount,sendData.asTerminalSN,0,sendData.asCustomerEnterPin,sendData.asHashData);
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0)
	{
	
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);

		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			
			transaction=true;              //  for pageagentcheckbalance
			secondtransactionstart=true;   //  for pageagentcheckbalance
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

		
			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			js_tok(receivedData.commission,"charge",0);
			
			js_tok(receivedData.CustomerCardNum,"toCusCard",0);
			
			js_tok(receivedData.TransferAmount,"transferAmount",0);
			
			js_tok(receivedData.TransferFee,"transferFee",0);
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);
			
			js_tok(receivedData.MemberRef,"customerCardRef",0);
			
			js_tok(receivedData.CardNo,"fromCusCard",0);
			
			js_tok(receivedData.BalanceAmount,"cusBalanceAmount",0);
			
			PrintMemToMemTransfer();
		
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;              //  for pageagentcheckbalance
			js_tok(receivedData.ErrorMessage,"errorMessage",0);
			js_tok(receivedData.ErrorCode,"code",0);
			js_tok(receivedData.ErrorCode,"code",0);
		if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}
			else
			{
				DisplayErrorMessage("Member Transfer",receivedData.ErrorMessage,false);
			}	
			
			
		}
		
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	
	MainMenu();
}

void TransferModule(void)
{
    u8 buf[16] = {0};
	s32 res;
	 
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/dotransfersenderph.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
		if (res == SDK_KEY_ENTER)
		{
			Trace("xgd", "buf= %s\r\n", buf);
			memset(sendData.asSenderPhone,0,sizeof(sendData.asSenderPhone));
			memcpy(sendData.asSenderPhone,&buf[1],buf[0]);
			Trace("xgd", "sendData.asSenderPhone= %s\r\n", sendData.asSenderPhone);
			TransferPhone();
		}
		else if(res==SDK_KEY_ESC)
		{
			TransferMainMenu();
		}
	TransferMainMenu();
}
void TransferPhone(void)
{
	 u8 buf[16] = {0};
	 s32 res;
	 
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/dotransferreciverph.bmp");
	sdkDispBrushScreen();
			
			memset(buf,0,sizeof(buf));
			res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
			if (res == SDK_KEY_ENTER)
			{
				Trace("xgd", "buf= %s\r\n", buf);
				memset(sendData.asReceiverPhone,0,sizeof(sendData.asReceiverPhone));
				memcpy(sendData.asReceiverPhone,&buf[1],buf[0]);
				Trace("xgd", "sendData.asReceiverPhone= %s\r\n", sendData.asReceiverPhone);
				TransferConfirm();

			}
			else if(res == SDK_KEY_ESC)
			{
				TransferMainMenu();
			}
			TransferMainMenu();			
				
}
void TransferConfirm(void)
{
	 u8 buf[16] = {0};
	 s32 key=0;
	 
	sdkDispClearScreen();
    sdkDispShowBmp(0, 5, 240, 150, "/mtd0/res/HLtransfer.bmp");
	sdkDispShowBmp(0, 45, 240, 150, "/mtd0/res/senderPhoneNo.bmp");
	
	memset(buf,0,sizeof(buf));
    sprintf(buf, "09%s", sendData.asSenderPhone);
    sdkDispAt(80, 75, buf);
    
    sdkDispShowBmp(0, 120, 240, 150, "/mtd0/res/receiverPhoneNo.bmp");
    
    memset(buf,0,sizeof(buf));
   	sprintf(buf, "09%s", sendData.asReceiverPhone);
   	sdkDispAt(80, 150, buf);
   	
   	sdkDispShowBmp(0, 185, 240, 100, "/mtd0/res/TransferAmount.bmp");
   	memset(buf,0,sizeof(buf));
   	sprintf(buf, "%sKs", sendData.asTransferAmount);
   	sdkDispAt(80, 210, buf);
   	
   	sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s","PRESS [ENTER] TO CONTINUE");
	sdkDispAt(12, 285, buf);
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
   	
   	sdkDispBrushScreen();
   	key=sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,30000);
   	switch(key)
   	{	
   		case SDK_KEY_ENTER:
   			{
   				
   			SendNT2NT();
   			
   			
		   }
		   break;
		case SDK_KEY_ESC:
			{
				TransferModule();
			}
			break;
			default:
			{
				TransferModule();
			}
			
   		
	 }
	return TransferModule();
}

void SendNT2NT(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTransferAmount,sendData.asReceiverPhone,sendData.asSenderPhone,sendData.asTerminalSN);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);

	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/nt2nt/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTransferAmount,sendData.asReceiverPhone,sendData.asSenderPhone,sendData.asTerminalSN,sendData.asHashData);
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0) 
	{
			
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
				
				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					transaction=true;              //  for pageagentcheckbalance
					secondtransactionstart=true;   //  for pageagentcheckbalance
			
					js_tok(receivedData.TransferAmount,"transferAmount",0);
					
					
					js_tok(receivedData.TransferFee,"transferFee",0);
					
					
					js_tok(receivedData.BalanceAmount,"cusBalanceAmount",0);
					
				
					js_tok(receivedData.Passcode,"passcode",0);
					
				
					js_tok(receivedData.ExpDate,"passExp",0);
					
					
					js_tok(receivedData.SenderPhone,"fromPhNo",0);
					Trace("LocalCashTransfer","receivedData.SenderPhone=%s\r\n",receivedData.SenderPhone);
				
					js_tok(receivedData.ReceiverPhone,"toPhNo",0);
					Trace("LocalCashTransfer","receivedData.SenderPhone=%s\r\n",receivedData.ReceiverPhone);
					
				
					js_tok(receivedData.Amount,"totalAmount",0);
					
					
					js_tok(receivedData.CardRef,"agentCardRef",0);

					
					js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);


					js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
					
				
					js_tok(receivedData.TRX,"trx",0);
					
					
					js_tok(receivedData.CardNo,"toCusCard",0);
			
					PrintCashTransfer();
				}
			
			else
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
				sdkDispBrushScreen();
				sdkmSleep(1500);
				
				transaction=false;              //  for pageagentcheckbalance
				js_tok(receivedData.ErrorMessage,"message",0);
				js_tok(receivedData.ErrorCode,"code",0);
				if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
				}	
				else
				{
					DisplayErrorMessage("TRANSFER",receivedData.ErrorMessage,false);
				}
				
			}
	}

	else
	{

		DisplayErrorMessage("","",true);
	}
	MainMenu();
	
}


