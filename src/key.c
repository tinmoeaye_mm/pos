/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : key.c
   Version       : Initial Draft
   Author        : Cody
   Created       : 2014/6/26
   Last Modified :
   Description   : Key module related functions are defined here.
   History       :
   1.Date        : 2014/6/26
    Author      : Cody
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"


static void DispKeyName(u8 *pName);
static void DispAnyKeyName(s32 nKey);

#define DISP_KEY_NAME(key)  DispKeyName(# key)


/*******************************************************************************
** Description :  Get key value, diplay the macro and its value onscreen
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-08
** Remark      :
*******************************************************************************/
void ModuleKey(void)
{
    s32 key;
    u8 buf[32] = {0};

    // Clear screen
    sdkDispClearScreen();
    // Write to the buffer
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "GET KEY VALUE", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
    sdkDispFillRowRam(SDK_DISP_LINE2, 0, "Key: Macro/Value", SDK_DISP_DEFAULT);
    sdkDispFillRowRam(SDK_DISP_LINE5, 0, "[ESC] TO QUIT", SDK_DISP_DEFAULT);
    // Flush the buffer
    sdkDispBrushScreen();

    while (1)
    {
        // Get a key, non-blocking function
        key = sdkKbGetKey();

        if (key <= 0)                // If no key is pressed, continue scanning
        {
            continue;
        }
        else if (key == SDK_KEY_ESC) // Quit if CANCEL key is pressed
        {
            break;
        }
        // Play a beep when a key is pressed
        sdkSysBeep(SDK_SYS_BEEP_OK);

        // Display key macro name onscreen
        DispAnyKeyName(key);
		
        // Display key value
        sdkDispClearRowRam(SDK_DISP_LINE4);
        sprintf(buf, "%d", key);
        sdkDispFillRowRam(SDK_DISP_LINE4, 0, buf, SDK_DISP_NOFDISP | SDK_DISP_CDISP | SDK_DISP_INCOL);
        sdkDispBrushScreen();
    }
}

/*******************************************************************************
** Description :  Display key name onscreen.
** Parameter   :  u8 *pName
** Return      :
** Author      :  Cody   2014-07-01
** Remark      :
*******************************************************************************/
static void DispKeyName(u8 *pName)
{
    sdkDispClearRowRam(SDK_DISP_LINE3);
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, pName, SDK_DISP_NOFDISP | SDK_DISP_CDISP | SDK_DISP_INCOL);
    sdkDispBrushScreen();
}

/*******************************************************************************
** Description :  Display the key name or value onscreen.
** Parameter   :  s32 nKey
** Return      :
** Author      :  Cody   2014-07-01
** Remark      :
*******************************************************************************/
static void DispAnyKeyName(s32 nKey)
{
    u8 buf[64] = {0};

    switch (nKey)
    {
         case SDK_KEY_1:
            {
                DISP_KEY_NAME(SDK_KEY_1);
            }
            break;

         case SDK_KEY_2:
            {
                DISP_KEY_NAME(SDK_KEY_2);
            }
            break;

         case SDK_KEY_3:
            {
                DISP_KEY_NAME(SDK_KEY_3);
            }
            break;

         case SDK_KEY_4:
            {
                DISP_KEY_NAME(SDK_KEY_4);
            }
            break;

         case SDK_KEY_5:
            {
                DISP_KEY_NAME(SDK_KEY_5);
            }
            break;

         case SDK_KEY_6:
            {
                DISP_KEY_NAME(SDK_KEY_6);
            }
            break;

         case SDK_KEY_7:
            {
                DISP_KEY_NAME(SDK_KEY_7);
            }
            break;

         case SDK_KEY_8:
            {
                DISP_KEY_NAME(SDK_KEY_8);
            }
            break;

         case SDK_KEY_9:
            {
                DISP_KEY_NAME(SDK_KEY_9);
            }
            break;

         case SDK_KEY_0:
            {
                DISP_KEY_NAME(SDK_KEY_0);
            }
            break;

         case SDK_KEY_UP:
            {
                DISP_KEY_NAME(SDK_KEY_UP);
            }
            break;

         case SDK_KEY_DOWN:
            {
                DISP_KEY_NAME(SDK_KEY_DOWN);
            }
            break;

         case SDK_KEY_BACKSPACE:
            {
                DISP_KEY_NAME(SDK_KEY_BACKSPACE);
            }
            break;

         case SDK_KEY_F1:
            {
                DISP_KEY_NAME(SDK_KEY_F1);
            }
            break;

         case SDK_KEY_F2:
            {
                DISP_KEY_NAME(SDK_KEY_F2);
            }
            break;

         case SDK_KEY_F3:
            {
                DISP_KEY_NAME(SDK_KEY_F3);
            }
            break;

         case SDK_KEY_F4:
            {
                DISP_KEY_NAME(SDK_KEY_F4);
            }
            break;

         case SDK_KEY_ENTER:
            {
                DISP_KEY_NAME(SDK_KEY_ENTER);
            }
            break;

         case SDK_KEY_FUNCTION:
            {
                DISP_KEY_NAME(SDK_KEY_FUNCTION);
            }
            break;

         default:
            {
                sprintf(buf, "KEY VALUE: [%d]", nKey);
                DispKeyName(buf);
            }
            break;
    }
}


int Getkey()
{
	int key , i ,j,numlen = 0;
	char buf1[32];
	char buf[64] = {0};
	while(1)
	{
		if ((key=sdkKbGetKey())>0){
			j++;
			if (key<SDK_KEY_0 || key > SDK_KEY_9){
				switch(key){
					case SDK_KEY_BACKSPACE:
					case SDK_KEY_CLEAR:
					{
						sdkSysBeep(SDK_SYS_BEEP_OK);
						for ( i = 0 ; i <13;i ++)
						{
							if (buf1[i]>0)
							{
								mobileNo[i] = '\0';
								buf1[i] = '\0';
								numlen = 0;
							}
						}
						sdkDispClearRow(SDK_DISP_LINE3);
						
					}continue;
				case SDK_KEY_ENTER:
				case SDK_KEY_ESC:
				case 15:
				case SDK_KEY_F2:
				case SDK_KEY_F3:
				     if (numlen == 1)
					 {
						sprintf(mobileNo,"%d",buf1[0]);
					 }
					 else if (numlen==2)
					 {
						sprintf(mobileNo,"%d%d",buf1[0],buf1[1]);
					 }
					 else if (numlen==3)
					 {
						 
							 sprintf(mobileNo,"%d%d%d",buf1[0],buf1[1],buf1[2]);
						 
					 }
					 else if (numlen==4)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3]);
						 
					 }
					 else if (numlen==5)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4]);
						 
					 }
					 else if (numlen==6)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4],buf1[5]);
						 
					 }
					 else if (numlen==7)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4],buf1[5],buf1[6]);
						 
					 }
					 else if (numlen==8)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4],buf1[5],buf1[6],buf1[7]);
						 
					 }
					  else if (numlen==9)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4],buf1[5],buf1[6],buf1[7],buf1[8]);
						
					 }
					    else if (numlen==10)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4],buf1[5],buf1[6],buf1[7],buf1[8],buf1[9]);
						
					 }
					 else if (numlen==11)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d%d%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4],buf1[5],buf1[6],buf1[7],buf1[8],buf1[9],buf1[10]);
						 
					 }
					 else if (numlen==12)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d%d%d%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4],buf1[5],buf1[6],buf1[7],buf1[8],buf1[9],buf1[10],buf1[11]);
						
					 }
					  else if (numlen==13)
					 {
						 
							 sprintf(mobileNo,"%d%d%d%d%d%d%d%d%d%d%d%d%d",buf1[0],buf1[1],buf1[2],buf1[3],buf1[4],buf1[5],buf1[6],buf1[7],buf1[8],buf1[9],buf1[10],buf1[11],buf1[12]);
						 
					 }
					  
					 return key;
					 default:
					    continue;
			}
		}
		else
		{
			switch (key)
			{
				case SDK_KEY_0:
				 if (numlen<13)
				 {
					 buf1[numlen++] = 0;
				 }
				 break;
				 case SDK_KEY_1:
				 if (numlen<13)
				 {
					buf1[numlen++] = 1;
				 }
				 break;
				  case SDK_KEY_2:
				 if (numlen<13)
				 {
					buf1[numlen++] = 2;
				 }
				 break;
				  case SDK_KEY_3:
				 if (numlen<13)
				 {
					buf1[numlen++] = 3;
				 }
				 break;
				  case SDK_KEY_4:
				 if (numlen<13)
				 {
					buf1[numlen++] = 4;
				 }
				 break;
				  case SDK_KEY_5:
				 if (numlen<13)
				 {
					buf1[numlen++] = 5;
				 }
				 break;
				  case SDK_KEY_6:
				 if (numlen<13)
				 {
					buf1[numlen++] = 6;
				 }
				 break;
				  case SDK_KEY_7:
				 if (numlen<13)
				 {
					buf1[numlen++] = 7;
				 }
				 break;
				  case SDK_KEY_8:
				 if (numlen<13)
				 {
					buf1[numlen++] = 8;
				 }
				 break;
				  case SDK_KEY_9:
				 if (numlen<13)
				 {
					buf1[numlen++] = 9;
				 }
				 break;
			}
			 sdkSysBeep(SDK_SYS_BEEP_OK);
			
			sprintf(buf,"%s","0");
			sdkDispAt(36+(12*0),133,buf);
			sprintf(buf,"%s","9");
			sdkDispAt(36+(12*1),133,buf);
			for (i=0,j=100;i<numlen;i++,j+=20)
			{
				sprintf(buf,"%d",buf1[i]);
				sdkDispAt(36+(12*(i+2)),133,buf);
				sdkDispBrushScreen();
			}
			sdkDispBrushScreen();
		}
		
	}
}
}


s32 GetPhoneNumber(s32 nTimeout, u8 *pBuf, s32 nLine, s32 nMinLen, s32 nMaxLen, const u8 *pPrompt, const u8 *pUnit, const u8 *pOperator) 
{
    s32 key;
    s32 timid;
    bool nomorekey = 0;
    u8 buf[64] = {0};
    s32 max_digits = nMaxLen;
    bool clr = 1;

	if (nMinLen > nMaxLen)
    {
        return SDK_PARA_ERR;
    }
	
    sdkKbKeyFlush();

    // 
    memset(buf, 0, sizeof(buf));
    if (pPrompt != NULL)
    {
        strcpy(buf, pPrompt);
    }
    // 
    sdkDispClearRowRam(nLine);
    pBuf[pBuf[0]+1] = '\0';  
    if (!pBuf[0])
    {
        //strcat(buf, "[_]");
//        sdkDispFillRowRam(nLine, 0, "[_]", SDK_DISP_DEFAULT);
    }
    else
    {
        sprintf(&buf[strlen(buf)], "%s", &pBuf[1]);
//        sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);
    }
    if (pUnit != NULL)          // CODY@2015/6/23
        strcat(buf, pUnit);
    sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);   
    sdkDispBrushScreen();

    
    if (pBuf[0] >= max_digits)
        nomorekey = 1;
    
    // Start timer for input timeout
    timid = sdkTimerGetId();
    // If timeout, quit the loop
    while (!sdkTimerIsEnd(timid, nTimeout))
    {
        // Check for valid key
        if ((key = sdkKbGetKey()) > 0)
        {
            // Restart timer
            timid = sdkTimerGetId();

            // Non-numeric key was pressed
            if (key < SDK_KEY_0 || key > SDK_KEY_9)
            {
                switch (key)
                {
                    // Finish inputting
                    case SDK_KEY_ENTER:
                        // Continue inputting if string length is less than minimum length

                        if (pBuf[0] < nMinLen)
                        {
                            sdkSysBeep(SDK_SYS_BEEP_ERR);
                            continue;
                        }
                            
                    case SDK_KEY_ESC:
                        sdkSysBeep(SDK_SYS_BEEP_OK);
                        return key;
					case SDK_KEY_FUNCTION:
						sdkSysBeep(SDK_SYS_BEEP_OK);
						return key;
                    // Delete the last character inputted if available
                    case SDK_KEY_BACKSPACE:
                    case SDK_KEY_CLEAR:
                        if (pBuf[0] > 0)
                        {
                            pBuf[pBuf[0]] = '\0';
                            pBuf[0]--;
                            nomorekey = 0;
                        }
                        else
                        {
                            sdkSysBeep(SDK_SYS_BEEP_ERR);
                            continue;
                        }
                        break;

                    default:
                        sdkSysBeep(SDK_SYS_BEEP_ERR);
                        continue;
                }
            }
            else   // Numeric key was pressed
            {
                if (clr)  
                {
                    pBuf[0] = 0;
                    clr = 0;
                    nomorekey = 0;
                }
                
                // No more input is allowed
                if (nomorekey)
                {
                    sdkSysBeep(SDK_SYS_BEEP_ERR);
                    continue;
                }

                // 
                //Trace("xgd","pOperator[0] = %s\r\n",pOperator);

				if(strcmp(pOperator,"MPT")==0)
				{
					if (pBuf[0] == 0 && key == SDK_KEY_0)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_1)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_3)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_6)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_7)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_8)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_9)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
				}
				else if(strcmp(pOperator,"OOREDOO")==0)
				{
					if (pBuf[0] == 0 && key == SDK_KEY_0)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_1)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_2)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_3)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_4)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_5)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_6)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_7)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_8)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
				}
				else if(strcmp(pOperator,"TELENOR")==0)
				{
					if (pBuf[0] == 0 && key == SDK_KEY_0)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_1)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_2)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_3)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_4)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_5)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_6)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_8)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_9)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
				}
				else if(strcmp(pOperator,"MECTEL")==0)
				{
					if (pBuf[0] == 0 && key == SDK_KEY_0)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_1)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_2)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_4)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_5)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_6)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_7)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_8)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_9)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
				}
				else if(strcmp(pOperator,"ALL")==0)
				{
					if (pBuf[0] == 0 && key == SDK_KEY_0)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_1)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_6)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
					else if (pBuf[0] == 0 && key == SDK_KEY_8)
	                {
	                    sdkSysBeep(SDK_SYS_BEEP_ERR);
	                    continue;
	                }
	
				}
               // 
                if (++pBuf[0] >= max_digits)
                    nomorekey = 1;
                pBuf[pBuf[0]] = key;
                pBuf[pBuf[0]+1] = '\0'; 
            }

            
            // 
            memset(buf, 0, sizeof(buf));
            if (pPrompt != NULL)
            {
                strcpy(buf, pPrompt);
            }
            // 
            // Display characters inputted
            sdkDispClearRowRam(nLine);
            if (!pBuf[0])
            {
                //strcat(buf, "[_]");
        //        sdkDispFillRowRam(nLine, 0, "[_]", SDK_DISP_DEFAULT);
            }
            else
            {
                sprintf(&buf[strlen(buf)], "%s", &pBuf[1]);
        //        sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);
            }
            if (pUnit != NULL)         
                strcat(buf, pUnit);
            sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);   
            sdkDispBrushScreen();
        }
    }

    return SDK_TIME_OUT;
}

s32 GetAmountNumber(s32 nTimeout, u8 *pBuf, s32 nLine, s32 nMinLen, s32 nMaxLen, const u8 *pPrompt, const u8 *pUnit) 
{
    s32 key;
    s32 timid;
    bool nomorekey = 0;
    u8 buf[64] = {0};
    const s32 max_digits = nMaxLen;
    bool clr = 1;

	if (nMinLen > nMaxLen)
    {
        return SDK_PARA_ERR;
    }
	
    sdkKbKeyFlush();

    // 
    memset(buf, 0, sizeof(buf));
    if (pPrompt != NULL)
    {
        strcpy(buf, pPrompt);
    }
    // 
    sdkDispClearRowRam(nLine);
    pBuf[pBuf[0]+1] = '\0';  
    if (!pBuf[0])
    {
        strcat(buf, "[_]");
//        sdkDispFillRowRam(nLine, 0, "[_]", SDK_DISP_DEFAULT);
    }
    else
    {
        sprintf(&buf[strlen(buf)], "[%s]", &pBuf[1]);
//        sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);
    }
    if (pUnit != NULL)          // CODY@2015/6/23
        strcat(buf, pUnit);
    sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);   
    sdkDispBrushScreen();

    
    if (pBuf[0] >= max_digits)
        nomorekey = 1;
    
    // Start timer for input timeout
    timid = sdkTimerGetId();
    // If timeout, quit the loop
    while (!sdkTimerIsEnd(timid, nTimeout))
    {
        // Check for valid key
        if ((key = sdkKbGetKey()) > 0)
        {
            // Restart timer
            timid = sdkTimerGetId();

            // Non-numeric key was pressed
            if (key < SDK_KEY_0 || key > SDK_KEY_9)
            {
                switch (key)
                {
                    // Finish inputting
                    case SDK_KEY_ENTER:
                        // Continue inputting if string length is less than minimum length

                        if (pBuf[0] < nMinLen)
                        {
                            sdkSysBeep(SDK_SYS_BEEP_ERR);
                            continue;
                        }
                            
                    case SDK_KEY_ESC:
                        sdkSysBeep(SDK_SYS_BEEP_OK);
                        return key;
					case SDK_KEY_F1:
						sdkSysBeep(SDK_SYS_BEEP_OK);
						return key;
					//Trace("xgd","key = %s\r\n",key);
                    // Delete the last character inputted if available
                    case 15:
						sdkSysBeep(SDK_SYS_BEEP_OK);
						return key;
						Trace("xgd","key = %s\r\n",key);
					//	TransferModule();
					//	ModuleTransferCustomerReadCard();
					
			break;
                    case SDK_KEY_BACKSPACE:
                    case SDK_KEY_CLEAR:
                        if (pBuf[0] > 0)
                        {
                            pBuf[pBuf[0]] = '\0';
                            pBuf[0]--;
                            nomorekey = 0;
                        }
                        else
                        {
                            sdkSysBeep(SDK_SYS_BEEP_ERR);
                            continue;
                        }
                        break;

                    default:
                        sdkSysBeep(SDK_SYS_BEEP_ERR);
                        continue;
                }
            }
            else   // Numeric key was pressed
            {
                if (clr)  
                {
                    pBuf[0] = 0;
                    clr = 0;
                    nomorekey = 0;
                }
                
                // No more input is allowed
                if (nomorekey)
                {
                    sdkSysBeep(SDK_SYS_BEEP_ERR);
                    continue;
                }

                // 
                if (pBuf[0] == 0 && key == SDK_KEY_0)
                {
                    sdkSysBeep(SDK_SYS_BEEP_ERR);
                    continue;
                }
                // 
                
                if (++pBuf[0] >= max_digits)
                    nomorekey = 1;
                pBuf[pBuf[0]] = key;
                pBuf[pBuf[0]+1] = '\0'; 
            }

            
            // 
            memset(buf, 0, sizeof(buf));
            if (pPrompt != NULL)
            {
                strcpy(buf, pPrompt);
            }
            // 
            // Display characters inputted
            sdkDispClearRowRam(nLine);
            if (!pBuf[0])
            {
                strcat(buf, "[_]");
        //        sdkDispFillRowRam(nLine, 0, "[_]", SDK_DISP_DEFAULT);
            }
            else
            {
                sprintf(&buf[strlen(buf)], "[%s]", &pBuf[1]);
        //        sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);
            }
            if (pUnit != NULL)         
                strcat(buf, pUnit);
            sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);   
            sdkDispBrushScreen();
        }
    }

    return SDK_TIME_OUT;
}

#define MIN(a, b)  ((a) > (b) ? (b) : (a))

s32 GetFormatNumber(s32 nTimeout, u8 *pBuf, s32 nLine, s32 nMinLen, s32 nMaxLen) 
{
    s32 key;
    s32 timid;
    bool nomorekey = 0;
    u8 buf[64] = {0};
    s32 max_digits = nMaxLen;
    bool clr = 1;

	if (nMinLen > nMaxLen)
    {
        return SDK_PARA_ERR;
    }
	
    sdkKbKeyFlush();

    // 
    memset(buf, 0, sizeof(buf));
        // 
    sdkDispClearRowRam(nLine);
    pBuf[pBuf[0]+1] = '\0';  
    if (!pBuf[0])
    {
        //strcat(buf, "[_]");
//        sdkDispFillRowRam(nLine, 0, "[_]", SDK_DISP_DEFAULT);
    }
    else
    {
        sprintf(&buf[strlen(buf)], "%s", &pBuf[1]);
//        sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);
    }
    sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);   
    sdkDispBrushScreen();

    
    if (pBuf[0] >= max_digits)
        nomorekey = 1;
    
    // Start timer for input timeout
    timid = sdkTimerGetId();
    // If timeout, quit the loop
    while (!sdkTimerIsEnd(timid, nTimeout))
    {
        // Check for valid key
        if ((key = sdkKbGetKey()) > 0)
        {
            // Restart timer
            timid = sdkTimerGetId();

            // Non-numeric key was pressed
            if (key < SDK_KEY_0 || key > SDK_KEY_9)
            {
                switch (key)
                {
                    // Finish inputting
                    case SDK_KEY_ENTER:
                        // Continue inputting if string length is less than minimum length

                        if (pBuf[0] < nMinLen)
                        {
                            sdkSysBeep(SDK_SYS_BEEP_ERR);
                            continue;
                        }
                            
                    case SDK_KEY_ESC:
                        sdkSysBeep(SDK_SYS_BEEP_OK);
                        return key;
					case SDK_KEY_F1:
						sdkSysBeep(SDK_SYS_BEEP_OK);
						return key;
                    // Delete the last character inputted if available
                    case SDK_KEY_BACKSPACE:
                    case SDK_KEY_CLEAR:
                        if (pBuf[0] > 0)
                        {
                            pBuf[pBuf[0]] = '\0';
                            pBuf[0]--;
                            nomorekey = 0;
                        }
                        else
                        {
                            sdkSysBeep(SDK_SYS_BEEP_ERR);
                            continue;
                        }
                        break;

                    default:
                        sdkSysBeep(SDK_SYS_BEEP_ERR);
                        continue;
                }
            }
            else   // Numeric key was pressed
            {
                if (clr)  
                {
                    pBuf[0] = 0;
                    clr = 0;
                    nomorekey = 0;
                }
                
                // No more input is allowed
                if (nomorekey)
                {
                    sdkSysBeep(SDK_SYS_BEEP_ERR);
                    continue;
                }

                // 
                if (pBuf[0] == 0 && key == SDK_KEY_0)
                {
                    sdkSysBeep(SDK_SYS_BEEP_ERR);
                    continue;
                }
                // 
                
                if (++pBuf[0] >= max_digits)
                    nomorekey = 1;
                pBuf[pBuf[0]] = key;
                pBuf[pBuf[0]+1] = '\0'; 
            }

            
            // 
            memset(buf, 0, sizeof(buf));
            // Display characters inputted
            sdkDispClearRowRam(nLine);
            if (!pBuf[0])
            {
                //strcat(buf, "[_]");
        //        sdkDispFillRowRam(nLine, 0, "[_]", SDK_DISP_DEFAULT);
            }
            else
            {   
				//1503-1-0000047904-1
                strncat(buf, &pBuf[1], MIN(4, pBuf[0]));
				if (pBuf[0] > 4)
				{
					strcat(buf, "-");
					strncat(buf, &pBuf[5], MIN(1, pBuf[0] - 4));
				}
				if (pBuf[0] > 5)
				{
					strcat(buf, "-");
					strncat(buf, &pBuf[6], MIN(10, pBuf[0] - 5));
				}
				if (pBuf[0] > 15)
				{
					strcat(buf, "-");
					strncat(buf, &pBuf[16], MIN(1, pBuf[0] - 15));
				}
        //        sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);
            }
            sdkDispFillRowRam(nLine, 0, buf, SDK_DISP_DEFAULT);   
            sdkDispBrushScreen();
        }
    }

    return SDK_TIME_OUT;
}

char * AgreementNumFormat(char * pBuf)
{
	static char buf[64]={0};
   static char  agreementNum[64]={0};
	Trace("xgd","agreementno= %s\r\n",pBuf);
	memset(buf,0,sizeof(buf));
	strncat(buf, &pBuf[0], MIN(4, pBuf[0]));
			if (pBuf[0] > 4)
				{
					strcat(buf, "-");
					strncat(buf, &pBuf[4], MIN(1, pBuf[0] - 4));
				}
				if (pBuf[0] > 5)
				{
					strcat(buf, "-");
					strncat(buf, &pBuf[5], MIN(10, pBuf[0] - 5));
				}
				if( (pBuf[0] > 16))
				{
					strcat(buf, "-");
					strncat(buf,&pBuf[15],MIN(1, pBuf[0] - 15));
				}
			//memset(pBuf,0,sizeof(pBuf));
        	memset(agreementNum,0,sizeof(agreementNum));
			strcpy(agreementNum,buf);
			return (char *)agreementNum;
}
char *Decrypt(char *pBuf)
{
	static char buf[16]={0};
   static char  Pincode[16]={0};
	Trace("xgd","Decrypt= %s\r\n",pBuf);
	memset(buf,0,sizeof(buf));
	strncat(buf, &pBuf[0], MIN(4, pBuf[0]));
	if (pBuf[0] > 4)
				{
					strcat(buf, " ");
					strncat(buf, &pBuf[4], MIN(1, pBuf[0] - 4));
				}
	if (pBuf[0] > 8)
				{
					strcat(buf, " ");
					strncat(buf, &pBuf[8], MIN(10, pBuf[0] - 8));
				}
	if (pBuf[0] > 12)
				{
					strcat(buf, " ");
					strncat(buf, &pBuf[10], MIN(10, pBuf[0] - 12));
				}
	if(pBuf[0]>16)
				{
					strcat(buf, " ");
					strncat(buf, &pBuf[16], MIN(10, pBuf[0] - 16));
				}
				memset(Pincode,0,sizeof(Pincode));
			strcpy(Pincode,buf);
			return (char *)Pincode;
							
}

