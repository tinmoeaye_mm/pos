/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : display.c
   Version       : Initial Draft
   Author        : Cody
   Created       : 2014/6/26
   Last Modified :
   Description   : Display module related functions are defined here
   History       :
   1.Date        : 2014/6/26
    Author      : Cody
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"

// Internal function prototypes
static void DisplayText(void);
static void DisplayBmp(void);
static void DisplayProgbar(void);
static void DisplayAt(void);
void AgentCheckBalanceV2(void);


/*******************************************************************************
** Description :  Display module submenu
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-09
** Remark      :
*******************************************************************************/
void ModuleDisplay(void)
{
    s32 key;

    while (1)
    {
        // Display menu for DISPLAY module
        sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "DISPLAY", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "1.TEXT", SDK_DISP_LEFT_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "2.BMP", SDK_DISP_LEFT_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE4, 0, "3.PROGESS BAR", SDK_DISP_LEFT_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE5, 0, "4.AT ANY POSITION", SDK_DISP_LEFT_DEFAULT);
        sdkDispBrushScreen();

        	// Wait for key input
    	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | SDK_KEY_MASK_ESC, 30000);

        switch (key)
        {
             // Display text onscreen
             case SDK_KEY_1:
                {
                    DisplayText();
                }
                break;

             // Display graphics in bitmap format onscreen
             case SDK_KEY_2:
                {
                    DisplayBmp();
                }
                break;

             // Display progress bar onscreen
             case SDK_KEY_3:
                {
                    DisplayProgbar();
                }
                break;

             // Display text at any location on screen
             case SDK_KEY_4:
                {
                    DisplayAt();
                }
                break;
			 case SDK_KEY_ESC:
			 	return;
             default:
               break;
        }
    }
}

/*******************************************************************************
** Description :  Display text onscreen
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-09
** Remark      :  1. clear screen
                  2. write data to buffer
                  3. flush the buffer
*******************************************************************************/
static void DisplayText(void)
{
    SDK_DISP_MULT_DATA st_bigfont, st_smallfont;
    SDK_DISP_PIXEL st_pix = sdkDispGetScreenPixel();

    
    // Clear the whole screen
    sdkDispClearScreen();

    // Write to the buffer
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "TEXT DISPLAY", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);

    sdkDispFillRowRam(SDK_DISP_LINE2, 0,         // Display on line 2
                      "Left,normal",             // String to be displayed
                      SDK_DISP_FDISP |           // Normal display
                      SDK_DISP_LDISP |           // Left aligned
                      SDK_DISP_INCOL             // Insert one column when displaying Chinese
                      );

    sdkDispFillRowRam(SDK_DISP_LINE3, 0,         // Display on line 3
                      "Right,inverted char",     // String to be displayed
                      SDK_DISP_NOFDISP |         // Inverted display characters
                      SDK_DISP_RDISP |           // Right aligned
                      SDK_DISP_INCOL             // Insert one column when displaying Chinese
                      );


    // Set big font structure
    memset(&st_bigfont, 0, sizeof(st_bigfont));
    st_bigfont.eAscFont = SDK_PRN_ASCII12X24;
    st_bigfont.eChFont = SDK_PRN_CH16X16;
    st_bigfont.siX = 0;
    st_bigfont.siY = st_pix.siY - 25;
    strcpy(st_bigfont.pasText, "[ESC]");
    // Set small font structure
    memset(&st_smallfont, 0, sizeof(st_smallfont));
    st_smallfont.eAscFont = SDK_PRN_ASCII8X16;
    st_smallfont.eChFont = SDK_PRN_CH16X16;
    st_smallfont.siX = 72;
    st_smallfont.siY = st_pix.siY - 25;
    strcpy(st_smallfont.pasText, "TO QUIT");
    // Display text using multiple fonts in one line
    sdkDispFormatFillRam(SDK_DISP_DEFAULT, &st_bigfont, &st_smallfont, NULL);

    //sdkDispFillRowRam(SDK_DISP_LINE5, 0, "[ESC] TO QUIT", SDK_DISP_DEFAULT);

    // Flush the buffer
    sdkDispBrushScreen();

    // Polling for ESC key to quit this demo
    while (sdkKbGetKey() != SDK_KEY_ESC)
    {
        sdkmSleep(500);
    }
}

/*******************************************************************************
** Description :  Display a bmp picture at the center of the screen
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-10-21
** Remark      :  1. bitmap file could be 1 bpp for black and white screen and 
                     up to 24 bpp for color screen, and its content to be displayed 
                     must NOT exceed the screen boundries;
                  2. bitmap file is usually put into folder /mtd0/res/;
                  3. make sure file logo-1bpp.bmp is placed in folder /mtd0/res/ 
                     for black and white screen and logo-24bpp.bmp for color screen
                     in order for this demo to work properly.
*******************************************************************************/
static void DisplayBmp(void)
{
    u8 path[32] = "/mtd0/res/";
    SDK_DISP_PIXEL pix = sdkDispGetScreenPixel();   // Get screen resolution
    SDK_DISP_PIXEL p1;
    s32 width, height;
    u8 buf[32] = {0}, model[8] = {0};
    s32 res, len;

    // Check the type of screen: color screen for G870 & G3 models, b & w screen for other models
    res = sdkSysGetMachineModel(model);
    if (res <= 0)
    {
        Trace("cody", "sdkSysGetMachineModel() = %d\r\n", res);
        return;
    }
    if (!memcmp(model, "G870", 4) || !memcmp(model, "G3", 2))  // Color screen
    {
        strcat(path, "logo-24bpp.bmp");
    }
    else  // Black-and-white screen
    {
        strcat(path, "logo-1bpp.bmp");
    }

    // Clear the whole screen
    sdkDispClearScreen();

    if (sdkAccessFile(path) == true)
    {
        // Read the width of BMP
        len = 4;
        res = sdkReadFile(path, buf, 18, &len);
        if (res != SDK_FILE_OK)
        {
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, path, SDK_DISP_DEFAULT);
            sdkDispFillRowRam(SDK_DISP_LINE4, 0, "read file error!", SDK_DISP_DEFAULT);
        }
        width = buf[0] + buf[1]*0x100 + buf[2]*0x10000 + buf[3]*0x1000000;
        Trace("cody", "BMP width = %d\r\n", width);
        // Read the height of BMP
        len = 4;
        res = sdkReadFile(path, buf, 22, &len);
        if (res != SDK_FILE_OK)
        {
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, path, SDK_DISP_DEFAULT);
            sdkDispFillRowRam(SDK_DISP_LINE4, 0, "read file error!", SDK_DISP_DEFAULT);
        }
        height = buf[0] + buf[1]*0x100 + buf[2]*0x10000 + buf[3]*0x1000000;
        Trace("cody", "BMP height = %d\r\n", height);

        width = width > pix.siX ? pix.siX : width;
        height = height > pix.siY ? pix.siY : height;

        p1.siX = (pix.siX - width) / 2;
        p1.siY = (pix.siY - height) / 2;
        
        sdkDispShowBmp(p1.siX, p1.siY, width, height, path);
    }
    else
    {
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, path, SDK_DISP_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE4, 0, "doesn't exist!", SDK_DISP_DEFAULT);
    }
    sdkDispBrushScreen();

    // Polling for ESC key to quit this demo
    while (sdkKbGetKey() != SDK_KEY_ESC)
    {
        sdkmSleep(500);
    }
}

/*******************************************************************************
** Description :  Display progress bar onscreen
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-09
** Remark      :
*******************************************************************************/
static void DisplayProgbar(void)
{
    SDK_DISP_PIXEL pix = sdkDispGetScreenPixel();
    u8 value, step;
    char sign;
    u8 buf[64] = {0};

    // Clear the whole screen
    sdkDispClearScreen();

    // Write to the buffer
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "PROGBAR DISPLAY", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
    sdkDispFillRowRam(SDK_DISP_LINE2, 0, "In progess...", SDK_DISP_DEFAULT);

    for (value = 50, sign = 1, step = 2; sdkKbGetKey() != SDK_KEY_ESC; value += sign * step)
    {
        sprintf(buf, "%d%%", value);
        sdkDispDrawProgbar(SDK_DISP_LINE3,        // Display progress bar on line 3
                           pix.siX / 2 - 50,        // Starting X position of progress bar
                           pix.siX / 2 + 50,        // Ending X position of progress bar
                           value);                // Percentage ranging from 1 to 100
        sdkDispClearRowRam(SDK_DISP_LINE4);
        sdkDispFillRowRam(SDK_DISP_LINE4, 0, buf, SDK_DISP_DEFAULT);
        sdkDispBrushScreen();

        // A short dalay
        sdkmSleep(100);

        // Change the progress direction
        if (value <= 0 || value >= 100)
        {
            sign *= -1;
        }
    }
}

/*******************************************************************************
** Description :  Display text at any position onscreen
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-21
** Remark      :
*******************************************************************************/
static void DisplayAt(void)
{
    s32 x, y, signx, signy;
    const s32 step = 2;
    u8 buf[64] = {0};
    SDK_DISP_PIXEL pix;
    s32 fontx, max_char;

    // Get the number of pixels in horizontal and vertical direction
    pix = sdkDispGetScreenPixel();

    max_char = sdkSysGetMaxLetterPerLine();
    fontx = pix.siX/max_char;    // Width of default font: 6 for b&w screen, 12 for color screen
    

    // Clear the whole screen
    sdkDispClearScreen();

    // Write to the buffer
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "DISPLAY AT", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);

    for (x = pix.siX / 2, y = pix.siY / 2, signx = 1, signy = 1; sdkKbGetKey() != SDK_KEY_ESC; x += signx * step, y += signy * step)
    {
        // Coordinate string
        sprintf(buf, "(%d,%d)", x, y);
        // Display coordinate at screen position (x, y)
        sdkDispAt(x, y, buf);
        sdkDispBrushScreen();

        // Delay 800 milliseconds
        sdkmSleep(800);

        // Clear a specified area onscreen
        sdkDispClearAt(x, y, pix.siX - 1, pix.siY - 1);
        sdkDispBrushScreen();

        // Reverse string's moving direction along x
        if (x <= 1 || x >= pix.siX - strlen(buf)*fontx - 1)
        {
            signx *= -1;
        }

        // Reverse string's moving direction along y
        if (y <= pix.siY/5 || y >= pix.siY - fontx*2 - 1)
        {
            signy *= -1;
        }
    }
}

/*******************************************************************************
** Description :  Clear lines except the 1st line
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-22
** Remark      :
*******************************************************************************/
void DispClearContent(void)
{
    sdkDispClearRowRam(SDK_DISP_LINE2);
    sdkDispClearRowRam(SDK_DISP_LINE3);
    sdkDispClearRowRam(SDK_DISP_LINE4);
    sdkDispClearRowRam(SDK_DISP_LINE5);
//    sdkDispBrushScreen();
}

