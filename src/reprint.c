/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : Reprint.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/3/17
   Last Modified :
   Description   : agent module-related functions are defined
                  here
   History       :
   1.Date        : 2016/3/17
    Author      :  Tin Moe Aye
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"
void ReprintTRX(void);
void ReprintMainMenu(void);
void ReprintSendData(void);
void PrintReprint(s32 type);
u8 tmnLogo1[] = "/mtd0/res/tmnprintlogo.bmp";
void ReprintMainMenu(void)
{
	s32 key=0;
	sdkDispClearScreen();
    sdkDispShowBmp(0, 0, 240, 320,"/mtd0/res/ReprintMainMeun.bmp");
    sdkDispBrushScreen();

        
        key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 30000);

        switch (key)
        {
             case SDK_KEY_1:
                {
                	reprint.Type=LastTran;
                    ReprintSendData();
                }
                break;

             case SDK_KEY_2:
                {
                	reprint.Type=TRX;
                    ReprintTRX();
                }
                break;

             case SDK_KEY_ESC:
                {
                    AgentRqeuestMenu();
                }

             default:
                {
                }
                break;
        
   		 }
   	AgentRqeuestMenu();
}
void ReprintSendData(void)
{
	s32 ret;
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key=0;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(message,0,sizeof(message));
	if(reprint.Type==TRX)
	{
		Trace("reprint","TRX");
		sprintf(message,"%s%s%s%s",sendData.asBill,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
	}
	else if(reprint.Type==LastTran)
	{
		
		Trace("reprint","LastTran");
		sprintf(message,"%d%s%s%s",0,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN);
	}
	
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	Trace("reprint","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	if(reprint.Type==TRX)
	{
		Trace("reprint","TRXSendBuf");
		sprintf(sendData.asSendBuf, "%s%s/reprint/%s/%s/%s/%s/%s",uri,IPAddress,sendData.asBill,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	}
	else if(reprint.Type==LastTran)
	{
		Trace("reprint","LastTranSendBuf");
		sprintf(sendData.asSendBuf, "%s%s/reprint/%d/%s/%s/%s/%s",uri,IPAddress,0,sendData.asAgentEnterPin,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	}
	Trace("reprint","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0) 
	{
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				js_tok(receivedData.CardRef,"agentCardRef",0);

				js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

				js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);

				js_tok(receivedData.TRX,"trx",0);
				
				js_tok(receivedData.Activity,"activity",0);
				Trace("reprint","receivedData.Activity=%s\r\n",receivedData.Activity);
				
				js_tok(receivedData.PhoneNo,"mobileNo",0);
				
				js_tok(receivedData.BillRefNo,"ref_no",0);
				
				js_tok(receivedData.Amount,"amount",0);
				
				js_tok(receivedData.commission,"charge",0);
				
				js_tok(receivedData.ReprintType,"reprintType",0);
				
				js_tok(receivedData.CardNo,"receiverCardNo",0);
				
				js_tok(receivedData.CashOutFee,"agentCardNo",0);
				
				js_tok(receivedData.TotalAmount,"totalAmount",0);
				
				js_tok(receivedData.CustRef,"customerAgentCardRef",0);
				
				js_tok(receivedData.CustomerCardNum,"customerAgentCardNo",0);
				
				js_tok(receivedData.ShopName,"shopName",0);
				
				Trace("reprint","receivedData.shopName=%s\r\n",receivedData.ShopName);
				
				js_tok(receivedData.MasterCardRef,"masterAgentCardRef",0);
				
				if((strcmp(receivedData.Activity,"Ord-Pin-1000-Cash")==0)||(strcmp(receivedData.Activity,"Ord-Pin-3000-Cash")==0)||(strcmp(receivedData.Activity,"Ord-Pin-5000-Cash")==0)||(strcmp(receivedData.Activity,"Ord-Pin-10000-Cash")==0)||(strcmp(receivedData.Activity,"Ord-Pin-20000-Cash")==0)||(strcmp(receivedData.Activity,"Ord-Pin-1000-TrueCard")==0)||(strcmp(receivedData.Activity,"Ord-Pin-3000-TrueCard")==0)||(strcmp(receivedData.Activity,"Ord-Pin-5000-TrueCard")==0)||(strcmp(receivedData.Activity,"Ord-Pin-10000-TrueCard")==0)||(strcmp(receivedData.Activity,"Ord-Pin-20000-TrueCard")==0))
				{
					
					js_tok(receivedData.AccessPinCode,"pinCode",0);
    				Trace("reprint sendData","receivedData.pincode=%s\r\n",receivedData.AccessPinCode);
					
					dc_message(receivedData.AccessPinCode);
     				Trace("decrption","receivedData.PhPincode=%s\r\n",receivedData.AccessPinCode);
     				strcpy(receivedData.PhPinCode,SpaceFormat(receivedData.AccessPinCode));
    				 Trace("Space","After Space=%s\r\n",receivedData.PhPinCode);
				}
				 else
   				 {
		   			js_tok(receivedData.PhPinCode,"pinCode",0);
				     Trace("reprint sendData","receivedData.pincode=%s\r\n",receivedData.PhPinCode);
    
				}
		     
			
			
			
				if(strcmp(receivedData.ReprintType,"Topup")==0)
				{
					PrintReprint(1);
				}
				else if(strcmp(receivedData.ReprintType,"BillPayment")==0)
				{
					PrintReprint(2);
				}
				else if(strcmp(receivedData.ReprintType,"Remittance")==0)
				{ 
					PrintReprint(3);
					
				}
			
				
				
				else if(strcmp(receivedData.ReprintType,"Others")==0)
				{
					sdkDispClearScreen();
					sdkDispFillRowRam(SDK_DISP_LINE1,0,"REPRINT",SDK_DISP_DEFAULT);
					
					sdkDispFillRowRam(SDK_DISP_LINE3,0,"REPRINT NOT ALLOW.",SDK_DISP_LEFT_DEFAULT);
					
					sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
					sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
					sdkDispBrushScreen();
					key = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);
					switch(key)
					{
						case SDK_KEY_ENTER:
							{
								ReprintMainMenu();
							}
							break;
						case SDK_KEY_ESC:
							{
								ReprintMainMenu();
							}
							break;
							default:
							{
								
							}	
					}
					ReprintMainMenu();
					
									
				}
			}
			else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				js_tok(receivedData.ErrorMessage,"message",0);
		
				DisplayErrorMessage("Reprint",receivedData.ErrorMessage,false);
			}
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	ReprintMainMenu();
}

void ReprintTRX(void)
{
	u8 num[16]={0};
	s32 key;
	memset(num,0,sizeof(num));
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320,"/mtd0/res/ReprintTRX.bmp");
    sdkDispBrushScreen();
	key =  sdkKbGetScanf(60000,num,10,20,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	sdkDispBrushScreen();
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				Trace("reprint","TRX NUM= %s\r\n",num);
				memset(sendData.asBill,0,sizeof(sendData.asBill));
				memcpy(sendData.asBill,&num[1],num[0]);
				Trace("reprint","TRX NUM=%s\r\n",sendData.asBill);
				ReprintSendData();
			}
			break;
			case SDK_KEY_ESC:
			{
				ReprintMainMenu();	
			}
			break;
			default:
			{
				
			}	
	}
	ReprintMainMenu();
}

void PrintReprint(s32 type)
{
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "REPRINT", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SUCCESS.", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PRINTING...", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	u8 name[32] = {0};
    SDK_PRINT_FONT st_font;
    u8 buf[64] = {0};
    s32 spacing = 4;
    
	strcpy(name,"REPRINT SLIP");
	sdkPrintInit();
  	 
  	memset(&st_font, 0, sizeof(st_font));
    st_font.uiAscFont = SDK_PRN_ASCII16X24B;
    st_font.uiAscZoom = SDK_PRN_ZOOM_B;
    sdkPrintStr(" ", st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
    
    sdkPrintBitMap(tmnLogo1, SDK_PRINT_MIDDLEALIGN, 0);
    
   	sdkPrintStr(name, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
   	
   	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
	if(reprint.Type==LastTran)
	{
		memset(buf, 0, sizeof(buf));
		sprintf(buf,"REPRINT BY LASTTRX");
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	}
	else if(reprint.Type==TRX)
	{
		memset(buf, 0, sizeof(buf));
		sprintf(buf,"REPRINT BY TRX");
		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, 20);
	}
	memset(buf, 0, sizeof(buf));
    memset(buf, '-', 32);
    sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    
    if(type==1)
    {
    	memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","ACTIVITY  :",receivedData.Activity);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s  %s","MOBILE No.:",receivedData.PhoneNo);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","PIN CODE  :",receivedData.PhPinCode);
		Trace("reprint for topup","receivedData.pincode=%s\r\n",receivedData.PhPinCode);
		Trace("reprint for topup","receivedData.pincode=%s\r\n",buf);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","AMOUNT    : ",receivedData.Amount);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32);
   		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
					
	}
	else if(type==2)
	{
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","ACTIVITY   :",receivedData.Activity);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
		
		if(strcmp(receivedData.Activity,"HelloCabs-Card") == 0 || strcmp(receivedData.Activity,"HelloCabs-Cash") == 0)	
		{
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s %s","DRIVERID   :",receivedData.BillRefNo);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		}
		else if(strcmp(receivedData.Activity,"HelloCabs-Card") != 0 || strcmp(receivedData.Activity,"HelloCabs-Cash") != 0)
		{
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s %s","REF No.    :",receivedData.BillRefNo);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s %s","MOBILE No. :",receivedData.PhoneNo);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		
		}
		if(strcmp(receivedData.Activity,"CP-Outlet-Cash") == 0) 
		{
			if(strlen(receivedData.ShopName) >= 18)
			{
				
				memset(buf,0,sizeof(buf));
				strcpy(receivedData.ShopAddress,"SHOP NAME:");
				sprintf(buf, "%s",receivedData.ShopAddress);
				sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				memset(buf,0,sizeof(buf));
				sprintf(buf, "%s",receivedData.ShopName);
				sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
			
			}
			else
			{
				memset(buf,0,sizeof(buf));
				sprintf(buf,"%s %s","SHOP       :",receivedData.ShopName);
				sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
				
			}
		
		}
		
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32);
   		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","AMOUNT     :",receivedData.Amount);
		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
				
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s KS %s","FEECHARGE  :",receivedData.commission);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
		memset(buf,0,sizeof(buf));
		sprintf(buf,"%s KS %s","TOTAL AMT  :",receivedData.TotalAmount);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
		
		memset(buf, 0, sizeof(buf));
		memset(buf, '-', 32);
   		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
   		
   			if((strcmp(receivedData.Activity,"AeonPay_Card")==0)||(strcmp(receivedData.Activity,"TitanPayment_Card")==0)|| strcmp(receivedData.Activity,"HelloCabs-Card")==0 || strcmp(receivedData.Activity,"YESC Card Payment")==0)
   			{
   				memset(buf,0,sizeof(buf));
   			 	sprintf(buf,"MID       : %s",receivedData.CustRef);
    			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			}
		
   			
	}	
	else if(type==3)
	{
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s %s","ACTIVITY  :",receivedData.Activity);
		sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		if(strcmp(receivedData.Activity,"RefillAgentByMaster")==0)
		{
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s %s","AGENT CARD NO.:",receivedData.CashOutFee);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","REFILL AMT    :",receivedData.Amount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32);
    		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    			
    		memset(buf,0,sizeof(buf));
   			sprintf(buf,"MAS       : %s",receivedData.MasterCardRef);
    		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
    			
    			
		}
		else if(strcmp(receivedData.Activity,"WithdrawAgentFromMaster")==0)
		{
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s %s","AGENT CARD NO.:",receivedData.CashOutFee);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","WITHDRAW AMT 	:",receivedData.Amount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
				memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32);
    		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    		
    		memset(buf,0,sizeof(buf));
   			 sprintf(buf,"MAS       : %s",receivedData.MasterCardRef);
    		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		}
		
		else if(strcmp(receivedData.Activity,"Non member cashout")==0)
		{
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s %s","MOBILE No.:",receivedData.PhoneNo);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32);
   			 sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
   			 
   			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s KS %s","CASHOUT AMT  :",receivedData.Amount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32);
    		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		}
		else if(strcmp(receivedData.Activity,"Member cashout")==0)

		{
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s %s","MEMBER CARD NO.:",receivedData.CardNo);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32);
   			 sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
   			 
   			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s KS %s","CASHOUT AMT  :",receivedData.Amount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			memset(buf, 0, sizeof(buf));
			memset(buf, '-', 32);
    		sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
    
   
  		  	memset(buf,0,sizeof(buf));
   			 sprintf(buf,"MID       : %s",receivedData.CustRef);
    		sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
		}
		else if(strcmp(receivedData.Activity,":IRS_Cashout"))		
		{
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s %s","MOBILE No.:",receivedData.PhoneNo);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
				memset(buf, 0, sizeof(buf));
				memset(buf, '-', 32);
    			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
			
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s KS %s","CASHOUT AMT 	:",receivedData.Amount);
			sdkPrintStr(buf,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
			
			memset(buf, 0, sizeof(buf));
				memset(buf, '-', 32);
    			sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
				
		}
			
	}

	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AID       : %s",receivedData.CardRef);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		   	
	memset(&buf, 0, sizeof(buf));
	sprintf(buf, "TRX       : %s",receivedData.TRX);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);	
		
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
	
	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	memset(buf, 0, sizeof(buf));
	memset(buf, '-', 32);
	sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);

	memset(&st_font, 0, sizeof(st_font));
	st_font.uiAscFont = SDK_PRN_ASCII8X16;
	st_font.uiAscZoom = SDK_PRN_ZOOM_N;

	sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
	sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
	sdkPrintStr(" ", st_font, SDK_PRINT_LEFTALIGN, 0, 150);

		/*
	     * sdkPrintStartNoRollPaper & sdkPrintStartNoRollBack could be used on both stylus printer and thermal printer.
	     * The differences between them are as follows:
	     * NoRollPaper would roll back paper for stylus printer and NOT feed paper for thermal printer;
	     * NoRollBack would NOT roll back paper, nor would it feed paper for both printers.
	     */
	sdkPrintStartNoRollBack();
	CheckPaperRollStatus();
	//PrintSlipConfirm(5);
	 ReprintMainMenu();
	
}
