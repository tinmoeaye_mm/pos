#include "../inc/global.h"
//#include "../inc/sdkFpc.h"

/*******************************************************************
**  CORP.    :	ShenZhen Xinguodu Technology Co.,Ltd.
**  AUTHOR   :	Ashur
**  NAME     :  EnrollFPC
**  FUNCTION :  fingerprint
**  REFERENCE:  u8 iCount:count times; bool bisMix: 1;u8 *lpOut: FPC tempdata
**  RETURN   :  Result
**  DATE/TIME:	2013.12.17  10:26:20
**  REMARK   :
********************************************************************/
void NewMainMenu(void);
s32 EnrollFPC(u8 iCount, bool bisMix, u8 *lpOut)
{
	s32 key = 0;
	u8 counttime = 0;																					  //the number of place finger
	u8 buf[256 * 3] = { 0 };
	u8 FPCdata[512] = { 0 };																			  //fingerprint template data buffer
	u8 temp[128] = { 0 };
	s32 ret = 0, siTimer;
	char *bmp = NULL;																									   //image buffer 152*200

	if (iCount>3) iCount = 3; //Max times is 3
	bmp = sdkGetMem(30400 * sizeof(char));
	siTimer = sdkTimerGetId();

	//sdkFpcReset(100);																							  //open  fingerprint sensor
	//sdkFpcInit();																						  //initialization fingerprint sensor

	while (1)
	{

		key = sdkKbGetKey();
		if (key != SDK_KEY_ERR)
		{
			if (key == SDK_KEY_ESC)
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				sdkFreeMem(bmp);
				AppMain();
				return SDK_ESC;
			}
			else
			{
				sdkSysBeep(SDK_SYS_BEEP_ERR);
			}
		}

		if (sdkTimerIsEnd(siTimer, 120 * 1000)) 	//Timer-out 60s
		{
			sdkSysBeep(SDK_SYS_BEEP_OK);
			sdkFreeMem(bmp);
			AppMain();
			return SDK_ESC;
		}

		memset(temp, 0, sizeof(temp));
		sprintf(temp, "NO.%d collection", counttime + 1);
		DispClearContent();
		if(counttime+1 == 1)
		{
			
		sdkDispShowBmp(0,0, 240, 320,"/mtd0/res/firstfp.bmp");
		
		}
		else if(counttime+1 == 2)
		{
		
		sdkDispShowBmp(0,0, 240, 320,"/mtd0/res/regfpsecond.bmp");
		
		}
		sdkDispBrushScreen();
		/*DispClearContent();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, temp, SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Press finger", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();*/
		if (sdkFpcGetImage(bmp) == SDK_OK)																				//finger is placed
		{
			if (sdkFpcEnroll(bmp, SDK_FPC_ISO, &buf[256 * counttime]))
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				counttime++;
				if (counttime == iCount)																							   //place finger 3 times to generate fingerprint template
				{
					if (bisMix) //	 iCount>=2
					{
						ret = sdkFpcMix(buf, buf + 256, buf + 512, FPCdata, SDK_FPC_ISO, iCount);
						if (ret < SDK_OK)		   //generate fingerprint template
						{

							DispClearContent();
							//sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OPERATION FAILED", SDK_DISP_DEFAULT);
							//sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Move finger away", SDK_DISP_DEFAULT);
							sdkDispShowBmp(0,0, 240, 320,"/mtd0/res/secondfail.bmp");
							sdkDispBrushScreen();
							while (sdkFpcGetImage(bmp) == SDK_OK)	  //move finger away then continue
							{
								;																			//finger is move away or not
							}
							counttime = 0;
							continue;
						}
						memcpy(lpOut, FPCdata, sizeof(FPCdata));												  //copy the fingerprint template data
						//TraceM("xgd", DBG_MODE_RS232, "enroll lpOut2 %s\r\n", lpOut);
					}
					else
					{
						memcpy(lpOut, buf, 256 * iCount);
						//TraceM("xgd", DBG_MODE_RS232, "enroll lpOut1 %s\r\n", lpOut);
					}

					DispClearContent();
					//sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OBTAIN SUCCESS", SDK_DISP_DEFAULT);
					//sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Move finger away", SDK_DISP_DEFAULT);
					sdkDispShowBmp(0,0, 240, 320,"/mtd0/res/secondsuccess.bmp");
					sdkDispBrushScreen();
					while (sdkFpcGetImage(bmp) == 1)
					{
						;
					}
					break;
				}
				else
				{

					sprintf(temp, "NO.%d OK", counttime);
					if(counttime==1)
					{
					DispClearContent();							
					sdkDispShowBmp(0,0, 240, 320,"/mtd0/res/firstsuccess.bmp");			
					sdkDispBrushScreen();
					}
				
					/*DispClearContent();
					sdkDispFillRowRam(SDK_DISP_LINE2, 0, temp, SDK_DISP_DEFAULT);
					sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Move finger away", SDK_DISP_DEFAULT);
					sdkDispBrushScreen();
					*/
					while (sdkFpcGetImage(bmp) == SDK_OK)
					{
						;
					}

					continue;
				}
			}
			else
			{
				sprintf(temp, "NO.%d failed", counttime + 1);
				DispClearContent();
				if(counttime+1 == 1)
				{
				
				sdkDispShowBmp(0,0, 240, 320,"/mtd0/res/firstfail.bmp");
				
				}
				else if(counttime+1 ==2)
				{
				sdkDispShowBmp(0,0, 240, 320,"/mtd0/res/secondfail.bmp");	
				}	
				sdkDispBrushScreen();
				/*DispClearContent();
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, temp, SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Move finger away", SDK_DISP_DEFAULT);
				sdkDispBrushScreen();*/
				while (sdkFpcGetImage(bmp) == SDK_OK)
				{
					;
				}

				continue;
			}
		}
	}
	sdkFreeMem(bmp);

	return SDK_OK;
}


/*******************************************************************
**  CORP.    :	ShenZhen Xinguodu Technology Co.,Ltd.
**  AUTHOR   :	Ashur
**  NAME     :  VerifyFPC
**  FUNCTION :  verify finger print data
**  REFERENCE:  u8 *FPCdata: the data to be compare
SafeLevel:1-5L￡?Normal set 3; False accpet rate:0.1%,0.01%,0.001%,0.0001%,0
**  RETURN   :
**  DATE/TIME:	2014.01.03  15:43:38
**  REMARK   :
********************************************************************/
s32 VerifyFPC(u8 *FPCdata, s32 iSafeLevel)
{
	u8 temp[512] = { 0 };
	s32 ret = 0, key = 0;
	s32 siTimer;
	char *bmp = NULL;

	int i=1;
	bmp = sdkGetMem(30400 * sizeof(char));
	//sdkFpcReset(100);
	//ret = sdkFpcInit();                                                                                         //initialization
	siTimer = sdkTimerGetId();
	
	

	while (1)
	{
		key = sdkKbGetKey();
		if (key != SDK_KEY_ERR)
		{
			if (key == SDK_KEY_ESC)
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				AppMain();
				sdkFreeMem(bmp);
				return SDK_ESC;
			}
			else
			{
				sdkSysBeep(SDK_SYS_BEEP_ERR);
			}
		}
		if (sdkTimerIsEnd(siTimer, 60 * 1000))//Timer-out 60s
		{
			sdkSysBeep(SDK_SYS_BEEP_OK);
			sdkFreeMem(bmp);
			return SDK_ESC;
		}
		step1:
		sdkDispClearScreen();
		//sdkDispFillRowRam(SDK_DISP_LINE2, 0, "VERIFICATION", SDK_DISP_DEFAULT);
		//sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Press finger", SDK_DISP_DEFAULT);
		sdkDispShowBmp(50, 135, 170, 150, "/mtd0/res/fingerprint.bmp");
		//sdkDispShowBmp(0, 0, 240, 320, "mtd0/res/fingerprint.bmp");
		sdkDispBrushScreen();
		
		if (sdkFpcGetImage(bmp) == SDK_OK)   // check if FPC data exist
		{
			ret = sdkFpcEnroll(bmp, SDK_FPC_ISO , temp);
			if (ret == SDK_OK)
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				ret = sdkFpcMatch(temp, FPCdata, iSafeLevel, SDK_FPC_ISO );           //verifation
				if (ret == SDK_OK)
				{
					DispClearContent();
					//sdkDispFillRowRam(SDK_DISP_LINE2, 0, "VERIFICATION OK", SDK_DISP_DEFAULT);
					//sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Move finger away", SDK_DISP_DEFAULT);
					sdkDispShowBmp(0,0,240,320,"/mtd0/res/fpsuccess.bmp");
					sdkDispBrushScreen();
					while (sdkFpcGetImage(bmp) == SDK_OK)
					{
						;
					}
					sdkFreeMem(bmp);
					return SDK_OK;
				}
				else
				{
					
					while(i<4)
					{
						DispClearContent();
					//sdkDispFillRowRam(SDK_DISP_LINE2, 0, "VERIFICATION FAILED", SDK_DISP_DEFAULT);
					//sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Move finger away", SDK_DISP_DEFAULT);
						sdkDispShowBmp(0,0,240,320,"/mtd0/res/fpfail.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1000);
							i=i+1;
						goto step1;
						
						Trace("xgd", "Menu for\r\n",i);
						
						
					}
					if(fingerprint.Error==AGENT)
					{
						DispClearContent();
						sdkDispFillRowRam(SDK_DISP_LINE2, 0, "WRONG FINGERPRINT.", SDK_DISP_DEFAULT);
						sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRY AGAIN.", SDK_DISP_DEFAULT);
						
						sdkDispBrushScreen();
						sdkmSleep(2000);
						AgentMenu();
						Trace("xgd", "Menu for\r\n");
					}
					else if (fingerprint.Error==SALE)
					{
						DispClearContent();
						sdkDispFillRowRam(SDK_DISP_LINE2, 0, "WRONG FINGERPRINT.", SDK_DISP_DEFAULT);
						sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRY AGAIN.", SDK_DISP_DEFAULT);
						
						sdkDispBrushScreen();
						sdkmSleep(2000);
						SalesMenu();
						Trace("xgd", "Menu for\r\n");
					}
					else if(fingerprint.Error==AGENTMENUCHECK)
					{
						DispClearContent();
						sdkDispFillRowRam(SDK_DISP_LINE2, 0, "WRONG FINGERPRINT.", SDK_DISP_DEFAULT);
						sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRY AGAIN.", SDK_DISP_DEFAULT);
						
						sdkDispBrushScreen();
						sdkmSleep(2000);
						AppMain();
						Trace("xgd", "Menu for\r\n");
					}
					else if(fingerprint.Error==MEMCASHOUTCASHIN || fingerprint.Error == TOPUP)
					{
						DispClearContent();
						sdkDispFillRowRam(SDK_DISP_LINE2, 0, "WRONG FINGERPRINT.", SDK_DISP_DEFAULT);
						sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRY AGAIN.", SDK_DISP_DEFAULT);
						
						sdkDispBrushScreen();
						sdkmSleep(2000);
						MainMenu();
						Trace("xgd", "Menu for\r\n");
					}
					else if (fingerprint.Error==NEWMAINMENU) 
					{
					
						DispClearContent();
						sdkDispFillRowRam(SDK_DISP_LINE2, 0, "WRONG FINGERPRINT.", SDK_DISP_DEFAULT);
						sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRY AGAIN.", SDK_DISP_DEFAULT);
						
						sdkDispBrushScreen();
						sdkmSleep(2000);
						NewMainMenu();
						Trace("xgd", "Menu for\r\n");
					}
					
				
				}
			}
			else if (ret == SDK_ERR)
			{
				DispClearContent();
				//sdkDispFillRowRam(SDK_DISP_LINE2, 0, "DEVICE ERROR", SDK_DISP_DEFAULT);
				//sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Try again", SDK_DISP_DEFAULT);
				sdkDispShowBmp(0,0,240,320,"/mtd0/res/fpfail.bmp");
				sdkDispBrushScreen();
				while (sdkFpcGetImage(bmp) == SDK_OK)
				{
					;
				}
				continue;
			}
			else
			{
				DispClearContent();
				//sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OBTAIN FAILED", SDK_DISP_DEFAULT);
				//sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Obtain again", SDK_DISP_DEFAULT);
				sdkDispShowBmp(0,0,240,320,"/mtd0/res/fpfail.bmp");
				sdkDispBrushScreen();
				while (sdkFpcGetImage(bmp) == SDK_OK)
				{
					;
				}
				continue;
			}
		}
	}
}
/*******************************************************************
版          权: 新国都
作          者: 赵江
函数名称:SelectFinger
函数功能:选择相应指纹去注册
输入参数:无
输出参数:const u8 *fingerimage: 手指类型
返   回  值:
修改备注:
日期时间:2014.03.14 16:44:31
*******************************************************************/
u32 SelectFinger(u8 *AllFingers, u8 *fingerimage)
{
	s32 TV_AUTORET = 10000;
	s32 key, rslt;
	u32 Temp_timer;
	u8 flag = 0;

	Temp_timer = sdkTimerGetId();

	if (AllFingers == NULL)
	{
		return false;
	}

	if (strcmp(AllFingers, "rthumb") == 0)
	{
		flag = 1;
	}
	else if (strcmp(AllFingers, "lindex") == 0)
	{
		flag = 2;

	}

	while (1)
	{
		if (sdkTimerIsEnd(Temp_timer, 120 * 1000))
		{
			sdkSysBeep(SDK_SYS_BEEP_OK);
			rslt = SDK_ESC;
			break;
		}
		sdkDispClearScreen();
		if (flag == 1)
		{
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "1.Left index", SDK_DISP_LEFT_DEFAULT);
		}
		else if (flag == 2)
		{
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "1.Right thumb", SDK_DISP_LEFT_DEFAULT);
		}
		else
		{
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/fingerprints.bmp");
			sdkDispBrushScreen();
		}
		sdkDispBrushScreen();
		key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, TV_AUTORET);

		if (key == SDK_KEY_1)
		{
			strcpy(fingerimage, "lindex");
			rslt = SDK_OK;
			break;
		}
		else if (key == SDK_KEY_2)
		{
			strcpy(fingerimage, "rthumb");
			rslt = SDK_OK;
			break;
		}
		else
		{
			rslt = SDK_ESC;
			break;
		}
	}
	return rslt;
}

u32 FingerPrintEigen(u8 *allfingers, u8 *FPCdata)
{
	u8 temp[512] = { 0 };
	s32 ret = 0, key = 0;
	s32 siTimer;

	u8 buf[256 * 3] = { 0 };
	char *bmp = NULL;

	bmp = sdkGetMem(30400 * sizeof(char));
	//sdkFpcReset(100);
	//ret = sdkFpcInit();                                                                                         //initialization
	siTimer = sdkTimerGetId();

	while (1)
	{

		key = sdkKbGetKey();
		if (key != SDK_KEY_ERR)
		{
			if (key == SDK_KEY_ESC)
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);

				sdkFreeMem(bmp);
				return SDK_ESC;
			}
			else
			{
				sdkSysBeep(SDK_SYS_BEEP_ERR);
			}
		}
		if (sdkTimerIsEnd(siTimer, 60 * 1000))//Timer-out 60s
		{
			sdkSysBeep(SDK_SYS_BEEP_OK);
			sdkFreeMem(bmp);
			return SDK_ESC;
		}

		memset(temp, 0, sizeof(temp));
		memset(buf, 0, sizeof(buf));
		sdkDispClearScreen();
		sdkDispShowBmp(50, 135, 170, 150, "/mtd0/res/fingerprint.bmp");
		sdkDispBrushScreen();


		if (sdkFpcGetImage(bmp) == SDK_OK)   // check if FPC data exist
		{
			ret = sdkFpcEnroll(bmp, SDK_FPC_ISO, buf);
			if (ret == SDK_OK)
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				memcpy(FPCdata, buf, 256);
				sdkDispClearScreen();
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OBTAIN SUCCESS", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Move finger away", SDK_DISP_DEFAULT);
				
				sdkDispBrushScreen();

				while (sdkFpcGetImage(bmp) == SDK_OK)
				{
					;
				}
				sdkFreeMem(bmp);
				return SDK_OK;
			}
			else
			{
				sdkDispClearScreen();
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OBTAIN FAILED", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Move finger away", SDK_DISP_DEFAULT);
				sdkDispBrushScreen();
				while (sdkFpcGetImage(bmp) == SDK_OK)
				{
					;
				}
				sdkDispClearScreen();
				continue;
			}
		}

	}

	
}


