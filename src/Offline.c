/*#include "../inc/global.h"
const u8 aeon[] = "/mtd0/res/aeon.txt";
void LoandConfirm(void);
void LoandID(void);
void LoandAmt (void);
void LoandName(void);
void LoandPhNum(void);
void LoandID(void)
{
	
	s32 key=0;
	u8 loandID [128] = {0};
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"ENTER LOAND ID ",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	memset(loandID,0,sizeof(loandID));
	 key =  sdkKbGetScanf(60000,loandID,4,25,SDK_MMI_LETTER,SDK_DISP_LINE3);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				Trace("OfflineID", "LoandID= %s\r\n",LoandID);
				memset(sendData.asAeonAgreeNo,0,sizeof(sendData.asAeonAgreeNo));
				memcpy(sendData.asAeonAgreeNo,&loandID[1],loandID[0]);
				Trace("OfflineID", "LoandId= %s\r\n",loandID);
				LoandAmt();
			}
			break;
		case SDK_KEY_ESC:        
			{
				AppMain();
			}
			break;
		default:
			{
				
			}	
	}

	AppMain();
	
}
void LoandAmt (void)
{
	u8 buf[16] ={0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"ENTER LOAND AMOUNT",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetAmountNumber(30*1000,buf,SDK_DISP_LINE3,3,8, NULL," KS");
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("LOANDAMOUNT", "buf= %s\r\n", buf);
				memset(sendData.asAmount,0,sizeof(sendData.asAmount));
				memcpy(sendData.asAmount,&buf[1],buf[0]);
				Trace("LOANDAMOUNT", "sendData.asAmount= %s\r\n",sendData.asAmount);
				LoandPhNum();
			}
			break;
		case SDK_KEY_ESC:
			{
				LoandID();
			}
			break;	
			default:
			{
					
			}	
	}
	LoandID();
}
void LoandPhNum(void)
{
	u8 buf[16] = {0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"ENTER Phone Number",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	switch(res)
	{	
		case SDK_KEY_ENTER:
			{
				Trace("LoandPhone","buf=%s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("LoandPhone","sendData.asPhoneNum=%s\r\n",sendData.asPhoneNum);
				LoandName();
			}
			break;
		case SDK_KEY_ESC:
			{
				LoandAmt();
			}
			break;
			default:
			{
				
			}	
		
	}
	LoandAmt();
	
}
void LoandName(void)
{
	u8 buf[16] = {0};
	s32 res;
	char src[32] = {0};

	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"ENTER Name",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	memset(buf,0,sizeof(buf));
	res = sdkKbGetScanf(60000, buf, 1, 20, SDK_MMI_LETTER ,SDK_DISP_LINE4);
	switch(res)
	 {
 		 case SDK_KEY_ENTER:
  			 {
				    Trace("LoandName", "buf= %s\r\n", buf);
				    Trace("LoandName","len=%d\r\n",strlen(buf));
				    memset(sendData.asName,0,sizeof(sendData.asName));
				
				    memcpy(sendData.asName,&buf[1],buf[0]);
				    Trace("LoandName","sendData.asName=%s\r\n",sendData.asName);
					LoandConfirm();
				   
  			 }
			break;
		case SDK_KEY_ESC:
			{
				LoandPhNum();
			}
			break;
			default:
			{
				
			}	
	}
	LoandPhNum();
}
void LoandConfirm(void)
{
	s32 key=0;
	u8 buf[64] = {0};
	
	u8 buf2[64] = {0};
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS%s",sendData.asAmount);
	
	memset(buf2,0,sizeof(buf2));
	sprintf(buf2,"09%s",sendData.asPhoneNum);
	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"LOAND ID:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE1,0,sendData.asAeonAgreeNo,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"AMOUNT:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,buf,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"PHONE:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,buf2,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4,0,"NAME:",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4,0,sendData.asName,SDK_DISP_RIGHT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	
	sdkDispBrushScreen();

	key = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,KbWaitTime);

	switch(key)
	{
		case  SDK_KEY_ENTER:
		{
		
			SaveData();
		}
		break;
		case SDK_KEY_ESC:
		{
			//HelloCabAmount();
		}
		break;
		default:
		{
			//BillPayMainMenu();	
		}	
		
	}
	
}
void SaveData(void)
{
	Trace("SaveData","test\r\n");
	u8 buf[128]={0};
	u8 buf1[128]={0};
	u8 buf2[128]={0};
	u8 buf3[128]={0};
	 int len ; 
	 int len2;
	u8 temp;
	if(sdkAccessFile(aeon))
	{
		Trace("SaveData","Aeon.txt file exit.");
		memset(buf3,0,sizeof(buf3));
		memset(buf2,0,sizeof(buf2));
		sprintf(buf2, "%s/%s/%s/%s",sendData.asAeonAgreeNo,sendData.asAmount,sendData.asPhoneNum,sendData.asName);
		Trace("SaveData","buf2=%s",buf2);
		len=strlen(buf2);
		
	
		Trace ("SaveData","length=%d\r\n",len);
		
		temp=sdkAppendFile(aeon,buf2,len);
			if(temp==SDK_FILE_OK)
			{
				//Trace("SaveData","buf2 = %s, len = %d\r\n", buf2, len);
				Trace("Save Data","Append File success");
				
				temp=sdkReadFile (aeon, buf3,0,&len2);
				if(temp==SDK_FILE_OK) 
				{
					Trace("SaveData", "buf3 = %s, len2 = %d\r\n", buf3, len2);
				}
				else
				{
					Trace("Save Data","Read fail");
				}
				 
				
			}
		
		else
		{
			Trace("Save Data","Append  file");
		}
			
			
	}
	else
	{
		memset(buf,0,sizeof(buf));
		sprintf(buf, "%s/%s/%s/%s",sendData.asAeonAgreeNo,sendData.asAmount,sendData.asPhoneNum,sendData.asName);
		Trace("SaveData","buf=%s",buf);
		len=strlen(buf);
	
		Trace ("SaveData","length=%d\r\n",strlen(buf));
		
		Trace("SaveData","Aeon.txt not exit");
		temp=sdkWriteFile(aeon,buf,len); 
		if (temp==SDK_FILE_OK)
		{
			Trace ("SaveData","Write Success\r\n");
			 
			sdkReadFile (aeon, buf1,0,&len); 
			Trace("SaveData", "buf1 = %s, len = %d\r\n", buf1, len); 
		}
		
		
	}
	

	
	AppMain();

	 //sdkInsertFile ("test.txt", "abc", 0, 3);     
	 //test.txt will have �abc456789� finally. 
}
void Deleteoffline(void)
{
	if(sdkAccessFile(aeon))
	{
		Trace("Deleteoffline","Aeon.txt exit");
		sdkDelFile("/mtd0/res/aeon.txt");
		Trace("Deleteoffline","Delete success");
	}
	else 
	{
		Trace("Deleteofflin","Aeon.txt file not exit");		
	}
	AppMain();
	
}*/
