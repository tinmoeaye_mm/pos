/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : http.c
   Version       : Initial Draft
   Author        : xiaokai
   Created       : 2015/7/24
   Last Modified :
   Description   : Get data by the way of https. 
   History       :
   1.Date        : 2015/7/24
    Author      : xiaokai 
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"
//#include "../inc/openssl/sha.h"
unsigned char recbuf[MAXREC];
int send_mess(char * buffer);

int send_mess_post(char * url,char * buffer);

/*******************************************************************************
** Description :  Get token
** Parameter   :  void
** Return      :  
** Author      :  ThuKha   2015-11-30 
** Remark      :  
*******************************************************************************/
void ModuleGetToken(void)

{
    s32 ret = SDK_ERR;
    u8 recbuf[100 *1024] = {0};

    u8 sendbuf[1024] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len;
	u8 access_token[256] = {0};
    memset(sendbuf, 0, sizeof(sendbuf));
	strcpy(sendbuf,"grant_type=password&client_id=posClient&client_secret=true_secret&username=truemoneypos&password=P@55w0rd");
    SDK_EXT_CURL_DATA curl_data = {
          .bMethod = false,                            // POST method
		  .psURL = "http://192.168.1.10:8888/oauth/token"  ,
          .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
          .nPort = 8888,                               // HTTP default port          
          .psSend = sendbuf,                           // POST data
          .nSendLen = strlen(sendbuf),
          .psRecv = recbuf,                            // receive buffer
          .nRecvLen = sizeof(recbuf),  
          .nTimeout = 30*1000,                         // time out          
    };


    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "GETTING TOKEN...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen(); 
    
    memset(recbuf, 0, sizeof(recbuf));   
    Trace("xgd","sdkExtCurl before curl_data.nRecvLen = %d\r\n", curl_data.nRecvLen);
    // Start exchanging data through HTTPS protocol
    ret =  sdkExtCurl(&curl_data);
    Trace("xgd","sdkExtCurl ret = %d\r\n",ret);
    if(ret != SDK_OK)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
        return;
    }

    // Output debug info
    Trace("xgd", ">>>> Tatal length = %d\r\n", curl_data.nRecvLen);
    len = curl_data.nRecvLen > 128 ? 128 : curl_data.nRecvLen;
    memcpy(tracebuf, curl_data.psRecv, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);
	 
    // Display response message
    sdkDispClearScreen();
	

	sdkDispFillRowRam(SDK_DISP_LINE4, 0, access_token, SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5, 0, recbuf, SDK_DISP_LEFT_DEFAULT);
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);
}



/*******************************************************************************
** Description :  A demo on how to exchange message through HTTP
** Parameter   :  void
** Return      :  
** Author      :  xiaokai   2015-10-08 
** Remark      :  
*******************************************************************************/
void ModuleHttpGreetingGET(void)

{
    
    s32 ret = SDK_ERR;
    
    u8 sendbuf[1024] = {0};
    u8 dispbuf[32+1] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len, len1;
    s32 i;

    memset(sendbuf, 0, sizeof(sendbuf));
    //strcpy(sendbuf, "auth_code=130169872238415019&device_info=G8703K00051&fee_type=CNY&nonce_str=811B10D2FEEDDDC865F402E8949476A6&total_fee=1&sign=C7356659ADE51470D15313B341D79C31");
	strcpy(sendbuf,"access_token=f330bfe8-a29f-4d25-96f3-2332bcf12585");
    SDK_EXT_CURL_DATA curl_data = {
          .bMethod = true,                            // GET method
		  .psURL = "http://192.168.1.10/greeting",
          .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
          .nPort = 8888,                                 // HTTP default port          
          .psSend = sendbuf,                           // POST data
          .nSendLen = strlen(sendbuf),
          .psRecv = recbuf,                            // receive buffer
          .nRecvLen = sizeof(recbuf),  
          .nTimeout = 30*1000,                         // time out          
    };


    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "EXCHANGING MSG...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen(); 
    
    memset(recbuf, 0, sizeof(recbuf));   
    Trace("xgd","sdkExtCurl before curl_data.nRecvLen = %d\r\n", curl_data.nRecvLen);
    // Start exchanging data through HTTPS protocol
    ret =  sdkExtCurl(&curl_data);
    Trace("xgd","sdkExtCurl ret = %d\r\n",ret);
    if(ret != SDK_OK)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
        return;
    }

    // Output debug info
    Trace("xgd", ">>>> Tatal length = %d\r\n", curl_data.nRecvLen);
    len = curl_data.nRecvLen > 128 ? 128 : curl_data.nRecvLen;
    memcpy(tracebuf, curl_data.psRecv, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);

    // Display response message
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESPONSE:", SDK_DISP_LEFT_DEFAULT);
    for (i = 0; i < 4 && len > 0; i++)
    {
        len1 = len > 20 ? 20 : len;
        len -= len1;
        memcpy(dispbuf, &tracebuf[20*i], len1);
        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
    }
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);
}

void ModuleHttpGreetingPOST(void)

{
    
    s32 ret = SDK_ERR;
    u8 recbuf[100 *1024] = {0};

    u8 sendbuf[1024] = {0};
    u8 dispbuf[32+1] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len, len1;
    s32 i;

    memset(sendbuf, 0, sizeof(sendbuf));
	strcpy(sendbuf,"access_token=f330bfe8-a29f-4d25-96f3-2332bcf12585&name=thukha");
    SDK_EXT_CURL_DATA curl_data = {
          .bMethod = false,                            // POST method
		  .psURL = "http://192.168.1.10/greeting1"  ,
          .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
          .nPort = 8888,                                 // HTTP default port          
          .psSend = sendbuf,                           // POST data
          .nSendLen = strlen(sendbuf),
          .psRecv = recbuf,                            // receive buffer
          .nRecvLen = sizeof(recbuf),  
          .nTimeout = 30*1000,                         // time out          
    };


    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "EXCHANGING MSG...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen(); 
    
    memset(recbuf, 0, sizeof(recbuf));   
    Trace("xgd","sdkExtCurl before curl_data.nRecvLen = %d\r\n", curl_data.nRecvLen);
    // Start exchanging data through HTTPS protocol
    ret =  sdkExtCurl(&curl_data);
    Trace("xgd","sdkExtCurl ret = %d\r\n",ret);
    if(ret != SDK_OK)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
        return;
    }

    // Output debug info
    Trace("xgd", ">>>> Tatal length = %d\r\n", curl_data.nRecvLen);
    len = curl_data.nRecvLen > 128 ? 128 : curl_data.nRecvLen;
    memcpy(tracebuf, curl_data.psRecv, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);

    // Display response message
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESPONSE:", SDK_DISP_LEFT_DEFAULT);
    for (i = 0; i < 4 && len > 0; i++)
    {
        len1 = len > 20 ? 20 : len;
        len -= len1;
        memcpy(dispbuf, &tracebuf[20*i], len1);
        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
    }
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);
}

void ModuleTerminalVersionChange(void)

{
    
    s32 ret = SDK_ERR;
    u8 recbuf[100 *1024] = {0};

    u8 sendbuf[1024] = {0};
    u8 dispbuf[32+1] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len, len1;
    s32 i;
	
    memset(sendbuf, 0, sizeof(sendbuf));
	//strcpy(sendbuf,"");
    SDK_EXT_CURL_DATA curl_data = {
          .bMethod = true,                            // GET method
		  .psURL = "http://122.248.120.162/terVersionChange/G8703S02052/2-1"  ,
          .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
          .nPort = 8785,                                 // HTTP default port          
          .psSend = sendbuf,                           // POST data
          .nSendLen = strlen(sendbuf),
          .psRecv = recbuf,                            // receive buffer
          .nRecvLen = sizeof(recbuf),  
          .nTimeout = 30*1000,                         // time out          
    };

    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "EXCHANGING MSG...", SDK_DISP_DEFAULT);

	sdkmSleep(1000);
	sdkDispBrushScreen(); 
    
    memset(recbuf, 0, sizeof(recbuf));   
    Trace("xgd","sdkExtCurl before curl_data.nRecvLen = %d\r\n", curl_data.nRecvLen);
    // Start exchanging data through HTTPS protocol
    ret =  sdkExtCurl(&curl_data);
    Trace("xgd","sdkExtCurl ret = %d\r\n",ret);
    if(ret != SDK_OK)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
        return;
    }

    // Output debug info
    Trace("xgd", ">>>> Tatal length = %d\r\n", curl_data.nRecvLen);
    len = curl_data.nRecvLen > 128 ? 128 : curl_data.nRecvLen;
    memcpy(tracebuf, curl_data.psRecv, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);

    // Display response message
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESPONSE:", SDK_DISP_LEFT_DEFAULT);
    for (i = 0; i < 4 && len > 0; i++)
    {
        len1 = len > 20 ? 20 : len;
        len -= len1;
        memcpy(dispbuf, &tracebuf[20*i], len1);
        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
    }
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);
}


void ModuleAgentCheckBalanceV1(void)

{
    
    s32 ret = SDK_ERR;
    u8 recbuf[100 *1024] = {0};

    u8 sendbuf[1024] = {0};
    u8 dispbuf[32+1] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len, len1;
    s32 i;

    memset(sendbuf, 0, sizeof(sendbuf));
	strcpy(sendbuf,"name=thukha");
    SDK_EXT_CURL_DATA curl_data = {
          .bMethod = true,                            // GET method
		  .psURL = "http://122.248.120.162/agentChkBalance/100000004/G8703S02052/0/0/"  ,
          .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
          .nPort = 8785,                                 // HTTP default port          
          .psSend = sendbuf,                           // POST data
          .nSendLen = strlen(sendbuf),
          .psRecv = recbuf,                            // receive buffer
          .nRecvLen = sizeof(recbuf),  
          .nTimeout = 30*1000,                         // time out          
    };

    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "EXCHANGING MSG...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen(); 
    
    memset(recbuf, 0, sizeof(recbuf));   
    Trace("xgd","sdkExtCurl before curl_data.nRecvLen = %d\r\n", curl_data.nRecvLen);
    // Start exchanging data through HTTPS protocol
    ret =  sdkExtCurl(&curl_data);
    Trace("xgd","sdkExtCurl ret = %d\r\n",ret);
    if(ret != SDK_OK)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
        return;
    }

    // Output debug info
    Trace("xgd", ">>>> Tatal length = %d\r\n", curl_data.nRecvLen);
    len = curl_data.nRecvLen > 128 ? 128 : curl_data.nRecvLen;
    memcpy(tracebuf, curl_data.psRecv, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);

    // Display response message
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESPONSE:", SDK_DISP_LEFT_DEFAULT);
    for (i = 0; i < 4 && len > 0; i++)
    {
        len1 = len > 20 ? 20 : len;
        len -= len1;
        memcpy(dispbuf, &tracebuf[20*i], len1);
        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
    }
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);
}


void ModuleAgentCheckBalanceV2(void)

{
    
    s32 ret = SDK_ERR;
    u8 recbuf[100 *1024] = {0};

    u8 sendbuf[1024] = {0};
    u8 dispbuf[32+1] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len, len1;
    s32 i;
	
	u8 uUrl[128] = "http://122.248.120.162/agentChkBalance/v2/100000004/G8703S02052/0/0/";

	u8 message[128] = "100000004G8703S0205200";
	u8 tsk[64] = "TRuEm0n3yTe8tH@sh";
	u8 out[64] = {0};
	
	sdkCalcHmacSha256(tsk, strlen(tsk), message, strlen(message), out);

	memset(tsk, 0, 64);
	sdkBcdToAsc(tsk, &out[0], 32);
	sprintf(sendbuf,"%s%s",uUrl,tsk);	

	Trace("xgd","sendbuf = %s\r\n",sendbuf);

	
    SDK_EXT_CURL_DATA curl_data = {
          .bMethod = true,                            // GET method
		  .psURL = sendbuf  ,
          .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
          .nPort = 8785,                                 // HTTP default port          
          .psSend = sendbuf,                           // POST data
          .nSendLen = strlen(sendbuf),
          .psRecv = recbuf,                            // receive buffer
          .nRecvLen = sizeof(recbuf),  
          .nTimeout = 30*1000,                         // time out          
    };

	sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "EXCHANGING MSG...", SDK_DISP_DEFAULT);

	sdkmSleep(1000);
	sdkDispBrushScreen(); 
    
    memset(recbuf, 0, sizeof(recbuf));   
    Trace("xgd","sdkExtCurl before curl_data.nRecvLen = %d\r\n", curl_data.nRecvLen);
    // Start exchanging data through HTTPS protocol
    ret =  sdkExtCurl(&curl_data);
    Trace("xgd","sdkExtCurl ret = %d\r\n",ret);
    if(ret != SDK_OK)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
        return;
    }

    // Output debug info
    Trace("xgd", ">>>> Total length = %d\r\n", curl_data.nRecvLen);
    len = curl_data.nRecvLen > 128 ? 128 : curl_data.nRecvLen;
    memcpy(tracebuf, curl_data.psRecv, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);

    // Display response message
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESPONSE:", SDK_DISP_LEFT_DEFAULT);
    for (i = 0; i < 4 && len > 0; i++)
    {
        len1 = len > 20 ? 20 : len;
        len -= len1;
        memcpy(dispbuf, &tracebuf[20*i], len1);
        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
    }
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);
}

//char ServiceStatus[128];
int SendReceivedData(char * sendbuf,bool tcp)

{

	memset(recbuf, 0, sizeof(recbuf));

	int ret = 0;
	if(tcp)
	{
		Trace("xgd","TCP");
	}
	else
	{
	
		ret= send_mess(sendbuf);
	}
	if(ret == SDK_OK)
	{
			memset(sendData.asSendBuf, 0, sizeof(sendData.asSendBuf));
	
		json_parse((char *)recbuf);
	}

	return ret;
	
}

int send_mess(char * buffer) {

	Trace("xgd","sendbuf = %s\r\n",buffer);
	s32 ret = SDK_ERR;

	 
	 
	 	SDK_EXT_CURL_DATA curl_data = {
          .bMethod = true,                            // GET method
		  .psURL = buffer  ,
          .psCaFile = NULL,
		  .nPort = Port,                              // HTTP default port          
          .psSend = buffer,                           // POST data
          .nSendLen = strlen(buffer),
          .psRecv = recbuf,                           // receive buffer
          .nRecvLen = sizeof(recbuf),  
          .nTimeout = 150*1000,                        // time out  
		  };
		                 
	 
	
    

	ret =  sdkExtCurl(&curl_data);


	
	Trace("xgd","recbuf = %s\r\n",recbuf);

	return ret;
	
}
int SendReceivedData_POST(char * sendurl,char * sendbuf,bool tcp)

{
	
	memset(recbuf, 0, sizeof(recbuf));

	int ret = 0;
	if(tcp)
	{
		Trace("xgd","TCP");
	}
	else
	{
	
		ret= send_mess_post(sendurl,sendbuf);
	}
	if(ret == SDK_OK)
	{
			memset(sendData.asSendBuf, 0, sizeof(sendData.asSendBuf));
	
		json_parse((char *)recbuf);
	}

	return ret;
	
}
int send_mess_post(char * url,char * buffer) {

	Trace("xgd","url = %s\r\n",url);
	Trace("xgd","sendbuf = %s\r\n",buffer);
	s32 ret = SDK_ERR;

	 
	 
	 	SDK_EXT_CURL_DATA curl_data = {
          .bMethod = false,                            // POST method
		  .psURL = url  ,
          .psCaFile = NULL,
		  .nPort = Port,                              // HTTP default port          
          .psSend = buffer,                           // POST data
          .nSendLen = strlen(buffer),
          .psRecv = recbuf,                           // receive buffer
          .nRecvLen = sizeof(recbuf),  
          .nTimeout = 150*1000,                        // time out  
		  };
		                 
	 
	
    

	ret =  sdkExtCurl(&curl_data);


	
	Trace("xgd","recbuf = %s\r\n",recbuf);

	return ret;
	
}


