	
#include "../inc/global.h"
bool IsTouchScreenModel();
typedef struct {
    SDK_DISP_SRECT rect;
    s32 value;
    u8 *name;
} ST_KEY_MAP;

static s32 TsDrawKeyUIFun(const SDK_DISP_SRECT *pstRect, u8 *ucKeyName, SDK_TS_DRAW_KEY *pDrawInfo);


/*******************************************************************************
** Description :  Touch screen demo routine
** Parameter   :  void
** Return      :  
** Author      :  Cody   2015-09-29 
** Remark      :  
*******************************************************************************/
void ModuleTouchScreen(void)
{
    bool bis_touch;
    s32 key;
     ST_KEY_MAP ast_keymap870[] =
    {
         {{0, 64, 239, 127}, SDK_KEY_1, "ADD KEY MAP"},
         {{0, 128, 239, 191}, SDK_KEY_2, "HANDWRITE"},
         {{0, 192, 239, 255}, SDK_KEY_3, "MESSAGE BOX"},
         {{0, 256, 239, 319}, SDK_KEY_4, "4th Item"}
     };
     ST_KEY_MAP ast_keymapg3[] =
     {
       /* {{0, 48, 319, 95}, SDK_KEY_1, "ADD KEY MAP"},
         {{0, 96, 319, 143}, SDK_KEY_2, "HANDWRITE"},
         {{0, 144, 319, 191}, SDK_KEY_3, "MESSAGE BOX"},
        {{0, 192, 319, 239}, SDK_KEY_4, "4th Item"}*/
    };
    ST_KEY_MAP *pst_keymap = NULL;
    s32 num_ent, i;
    u8 model[8] = {0};
    s32 ret;
    SDK_TS_DRAW_KEY st_drawinfo =
    {
        SDK_DISP_WHITE_COLOR,   // Rectangle color
        SDK_DISP_BLACK_COLOR,   // Border color
        SDK_DISP_BLACK_COLOR,   // Text color
        "",
        NULL,
    };
   SDK_DISP_SRECT rect1, rect2;
    SDK_DISP_PIXEL pix = sdkDispGetScreenPixel();
    s32 fh = sdkDispGetFontSize();
    u8 buf[128] = {0};
    u8 msgbox_img[] = "/mtd0/res/msgbox.bmp";
    s32 font_color = sdkDispGetFontColor();

    
    // Display header
    sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1, 0, "TOUCH SCREEN", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
    sdkDispBrushScreen();

    
    // Check if touch screen is supported for this model
	bis_touch = IsTouchScreenModel();

    // If TS is not supported, return from this function
    if (!bis_touch)
    {
        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "NOT SUPPORTED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        
        sdkKbWaitKey(SDK_KEY_MASK_ESC | SDK_KEY_MASK_ENTER, 3000);
        return;
    }

    // Open TS device
    sdkTsOpen();

    // Ask user if to adjust screen now
    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ADJUST SCREEN NOW?", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    key = sdkKbWaitKey(SDK_KEY_MASK_ESC | SDK_KEY_MASK_ENTER, 3000);
    if (key == SDK_KEY_ENTER)
    { 
        // Adjust screen
        sdkTsAdjust();
        
        sdkKbKeyFlush();
    }

    // Check model and initialize key map info
    sdkSysGetMachineModel(model);
    if (!strcmp(model, "G870"))
    {
        pst_keymap = ast_keymap870;
        num_ent = sizeof(ast_keymap870) / sizeof(ast_keymap870[0]);
        
        //Trace("Touchscreen","num_ent=%s\r\n",num_ent);
    }
    else if (!strcmp(model, "G3"))
    {
        pst_keymap = ast_keymapg3;
        num_ent = sizeof(ast_keymap870) / sizeof(ast_keymapg3[0]);
         //Trace("Touchscreen","num_ent=%s\r\n",num_ent);
    }
    else    // TS is not supported for this model
    {
        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "NOT SUPPORTED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        
        sdkKbWaitKey(SDK_KEY_MASK_ESC | SDK_KEY_MASK_ENTER, 3000);
        // Close TS device
        sdkTsClose();
        return;
    }

    // Clear content area
    DispClearContent();
    
    // Map key to screen area
    for (i = 0; i < num_ent; i++)
    {
    	 //Trace("Touchscreen","num_ent=%s\r\n",num_ent);
        ret = sdkTsAddUIKey(&pst_keymap[i].rect, pst_keymap[i].value, pst_keymap[i].name, &st_drawinfo, TsDrawKeyUIFun);
         Trace("Touchscreen","ret=%s\r\n",ret);
        if (ret != SDK_OK)
        {
            Trace("cody", "sdkTsAddUIKey() = %d\r\n", ret);
            DispClearContent();
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, "KEY MAP ERROR!", SDK_DISP_DEFAULT);
            sdkDispBrushScreen();
            sdkKbWaitKey(SDK_KEY_MASK_ESC | SDK_KEY_MASK_ENTER,3000);
            // Close TS device
            sdkTsClose();
            return;
        }
    }
    sdkDispBrushScreen();

    // Wait for user to select item or quit
    while (1)
    {
        // Check if user pressed screen to select an item
        key = sdkTsGetKeyValue();
        if (key <= 0)
            continue;
        else if (key == SDK_KEY_ESC)  // Check if user pressed CANCEL to exit
            break;

        // Free key map
        sdkTsFlush();
        sdkTsFreeKeyMap();


        switch (key)
        {
            case SDK_KEY_1:
                rect1.x0 = 0, rect1.y0 = pix.siY - 64;
                rect1.x1 = pix.siX/2 - 1, rect1.y1 = pix.siY - 1;
                rect2.x0 = pix.siX/2, rect2.y0 = pix.siY - 64;
                rect2.x1 = pix.siX - 1, rect2.y1 = pix.siY - 1;

                DispClearContent();
                // Draw button CANCEL
                sdkDispSetRectColor(&rect1, SDK_DISP_RED_COLOR);
                sdkDispDrawLine(rect1.x1, rect1.y0, rect1.x1, rect1.y1, SDK_DISP_BLACK_COLOR);
                sdkDispDrawLine(rect1.x0, rect1.y1, rect1.x1, rect1.y1, SDK_DISP_BLACK_COLOR);
                sdkDispSetFontColor(SDK_DISP_WHITE_COLOR);
                sdkDispAt(rect1.x0+3, rect2.y0+(64-fh)/2, "CANCEL");
                // Draw button ENTER
                sdkDispSetRectColor(&rect2, SDK_DISP_GREEN_COLOR);
                sdkDispDrawLine(rect2.x1, rect2.y0, rect2.x1, rect2.y1, SDK_DISP_BLACK_COLOR);
                sdkDispDrawLine(rect2.x0, rect2.y1, rect2.x1, rect2.y1, SDK_DISP_BLACK_COLOR);
                sdkDispSetFontColor(SDK_DISP_WHITE_COLOR);
                sdkDispAt(rect2.x0+3, rect2.y0+(64-fh)/2, "ENTER");
                sdkDispBrushScreen();
                sdkDispSetFontColor(font_color);

                // Add key map
                sdkTsAddKeyMap(&rect1, SDK_KEY_ESC);
                sdkTsAddKeyMap(&rect2, SDK_KEY_ENTER);

                // Wait for user to press button
                while (1)
                {
                    key = sdkTsGetKeyValue();
                    if (key > 0)
                        break;
                }

                // Flush key
                sdkTsFlush();

                // Remove added key map
                sdkTsDelKeyMap(SDK_KEY_ESC);
                sdkTsDelKeyMap(SDK_KEY_ENTER);

                break;

            case SDK_KEY_2:
                sdkDispClearScreen();
                
                rect1.x0 = 0, rect1.y0 = 64;
                rect1.x1 = pix.siX - 1, rect1.y1 = 128;

                memset(buf, 0, sizeof(buf));
                ret = sdkTsGetScanf(30*1000, buf, 1, 16, SDK_MMI_HAND | SDK_MMI_LETTER, &rect1);
                Trace("cody", "sdkTsGetScanf()=%d\r\n", ret);
                Trace("cody", "Inputed text: %s\r\n", &buf[1]);

                sdkSysMultiLangInit(SDK_SYS_LANG_CHN, 5);

                break;  

            case SDK_KEY_3:
                DispClearContent();

                // Check if background file exists
                if (sdkAccessFile(msgbox_img) == false)
                {
                    sdkDispFillRowRam(SDK_DISP_LINE3, 0, msgbox_img, SDK_DISP_DEFAULT);
                    sdkDispFillRowRam(SDK_DISP_LINE4, 0, "FILE NOT EXIST!", SDK_DISP_DEFAULT);
                    sdkDispBrushScreen();
                    sdkKbWaitKey(SDK_KEY_MASK_ESC | SDK_KEY_MASK_ENTER, 3000);
                    break;
                }

                // Display message box
                ret = sdkTsMessageBox("Fatal error!", msgbox_img, NULL, 0);
                Trace("cody", "sdkTsMessageBox()=%d\r\n", ret);

                sdkSysMultiLangInit(SDK_SYS_LANG_CHN, 5);

                break;

            default:
                break;
        }

        sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "TOUCH SCREEN", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
        sdkDispBrushScreen();
        
        // Map key to screen area
        for (i = 0; i < num_ent; i++)
        {
            ret = sdkTsAddUIKey(&pst_keymap[i].rect, pst_keymap[i].value, pst_keymap[i].name, &st_drawinfo, TsDrawKeyUIFun);
            if (ret != SDK_OK)
            {
                Trace("cody", "sdkTsAddUIKey() = %d\r\n", ret);
                DispClearContent();
                sdkDispFillRowRam(SDK_DISP_LINE3, 0, "KEY MAP ERROR!", SDK_DISP_DEFAULT);
                sdkDispBrushScreen();
                sdkKbWaitKey(SDK_KEY_MASK_ESC | SDK_KEY_MASK_ENTER, 3000);
                // Close TS device
                sdkTsClose();
                return;
            }
        }
        sdkDispBrushScreen();
    }

    // Close TS device
    sdkTsClose();
}


/*******************************************************************************
** Description :  Determine if touch screen is supported for a specific model
** Parameter   :  
** Return      :    TRUE /FALSE
** Author      :  2015-07-17
** Remark      :
*******************************************************************************/
bool IsTouchScreenModel()
{
	u8 model[8];
	s32 ret;
	
	memset(model,0,sizeof(8));
	ret = sdkSysGetMachineModel(model);
    Trace("xk","IsTouchScreenModel model = %s\r\n",model);
	if((0 != memcmp(model,"G870",4)
		&& 0 != memcmp(model,"G3",2))	
		|| (ret <= 0))
	{
		return false;
	}

	return true;		
}

/*******************************************************************************
** Description :  Callback for key UI drawing
** Parameter   :  const SDK_DISP_SRECT *pstRect
                  u8 *ucKeyName
                  SDK_TS_DRAW_KEY *pDrawInfo
** Return      :  
** Author      :  Cody   2015-10-09 
** Remark      :  
*******************************************************************************/
static s32 TsDrawKeyUIFun(const SDK_DISP_SRECT *pstRect, u8 *ucKeyName, SDK_TS_DRAW_KEY *pDrawInfo)
{
    s32 font_color = sdkDispGetFontColor();
    s32 font_high = sdkDispGetFontSize();
    s32 disp_x = 0, disp_y = 0;
    s32 len = 0;

    // Draw button
    sdkDispClearAt(pstRect->x0, pstRect->y0, pstRect->x1 + 1, pstRect->y0 + 1);
    sdkDispSetRectColor(pstRect, pDrawInfo->uiRectColor);
    sdkDispDrawLine(pstRect->x0, pstRect->y0, pstRect->x1, pstRect->y0, pDrawInfo->uiBorderColor);
    sdkDispDrawLine(pstRect->x1, pstRect->y0, pstRect->x1, pstRect->y1, pDrawInfo->uiBorderColor);
    sdkDispDrawLine(pstRect->x0, pstRect->y0, pstRect->x0, pstRect->y1, pDrawInfo->uiBorderColor);
    sdkDispDrawLine(pstRect->x0, pstRect->y1, pstRect->x1, pstRect->y1, pDrawInfo->uiBorderColor);
    sdkDispDrawLine(pstRect->x0 + 1, pstRect->y0 + 1, pstRect->x1 - 1, pstRect->y0 - 1, pDrawInfo->uiBorderColor);
    sdkDispDrawLine(pstRect->x1 - 1, pstRect->y0 + 1, pstRect->x1 - 1, pstRect->y1 - 1, pDrawInfo->uiBorderColor);
    sdkDispDrawLine(pstRect->x0 + 1, pstRect->y0 + 1, pstRect->x0 + 1, pstRect->y1 - 1, pDrawInfo->uiBorderColor);
    sdkDispDrawLine(pstRect->x0 + 1, pstRect->y1 - 1, pstRect->x1 - 1, pstRect->y1 - 1, pDrawInfo->uiBorderColor);

    // Display key name
    if (font_high >= pstRect->y1 - pstRect->y0)
    {
        disp_y = pstRect->y0 - (font_high - (pstRect->y1 - pstRect->y0)) / 2;
        disp_y = (disp_y < 0 ? 0 : disp_y);
    }
    else
    {
        disp_y = pstRect->y0 + (pstRect->y1 - pstRect->y0 - font_high) / 2;
    }
    len = strlen(ucKeyName) * font_high / 2;
    if (len >= pstRect->x1 - pstRect->x0)
    {
        disp_x = pstRect->x0 - (len - (pstRect->x1 - pstRect->x0)) / 2;
        disp_x = (disp_x < 0 ? 0 : disp_x);
    }
    else
    {
        disp_x = pstRect->x0 + (pstRect->x1 - pstRect->x0 - len) / 2;
    }
    sdkDispSetFontColor(pDrawInfo->uiFontColor);
    sdkDispAt(disp_x, disp_y, ucKeyName);

    sdkDispSetFontColor(font_color);

    return SDK_OK;
}



