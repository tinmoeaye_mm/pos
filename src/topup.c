/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : agent.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/3/17
   Last Modified :
   Description   : agent module-related functions are defined
                  here
   History       :
   1.Date        : 2016/3/17
    Author      : Thu Kha, Tin Moe Aye
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"

int amount;
char topupamount[64];

void OperatorMenu(void);
void SelectAmountMenu(void);
void ModulePayCash(void);
void ModulePayment(void);
void TopUpModule(void);
void ForSendData(void);
void ModuleCustomerReadCardForTopUp(void);
void ModulePayTMMCard(void);
int SendReceivedDataForTopUp(char * sendbuf,bool tcp);
void SelectAmount_TelenorEloadMenu(void);
void Select_Eload_Epin(void);
void Select_Amount_ForTelenorEpin(void);
void TopUpModuleForTelenor(void);
void NewMainMenu(void);
int SendReceivedData_POST(char * sendurl,char * sendbuf,bool tcp);


void ChooseOperatorMenu(void)
{
	s32 key;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/topup.bmp");
	sdkDispBrushScreen();

	key=sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 |SDK_KEY_MASK_4|SDK_KEY_MASK_ESC, KbWaitTime);
	sdkDispClearScreen();
	switch(key)
	{
		case SDK_KEY_1:
			{	
				topup.MAINMenu=Old_Menu;//For pageagent checkbalance 	
				topup.Operator = MPT;
				sdkDispShowBmp(0,0,240,320,"/mtd0/res/mptmenu.bmp");
				sdkDispBrushScreen();
				SelectAmountMenu();
			}
			break;
		case SDK_KEY_2:
			{
				topup.MAINMenu=Old_Menu;
				topup.Operator=TELENOR;
				sdkDispShowBmp(0,0,240,320,"/mtd0/res/telenornewmenu.bmp");
				sdkDispBrushScreen();
				Select_Eload_Epin();
			}
			break;
		case SDK_KEY_3:
			{
				topup.MAINMenu=Old_Menu;
				topup.Operator=OOREDOO;
				sdkDispShowBmp(0,0,240,320,"/mtd0/res/odomenu.bmp");
				sdkDispBrushScreen();
				SelectAmountMenu();
			}
			break;
		case SDK_KEY_4:
			{	
				topup.MAINMenu=Old_Menu;
				topup.Operator=MECTEL;
				sdkDispShowBmp(0,0,240,320,"/mtd0/res/mecmenu.bmp");
				sdkDispBrushScreen();
				SelectAmountMenu();
			}
			break;
		case SDK_KEY_ESC:
		    {
		        MainMenu();
		    }
		default:
		    {
		    	
		    }
		    break;
	}
	
	 MainMenu();
}

void Select_Eload_Epin(void)
{
	s32 key;
	
	topup.Operator=TELENOR;
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/telenornewmenu.bmp");
	sdkDispBrushScreen();
	secondtransactionstart=false;  // for pageagentcheckbalance
	key=sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 |SDK_KEY_MASK_ESC,KbWaitTime);
	sdkDispClearScreen();
	switch(key)
	{
		case SDK_KEY_1:
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/telenorEload.bmp");
				sdkDispBrushScreen();
				topup.TeleMenu = TelenorMenu;
				topup.TopUpType =TOPUP_ELOAD;
				SelectAmount_TelenorEloadMenu();	
			}
			break;
		case SDK_KEY_2:
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/telenorEpin.bmp");
				sdkDispBrushScreen();
				topup.TeleMenu = TelenorMenu;
				topup.TopUpType =TOPUP_EPIN;
				Select_Amount_ForTelenorEpin();
			}
			break;
		case SDK_KEY_ESC://For return menu
			{
				if(topup.MAINMenu==New_Menu)
				{
	        		 
	           		NewMainMenu();
				}
				else
				{
					
				
				MainMenu();
				}
	        									
			}
	       break;
	        			
	}
	
	if(topup.MAINMenu==New_Menu)//For return menu
	{
	        		 
	    NewMainMenu();
	}
	else
	{
					
				
		MainMenu();
	}
		
}
void Select_Amount_ForTelenorEpin(void)	
{
	s32 key;
	key=sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 |SDK_KEY_MASK_4|SDK_KEY_MASK_5|SDK_KEY_MASK_ESC, KbWaitTime);
	secondtransactionstart=false; // for pageagentcheckbalance
	memset(&sendData.asTopUpAmount,0,sizeof(sendData.asTopUpAmount));
	switch(key)
	{	
		case SDK_KEY_1:
			{	
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"1");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 1000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				ModulePayment();
			}
			break;
		case SDK_KEY_2:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"2");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 3000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				ModulePayment();
			}
			break;
		case SDK_KEY_3:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"3");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 5000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				ModulePayment();
			}
			break;
		case SDK_KEY_4:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"4");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 6000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				ModulePayment();
			}
			break;
		case SDK_KEY_5:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"5");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 10000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				ModulePayment();
			}
			break;
				case SDK_KEY_ESC:
	        {
	        	
					sendData.asTopUpAmount = 0;
					if(topup.MAINMenu==New_Menu)//For return menu
					{
						NewMainMenu();
					}
					else
					{
		
						ChooseOperatorMenu();
						
					}
				
				
					   
	        }
			break;
        default:
	        {
	        	
	        }
	        break;		
	}
	sendData.asTopUpAmount = 0;
	if(topup.MAINMenu==New_Menu)//For return menu
		{
			NewMainMenu();
		}
	else
		{
			ChooseOperatorMenu();
						
		}
				
}
void SelectAmount_TelenorEloadMenu(void)
{
	s32 key;
	key=sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 |SDK_KEY_MASK_4|SDK_KEY_MASK_5|SDK_KEY_MASK_6 |SDK_KEY_MASK_7| SDK_KEY_MASK_8 |SDK_KEY_MASK_ESC, KbWaitTime);
	memset(&sendData.asTopUpAmount,0,sizeof(sendData.asTopUpAmount));
	switch(key)
	{	
		case SDK_KEY_1:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"1");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 1000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModuleForTelenor();
			}
			break;
		case SDK_KEY_2:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"2");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 2000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModuleForTelenor();
			}
			break;
		case SDK_KEY_3:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"3");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 3000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModuleForTelenor();
			}
			break;
		case SDK_KEY_4:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"4");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 5000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModuleForTelenor();
			}
			break;
		case SDK_KEY_5:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"5");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 6000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModuleForTelenor();
			}
			break;
			case SDK_KEY_6:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"6");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 10000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModuleForTelenor();
			}
			break;
			case SDK_KEY_7:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"7");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 20000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModuleForTelenor();
			}
			break;
			case SDK_KEY_8:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"8");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 30000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModuleForTelenor();
			}
			break;
		case SDK_KEY_ESC:
			{
				sendData.asTopUpAmount = 0;
					if(topup.MAINMenu==New_Menu)//For return menu
					{
						NewMainMenu();
					}
					else
					{
		
						ChooseOperatorMenu();
						
					}
			}
	     	
		   	break;
        default:
	        {
	        	
	        }
	        break;		
	}
			
	sendData.asTopUpAmount = 0;
	if(topup.MAINMenu==New_Menu)
		{
			NewMainMenu();
		}	
	else
		{
			ChooseOperatorMenu();
		}
}

void TopUpModuleForTelenor(void)
{
	int key=0;
	u8 buf[128]={0};

	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/TelenorEloadPhNum.bmp");
	sdkDispBrushScreen();
	memset(buf,0,sizeof(buf));
	memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
	
	switch(topup.Operator)
	{
		case TELENOR:
			{	
				key = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE4,7,13, "09", NULL,"TELENOR");
			}
			break;
		default:
		    {
		    	
		    }
		    break;
	}
	
	memcpy(sendData.asPhoneNum,&buf[1],buf[0]); 
	Trace("xgd","sendData.asPhoneNum = %s\r\n",sendData.asPhoneNum);
	switch(key)
	{
		case SDK_KEY_ENTER :
			{
				topup.TopUpType =TOPUP_ELOAD;
			
				ModulePayment();
			}
		break;
		
		case SDK_KEY_ESC:
			{
					if(topup.MAINMenu==New_Menu)
					{
						NewMainMenu();
					}
					else
					{
		
						ChooseOperatorMenu();
						
					}
			}
			break; 	
	
		
		default:
			{
				
			}	
		break; 
				
	}		
	
	if(topup.MAINMenu==New_Menu)
	{
						NewMainMenu();
	}
					else
					{
		
						
							
							ChooseOperatorMenu();
						
					}
}

void SelectAmountMenu(void)
{	
	s32 key;
 	

	key=sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 |SDK_KEY_MASK_4|SDK_KEY_MASK_5|SDK_KEY_MASK_ESC, KbWaitTime);
	secondtransactionstart=false; // for pageagentcheckbalance
	memset(&sendData.asTopUpAmount,0,sizeof(sendData.asTopUpAmount));
	switch(key)
	{	
		case SDK_KEY_1:
			{
				
			
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"1");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 1000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModule();
			}
			break;
		case SDK_KEY_2:
			{
				
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"2");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 3000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModule();
			}
			break;
		case SDK_KEY_3:
			
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"3");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 5000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModule();
			}
			break;
		case SDK_KEY_4:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"4");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 10000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModule();
			}
			break;
		case SDK_KEY_5:
			{
				ForSendData();
				Trace("selectamountmenu","sendData.asEDCserial&time=%s\r\n",sendData.asEDCserial);
				strcpy(topupamount,sendData.asEDCserial);
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				strcat(topupamount,"5");
				Trace("selectamountmenu","topupamount=%s\r\n",topupamount);
				memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial));
				strcpy(sendData.asEDCserial,topupamount);
				Trace("selectamountmenu","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
				sendData.asTopUpAmount = 20000;
				Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
				TopUpModule();
			}
			break;
		case SDK_KEY_ESC:
	        {
	        		sendData.asTopUpAmount = 0;
	        		Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
	            	if(topup.MAINMenu==New_Menu)
	        	{
	        		 
	            	NewMainMenu();
				}
				else
				{
					
				
					ChooseOperatorMenu();
					
					
				}
	        }
			break;
        default:
	        {
	        }
	        break;		
	}
	sendData.asTopUpAmount = 0;
	Trace("selectamountmenu","sendData.asTopupAmount=%d\r\n",sendData.asTopUpAmount);
	if(topup.MAINMenu==New_Menu)
	{
		NewMainMenu();
	}
	else
	{
		
		
			
			ChooseOperatorMenu();
		
	}
	
		
}

void TopUpModule(void)
{
	int key=0;
	u8 buf[128]={0};

	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/toptype.bmp");
	sdkDispBrushScreen();
	memset(buf,0,sizeof(buf));
	memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
	
	switch(topup.Operator)
	{
		case MPT:
			{		
				key = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"MPT");
			}
			break;
		case OOREDOO:
			{
				key = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"OOREDOO");
			}
			break;
		case TELENOR:
			{	
				key = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"TELENOR");
			}
			break;
		case MECTEL:
			{
				key = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"MECTEL");
			}
			break;
		default:
		    {
		    	
		    }
		    break;
	}
	
	memcpy(sendData.asPhoneNum,&buf[1],buf[0]); 
	Trace("xgd","sendData.asPhoneNum = %s\r\n",sendData.asPhoneNum);
	
	switch(key)
	{
		case SDK_KEY_ENTER :
			{
				topup.TopUpType =TOPUP_ELOAD;
			
				ModulePayment();
			}
		break;
		case SDK_KEY_F1:
			{
				Trace("xgd","F1");
				topup.TopUpType =TOPUP_EPIN;
				ModulePayment();
			}
		break;
		case 15:
			{
				Trace("xgd","15");
				topup.TopUpType =TOPUP_EPIN;
				ModulePayment();
			}
		break;
		case SDK_KEY_ESC:
			{
				if(topup.MAINMenu==New_Menu)
	        	{
	        		 
	            	NewMainMenu();
				}
				else
				{
					
				
							ChooseOperatorMenu();	
					
				
				}
	        
			}	
		break; 
		default:
			{
				
			}	
		break; 
				
	}

	if(topup.MAINMenu==New_Menu)
	  	{
	        		 
	    	NewMainMenu();
		}
	else
		{
					
		
				ChooseOperatorMenu();	
								
			
		}
	
}

void ModulePayment(void)
{
	s32 key;
	u8 buf[128]={0};
	u8 buf1[128]={0};
	
	memset(buf,0,sizeof(buf));
	memset(buf1,0,sizeof(buf));
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240, 320, "/mtd0/res/cashorTap.bmp");
	
	if(topup.TopUpType==TOPUP_ELOAD)
	{
	
			sprintf(buf,"09%s",sendData.asPhoneNum);
			sprintf(buf1,"%dKS",sendData.asTopUpAmount);
			sdkDispAt(5,40,buf);
			sdkDispAt(160,40,buf1);
			sdkDispBrushScreen();
	}	
	else 
	{
		if(topup.Operator==MECTEL)
		{
			sprintf(buf," MEC-PIN");
			sprintf(buf1,"%dKS",sendData.asTopUpAmount);
			
			sdkDispAt(5,40,buf);
			sdkDispAt(160,40,buf1);
			sdkDispBrushScreen();
		}
		else if(topup.Operator==OOREDOO)
		{
			sprintf(buf," ORD0-PIN");
			sprintf(buf1,"%dKS",sendData.asTopUpAmount);
			
			sdkDispAt(5,40,buf);
			sdkDispAt(160,40,buf1);
			sdkDispBrushScreen();
		}
		else if(topup.Operator==TELENOR)
		{
			sprintf(buf," TEL-PIN");
			sprintf(buf1,"%dKS",sendData.asTopUpAmount);
			
			sdkDispAt(5,40,buf);
			sdkDispAt(160,40,buf1);
			sdkDispBrushScreen();
		}
		else if(topup.Operator==MPT)
		{
			sprintf(buf," MPT-PIN");
			sprintf(buf1,"%dKS",sendData.asTopUpAmount);
			
			sdkDispAt(5,40,buf);
			sdkDispAt(160,40,buf1);
			sdkDispBrushScreen();
		}
		
	}
	

	key=sdkKbWaitKey (SDK_KEY_MASK_1|SDK_KEY_MASK_2|SDK_KEY_MASK_ESC,KbWaitTime);

	switch(key)
	{
		case SDK_KEY_1:
			{
				topup.PaymentType=PAY_TCARD;
				ModuleCustomerReadCardForTopUp();
			}
			break;
		case SDK_KEY_2:
			{
				topup.PaymentType=PAY_CASH;
				ModulePayCash();
			}
			break;
		case SDK_KEY_ESC:
			{
				if(topup.MAINMenu==New_Menu)
	        	{
	        		 
	            	NewMainMenu();
				}
				else
				{
					
				
					ChooseOperatorMenu();
				}
			}	
		default:
			{
				
			}
			break;
	}
	if(topup.MAINMenu==New_Menu)
	        	{
	        		 
	            	NewMainMenu();
				}
				else
				{
					
				
					ChooseOperatorMenu();
				}
}

void ModulePayCash(void)
{	
	u8 out[128] = {0};
	//u8 buf[128] = {0};
	u8 message[128] ={0};
	s32 ret;
		
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	Trace("modulepaycash","topup.Operator = %d\r\n",topup.Operator);
	Trace("modulepaycash","topup.TopUpType = %d\r\n",topup.TopUpType);

	if(topup.TopUpType==TOPUP_ELOAD)
{
		switch(topup.Operator)
		{
			case MPT:
			{
				Trace("modulepaycash","MPT ELoad");
				
				/*ForSendData();
				Trace("modulepaycash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);*/
				
				memset(out,0,sizeof(out));
				sdkMD5(out,sendData.asAgentEnterPin,6);	
				memset(sendData.asHashEnterPin,0,sizeof(sendData.asHashEnterPin));
				sdkBcdToAsc(sendData.asHashEnterPin,out,16);
				Trace("modulepaycash","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
				memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
				strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
				Trace("modulepaycash ","Post Agent Enter pin  = %s\r\n",sendData.asHashEnterPin);
				
				memset(message,0,sizeof(message));
				memset(out,0,sizeof(out));
				sprintf(message,"%s%s%s%s%d%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asEDCserial);
				Trace("modulepaycash","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
				sdkBcdToAsc(sendData.asSignature, out, 32);
				Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
				
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&terminalRef=%s&signature=%s&mobile=%s&amount=%d&pinNo=%s&checkTrx=%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSignature,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asPostAgentEnterPin,sendData.asEDCserial);
				Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
				
				
				memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
				sprintf(sendData.asSendURL, "%s%s/mptELoadCash/",uri,IPAddress);
				Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				

				memset(&receivedData,0,sizeof(receivedData));
				ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
				if(ret==SDK_OK)
				{
					memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
					if(strcmp(receivedData.ServiceStatus,"Success")==0)
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						transaction=true;              //  for pageagentcheckbalance
						secondtransactionstart=true;
						js_tok(receivedData.PhoneNo,"mobileNo",0);
						js_tok(receivedData.TRX,"trx",0);
						js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
						js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
						
						
						js_tok(receivedData.Amount,"amount",0);
						PrintTopUp();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
					}
					else
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						transaction=false;
						js_tok(receivedData.ErrorMessage,"message",0);
						js_tok(receivedData.ErrorCode,"code",0);
						if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
						{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}	
						else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
						{
							sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
						{
							sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else
						{
							DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
						}
						

					}
				}
				else
				{
					Trace("modulepaycash","MPT ELoad checkTrx first url");
					memset(message,0,sizeof(message));
					sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
					sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
					memset(sendData.asHashData, 0, 128);
					sdkBcdToAsc(sendData.asHashData, out, 32);
					Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);

					memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
					sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
					
					memset(&receivedData,0,sizeof(receivedData));
					ret = SendReceivedData(sendData.asSendBuf,false);
					if(ret==SDK_OK)
					{
						memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
						js_tok(receivedData.ServiceStatus,"serviceStatus",0);
						if(strcmp(receivedData.ServiceStatus,"Success")==0)
						{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
							secondtransactionstart=true;
							js_tok(receivedData.PhoneNo,"mobileNo",0);
							js_tok(receivedData.TRX,"trx",0);
							js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
							js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
							js_tok(receivedData.Amount,"amount",0);
							PrintTopUp();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
						}
						else
						{	
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=false;
										
							js_tok(receivedData.ErrorMessage,"message",0);
	
							js_tok(receivedData.ErrorCode,"code",0);
							if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
							{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}	
							else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
						{
							sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
							else
							{
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
							}
						

						}
					}
					else
					{
						Trace("modulepaycash","MPT ELoad checkTrx second  url");
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
						Trace("modulepaycash","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
	
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret==SDK_OK)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
									transaction=true;              //  for pageagentcheckbalance
									secondtransactionstart=true;
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								
								js_tok(receivedData.Amount,"amount",0);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{		
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;
						
								js_tok(receivedData.ErrorMessage,"message",0);
		
								js_tok(receivedData.ErrorCode,"code",0);
								if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
								{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}	
								else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}
								else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
								else
								{
									DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
								}
							

							}
				
				
						}
							else
						{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
									{
										
										case SDK_KEY_ESC:
											{
												//MainMenu();
												
												if(topup.MAINMenu==New_Menu)
	        									{
	        		 
	            									NewMainMenu();
												}
												else
												{
					
				
													MainMenu();
												}
											}
											break;
											default:
											{
												
											}
										
									}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
						}
				
					}
				}
			}
			
			break;
			
			case OOREDOO:
			{
				Trace("modulepaycash","Ooredoo ELoad first url ");
				
				/*ForSendData();
				Trace("modulepaycash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);*/
				
				memset(out,0,sizeof(out));
				sdkMD5(out,sendData.asAgentEnterPin,6);	
				memset(sendData.asHashEnterPin, 0,sizeof(sendData.asHashEnterPin));
				sdkBcdToAsc(sendData.asHashEnterPin,out,16);
				Trace("modulepaycash","sendData.asHashEnterPin =%s\r\n",sendData.asHashEnterPin);
				memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
				strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
				Trace("modulepaycash","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
				
				
				memset(message,0,sizeof(message));
				memset(out,0,sizeof(out));
				
				sprintf(message,"%s%s%s%s%d%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asEDCserial);
				Trace("modulepaycash","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
				sdkBcdToAsc(sendData.asSignature, out, 32);
				Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
				
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&terminalRef=%s&signature=%s&mobile=%s&amount=%d&pinNo=%s&checkTrx=%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSignature,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asPostAgentEnterPin,sendData.asEDCserial);
				Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);

				memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
				sprintf(sendData.asSendURL, "%s%s/ooredooELoadCash/",uri,IPAddress);
				Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				/*memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"%s%s/ooredooELoadCash/%s/%s/%s/%s/%d/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);*/
				memset(&receivedData,0,sizeof(receivedData));	
				ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
				if(ret==SDK_OK)
				{
					memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
					if(strcmp(receivedData.ServiceStatus,"Success")==0)
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						transaction=true;              //  for pageagentcheckbalance
						secondtransactionstart=true;
						js_tok(receivedData.PhoneNo,"mobileNo",0);
						js_tok(receivedData.TRX,"trx",0);
						js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
						js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
						js_tok(receivedData.Amount,"amount",0);
						js_tok(receivedData.RefNo,"refNo",0);
						PrintTopUp();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
					}
					else
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						transaction=false;
						js_tok(receivedData.ErrorMessage,"message",0);

						js_tok(receivedData.ErrorCode,"code",0);
						if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
						{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}	
						else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
						{
							sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							
						else
						{
							DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
						}
					

					}
				}
				else
				{
					Trace("modulepaycash","Ooredoo ELoad checkTrx second url first time");
					memset(message,0,sizeof(message));
					sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
					Trace("modulepaycash","message=%s\r\n",message);
					sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);


					memset(sendData.asHashData, 0, 128);
					sdkBcdToAsc(sendData.asHashData, out, 32);
					Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
	
					memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
					sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
					
					memset(&receivedData,0,sizeof(receivedData));
					ret = SendReceivedData(sendData.asSendBuf,false);
					if(ret==SDK_OK)
					{
						memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
						js_tok(receivedData.ServiceStatus,"serviceStatus",0);
						
						if(strcmp(receivedData.ServiceStatus,"Success")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=true;              //  for pageagentcheckbalance
							secondtransactionstart=true;
							js_tok(receivedData.PhoneNo,"mobileNo",0);
							js_tok(receivedData.TRX,"trx",0);
							js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
							js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
							js_tok(receivedData.Amount,"amount",0);
							js_tok(receivedData.RefNo,"refNo",0);
							PrintTopUp();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
						}
						else
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=false;
							js_tok(receivedData.ErrorMessage,"message",0);
	
							js_tok(receivedData.ErrorCode,"code",0);
							if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
								{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}	
							else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
							{
								sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							
							else
							{
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
							}
						

						}
					}
					else
					{
						Trace("modulepaycash","Ooredoo ELoad checkTrx second url second time");
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
						Trace("modulepaycash","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret==SDK_OK)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));	
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.Amount,"amount",0);
								js_tok(receivedData.RefNo,"refNo",0);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;
								js_tok(receivedData.ErrorMessage,"message",0);
		
									js_tok(receivedData.ErrorCode,"code",0);
								if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
								{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}	
								else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
								{			
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}
								else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							
								else
								{
									DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
								}
							

							}
							
						}
						else
						{	
							transaction=false;
							s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
									{
										
										case SDK_KEY_ESC:
											{
												//MainMenu();
												
												if(topup.MAINMenu==New_Menu)
	        									{
	        		 
	            									NewMainMenu();
												}
												else
												{
					
				
													MainMenu();
												}
											}
											break;
											default:
											{
												
											}
										
									}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
						}
				
					}
				}
				
			}
			break;
			case MECTEL:
			{
				Trace("modulepaycash","MECTEL ELoad ");
			
				/*ForSendData();
				Trace("modulepaycash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);*/
				
				memset(out,0,sizeof(out));
				sdkMD5(out,sendData.asAgentEnterPin,6);	
				memset(sendData.asHashEnterPin, 0,sizeof(sendData.asHashEnterPin));
				sdkBcdToAsc(sendData.asHashEnterPin,out,16);
				Trace("modulepaycash","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
				memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
				strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
				Trace("modulepaycash","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
				
				
				memset(message,0,sizeof(message));
				memset(out,0,sizeof(out));
				sprintf(message,"%s%s%s%s%d%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asEDCserial);
				Trace("xgd","message = %s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
				//sdkDispBrushScreen();
				
				Trace("xgd","MEC ELOAD First url");
				memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
				sdkBcdToAsc(sendData.asSignature, out, 32);
				Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);

				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&pinNo=%s&signature=%s&mobile=%s&amount=%d&terminalRef=%s&checkTrx=%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asSignature,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asTerminalSN,sendData.asEDCserial);
				Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
				
				//sprintf(sendData.asSendBuf,"%s%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,buf,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asEDCserial,sendData.asHashData);
				memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
				sprintf(sendData.asSendURL, "%s%s/mecELoadCashWithTrx/",uri,IPAddress);
				Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				memset(&receivedData,0,sizeof(receivedData));	
				ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
				if(ret==SDK_OK)
				{
					memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
					if(strcmp(receivedData.ServiceStatus,"Success")==0)
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						transaction=true;              //  for pageagentcheckbalance
						secondtransactionstart=true;
						js_tok(receivedData.PhoneNo,"mobileNo",0);
						js_tok(receivedData.TRX,"trx",0);
						js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
						js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
						js_tok(receivedData.Amount,"amount",0);
						
						PrintTopUp();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
					}
					else
					{	
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
							transaction=false;		
						js_tok(receivedData.ErrorMessage,"message",0);

					js_tok(receivedData.ErrorCode,"code",0);
						if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
						{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}	
						else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							
						else
						{
							DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
						}
					

					}
				}
				else
				{
					Trace("modulepaycash","MEC ELOAD checek TRX firsturl");
					memset(message,0,sizeof(message));
					sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
					sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
					memset(sendData.asHashData, 0, 128);
					sdkBcdToAsc(sendData.asHashData, out, 32);
					Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);

					memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
					sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
					
						memset(&receivedData,0,sizeof(receivedData));
					ret = SendReceivedData(sendData.asSendBuf,false);
					if(ret==SDK_OK)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{	
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.Amount,"amount",0);
								
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;
							js_tok(receivedData.ErrorMessage,"message",0);

							js_tok(receivedData.ErrorCode,"code",0);
								if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}	
								else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
								{			
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}
								else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							
								else
								{
									DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
								}
							

							}
						}
						else
						{
							Trace("modulepaycash","MECELOAD checkTrx second URL");
							memset(message,0,sizeof(message));
							sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
							sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
							memset(sendData.asHashData, 0, 128);
							sdkBcdToAsc(sendData.asHashData, out, 32);
							Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);

							memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
							sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
	
								memset(&receivedData,0,sizeof(receivedData));
							ret = SendReceivedData(sendData.asSendBuf,false);
							if(ret==SDK_OK)
							{
									memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
								js_tok(receivedData.ServiceStatus,"serviceStatus",0);
								if(strcmp(receivedData.ServiceStatus,"Success")==0)
								{	
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
									transaction=true;              //  for pageagentcheckbalance
									secondtransactionstart=true;
									js_tok(receivedData.PhoneNo,"mobileNo",0);
									js_tok(receivedData.TRX,"trx",0);
									js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
									js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
									//sprintf(receivedData.Amount,"%d",sendData.asTopUpAmount);
									js_tok(receivedData.Amount,"amount",0);
									PrintTopUp();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
								}
								else
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
									js_tok(receivedData.ErrorMessage,"message",0);
									transaction=false;
									js_tok(receivedData.ErrorCode,"code",0);
									if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
		
									{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}	
									else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
									{
										sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}
									else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							
									else
									{
										DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
									}
								

								}
							}
								else
						{
							transaction=false;
							s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
									{
										
										case SDK_KEY_ESC:
											{
												//MainMenu();
												
												if(topup.MAINMenu==New_Menu)
	        									{
	        		 
	            									NewMainMenu();
												}
												else
												{
					
				
													MainMenu();
												}
											}
											break;
											default:
											{
												
											}
										
									}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
						}
				
						}
				
				}
			
			}
			break;
			case TELENOR:
			{
							
				Trace("ModulePayCash","Telenor ELoad  url ");				
				/*ForSendData();
				Trace("ModulePayCash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);*/
				
				memset(out,0,sizeof(out));
				sdkMD5(out,sendData.asAgentEnterPin,6);	
				memset(sendData.asHashEnterPin, 0,sizeof(sendData.asHashEnterPin));
				sdkBcdToAsc(sendData.asHashEnterPin,out,16);
				Trace("modulepaycash","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
				memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
				strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
				Trace("modulepaycash","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
				
				memset(message,0,sizeof(message));
				memset(out,0,sizeof(out));
				
				sprintf(message,"%s%s%s%s%d%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asEDCserial);
				Trace("modulepaycash","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
				sdkBcdToAsc(sendData.asSignature, out, 32);
				Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
				

				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&terminalRef=%s&signature=%s&mobile=%s&amount=%d&pinNo=%s&checkTrx=%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSignature,sendData.asPhoneNum,sendData.asTopUpAmount,sendData.asPostAgentEnterPin,sendData.asEDCserial);
				Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);

				memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
				sprintf(sendData.asSendURL, "%s%s/telenorELoadCash/",uri,IPAddress);
				Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				
				memset(&receivedData,0,sizeof(receivedData));	
				ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
				if(ret==SDK_OK)
				{
					memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
					
					Trace("Modulepaycash,TelenorEload,firsturl","receivedData.ServiceStatus=%s\r\n",receivedData.ServiceStatus);
					if(strcmp(receivedData.ServiceStatus,"Success")==0)
					{
						memset(receivedData.InvoiceNumber,0,sizeof(receivedData.InvoiceNumber));
						memset(receivedData.OrderNumber,0,sizeof(receivedData.OrderNumber));			
						js_tok(receivedData.InvoiceNumber,"invoiceNumber",0);						
						js_tok(receivedData.OrderNumber,"trx",0);
						
						strcpy(sendData.InvoiceNumber,receivedData.InvoiceNumber);
						strcpy(sendData.OrderNumber,receivedData.OrderNumber);						
						
						Trace("modulepaycash","receivedData.InvoiceNumber=%s\r\n",sendData.InvoiceNumber);
						Trace("modulepaycash","receivedData.Trx=%s\r\n",sendData.OrderNumber);
						Trace("modulepaycash","Telenor ELoad first EnquiryCash ");
				
						memset(message,0,sizeof(message));
				
						sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asHashEnterPin,sendData.asTerminalSN,sendData.InvoiceNumber,sendData.OrderNumber);
						Trace("modulepaycash","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
						sdkBcdToAsc(sendData.asSignature, out, 32);
						Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
						transaction=true;              //  for pageagentcheckbalance
						secondtransactionstart=true;
						
						sdkmSleep(15000);
						
						sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&pinNo=%s&terminalRef=%s&signature=%s&invoiceNumber=%s&orderNumber=%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asSignature,sendData.InvoiceNumber,sendData.OrderNumber);
						Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
						
		
						memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
						sprintf(sendData.asSendURL, "%s%s/telenorELoadEnquiryCash/",uri,IPAddress);
							Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				
						memset(&receivedData,0,sizeof(receivedData));	
						ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
						
						if(ret==SDK_OK)
						{	
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							Trace("ModulePayCash,TelenorEload,EnquiryCashfisturl","receivedData.ServiceStatus=%s\r\n",receivedData.ServiceStatus);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.Amount,"amount",0);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}							
							else if(strcmp(receivedData.ServiceStatus,"Pending")==0)
							{
							
								Trace("ModulePayCash","Telenor ELoad Equary second time");
								sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asHashEnterPin,sendData.asTerminalSN,sendData.InvoiceNumber,sendData.OrderNumber);
								Trace("modulepaycash","message=%s\r\n",message);
								sdkCalcHmacSha256(TSK,strlen(TSK),message,strlen(message), out);
		
								memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
								sdkBcdToAsc(sendData.asSignature, out, 32);
								Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;
						
								sdkmSleep(15000);
								sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&pinNo=%s&terminalRef=%s&signature=%s&invoiceNumber=%s&orderNumber=%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asSignature,sendData.InvoiceNumber,sendData.OrderNumber);
								Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
								
		
								memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
								sprintf(sendData.asSendURL, "%s%s/telenorELoadEnquiryCash/",uri,IPAddress);
								Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				
									memset(&receivedData,0,sizeof(receivedData));	
									ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
									if(ret==SDK_OK)
									{
										memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));	
										js_tok(receivedData.ServiceStatus,"serviceStatus",0);
										Trace("ModulePayCash","receivedData.ServiceStatus=%s\r\n",receivedData.ServiceStatus);
										
										if(strcmp(receivedData.ServiceStatus,"Success")== 0)	
										{
											sdkDispClearScreen();
											sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
											sdkDispBrushScreen();
											sdkmSleep(1500);
											transaction=true;              //  for pageagentcheckbalance
											secondtransactionstart=true;
											js_tok(receivedData.PhoneNo,"mobileNo",0);
											js_tok(receivedData.TRX,"trx",0);
											js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
											js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
											js_tok(receivedData.Amount,"amount",0);
											PrintTopUp();
											sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
										}							
								
										else if(strcmp(receivedData.ServiceStatus,"Pending")== 0)
										{
											Trace("ModulePayCash","Telenor ELoad Equary Third time");
											sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asHashEnterPin,sendData.asTerminalSN,sendData.InvoiceNumber,sendData.OrderNumber);
												Trace("xgd","message=%s\r\n",message);
												sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
											memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
											sdkBcdToAsc(sendData.asSignature, out, 32);
											Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
											transaction=true;              //  for pageagentcheckbalance
											secondtransactionstart=true;
											
											sdkmSleep(15000);
											sprintf(sendData.asSendBuf,"--data&agentCardNo=%s&pinNo=%s&terminalRef=%s&signature=%s&invoiceNumber=%s&orderNumber=%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asSignature,sendData.InvoiceNumber,sendData.OrderNumber);
											Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
											
		
											memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
											sprintf(sendData.asSendURL, "%s%s/telenorELoadEnquiryCash/",uri,IPAddress);
											Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				
												memset(&receivedData,0,sizeof(receivedData));	
												ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
												if(ret==SDK_OK)
												{
													memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));	
													js_tok(receivedData.ServiceStatus,"serviceStatus",0);
													Trace("ModulePayCash-ThirdURL","receivedData.ServiceStatus=%s\r\n",receivedData.ServiceStatus);	
													if((strcmp(receivedData.ServiceStatus,"Success")==0)||(strcmp (receivedData.ServiceStatus,"Pending")==0	))
													{	
														sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
														sdkDispBrushScreen();
														sdkmSleep(1500);
														transaction=true;              //  for pageagentcheckbalance
														secondtransactionstart=true;
														js_tok(receivedData.PhoneNo,"mobileNo",0);
														js_tok(receivedData.TRX,"trx",0);
														js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
														js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
														js_tok(receivedData.Amount,"amount",0);
														PrintTopUp();
														sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
													}
												
													else if (strcmp(receivedData.ServiceStatus,"Fail")==0)
													{
													
														sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
														sdkDispBrushScreen();
														sdkmSleep(1500);
														transaction=false;
														js_tok(receivedData.ErrorMessage,"message",0);
								
														js_tok(receivedData.ErrorCode,"code",0);
														
														if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
														{
														sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
														sdkDispBrushScreen();
														sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
														}	
														else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
														{
															
															sdkDispClearScreen();
															sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
															sdkDispBrushScreen();
															sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
																
														}
														else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
													{
															sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
														sdkDispBrushScreen();
														sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
													}
							
														else
														{
															DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
														}
													
				
													}
														
												}
												else
												{
													sdkDispClearScreen();
													sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
													sdkDispBrushScreen();
													sdkmSleep(1500);
													js_tok(receivedData.ErrorMessage,"message",0);
													transaction=false;
									
													js_tok(receivedData.ErrorCode,"code",0);
													if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
								
													{
														sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
														sdkDispBrushScreen();
														sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
													}	
													else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
													{
														sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
														sdkDispBrushScreen();
														sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
													}
													
													else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
													{
															sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
														sdkDispBrushScreen();
														sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
													}
							
													else
													{
														DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
													}
												}
										}	
										else if(strcmp(receivedData.ServiceStatus,"Fail"))
										{
													sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
														sdkDispBrushScreen();
														sdkmSleep(1500);
														transaction=false;
														js_tok(receivedData.ErrorMessage,"message",0);
								
														js_tok(receivedData.ErrorCode,"code",0);
														
														if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
														{
														sdkDispClearScreen();
														sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
														sdkDispBrushScreen();
														sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
														}	
														else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
														{
															
															sdkDispClearScreen();
															sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
															sdkDispBrushScreen();
															sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
																
														}
														else
														{
															DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
														}
													
										}
									}
									else
									{
										s32 key;
										sdkDispClearScreen();
										sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
										sdkDispBrushScreen();
										sdkmSleep(1500);
										
										sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
										sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
										sdkDispBrushScreen();
										
										key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
											switch(key)
											{
												
												case SDK_KEY_ESC:
													{
														//MainMenu();
														
														if(topup.MAINMenu==New_Menu)
			        									{
			        		 
			            									NewMainMenu();
														}
														else
														{
							
						
															MainMenu();
														}
													}
													break;
													default:
													{
														
													}
												
											}
										if(topup.MAINMenu==New_Menu)
			        					{
			        		 
			            					NewMainMenu();
										}
										else
										{					
						
											MainMenu();
										}
									}
						
							}
							else if(strcmp(receivedData.ServiceStatus,"Fail")==0)
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
									js_tok(receivedData.ErrorMessage,"message",0);
									transaction=false;
					
									js_tok(receivedData.ErrorCode,"code",0);
									if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
				
									{
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
										sdkDispBrushScreen();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}	
									else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
									{
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
										sdkDispBrushScreen();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}
									else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
									{
											sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
										sdkDispBrushScreen();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}
									else
									{
										DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
									}
							
								}	
								
						}
						else
						{
							//transaction=false;
							s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
									{
										
										case SDK_KEY_ESC:
											{
												//MainMenu();
												
												if(topup.MAINMenu==New_Menu)
	        									{
	        		 
	            									NewMainMenu();
												}
												else
												{
					
				
													MainMenu();
												}
											}
											break;
											default:
											{
												
											}
										
									}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
						}
				
					}
					else 
					{			
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);	
						js_tok(receivedData.ErrorMessage,"message",0);
						transaction=false;	
						js_tok(receivedData.ErrorCode,"code",0);
						if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
						{		
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}	
						else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
						{
							sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
						{
								sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else
						{
							DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
						}
					
	
					}
				}
					
				else
				{
					Trace("modulepaycash","Telenor ELoad checkTrx second url first time");
					memset(message,0,sizeof(message));
					sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
					Trace("modulepaycash","message=%s\r\n",message);
					sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

					memset(sendData.asHashData, 0,128);
					sdkBcdToAsc(sendData.asHashData,out, 32);
					Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
	
					memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
					sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
					
					memset(&receivedData,0,sizeof(receivedData));
					ret = SendReceivedData(sendData.asSendBuf,false);
					if(ret==SDK_OK)
					{
						memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
						js_tok(receivedData.ServiceStatus,"serviceStatus",0);
						
						if(strcmp(receivedData.ServiceStatus,"Success")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=true;              //  for pageagentcheckbalance
							secondtransactionstart=true;
							js_tok(receivedData.PhoneNo,"mobileNo",0);
							js_tok(receivedData.TRX,"trx",0);
							js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
							js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
							js_tok(receivedData.Amount,"amount",0);
							PrintTopUp();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
						}
						 else if(strcmp(receivedData.ServiceStatus,"Fail")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=false;
							js_tok(receivedData.ErrorMessage,"message",0);
	
							js_tok(receivedData.ErrorCode,"code",0);
							if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
							{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}	
							else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
							{
								sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
								else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
						{
								sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
							else
							{
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
							}
						
						}
					}
					else
					{
						Trace("modulepaycash","Telenor ELoad checkTrx second url second time");
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
						Trace("modulepaycash","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData,out, 32);
						Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret==SDK_OK)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));	
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.Amount,"amount",0);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							 else if(strcmp(receivedData.ServiceStatus,"Fail")==0)
							{
						
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;
								js_tok(receivedData.ErrorMessage,"message",0);
		
								js_tok(receivedData.ErrorCode,"code",0);
								if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
								{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}	
								else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}
								
									else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}	
								else
								{
									DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
								}
							

							}
						}
						else
						{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
									{
										
										case SDK_KEY_ESC:
											{
												//MainMenu();
												
												if(topup.MAINMenu==New_Menu)
	        									{
	        		 
	            									NewMainMenu();
												}
												else
												{
					
				
													MainMenu();
												}
											}
											break;
											default:
											{
												
											}
										
									}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
						}
				
					}
			
				
				}
			}
			break;
			default:
			{
				
			}		
			
		}
		if(topup.MAINMenu==New_Menu)
	        					{
	        		 			  Trace("Modulepaycash","topup shortcut");	
	            					NewMainMenu();
								}
								else
								{
					
									Trace("Modulepaycash","topup mainmenu");	
									MainMenu();
								}
	}
	else if(topup.TopUpType==TOPUP_EPIN)
	{
		switch(topup.Operator)
		{
			case MPT:
			{
				/*ForSendData();
				Trace("MPTEPinCash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);*/
				
				memset(out,0,sizeof(out));
				sdkMD5(out,sendData.asAgentEnterPin,6);	
				memset(sendData.asHashEnterPin, 0,sizeof(sendData.asHashEnterPin));
				sdkBcdToAsc(sendData.asHashEnterPin,out,16);
				Trace("modulepaycash","sendData.asHashEnterPin= %s\r\n",sendData.asHashEnterPin);
				memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
				strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
				Trace("modulepaycash","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
				
				
				memset(message,0,sizeof(message));
				memset(out,0,sizeof(out));
				sprintf(message,"%s%s%s%d%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asTopUpAmount,sendData.asEDCserial);
				Trace("modulepaycash","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
				sdkBcdToAsc(sendData.asSignature, out, 32);
				Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
				
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&terminalRef=%s&signature=%s&amount=%d&pinNo=%s&checkTrx=%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSignature,sendData.asTopUpAmount,sendData.asPostAgentEnterPin,sendData.asEDCserial);
				Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);

				memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
				sprintf(sendData.asSendURL, "%s%s/mptEpinCash/",uri,IPAddress);
				Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				
				memset(&receivedData,0,sizeof(receivedData));	
				ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
				//if(ret==SDK_OK)
				if(ret!=0)
					{
						js_tok(receivedData.ServiceStatus,"serviceStatus",0);
						
						if(strcmp(receivedData.ServiceStatus,"Success")==0)
						{	
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=true;              //  for pageagentcheckbalance
							secondtransactionstart=true;
							js_tok(receivedData.TRX,"trx",0);
							js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
							js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
							js_tok(receivedData.PhPinCode,"pinNumber",0);
							js_tok(receivedData.Amount,"amount",0);
							js_tok(receivedData.SerialNo,"serialNo",0);
							js_tok(receivedData.ExpDate,"expireDate",0);
							js_tok(receivedData.AgentCardRef,"agentCardRef",0);
							PrintTopUp();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
						}
						else
						{	
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=false;
							memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
							js_tok(receivedData.ErrorMessage,"message",0);
							js_tok(receivedData.ErrorCode,"code",0);
							if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
		
							{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}	
							else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
							{
								sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							
							else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
							else
							{
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
							}
						
						}	
					}
					
					else
					{
							s32 key;
							sdkDispClearScreen();
							sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
								
							sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
							sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
							sdkDispBrushScreen();
								
							key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
							switch(key)
							{
										
								case SDK_KEY_ESC:
									{											
												
									if(topup.MAINMenu==New_Menu)
	        						{
	        		 
	            						NewMainMenu();
									}
									else
									{					
				
									MainMenu();
									
									}
									}
									break;
									default:
									{
												
									}
										
							}
							if(topup.MAINMenu==New_Menu)
	        				{	        		 
	            				NewMainMenu();
							}
							else
							{					
				
								MainMenu();
							}
					}
				
				
				
			}
			break;
			case OOREDOO:
			{
				/*ForSendData();
				Trace("Ooredoo epin","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);*/
				
				memset(out,0,sizeof(out));
				sdkMD5(out,sendData.asAgentEnterPin,6);	
				memset(sendData.asHashEnterPin, 0,sizeof(sendData.asHashEnterPin));
				sdkBcdToAsc(sendData.asHashEnterPin,out,16);
				Trace("modulepaycash","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
				memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
				strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
				Trace("modulepaycash","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
				
				
				memset(message,0,sizeof(message));
				memset(out,0,sizeof(out));
				sprintf(message,"%s%s%s%d%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asTopUpAmount,sendData.asEDCserial);
				Trace("modulepaycash","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
				sdkBcdToAsc(sendData.asSignature, out, 32);
				Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
				
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&terminalRef=%s&signature=%s&amount=%d&pinNo=%s&checkTrx=%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSignature,sendData.asTopUpAmount,sendData.asPostAgentEnterPin,sendData.asEDCserial);
				Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);

				memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
				sprintf(sendData.asSendURL, "%s%s/ooredooEPinCash/",uri,IPAddress);
				Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				
				memset(&receivedData,0,sizeof(receivedData));	
				ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
				//if(ret==SDK_OK)
				if(ret!=0)
					{
						js_tok(receivedData.ServiceStatus,"serviceStatus",0);
						
						if(strcmp(receivedData.ServiceStatus,"Success")==0)
						{		
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=true;              //  for pageagentcheckbalance
							secondtransactionstart=true;
							js_tok(receivedData.PhoneNo,"mobileNo",0);
							js_tok(receivedData.TRX,"trx",0);
							js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
							js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
							js_tok(receivedData.AccessPinCode,"pinCode",0);
       						Trace("Ooredoo Epin","pincode(before decryption)=%s\r\n",receivedData.AccessPinCode);
      					 	dc_message(receivedData.AccessPinCode);
      						Trace("Ooredoo Epin","pincode(after decryption)=%s\r\n",receivedData.AccessPinCode);
      						 
      						strcpy(receivedData.PhPinCode,SpaceFormat(receivedData.AccessPinCode));
       						Trace("Ooredoo Epin","After Space=%s\r\n",receivedData.PhPinCode);
							js_tok(receivedData.Amount,"amount",0);
							js_tok(receivedData.SerialNo,"serialNo",0);
							js_tok(receivedData.ExpDate,"expireDate",0);
							js_tok(receivedData.AgentCardRef,"agentCardRef",0);
							
							PrintTopUp();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
						}
							else
						{	
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=false;
							memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
							js_tok(receivedData.ErrorMessage,"message",0);
	
							js_tok(receivedData.ErrorCode,"code",0);
							Trace("Modulepaycash","receivedData.ErrorCode=%s\r\n",receivedData.ErrorCode);
							if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
							{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}	
							else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
							{
								sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							else
							{
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
							}
						
						}	
					}
					else
						{
							s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
									{
										
										case SDK_KEY_ESC:
											{
												//MainMenu();
												
												if(topup.MAINMenu==New_Menu)
	        									{
	        		 
	            									NewMainMenu();
												}
												else
												{
					
				
													MainMenu();
												}
											}
											break;
											default:
											{
												
											}
										
									}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
						}
				
				
			
				
			}
			break;
			case MECTEL:
			{
				Trace("modulepaycash","MECTEL EPIN");
				
				/*ForSendData();
				Trace("modulepaycash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);*/
				
				memset(out,0,sizeof(out));
				sdkMD5(out,sendData.asAgentEnterPin,6);	
				memset(sendData.asHashEnterPin, 0,sizeof(sendData.asHashEnterPin));
				sdkBcdToAsc(sendData.asHashEnterPin,out,16);
				Trace("modulepaycash","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
				memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
				strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
				Trace("modulepaycash","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
				
				
				memset(message,0,sizeof(message));
				memset(out,0,sizeof(out));
				sprintf(message,"%s%s%s%d%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asTopUpAmount,sendData.asEDCserial);
				Trace("modulepaycash","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
				sdkBcdToAsc(sendData.asSignature, out, 32);
				Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
				
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&terminalRef=%s&signature=%s&amount=%d&pinNo=%s&checkTrx=%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSignature,sendData.asTopUpAmount,sendData.asPostAgentEnterPin,sendData.asEDCserial);
				Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);

				memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
				sprintf(sendData.asSendURL, "%s%s/mecEPinCash/",uri,IPAddress);
				Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				
				memset(&receivedData,0,sizeof(receivedData));	
				ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
				
				if(ret!=0)
				{
				
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		
					if(strcmp(receivedData.ServiceStatus,"Success")==0)
					{	
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=true;              //  for pageagentcheckbalance
							secondtransactionstart=true;
							js_tok(receivedData.TRX,"trx",0);
							js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
							js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
							
							js_tok(receivedData.PhPinCode,"pinCode",0);
							
							char src[32] = {0};
							 memcpy(src,receivedData.PhPinCode,strlen(receivedData.PhPinCode));
							
							int len = 0, spaces = 0;
				
				    		// Scan through src counting spaces and length at the same time //
				    		while (src[len]) {
							if (src[len] == ' ')
							++spaces;
							len++;
				    		}
				    		Trace("xgd", "Len= %d\r\n", len);
				    		if(len > 15)
				    		{
				    			MecNewPin = true;	
							
							}
							else
							{
								MecNewPin = false;
							}
							
							js_tok(receivedData.Amount,"amount",0);
							
						
							js_tok(receivedData.SerialNo,"serialNo",0);
							
							
							js_tok(receivedData.ExpDate,"expireDate",0);
							
							
							js_tok(receivedData.AgentCardRef,"agentCardRef",0);
							
							PrintTopUp();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
					}
					else
					{
							
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=false;
						js_tok(receivedData.ErrorMessage,"message",0);
	
						js_tok(receivedData.ErrorCode,"code",0);
						if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
		
						{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}	
						else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
						{
							sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else
						{
							DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
						}
					
					
					}
				}
				
				else
						{
							transaction=false;
							s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
									{
										
										case SDK_KEY_ESC:
											{
												//MainMenu();
												
												if(topup.MAINMenu==New_Menu)
	        									{
	        		 
	            									NewMainMenu();
												}
												else
												{
					
				
													MainMenu();
												}
											}
											break;
											default:
											{
												
											}
										
									}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
						}
				
			
		
			}
			break;
			
			case TELENOR:
			{
				
				/*ForSendData();
				Trace("modulepaycash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);*/
				
				Trace("modulepaycash","TELENOR Epin first url");
				memset(out,0,sizeof(out));
				sdkMD5(out,sendData.asAgentEnterPin,6);	
				memset(sendData.asHashEnterPin, 0,sizeof(sendData.asHashEnterPin));
				sdkBcdToAsc(sendData.asHashEnterPin,out,16);
				Trace("modulepaycash","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
				memset(sendData.asPostAgentEnterPin,0,sizeof(sendData.asPostAgentEnterPin));
				strcpy(sendData.asPostAgentEnterPin,sendData.asHashEnterPin);
				Trace("modulepaycash","Post Agent Enter pin  = %s\r\n",sendData.asPostAgentEnterPin);
				
				
				memset(message,0,sizeof(message));
				memset(out,0,sizeof(out));
				sprintf(message,"%s%s%s%d%s",sendData.asAgentCardNo,sendData.asPostAgentEnterPin,sendData.asTerminalSN,sendData.asTopUpAmount,sendData.asEDCserial);
				Trace("modulepaycash","message=%s\r\n",message);
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

				memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
				sdkBcdToAsc(sendData.asSignature, out, 32);
				Trace("modulepaycash","signature = %s\r\n",sendData.asSignature);
				
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf,"--data &agentCardNo=%s&terminalRef=%s&signature=%s&amount=%d&pinNo=%s&checkTrx=%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSignature,sendData.asTopUpAmount,sendData.asPostAgentEnterPin,sendData.asEDCserial);
				Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);

				memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
				sprintf(sendData.asSendURL, "%s%s/telenorEPinCash/",uri,IPAddress);
				Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
				
				
				memset(&receivedData,0,sizeof(receivedData));	
				ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
				if(ret==SDK_OK)
				//if(ret!=0)
				{
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
					if(strcmp(receivedData.ServiceStatus,"Success")==0)
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						transaction=true;              //  for pageagentcheckbalance
						secondtransactionstart=true;
						js_tok(receivedData.PhoneNo,"mobileNo",0);
						
						js_tok(receivedData.TRX,"trx",0);
						
						js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
						
						js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
						
						
						js_tok(receivedData.PhPinCode,"pinNumber",0);
						
						js_tok(receivedData.Amount,"amount",0);
						
						js_tok(receivedData.SerialNo,"serialNumber",0);
						
						js_tok(receivedData.ExpDate,"epinExpDate",0);
						
						js_tok(receivedData.AgentCardRef,"agentCardRef",0);
						PrintTopUp();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
					}
					
					else
					{
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
						sdkDispBrushScreen();
						sdkmSleep(1500);
						transaction=false;
						js_tok(receivedData.ErrorMessage,"message",0);

						js_tok(receivedData.ErrorCode,"code",0);
						if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
						{		
						sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}	
						else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
						{
							sdkDispClearScreen();
						sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
						sdkDispBrushScreen();
						sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
						}
						else
						{
							DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
						}
					
					}
				}
				else
				{
					Trace("modulepaycash","TELENOR Epin checkTrx first url");
					
					memset(message,0,sizeof(message));
					sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
					Trace("modulepaycash","message=%s\r\n",message);
					sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
					
					memset(sendData.asHashData, 0, 128);
					sdkBcdToAsc(sendData.asHashData, out, 32);
					Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
					
					memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
					sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
					
					memset(&receivedData,0,sizeof(receivedData));
					ret = SendReceivedData(sendData.asSendBuf,false);
					if(ret==SDK_OK)
					{
						js_tok(receivedData.ServiceStatus,"serviceStatus",0);
						if(strcmp(receivedData.ServiceStatus,"Success")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=true;              //  for pageagentcheckbalance
							secondtransactionstart=true;
							js_tok(receivedData.PhoneNo,"mobileNo",0);
							js_tok(receivedData.TRX,"trx",0);
							js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
							js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
							js_tok(receivedData.PhPinCode,"pinNumber",0);
							js_tok(receivedData.Amount,"amount",0);
							js_tok(receivedData.SerialNo,"serialNumber",0);
							js_tok(receivedData.ExpDate,"epinExpDate",0);
							js_tok(receivedData.CardRef,"agentCardRef",0);
							PrintTopUp();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
						}
					
						else
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							transaction=false;
							js_tok(receivedData.ErrorMessage,"message",0);
	
							js_tok(receivedData.ErrorCode,"code",0);
							if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
							{		
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}	
							else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
							{		
								sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
							sdkDispBrushScreen();
							sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
							else
							{
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
							}
						
						}
					}
					else
					{
						Trace("modulepaycash","TELENOR Epin checkTrx second url");
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
						
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
						
						
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret==SDK_OK)
						{
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
									secondtransactionstart=true;
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								
								js_tok(receivedData.PhPinCode,"pinNumber",0);
								
								js_tok(receivedData.Amount,"amount",0);
								
								js_tok(receivedData.SerialNo,"serialNumber",0);
								
								js_tok(receivedData.ExpDate,"epinExpDate",0);
								
								js_tok(receivedData.CardRef,"agentCardRef",0);
		
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
					
						else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;
								js_tok(receivedData.ErrorMessage,"message",0);
	
								js_tok(receivedData.ErrorCode,"code",0);
								if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
								{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}	
								else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}
								else if(strcmp(receivedData.ErrorCode,"Err00138")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/recheckphnum.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
							}
								else
								{
									DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
								}
							

							}
						}	
					
							
								else
						{
							transaction=false;
							s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
									{
										
										case SDK_KEY_ESC:
											{
												//MainMenu();
												
												if(topup.MAINMenu==New_Menu)
	        									{
	        		 
	            									NewMainMenu();
												}
												else
												{
					
				
													MainMenu();
												}
											}
											break;
											default:
											{
												
											}
										
									}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
						}
				
				
					}
				
				}
				
			}
			break;	
		}
		if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									MainMenu();
								}
	}

	
}

void ModuleCustomerReadCardForTopUp(void)
{
	step1:
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/membercard.bmp");
	sdkDispBrushScreen();
	sdkmSleep(1500);
	//Trace("xgd", "Start ModuleReadCard= \r\n");
	

   	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	
	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	
	  	
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asCustomerCardNo,0,sizeof(sendData.asCustomerCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("ModuleCustomerReadCardForTopUp", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	if(strcmp(sendData.asFirstDigit,"2")==0)
	{
		memcpy(sendData.asCustomerCardNo,&buf1[0],9);
		Trace("ModuleCustomerReadCardForTopUp","Customer Card No=%s\r\n",sendData.asCustomerCardNo);
		TopUpLoginFace();
		
	}
	else 
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "MEMBER CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		goto step1;
	}	
		
}

void TopUpLoginFace(void)
{
	s32 key;
	
	Trace("TopUpLoginFace","Amount=%d\r\n",sendData.asTopUpAmount);
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/auth.bmp");
	sdkDispBrushScreen();
	
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC,KbWaitTime);
		
			switch(key)
			{
				case SDK_KEY_1:
					{
						
						TopUpLoginCheck(1);
					}
					break;
				case SDK_KEY_2:
					{
					
						TopUpLoginCheck(2);
					}
								
					break;
				case SDK_KEY_ESC:
					{
						ModuleCustomerReadCardForTopUp();
					}
					break;
			}
	
	ModuleCustomerReadCardForTopUp();
}
void TopUpLoginCheck(int i)
{
	u32 Temp_timer;
	Temp_timer = sdkTimerGetId();

	
	s32 rslt;
	if(topup.MAINMenu==New_Menu)
	{
		fingerprint.Error=NEWMAINMENU;
		Trace("Topuppaywithcard","New_Menu");
		
	}
	else if (topup.MAINMenu==Old_Menu)
	{
		fingerprint.Error=TOPUP;
			Trace("Topuppaywithcard","old_Menu");
	}
	
	if(i==1)
	{	
		
		while(1)
		{	
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT...", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();

			if (sdkTimerIsEnd(Temp_timer, 120 * 1000))     //Timer-out 60s
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				//return ChooseOperatorMenu();
				if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									ChooseOperatorMenu();
								}
			}
		
			rslt = MemberFPCheck();

			if (rslt == SDK_OK)                                                               
			{
				rslt = VerifyFPC(receivedData.asFingerTemplate,3);

				if (rslt == SDK_OK)                                                               
				{
					memset(sendData.asCustomerEnterPin,0,sizeof(sendData.asCustomerEnterPin));
					strcpy(sendData.asCustomerEnterPin,"0");
					Trace("xgd", "Customer Enter Pin= %s\r\n",sendData.asCustomerEnterPin);	
					ModulePayTMMCard();
				}
				else			
				{
					if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									ChooseOperatorMenu();
								}
				}
			}
			else
			{
				if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									ChooseOperatorMenu();
								}
			}
		}
	}
	else
	{
		s32 key;
		u8 buf[64] = {0};
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/enterpincode.bmp");
		sdkDispBrushScreen();
		
	
		memset(sendData.asCustomerEnterPin, 0, sizeof(sendData.asCustomerEnterPin));
		
  		key = sdkKbGetScanf(60000, buf, 6, 6, SDK_MMI_PWD ,SDK_DISP_LINE4);
  		
  			if(key == SDK_OK)
  		{
  			memcpy(sendData.asCustomerEnterPin,&buf[1],6);
			Trace("xgd", "Customer Enter Pin= %s\r\n", sendData.asCustomerEnterPin);
			memset(receivedData.AccessFinger,0,sizeof(receivedData.AccessFinger));
			strcpy(receivedData.AccessFinger,"0");
			Trace("xgd", "Customer Finger= %s\r\n",receivedData.AccessFinger);
			ModulePayTMMCard();
		}
		else
		{
			if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									ChooseOperatorMenu();
								}
		}
  			
  		if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									ChooseOperatorMenu();
								}
	
	
	}
	if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									ChooseOperatorMenu();
								}
		
			
}
s32 MemberFPCheck(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	s32 len, len1, i;

	fingerprint.Error=TOPUP;
	u8 dispbuf[32+1] = {0};
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	memset(message,0,sizeof(message));				
	sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asCustomerCardNo);

	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
			
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
			
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
			
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/cusFpCheck/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asCustomerCardNo,sendData.asHashData);
			
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
			
	ret = SendReceivedData(sendData.asSendBuf,false);
		
		
		if(ret!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
					
			Trace("MEMBER FP CHECK","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
							
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{

				memset(receivedData.AccessFinger,0,sizeof(receivedData.AccessFinger));
				js_tok(receivedData.AccessFinger,"fingerPrint",0);
				
				Trace("xgd","receivedData.AccessFinger= %s\r\n",receivedData.AccessFinger);
				
				memset(receivedData.asFingerTemplate,0,sizeof(receivedData.asFingerTemplate));
				sdkAscToBcd(receivedData.asFingerTemplate, receivedData.AccessFinger, 512);
			
				ret = SDK_OK;
				
			
			}
			else
				{
				memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
				js_tok(receivedData.ErrorMessage,"message",0);
				len = 128;
				sdkDispClearScreen();
			    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "FP CHECK FAIL", SDK_DISP_DEFAULT);
			    for (i = 0; i < 4 && len > 0; i++)
			    {
			        len1 = len > 20 ? 20 : len;
			        len -= len1;
			        memcpy(dispbuf, &receivedData.ErrorMessage[20*i], len1);
			        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
			    }
			    sdkDispBrushScreen();
		        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
				ret = 0;
				}
		}
		else
		{
			sdkDispClearScreen();    
       	 	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        	sdkDispBrushScreen();
        	sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
			ret = 0;
		}
		return ret;
}


void ModulePayTMMCard(void)
{
	u8 out[128] = {0};
	//u8 buf[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	Trace("xgd","topup.Operator = %d\r\n",topup.Operator);
	Trace("xgd","topup.TopUpType = %d\r\n",topup.TopUpType);
	Trace("xgd","Topup Amount = %d\r\n",sendData.asTopUpAmount);
	
	if(topup.TopUpType==TOPUP_ELOAD)
	{
		switch(topup.Operator)
		{
			case MPT:
				{
						Trace("xgd","MPT ELoad Pay With Card ");
						
						ForSendData();
						Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
						
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s%s%d%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
						Trace("xgd","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/mptELoadCard/%s/%s/%s/%s/%s/%d/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret!=0)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
								js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								js_tok(receivedData.CustRef,"customerCardRef",0);
								js_tok(receivedData.Amount,"amount",0);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;              //  for pageagentcheckbalance
						
								js_tok(receivedData.ErrorMessage,"message",0);
		
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
								
							}
						}
						
						else
							{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
								{
									
									case SDK_KEY_ESC:
										{
											if(topup.MAINMenu==New_Menu)
	        								{
	        		 
	            								NewMainMenu();
											}
											else
											{
					
				
											NewMainMenu();
											}
										}
										break;
										default:
										{
											
										}
									
								}
									if(topup.MAINMenu==New_Menu)
	        						{
	        		 
	            								NewMainMenu();
									}
								else
									{
					
				
									NewMainMenu();
									}
							}
				}
				break;
				case OOREDOO:
					{
						Trace("xgd","Ooredoo ELoad Pay With Card ");
						
						ForSendData();
						Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
						
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s%s%d%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
						Trace("xgd","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/ooredooELoadCard/%s/%s/%s/%s/%s/%d/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret!=0)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;   //  for pageagentcheckbalance
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
								js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								js_tok(receivedData.CustRef,"customerCardRef",0);
								js_tok(receivedData.Amount,"amount",0);
								js_tok(receivedData.RefNo,"refNo",0);
								Trace("module pay card","ooredoo ref no=%s\r\n",receivedData.RefNo);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;              //  for pageagentcheckbalance
					
								js_tok(receivedData.ErrorMessage,"message",0);
		
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
		
							}
						}
						
						else
							{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
								{
									
									case SDK_KEY_ESC:
										{
											if(topup.MAINMenu==New_Menu)
	        								{
	        		 
	            								NewMainMenu();
											}
											else
											{
					
				
											NewMainMenu();
											}
										}
										break;
										default:
										{
										
										}
									
								}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									NewMainMenu();
								}
							}
					}
					break;
					
				case MECTEL:
				{
						Trace("modulepaycash","MEC ELoad Pay With Card ");
						
						ForSendData();
						Trace("modulepaycash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
						
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s%s%d%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
						Trace("modulepaycash","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/mecELoadCard/%s/%s/%s/%s/%s/%d/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret!=0)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{	
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;   //  for pageagentcheckbalance
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
								js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								js_tok(receivedData.CustRef,"customerCardRef",0);
								js_tok(receivedData.Amount,"amount",0);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{	
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
									transaction=false;              //  for pageagentcheckbalance
						
								
								js_tok(receivedData.ErrorMessage,"message",0);
		
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
						}
						
						else
							{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
								{
									
									case SDK_KEY_ESC:
										{
											if(topup.MAINMenu==New_Menu)
	        								{
	        		 
	            								NewMainMenu();
											}
										else
											{
						
				
											NewMainMenu();
											}
										}
										break;
										default:
										{
											
										}
									
								}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									NewMainMenu();
								}
							}
				}
				break;
				
				case TELENOR:
					{
													
						Trace("ModulePayCard","Telenor ELoad  url ");				
						ForSendData();
						Trace("ModulePayCard","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
						
						memset(message,0,sizeof(message));
				
						sprintf(message,"%s%s%s%s%s%d%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asTopUpAmount,sendData.asPhoneNum,sendData.asEDCserial);
						Trace("xgd","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						//telenorMyanPayELoadCard/{version}/{cusFactoryCardNo}/{agentFactoryCardNo}/{terminalRef}/{fingerPrint}/{pinNo}/{amount}/{phoneNo}/{checkTrx}/{hashValue}
						sprintf(sendData.asSendBuf,"%s%s/telenorMyanPayELoadCard/%s/%s/%s/%s/%s/%s/%d/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asTopUpAmount,sendData.asPhoneNum,sendData.asEDCserial,sendData.asHashData);
		
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret==SDK_OK)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							
							Trace("ModulepayCard,TelenorEload,firsturl","receivedData.ServiceStatus=%s\r\n",receivedData.ServiceStatus);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								memset(receivedData.InvoiceNumber,0,sizeof(receivedData.InvoiceNumber));
								memset(receivedData.OrderNumber,0,sizeof(receivedData.OrderNumber));			
								js_tok(receivedData.InvoiceNumber,"invoiceNumber",0);						
								js_tok(receivedData.OrderNumber,"trx",0);
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
								js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
								js_tok(receivedData.CardRef,"customerCardRef",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								
								
								strcpy(sendData.InvoiceNumber,receivedData.InvoiceNumber);
								strcpy(sendData.OrderNumber,receivedData.OrderNumber);						
								
								Trace("xgd","receivedData.InvoiceNumber=%s\r\n",sendData.InvoiceNumber);
								Trace("xgd","receivedData.Trx=%s\r\n",sendData.OrderNumber);
								Trace("xgd","Telenor ELoad first EnquiryCard ");
						
								memset(message,0,sizeof(message));
						
								sprintf(message,"%s%s%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.InvoiceNumber,sendData.OrderNumber);
								Trace("xgd","message=%s\r\n",message);
								sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
				
								memset(sendData.asHashData, 0, 128);
								sdkBcdToAsc(sendData.asHashData, out, 32);
								Trace("xgd","tmp = %s\r\n",sendData.asHashData);
								
								sdkmSleep(15000);
								memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
								sprintf(sendData.asSendBuf,"%s%s/telenorELoadEnquiryCard/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.InvoiceNumber,sendData.OrderNumber,sendData.asHashData);
				
								memset(&receivedData,0,sizeof(receivedData));
								ret = SendReceivedData(sendData.asSendBuf,false);
								
								if(ret==SDK_OK)
								{	
									memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
									js_tok(receivedData.ServiceStatus,"serviceStatus",0);
									Trace("ModulePayCard,TelenorEload,EnquiryCardfisturl","receivedData.ServiceStatus=%s\r\n",receivedData.ServiceStatus);
									if(strcmp(receivedData.ServiceStatus,"Success")==0)
									{
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
										sdkDispBrushScreen();
										sdkmSleep(1500);
										transaction=true;              //  for pageagentcheckbalance
										secondtransactionstart=true;
										js_tok(receivedData.PhoneNo,"mobileNo",0);
										js_tok(receivedData.TRX,"trx",0);
										js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
										js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
										js_tok(receivedData.Amount,"amount",0);
										
										PrintTopUp();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
									}							
									else if(strcmp(receivedData.ServiceStatus,"Pending")==0)
									{
									
										Trace("ModulePayCard","Telenor ELoad Equary second time");
										memset(message,0,sizeof(message));
										sprintf(message,"%s%s%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.InvoiceNumber,sendData.OrderNumber);
										Trace("xgd","message=%s\r\n",message);
										sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
					
										memset(sendData.asHashData, 0, 128);
										sdkBcdToAsc(sendData.asHashData, out, 32);
										Trace("ModulePayCash","tmp = %s\r\n",sendData.asHashData);
										sdkmSleep(15000);
										memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
										sprintf(sendData.asSendBuf,"%s%s/telenorELoadEnquiryCard/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.InvoiceNumber,sendData.OrderNumber,sendData.asHashData);
										
										memset(&receivedData,0,sizeof(receivedData));
										ret = SendReceivedData(sendData.asSendBuf,false);
									if(ret==SDK_OK)
									{
										memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));	
										js_tok(receivedData.ServiceStatus,"serviceStatus",0);
										Trace("ModulePayCash","receivedData.ServiceStatus=%s\r\n",receivedData.ServiceStatus);
										
										if((strcmp(receivedData.ServiceStatus,"Success")==0)||(strcmp (receivedData.ServiceStatus,"Pending")==0	))
										{	
												sdkDispClearScreen();
												sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
												sdkDispBrushScreen();
												sdkmSleep(1500);
												transaction=true;              //  for pageagentcheckbalance
												secondtransactionstart=true;
												js_tok(receivedData.PhoneNo,"mobileNo",0);
												js_tok(receivedData.TRX,"trx",0);
												js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
												js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
												js_tok(receivedData.Amount,"amount",0);
												PrintTopUp();
												sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
										}
										
										else
										{
											
											sdkDispClearScreen();
											sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
											sdkDispBrushScreen();
											sdkmSleep(1500);
											transaction=false;
											js_tok(receivedData.ErrorMessage,"message",0);
					
											js_tok(receivedData.ErrorCode,"code",0);
											if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
											{
											sdkDispClearScreen();
											sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
											sdkDispBrushScreen();
											sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
											}	
											else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
											{
												
												sdkDispClearScreen();
												sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
												sdkDispBrushScreen();
												sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
													
											}
											else
											{
												DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
											}
										
		
										}
									}
									else
									{
										s32 key;
										sdkDispClearScreen();
										sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
										sdkDispBrushScreen();
										sdkmSleep(1500);
										
										sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
										sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
										sdkDispBrushScreen();
										
										key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
											switch(key)
											{
												
												case SDK_KEY_ESC:
													{
														//MainMenu();
														
														if(topup.MAINMenu==New_Menu)
			        									{
			        		 
			            									NewMainMenu();
														}
														else
														{
							
						
															MainMenu();
														}
													}
													break;
													default:
													{
														
													}
												
											}
										if(topup.MAINMenu==New_Menu)
			        					{
			        		 
			            					NewMainMenu();
										}
										else
										{					
						
											MainMenu();
										}
									}
						
								}
								else
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
									js_tok(receivedData.ErrorMessage,"message",0);
									transaction=false;
					
									js_tok(receivedData.ErrorCode,"code",0);
									if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
				
									{
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
										sdkDispBrushScreen();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}	
									else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
									{
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
										sdkDispBrushScreen();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}
									else
									{
										DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
									}
							
								}	
										
								}
								else
								{
									transaction=false;
									s32 key;
										sdkDispClearScreen();
										sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
										sdkDispBrushScreen();
										sdkmSleep(1500);
										
										sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
										sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
										sdkDispBrushScreen();
										
										key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
											switch(key)
											{
												
												case SDK_KEY_ESC:
													{
														//MainMenu();
														
														if(topup.MAINMenu==New_Menu)
			        									{
			        		 
			            									NewMainMenu();
														}
														else
														{
							
						
															MainMenu();
														}
													}
													break;
													default:
													{
														
													}
												
											}
										if(topup.MAINMenu==New_Menu)
			        					{
			        		 
			            								NewMainMenu();
										}
										else
										{
							
						
											MainMenu();
										}
								}
						
							}
							else
							{			
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);	
								js_tok(receivedData.ErrorMessage,"message",0);
								transaction=false;	
								js_tok(receivedData.ErrorCode,"code",0);
								if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
								{		
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}	
								else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
								{
									sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
								sdkDispBrushScreen();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
								}
								else
								{
									DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
								}
							
			
							}
						}
							
						else
						{
							Trace("xgd","Telenor ELoad checkTrx second url first time");
							memset(message,0,sizeof(message));
							sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
							Trace("xgd","message=%s\r\n",message);
							sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
							memset(sendData.asHashData, 0, 128);
							sdkBcdToAsc(sendData.asHashData, out, 32);
							Trace("xgd","tmp = %s\r\n",sendData.asHashData);
			
							memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
							sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
							
							memset(&receivedData,0,sizeof(receivedData));
							ret = SendReceivedData(sendData.asSendBuf,false);
							if(ret==SDK_OK)
							{
								memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
								js_tok(receivedData.ServiceStatus,"serviceStatus",0);
								
								if(strcmp(receivedData.ServiceStatus,"Success")==0)
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
									transaction=true;              //  for pageagentcheckbalance
									secondtransactionstart=true;
									js_tok(receivedData.PhoneNo,"mobileNo",0);
									js_tok(receivedData.TRX,"trx",0);
									js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
									js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
									js_tok(receivedData.Amount,"amount",0);
									PrintTopUp();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
								}
								else
								{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
									sdkDispBrushScreen();
									sdkmSleep(1500);
									transaction=false;
									js_tok(receivedData.ErrorMessage,"message",0);
			
									js_tok(receivedData.ErrorCode,"code",0);
									if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
									{
									sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}	
									else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
									{
										sdkDispClearScreen();
									sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
									sdkDispBrushScreen();
									sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
									}
									else
									{
										DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
									}
								
								}
							}
							else
							{
								Trace("xgd","Telenor ELoad checkTrx second url second time");
								memset(message,0,sizeof(message));
								sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial);
								Trace("xgd","message=%s\r\n",message);
								sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
			
								memset(sendData.asHashData, 0, 128);
								sdkBcdToAsc(sendData.asHashData, out, 32);
								Trace("xgd","tmp = %s\r\n",sendData.asHashData);
				
								memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
								sprintf(sendData.asSendBuf,"%s%s/checkTransaction/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asEDCserial,sendData.asHashData);
								
								memset(&receivedData,0,sizeof(receivedData));
								ret = SendReceivedData(sendData.asSendBuf,false);
								if(ret==SDK_OK)
								{
									memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));	
									js_tok(receivedData.ServiceStatus,"serviceStatus",0);
									if(strcmp(receivedData.ServiceStatus,"Success")==0)
									{
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
										sdkDispBrushScreen();
										sdkmSleep(1500);
										transaction=true;              //  for pageagentcheckbalance
										secondtransactionstart=true;
										js_tok(receivedData.PhoneNo,"mobileNo",0);
										js_tok(receivedData.TRX,"trx",0);
										js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
										js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
										js_tok(receivedData.Amount,"amount",0);
										PrintTopUp();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
									}
									else
									{
								
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
										sdkDispBrushScreen();
										sdkmSleep(1500);
										transaction=false;
										js_tok(receivedData.ErrorMessage,"message",0);
				
										js_tok(receivedData.ErrorCode,"code",0);
										if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
										{
										sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
										sdkDispBrushScreen();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
										}	
										else if((strcmp(receivedData.ErrorCode,"Err00277")==0)||(strcmp(receivedData.ErrorCode,"Err00272")==0)||(strcmp(receivedData.ErrorCode,"Err00273")==0)||(strcmp(receivedData.ErrorCode,"Err00274")==0)||(strcmp(receivedData.ErrorCode,"Err00275")==0)||(strcmp(receivedData.ErrorCode,"Err00147")==0))
									{
											sdkDispClearScreen();
										sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/outofstock.bmp");
										sdkDispBrushScreen();
										sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
										}
										else
										{
											DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
										}
									
		
									}
								}
								else
								{
									s32 key;
										sdkDispClearScreen();
										sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
										sdkDispBrushScreen();
										sdkmSleep(1500);
										
										sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
										sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
										sdkDispBrushScreen();
										
										key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
											switch(key)
											{
												
												case SDK_KEY_ESC:
													{
														//MainMenu();
														
														if(topup.MAINMenu==New_Menu)
			        									{
			        		 
			            									NewMainMenu();
														}
														else
														{
							
						
															MainMenu();
														}
													}
													break;
													default:
													{
														
													}
												
											}
										if(topup.MAINMenu==New_Menu)
			        					{
			        		 
			            								NewMainMenu();
										}
										else
										{
							
						
											MainMenu();
										}
								}
						
							}
					
						
						}
					}
				break;
		}
					
		//MainMenu();
		if(topup.MAINMenu==New_Menu)
	    {
	        		 
	        NewMainMenu();
		}
		else
		{
					
				
		NewMainMenu();
		}
	}
	else if(topup.TopUpType==TOPUP_EPIN)
	{
			switch(topup.Operator)
			{
					case MPT:
					{
						
						Trace("xgd","MPT Epin Pay With Card ");
						
						ForSendData();
						Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
						
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s%d%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
						Trace("xgd","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/mptEPinCard/%s/%s/%s/%s/%d/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret!=0)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
								js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								js_tok(receivedData.CustRef,"customerCardRef",0);
								js_tok(receivedData.Amount,"amount",0);
								js_tok(receivedData.SerialNo,"serialNumber",0);
								js_tok(receivedData.ExpDate,"epinExpDate",0);				
							//	js_tok(receivedData.AccessPinCode,"pinCode",0);
								js_tok(receivedData.PhPinCode,"pinNumber",0);
       					
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
							
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;              //  for pageagentcheckbalance
					
								js_tok(receivedData.ErrorMessage,"message",0);
		
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
		
							}
						}
						
						else
							{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
								{
									
									case SDK_KEY_ESC:
										{
										if(topup.MAINMenu==New_Menu)
	        								{
	        		 
	            								NewMainMenu();
											}
										else
											{
												NewMainMenu();
											}
										}
										break;
										default:
										{
										//	MainMenu();
										}
									
								}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									NewMainMenu();
								}
							}
					}
					break;
				case TELENOR:
					{
						Trace("xgd","Telenor Epin Pay With Card ");
						
						ForSendData();
						Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
						
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s%d%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
						Trace("xgd","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/telenorEPinCard/%s/%s/%s/%s/%d/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret!=0)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;   //  for pageagentcheckbalance	
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
								js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								js_tok(receivedData.CustRef,"customerCardRef",0);
								js_tok(receivedData.Amount,"amount",0);
								js_tok(receivedData.SerialNo,"serialNumber",0);
								js_tok(receivedData.ExpDate,"epinExpDate",0);
								js_tok(receivedData.PhPinCode,"pinNumber",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;              //  for pageagentcheckbalance
					  
								js_tok(receivedData.ErrorMessage,"message",0);
		
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
		
							}
						}
						
						else
							{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
								{
									
									case SDK_KEY_ESC:
										{
											if(topup.MAINMenu==New_Menu)
	        								{
	        		 
	            								NewMainMenu();
											}
											else
											{
					
												ChooseOperatorMenu();
												}
										}
										break;
										default:
										{
											//MainMenu();
										}
									
								}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									NewMainMenu();
								}
							}
					}
					break;
				case MECTEL:
				{
					Trace("xgd","MEc Epin Pay With Card ");
						
						ForSendData();
						Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
						
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s%d%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
						Trace("xgd","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("xgd","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/mecEPinCard/%s/%s/%s/%s/%d/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret!=0)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
							secondtransactionstart=true;   //  for pageagentcheckbalance
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx1",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate1",0);
								js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
								js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								js_tok(receivedData.CustRef,"customerCardRef",0);
								js_tok(receivedData.Amount,"amount",0);
								js_tok(receivedData.SerialNo,"serialNo1",0);
								js_tok(receivedData.ExpDate,"expireDate1",0);
								js_tok(receivedData.PhPinCode,"pinCode1",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;              //  for pageagentcheckbalance
						
								
								js_tok(receivedData.ErrorMessage,"message",0);
		
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
		
							}
						}
						
						else
							{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
								{
									
									case SDK_KEY_ESC:
										{
											if(topup.MAINMenu==New_Menu)
	        							{
	        		 
	            								NewMainMenu();
										}
										else
										{
					
				
											NewMainMenu();
										}
										}
										break;
										default:
										{
											//MainMenu();
										}
									
								}
								if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									NewMainMenu();
								}
							}
				}
				break;
			case OOREDOO:
					{
						Trace("modulepaycash","Ooredoo Epin Pay With Card ");
						
						ForSendData();
						Trace("modulepaycash","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
						
						memset(message,0,sizeof(message));
						sprintf(message,"%s%s%s%d%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial);
						Trace("modulepaycash","message=%s\r\n",message);
						sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
						memset(sendData.asHashData, 0, 128);
						sdkBcdToAsc(sendData.asHashData, out, 32);
						Trace("modulepaycash","tmp = %s\r\n",sendData.asHashData);
		
						memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
						sprintf(sendData.asSendBuf,"%s%s/ooredooEPinCard/%s/%s/%s/%s/%d/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTopUpAmount,receivedData.AccessFinger,sendData.asCustomerEnterPin,sendData.asEDCserial,sendData.asHashData);
						
						memset(&receivedData,0,sizeof(receivedData));
						ret = SendReceivedData(sendData.asSendBuf,false);
						if(ret!=0)
						{
							memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
							js_tok(receivedData.ServiceStatus,"serviceStatus",0);
							if(strcmp(receivedData.ServiceStatus,"Success")==0)
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=true;              //  for pageagentcheckbalance
								secondtransactionstart=true;   //  for pageagentcheckbalance	
								
								js_tok(receivedData.PhoneNo,"mobileNo",0);
								js_tok(receivedData.TRX,"trx",0);
								js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
								js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
								js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
								js_tok(receivedData.BalanceAmount,"customerCardBalance",0);
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
								js_tok(receivedData.CustRef,"customerCardRef",0);
								js_tok(receivedData.Amount,"amount",0);
								js_tok(receivedData.SerialNo,"serialNo",0);
								js_tok(receivedData.ExpDate,"expireDate",0);
								
								/*js_tok(receivedData.PhPinCode,"pinCode",0);
								Trace("Ooredoo Epin","pincode(before decryption)=%s\r\n",receivedData.PhPinCode);
								dc_message(receivedData.PhPinCode);
								Trace("Ooredoo Epin","pincode(after decryption)=%s\r\n",receivedData.PhPinCode);*/
								
								 	js_tok(receivedData.AccessPinCode,"pinCode",0);
       								Trace("Ooredoo Epin","pincode(before decryption)=%s\r\n",receivedData.AccessPinCode);
      					 			dc_message(receivedData.AccessPinCode);
      							 Trace("Ooredoo Epin","pincode(after decryption)=%s\r\n",receivedData.AccessPinCode);
      						 
      						 	strcpy(receivedData.PhPinCode,SpaceFormat(receivedData.AccessPinCode));
       						Trace("Ooredoo Epin","After Space=%s\r\n",receivedData.PhPinCode);
								
								js_tok(receivedData.AgentCardRef,"agentCardRef",0);
							
								PrintTopUp();
								sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
							}
							else
							{
								sdkDispClearScreen();
								sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								transaction=false;              //  for pageagentcheckbalance
						
								js_tok(receivedData.ErrorMessage,"message",0);
		
								DisplayErrorMessage("TOPUP",receivedData.ErrorMessage,false);
		
							}
						}
						
						else
							{
								s32 key;
								sdkDispClearScreen();
								sdkDispShowBmp(0,0,240,320,"/mtd0/res/checkbalancefortopup.bmp");
								sdkDispBrushScreen();
								sdkmSleep(1500);
								
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"[CANCEL] TO ",SDK_DISP_LEFT_DEFAULT);
								sdkDispFillRowRam(SDK_DISP_LINE5,0,"MAIN MENU",SDK_DISP_RIGHT_DEFAULT);
								sdkDispBrushScreen();
								
								key=sdkKbWaitKey(SDK_KEY_MASK_ESC,60000);
									switch(key)
								{
									
									case SDK_KEY_ESC:
										{
											if(topup.MAINMenu==New_Menu)
	        							{
	        		 
	            								NewMainMenu();
										}
										else
										{
					
				
											NewMainMenu();
										}
										}
										break;
										default:
										{
										//	MainMenu();
										}
									
								}
							if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									NewMainMenu();
								}
							}
					}
					break;
			}
			
			if(topup.MAINMenu==New_Menu)
	        					{
	        		 
	            								NewMainMenu();
								}
								else
								{
					
				
									NewMainMenu();
								}
				
			}

}


