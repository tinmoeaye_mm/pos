/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : agent.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/2/10
   Last Modified :
   Description   : agent module-related functions are defined
                  here
   History       :
   1.Date        : 2016/2/10
    Author      : Thu Kha,Tin Moe Aye
    Modification: Created file

******************************************************************************/
#include "../inc/global.h"

const u8 asPathSelectFinger[] = "/mtd0/res/fpselect.bmp";
void AgentActivationV2(void);
void UpdateAgentFingerPrint();
void ReprintMainMenu(void);
void DisplayProgbar1();
void versionread(void);
void DownloadOTA();
void VersionImagedownload(void);
void NewMainMenu(void);
char timer[20] = {0};
const u8 delFp1[] = "/mtd0/app_sk.lic";
const u8 delFp2[]="mtd0/dll/libFPC.so";

void AgentCheckBalanceV2(void)
{
 	u8 out[128] = {0};
	u8 message[128] ={0};
	u8 fpData[32] ="0";
	s32 ret;
	s32 key = 0;
	u8 buf[128] = {0};

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);

	sdkDispBrushScreen();
	
	memset(message,0,sizeof(message));
 	sprintf(message,"%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin);
	Trace("agentcheckbalance","message=%s\r\n",message);
	
	sdkCalcHmacSha256(TSK, strlen(TSK),message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("agentcheckbalance","tmp = %s\r\n",sendData.asHashData);

	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/agentChkBalance/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asHashData);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{	
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			
			transaction=true;              //  for pageagentcheckbalance
			secondtransactionstart=true;
			
			js_tok(receivedData.BalanceAmount,"agentBalanceAmount",0);
			
			
			js_tok(receivedData.CardNo,"agentCardNo",0);

			
			js_tok(receivedData.CardRef,"agentCardRef",0);

			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

		
			js_tok(receivedData.TRX,"trx",0);

			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);

			sdkDispSetFontSize(SDK_DISP_FONT_BIG);
			sdkDispClearScreen();

			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s", "AGENT CHECK BALANCE");
			sdkDispAt(12, 20, buf);
		
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s","BALANCE:");
			sdkDispAt(12,80,buf);
			memset(buf,0,sizeof(buf));
			sprintf(buf,"KS %s",receivedData.BalanceAmount);
			sdkDispAt(12,120,buf);
			
			sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
			sdkDispFillRowRam(SDK_DISP_LINE5, 0, "PRESS [ENTER] TO PRINT SLIP", SDK_DISP_DEFAULT);
			sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	        sdkDispBrushScreen();

			key = sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);

			switch (key)
	        {
	             case SDK_KEY_ENTER:
	                {
	                	agent.AgentMainMenu = Agent_New_Menu;
	                	PrintAgentCheckBalance();
	                
						
						                    
	                }
	                break;
	             case SDK_KEY_ESC:
	                {
	                    NewMainMenu();
	                }
	             default:
	                {
	                }
	                break;
	        }
						
		}
		else
		{	
			transaction=false;
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
				
			js_tok(receivedData.ErrorMessage,"message",0);
			
			DisplayErrorMessage("AGENT CHECK BALANCE",receivedData.ErrorMessage,false);
		}
		
	}
	else
	{
		DisplayErrorMessage("","",true);
	}

	NewMainMenu();

}

void VersionChangeV2(void)
{
	u8 timetmp[128] = {0};
	u8 servertime [12] = {0};
	s32 ret;
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/terVersionChangeV3/%s/%s/%s",uri,IPAddress,SvcVersionV3,sendData.asTerminalSN,AppVersion);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);

	if(ret!=0)
	{
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			memset(timetmp, 0, sizeof(timetmp));
			js_tok(servertime,"transactionLogDate",0);
			sdkAscToBcd(timetmp,servertime,sizeof(servertime));
    		sdkSetRtc(timetmp);
			
			js_tok(receivedData.key,"key",0);
		
			Trace("versionchange","receivedData.key EN = %s\r\n",receivedData.key);
		
			dc_message(receivedData.key);

			Trace("versionchange","receivedData.key DC = %s\r\n",receivedData.key);
			
			memset(TSK,0,sizeof(TSK));
			memcpy(TSK,receivedData.key,strlen(receivedData.key));

			Trace("versionchange","TSK = %s\r\n",TSK);
				
		}
			
		else
		{
			sdkDispClearScreen();    
	        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "TERMINAL VERSION ", SDK_DISP_DEFAULT);
	        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "CHANGE", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "FAIL", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE5, 0, "PLEASE TRY AGAIN", SDK_DISP_DEFAULT);
	        sdkDispBrushScreen();
	        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
	        AppMain();
		}
	}
		
	else
	{
		sdkDispClearScreen();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
		AppMain();
	}
}


s32 AgentFPCheckV2(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret, len, len1, i;
	u8 dispbuf[32+1] = {0};
	u8 fpData[1024] = { 0 };
	
	sprintf(message,"%s%s",sendData.asAgentCardNo,sendData.asTerminalSN);

	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("AgentFPCheckV2","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/agentFpCheck/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	ret = SendReceivedData(sendData.asSendBuf,false);

	if(ret!=0)
	{
		memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			memset(fpData,0,sizeof(fpData));
			js_tok(fpData,"fingerPrint",0);
		
			Trace("AgentFPCheckV2","fpData = %s\r\n",fpData);
			memset(receivedData.asFingerTemplate,0,sizeof(receivedData.asFingerTemplate));
			sdkAscToBcd(receivedData.asFingerTemplate, fpData, 512);
			
			ret = SDK_OK;
		}
		else
		{
			memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
			js_tok(receivedData.ErrorMessage,"message",0);
			len = 128;
			sdkDispClearScreen();
		    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "FP CHECK FAIL", SDK_DISP_DEFAULT);
		    for (i = 0; i < 4 && len > 0; i++)
		    {
		        len1 = len > 20 ? 20 : len;
		        len -= len1;
		        memcpy(dispbuf, &receivedData.ErrorMessage[20*i], len1);
		        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
		    }
		    sdkDispBrushScreen();
	        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
			ret = 0;
		}
	}
	else
	{
		sdkDispClearScreen();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		ret = 0;
	}
	return ret;
}

void AgentSummReport(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);

	sdkDispBrushScreen();
	sprintf(message,"%s%s",sendData.asAgentCardNo,sendData.asTerminalSN);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message),out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("AgentSummReport","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/agentSumReport/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	Trace("AgentSummReport","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			js_tok(receivedData.openingBalance,"openingBalance",0);
			
			js_tok(receivedData.closingBalance,"closingBalance",0);

			js_tok(receivedData.commission,"commission",0);

			js_tok(receivedData.mpt,"mptTopup",0);

			js_tok(receivedData.ooredoo,"ooredooTopup",0);
			
			js_tok(receivedData.mectel,"mecTopup",0);
			
			js_tok(receivedData.TelenorEpin,"telenorTopup",0);
			
			js_tok(receivedData.cp,"cp",0);
			
			js_tok(receivedData.bnf,"bnf",0);
			
			js_tok(receivedData.remit,"totalRemittance",0);
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);

			js_tok(receivedData.HelloCab,"helloCabs",0);
			
			js_tok(receivedData.CnpAmount,"cnpAmount",0);
			
			js_tok(receivedData.AeonName,"aeon",0);
			
			js_tok(receivedData.Trip,"titanAmount",0);
			
			js_tok(receivedData.TripDate,"ticketBoAmount",0);
			
			js_tok(receivedData.RefillAmount,"refillAmount",0);
   
  			js_tok(receivedData.Amount,"localCashOutAmount",0);
   
   			js_tok(receivedData.BillAmount,"irsCashOutAmount",0);
   
  			js_tok(receivedData.CopyDueDate,"fromDate",0);
   
   			js_tok(receivedData.DueDate,"toDate",0);
   
   			js_tok(receivedData.Status,"nt2nt",0);
   			
   			js_tok(receivedData.key,"worldMyanmarTourAmount",0);
   			
   			js_tok(receivedData.WithdrawAmount,"withdrawAmount",0);
   			
   			js_tok(receivedData.BankName,"refundAmount",0);
   			
   			js_tok(receivedData.AeonPhoneNo,"cashBackAmont",0);
   			
   			js_tok(receivedData.AgentCardNo,"agentCardNo",0);
						

			Trace("AgentSummReport","receivedData.openingBalance = %s\r\n",receivedData.openingBalance);
			Trace("AgentSummReport","receivedData.bnf = %s\r\n",receivedData.bnf);
			Trace("AgentSummReport","receivedData.ooredoo= %s\r\n",receivedData.ooredoo);
				
			PrintAgentReport();
			
	    }
	    else
		{	
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			
			js_tok(receivedData.ErrorMessage,"message",0);
			
			DisplayErrorMessage("AGENT SUMMARY REPORT",receivedData.ErrorMessage,false);
		}
	    
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	
			MainMenu();
}
	

void BankInfo(void)
{	
	SDK_PRINT_FONT st_font;
	u8 message[128] ={0};
	u8 out[128] = {0};
	s32 ret;	
	char buf [128];
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	
	sdkDispBrushScreen();
	
	sprintf(message,"%s%s",sendData.asAgentCardNo,sendData.asTerminalSN);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("BankInfo","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/bankInfo/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asHashData);
	
	Trace("BankInfo","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0) 
	{
			
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
				
				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);	
					js_tok(receivedData.BankName,"bankName",0);     	
							  	
					  				  
					js_tok(receivedData.AccNum,"accNo",0);
							 	 
					sdkDispClearScreen();
					sdkDispFillRowRam(SDK_DISP_LINE1,0,receivedData.ServiceStatus,SDK_DISP_DEFAULT);
					sdkDispFillRowRam(SDK_DISP_LINE2,0,"Printing...",SDK_DISP_DEFAULT);
					sdkDispBrushScreen();
					PrintBankInfo();
							  
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
					
					js_tok(receivedData.CardRef,"agentCardRef",0);

					js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
					js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
					
						int num=0,j,numlen;
						int spacing = 4;
						char temp [256] = {0};
						char box [128] = {0};
						char *j_tok1 = "ACC NO :"; 
						char *J_tok2 = "BRANCH :"; 
					
				    	memset(temp,0,sizeof(temp));
						memset(box,0,sizeof(box));
				 		sdkPrintInit();	
				 		memset(&st_font, 0, sizeof(st_font));
				 		
				 		st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
				 		st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				 	
				 		numlen = js_array(&num,"accType");
						for (j= 26 ; j <37; j++)
						{
			    			if(js_get_array_element(temp,num,j++)==0)
			      			{
								  
			      				sdkDispMsgBox("Error","ArrayElements",SDK_KEY_MASK_ENTER,30000);
				   			 }
							if(j==27)
							{
								Trace("bankinfo","temp=%s\r\n",temp);
								sprintf(box,"%s %s",j_tok1,temp);
				          	    sdkPrintStr(box,st_font,SDK_PRINT_LEFTALIGN,0,spacing); 				 
							}
							else if (j==35)
							{
								Trace("bankinfo","temp=%s\r\n",temp);
								 sprintf(box,"%s %s",J_tok2,temp);
				          		 sdkPrintStr(box,st_font,SDK_PRINT_LEFTALIGN,0,spacing); 
								 sdkPrintStr(" ",st_font,SDK_PRINT_LEFTALIGN,0,spacing); 
				         		 
							}
					
				    		memset(box,0,sizeof(box));
							j=j+2;
						}
				
				 		for (j= 49 ; j <60; j++)
							{
			     				if(js_get_array_element(temp,num,j++)==0)
			      					 {
			      		 				sdkDispMsgBox("Error","ArrayElements",SDK_KEY_MASK_ENTER,30000);
				    				 }
								 if(j==50)
								 {
								 	Trace("bankinfo","temp=%s\r\n",temp);
									 sprintf(box,"%s %s",j_tok1,temp);
					          	     sdkPrintStr(box,st_font,SDK_PRINT_LEFTALIGN,0,spacing); 
									   				 
					 			}
								 else if (j==58)
					 			{
					 				Trace("bankinfo","temp=%s\r\n",temp);
								   	sprintf(box,"%s %s",J_tok2,temp);
				          		  	sdkPrintStr(box,st_font,SDK_PRINT_LEFTALIGN,0,spacing);
									sdkPrintStr(" ",st_font,SDK_PRINT_LEFTALIGN,0,spacing); 
				                				 
								 }
					
				    			 memset(box,0,sizeof(box));
					 			j=j+2;
					 			
							 }
							 for (j= 72 ; j <83; j++)
							{
			     				if(js_get_array_element(temp,num,j++)==0)
			      					 {
			      		 				sdkDispMsgBox("Error","ArrayElements",SDK_KEY_MASK_ENTER,30000);
				    				 }
								 if(j==73)
								 {
								 	Trace("bankinfo","temp=%s\r\n",temp);
									 sprintf(box,"%s %s",j_tok1,temp);
					          	     sdkPrintStr(box,st_font,SDK_PRINT_LEFTALIGN,0,spacing); 
									   				 
					 			}
								 else if (j==81)
					 			{	Trace("bankinfo","temp=%s\r\n",temp);
								   	sprintf(box,"%s %s",J_tok2,temp);
				          		  	sdkPrintStr(box,st_font,SDK_PRINT_LEFTALIGN,0,spacing); 
				                				 
								 }
								 
					
				    			 memset(box,0,sizeof(box));
					 			j=j+2;
					 			
							 }
			  
					 memset(&st_font, 0, sizeof(st_font));
					 st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
			         st_font.uiAscZoom = SDK_PRN_ZOOM_N;
					
					memset(temp, 0, sizeof(temp));
					memset(temp, '-', 32);	
				
					memset(&st_font, 0, sizeof(st_font));
			    	st_font.uiAscFont = SDK_PRN_ASCII12X24YB;
			    	st_font.uiAscZoom = SDK_PRN_ZOOM_N;
		    
				    memset(buf, 0, sizeof(buf));
					memset(buf, '-', 32);
					sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
					
					memset(buf, 0, sizeof(buf));
					sprintf(buf, "AID       : %s",receivedData.CardRef);
				   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
		
				   	memset(buf, 0, sizeof(buf));
					sprintf(buf, "POS       : %s",receivedData.TerminalSerialNo);
				   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				   	
				   	memset(buf, 0, sizeof(buf));
					sprintf(buf, "DATE/TIME : %s",receivedData.TransactionLogDate);
				   	sdkPrintStr(buf, st_font, SDK_PRINT_LEFTALIGN, 0, spacing);
				   	
				   	 memset(buf, 0, sizeof(buf));
					 memset(buf, '-', 32);
					 sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
     				sdkPrintBitMap("/mtd0/res/remittanceinfo.bmp", SDK_PRINT_MIDDLEALIGN, 0);
     				
				   	memset(buf, 0, sizeof(buf));
					memset(buf, '-', 32);
					sdkPrintStr(buf, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
					
					
					// Set font
				    memset(&st_font, 0, sizeof(st_font));
				    st_font.uiAscFont = SDK_PRN_ASCII8X16;
				    st_font.uiAscZoom = SDK_PRN_ZOOM_N;
				
					sdkPrintStr(PrintBLine1, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
					sdkPrintStr(PrintBLine2, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
					sdkPrintStr(PrintBLine3, st_font, SDK_PRINT_MIDDLEALIGN, 0, spacing);
		
					sdkPrintStart();
					CheckPaperRollStatus();
				}
				
			else
			{	sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
				sdkDispBrushScreen();
				sdkmSleep(1500);
				memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
				js_tok(receivedData.ErrorMessage,"message",0);
			
				DisplayErrorMessage("BANK INFO",receivedData.ErrorMessage,false);
			}
					
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	
		MainMenu();
	
}

s32 CheckAgentPINModule()
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	u8 dispbuf[32+1] = {0};
	s32 len, len1, i;

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asAgentEnterPin);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("CheckAgentPINModule","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/agentPinCheck/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asAgentEnterPin,sendData.asHashData);
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);

	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			ret = SDK_OK;
		}
		else
		{
			
			js_tok(receivedData.ErrorMessage,"message",0);
			js_tok(receivedData.ErrorCode,"code",0);
			if(strcmp(receivedData.ErrorCode,"Err00142")==0)
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/wrongpin.bmp");
				sdkDispBrushScreen();	
			}
			else
			{
				len = 128;
				sdkDispClearScreen();
			    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "LOGIN FAIL", SDK_DISP_DEFAULT);
			    for (i = 0; i < 4 && len > 0; i++)
			    {
			        len1 = len > 20 ? 20 : len;
			        len -= len1;
			        memcpy(dispbuf, &receivedData.ErrorMessage[20*i], len1);
			        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
			    }
				sdkDispBrushScreen();
		        	
			}
			sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
			ret =0;
		    
		}
	}
	else
	{
		sdkDispClearScreen();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
		ret = 0;
	}
	return ret;
}

void AgentRqeuestMenu(void)

{
	s32 key = 0;
	u32 Temp_timer;
	Temp_timer = sdkTimerGetId();
	int tmp;
	u8 buf[1024];
	
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/agentrequest1.bmp");
	sdkDispBrushScreen();

	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | 
							   SDK_KEY_MASK_5 | SDK_KEY_MASK_6 |SDK_KEY_MASK_7 | SDK_KEY_MASK_8 |SDK_KEY_MASK_9| SDK_KEY_MASK_ESC, 120000);
	
	sprintf(buf,"[APPNUM]\nappnum=02\nupdatenum=00\ndefaultapp=OTA\n[MANAGE]\napp=manage\ndisplay=Manager\nver=V4BCNMNG120614152\nappdate=00000000000000\nparamdate=YYYYMMDDHHMMSS\nenable=1\nselect=MN101\n[APP01]\napp=TMD\ndisplay=TMDMain\nver=V11NMDQM120719251\nappdate=00000000000000\nparamdate=YYYYMMDDHHMMSS\nenable=1\nselect=YL101\n[APP02]\napp=OTA\ndisplay=OTAApp\nver=V11NMDQM120719252\nappdate=00000000000000\nparamdate=YYYYMMDDHHMMSS\nenable=1\nselect=YL101\n[LIB]\nlibnum=07\nlib01=daemon.bin.138\nlib02=panel.so.216\nlib03=libdev.so.194\nlib04=libxgdemv.so.118\nlib05=libxgdmodule.so.129\nlib06=libxgdmaths.so.110\nlib07=libappdevice.so.103");
				
	switch (key)
	{
		 case SDK_KEY_1:
			{
				 FunUndDev(3);
			}
			break;

		 case SDK_KEY_2:
			{
				ForOTA=true;//For return
				tmp = sdkWriteFile("/mtd0/res/multitask.ini",buf,sizeof(buf));
				//tmp = sdkWriteFile("multitask.ini",buf,sizeof(buf));

				if(tmp == SDK_FILE_OK)
				{
							 
					u32 Temp_timer;
					Temp_timer = sdkTimerGetId();
					sdkDispClearScreen();
					sdkDispShowBmp(0,0,240,320,"/mtd0/res/otaverupdate.bmp");
					sdkDispBrushScreen();
					sdkmSleep(10000);
					
					s32 key = sdkKbWaitKey(SDK_KEY_POWER_OFF,60000);
					
					if(key==SDK_KEY_POWER_OFF)
					{
						sdkSysBeep(SDK_SYS_BEEP_OK);
						
					}
						
				}
				else
				{
					sdkDispClearScreen();	  
					sdkDispFillRowRam(SDK_DISP_LINE2, 0, "WRITE FAIL", SDK_DISP_DEFAULT);
					sdkDispBrushScreen();
				}
				
			}
			break;

		 case SDK_KEY_3:
			{
				FunUndDev(3);
			}
			break;

		 case SDK_KEY_4:
			{
				FunUndDev(3);
			}
			break;

		 case SDK_KEY_5:
			{
				FunUndDev(3);
			}
			break;

		case SDK_KEY_6:
			{
				FunUndDev(3);
			}
			break;
		case SDK_KEY_7:
			{
				
				PinChange();
			}
			break;
		case SDK_KEY_8:
			{
				UpdateAgentFingerPrint();
			}
			break;
		case SDK_KEY_9:
			{
				ReprintMainMenu();
			}	
		 case SDK_KEY_ESC:
			{
				AgentMenu();
			}
			break;
			
		 default:
			{
			}
			break;
	}
	if(ForOTA==true)
	{	
		ForOTA=false;
		
		AppMain();
	}
	else if(ForOTA==false)
	{
		AgentMenu();
	}
	
}

/*void DownloadOTA()
{
	u8 recvbuf[300*1024]={0};
	u8 sendbuf[1024]={0};
	int ret = 0;
	int len = 0;
	
	sdkDelFile("/mtd0/dll/OTA.so");
	
	sdkDispClearScreen();    
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OTA Version 0.1.0", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "DELETE OK.", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	memset(recvbuf, 0, sizeof(recvbuf));
	
	SDK_EXT_CURL_DATA curl_data = {
            .bMethod = true,                            // GET method
            .psURL = "http://122.248.120.162/edcdev/OTA.so",
            .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
            .nPort = 9988,                                 // HTTP default port
            .psSend = sendbuf,                           // POST data
            .nSendLen = strlen(sendbuf),
            .psRecv = recvbuf,                            // receive buffer
            .nRecvLen = sizeof(recvbuf),
            .nTimeout =  120 * 1000 ,                         // time out 2 mins
    };
    
	
    ret = sdkExtCurl(&curl_data);
	
	len= sizeof(recvbuf);
	Trace("DownloadUpdate","recBufSize = %d\r\n",len);
	
	if(SDK_OK == ret)
    {   
		
		sdkDispClearScreen();    
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OTA(0.2.0)DOWNLOAD", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "OK.", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "WRITING FILE.", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();

		Trace("DownloadUpdate","Writing file...\r\n");
		

        int temp = sdkWriteFile("/mtd0/dll/OTA.so",recvbuf,sizeof(recvbuf));
		
		
        if(temp == SDK_FILE_OK){
			
			sdkDispClearScreen(); 
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OTA(0.2.0)WRITE OK.", SDK_DISP_DEFAULT);   
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "WRITE COMPLETE", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
	
			Trace("DownloadUpdate","Write complete.\r\n");
				 
        }
		else
		{
			sdkDispClearScreen();    
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "OTA(0.2.0)WRITE FAIL.", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "WRITE FAIL!", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();

			Trace("DownloadUpdate","Write fail!");
		}
		
		
    }
    else if(SDK_ERR == ret || SDK_TIME_OUT == ret)
    {
        sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "DOWNLOAD FAIL!", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "[ENTER] 	DOWNLOAD AGAIN", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE5, 0, "[CANCEL] 	SKIP", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
        
		
       
    }
}*/
void AgentPinChange(void)
{
	u8 temp[1024];
	u8 temp2[128];
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key = 0;
	s32 ret = 0;
	u8 dispbuf[32+1] = {0};
    s32 len, len1, i;
	u8 flag = 0;
		
	memset(sendData.asAgentEnterPin, 0, sizeof(sendData.asAgentEnterPin));
	
	memset(temp, 0, sizeof(temp));

	sdkDispClearScreen();
	sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/pincodeold.bmp");
	sdkDispBrushScreen();				  

	key = sdkKbGetScanf(30000, temp, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE4);
	
	if (key == SDK_KEY_ENTER)
	{
		memcpy(sendData.asAgentEnterPin, &temp[1], 6);
		Trace("AgentPinChange","sendData.asAgentEnterPin (Old) = %s\r\n",sendData.asAgentEnterPin);

		//Step1:
		memset(sendData.asAgentEnterNewPin, 0, sizeof(sendData.asAgentEnterNewPin));
		memset(temp2, 0, sizeof(temp2));
		sdkDispClearScreen();
		sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/pincodenew.bmp");
		sdkDispBrushScreen();	

		key = sdkKbGetScanf(30000, temp2, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE4);

		if (key == SDK_KEY_ENTER)
		{
			memcpy(sendData.asAgentEnterNewPin, &temp2[1], 6);
			Trace("AgentPinChange","sendData.asAgentEnterNewPin (1) = %s\r\n",sendData.asAgentEnterNewPin);
			
			while (1)
			{
				sdkDispClearScreen();
				sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/pincodenewagain.bmp");
				sdkDispBrushScreen();
				memset(temp2, 0, sizeof(temp2));

				key = sdkKbGetScanf(30000, temp2, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE4);

				if (key == SDK_KEY_ESC || key == SDK_TIME_OUT)
				{
					//AgentMenu();
					AppMain();
				}

				ret = memcmp(sendData.asAgentEnterNewPin, &temp2[1], sizeof(sendData.asAgentEnterNewPin));

				if (SDK_EQU != ret)
				{
					sdkDispClearRow(SDK_DISP_LINE3);
					sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PIN NOT MATCH", SDK_DISP_NOFDISP | SDK_DISP_CDISP | SDK_DISP_INCOL);
					sdkDispBrushScreen();
					sdkmSleep(1000);
					flag++;

					if (flag == 3)
					{
						//AgentMenu();
						AppMain();
					}
				}
				else
				{
					break;
				}
			}
	
			sdkDispClearScreen();	 
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();

		 	sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asAgentEnterPin,sendData.asAgentEnterNewPin,sendData.asAgentEnterNewPin);
			Trace("AgentPinChange","message = %s\r\n",message);

			sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

			memset(sendData.asHashData, 0, 128);
			sdkBcdToAsc(sendData.asHashData, out, 32);

			Trace("AgentPinChange","tmp = %s\r\n",sendData.asHashData);

			//en_message(sendData.asAgentEnterPin);

			memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
			sprintf(sendData.asSendBuf, "%s%s/agentPinChange/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asAgentEnterPin,sendData.asAgentEnterNewPin,sendData.asAgentEnterNewPin,sendData.asHashData);

			Trace("AgentPinChange","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
			
			memset(&receivedData,0,sizeof(receivedData));
			ret = SendReceivedData(sendData.asSendBuf,false);
			
			

			if(ret!=0)
			{
				
				js_tok(receivedData.ServiceStatus,"serviceStatus",0);

				Trace("AgentPinChange","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
				
				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					sdkDispClearScreen();    
			        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "AGENT PIN CHANGE", SDK_DISP_DEFAULT);		
			        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
					sdkDispBrushScreen();
					sdkmSleep(1500);
				
					AppMain();
				}
				else
				{
				
					js_tok(receivedData.ErrorMessage,"message",0);
					len = 128;
					sdkDispClearScreen();
				    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "PIN CHANGE FAIL", SDK_DISP_DEFAULT);
				    for (i = 0; i < 4 && len > 0; i++)
				    {
				        len1 = len > 20 ? 20 : len;
				        len -= len1;
				        memcpy(dispbuf, &receivedData.ErrorMessage[20*i], len1);
				        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
				    }
				    sdkDispBrushScreen();
				    sdkmSleep(1500);
				    
			       
				}
			
			}
			else
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
				sdkDispBrushScreen();
				sdkmSleep(1500);
				sdkDispClearScreen();    
		        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PLEASE TRY AGAIN", SDK_DISP_DEFAULT);
		        sdkDispBrushScreen();
		        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
		        AppMain();
			}
			
		}	
		else if(key == SDK_KEY_ESC)
		{
			
			AppMain();
		}
		
	}	
	else if(key == SDK_KEY_ESC)
	{
		
		AppMain();
	}

	
	AppMain();
}

void UpdateAgentFingerPrint(void)
{
	u8 temp[1024];
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key = 0;
	s32 ret = 0;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, asPathSelectFinger);
	sdkDispBrushScreen();
	u32 Temp_timer;
	fingerprint.Error=AGENT;

	Temp_timer = sdkTimerGetId();
	
    key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);

	if (key == SDK_KEY_1)
	{
		strcpy(sendData.asFingerImage, "lindex");
	}
	else if (key == SDK_KEY_2)
	{
		strcpy(sendData.asFingerImage, "rthumb");
	}
	else
	{
		AgentMenu();
	}

	Trace("UpdateAgentFingerPrint","sendData.asFingerimage = %s\r\n",sendData.asFingerImage);
	
	ret = EnrollFPC(2, true, temp);

	s32 len2;
	u8 *pBufLen = &temp[8];
	len2 = ((pBufLen[0] << 24) 
		  | (pBufLen[1] << 16)
		  | (pBufLen[2] << 8)
		  | pBufLen[3]);
	
	u8 buf2[1024] = {0};
	sdkBcdToAsc(buf2, temp, len2);
	TraceHex("UpdateAgentFingerPrint", "Fingerprint Template", temp, len2);
	
	Trace("UpdateAgentFingerPrint","buf2 = %s\r\n",buf2);

	if (ret != SDK_OK)
	{
		AgentMenu();
	}
	memset(sendData.asFingerTemplate,0,sizeof(sendData.asFingerTemplate));

	memcpy(sendData.asFingerTemplate,buf2,len2*2); //cody for pst data
	
	ret = VerifyFPC(temp, SafeLEVEL_3);
	if (ret != SDK_OK)									  //cancel key to exit
	{
		AgentMenu();
	}
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

 	sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asFingerTemplate);

	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("UpdateAgentFingerPrint","tmp = %s\r\n",sendData.asHashData);

	en_message(sendData.asAgentEnterPin);

	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/agentFpUpdate/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asFingerTemplate,sendData.asHashData);

	Trace("UpdateAgentFingerPrint","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);

	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);

		Trace("UpdateAgentFingerPrint","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();    
	        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "UPDATE AGENT", SDK_DISP_DEFAULT);		
	        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "FINGERPRINT", SDK_DISP_DEFAULT);		
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
			sdkDispBrushScreen();
			sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
		}
		else
		{
			
			js_tok(receivedData.ErrorMessage,"message",0);
			DisplayErrorMessage("UPDATE FINGERPRINT",receivedData.ErrorMessage,false);
		}
	
	}
	else
	{
		DisplayErrorMessage("","",true);
	}

	//AgentMenu();
	//MainMenu();
	AppMain();
}
void MemberPinChange(void)
{
	u8 temp[1024];
	u8 temp2[128];
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key = 0;
	s32 ret = 0;
	u8 dispbuf[32+1] = {0};
    s32 len, len1, i;
	u8 flag = 0;
		
	memset(sendData.asMemberEnterPin, 0, sizeof(sendData.asMemberEnterPin));
	memset(temp, 0, sizeof(temp));

	sdkDispClearScreen();
	sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/pincodeold.bmp");
	sdkDispBrushScreen();				  

	key = sdkKbGetScanf(30000, temp, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE4);
	
	if (key == SDK_KEY_ENTER)
	{
		memcpy(sendData.asMemberEnterPin, &temp[1], 6);
		Trace("MemberPinChange","sendData.asMemberEnterPin (Old) = %s\r\n",sendData.asMemberEnterPin);

		//Step1:
		memset(sendData.asMemberEnterNewPin, 0, sizeof(sendData.asMemberEnterNewPin));
		memset(temp2, 0, sizeof(temp2));
		sdkDispClearScreen();
		sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/pincodenew.bmp");
		sdkDispBrushScreen();	

		key = sdkKbGetScanf(30000, temp2, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE4);

		if (key == SDK_KEY_ENTER)
		{
			memcpy(sendData.asMemberEnterNewPin, &temp2[1], 6);
			Trace("MemberPinChange","sendData.asMemberEnterNewPin (1) = %s\r\n",sendData.asMemberEnterNewPin);
			
			while (1)
			{
				sdkDispClearScreen();
				sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/pincodenewagain.bmp");
				sdkDispBrushScreen();
				memset(temp2, 0, sizeof(temp2));

				key = sdkKbGetScanf(30000, temp2, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE4);

				if (key == SDK_KEY_ESC || key == SDK_TIME_OUT)
				{
					//PinChange();
					AppMain();
				}

				ret = memcmp(sendData.asMemberEnterNewPin, &temp2[1], sizeof(sendData.asMemberEnterNewPin));

				if (SDK_EQU != ret)
				{
					sdkDispClearRow(SDK_DISP_LINE3);
					sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PIN NOT MATCH", SDK_DISP_NOFDISP | SDK_DISP_CDISP | SDK_DISP_INCOL);
					sdkDispBrushScreen();
					sdkmSleep(1000);
					flag++;

					if (flag == 3)
					{
						//PinChange();
						AppMain();
					}
				}
				else
				{
					break;
				}
			}
			
	
			sdkDispClearScreen();	 
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();

		 	sprintf(message,"%s%s%s%s%s%s",sendData.asAgentCardNo,sendData.asMemberCardNo,sendData.asTerminalSN,sendData.asMemberEnterPin,sendData.asMemberEnterNewPin,sendData.asMemberEnterNewPin);
			Trace("MemberPinChange","message = %s\r\n",message);

			sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

			memset(sendData.asHashData, 0, 128);
			sdkBcdToAsc(sendData.asHashData, out, 32);

			Trace("MemberPinChange","tmp = %s\r\n",sendData.asHashData);

			//en_message(sendData.asAgentEnterPin);

			memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
			sprintf(sendData.asSendBuf, "%s%s/memberPinChange/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asMemberCardNo,sendData.asTerminalSN,sendData.asMemberEnterPin,sendData.asMemberEnterNewPin,sendData.asMemberEnterNewPin,sendData.asHashData);

			Trace("MemberPinChange","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
			
			memset(&receivedData,0,sizeof(receivedData));
			ret = SendReceivedData(sendData.asSendBuf,false);
			
			if(ret!=0)
			{
				
				js_tok(receivedData.ServiceStatus,"serviceStatus",0);

				Trace("MemberPinChange","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
				
				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
					sdkDispClearScreen();    
			        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "MEMBER PIN CHANGE", SDK_DISP_DEFAULT);		
			        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
					sdkDispBrushScreen();
					sdkmSleep(1500);
					
					AppMain();
				}
				else
				{
				
					js_tok(receivedData.ErrorMessage,"message",0);
					len = 128;
					sdkDispClearScreen();
				    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "PIN CHANGE FAIL", SDK_DISP_DEFAULT);
				    for (i = 0; i < 4 && len > 0; i++)
				    {
				        len1 = len > 20 ? 20 : len;
				        len -= len1;
				        memcpy(dispbuf, &receivedData.ErrorMessage[20*i], len1);
				        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
				    }
				    sdkDispBrushScreen();
				    sdkmSleep(1500);
			       
				}
			
			}
			else
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
				sdkDispBrushScreen();
				sdkmSleep(1500);
				sdkDispClearScreen();    
		        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PLEASE TRY AGAIN", SDK_DISP_DEFAULT);
		        sdkDispBrushScreen();
		        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
			}
			
		}	
		else if(key == SDK_KEY_ESC)
		{
			
			AppMain();
			
		}
		
	}	
	else if(key == SDK_KEY_ESC)
	{
	
		AppMain();
	}

	AppMain();
		
}
void PinChange(void)
{
	s32 key=0;
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/pinchangemenu.bmp");
    sdkDispBrushScreen();

	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);
	switch(key)
	{
		case SDK_KEY_1:
			{
				AgentPinChange();
			}
			break;
		case SDK_KEY_2:
			{				
				showMemberCard();
			}
			break;
		case SDK_KEY_ESC:
			{
				AgentMenu();
			}
			break;
			default:
			{
				
			}
			
	}
	AgentMenu();
}
void showMemberCard(void)
{
	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	
	
	step1:
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/membercard.bmp");
	sdkDispBrushScreen();
	sdkmSleep(1500);
	
	printtype.CashPrint=CASHBALANCE;
   	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	
	  	
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asMemberCardNo,0,sizeof(sendData.asMemberCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("showMemberCard", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	
	if(strcmp(sendData.asFirstDigit,"0")==0||strcmp(sendData.asFirstDigit,"1")==0)
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Please Tap Member Card.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(1500);
		goto step1;
	}
	if(strcmp(sendData.asFirstDigit,"2")==0)
	{
		memcpy(sendData.asMemberCardNo,&buf1[0],9);
		
	}	
	Trace("showMemberCard","Customer Card No=%s\r\n",sendData.asMemberCardNo);
	MemberPinChange();
}

void ForSendData(void)
{
	char temp[50] = {0};
	
	char *str = (char *)malloc(23);
	

	strcpy(str,sendData.asTerminalSN);
	
	str=str+6;
	memset(sendData.asEDCtime,0,sizeof(sendData.asEDCtime));
	strcpy(sendData.asEDCtime,str);
	Trace("ForSendData","EDCTime=%\r\n",sendData.asEDCtime);
	
	
	memset(temp,0,sizeof(temp));
	memset(timer,0,sizeof(timer));
	sdkGetRtc(temp);    
	sdkBcdToAsc(timer, temp, 6);
	Trace ("ForSendData","EDCTime=%s\r\n",timer);
	
	strcat(sendData.asEDCtime,timer);
	memset(sendData.asEDCserial,0,sizeof(sendData.asEDCserial)); 
	strcpy(sendData.asEDCserial,sendData.asEDCtime);
	Trace ("ForSendData","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
	 //trxtype.TRX=today;
	
}
/*void savetransactionTRX(void)
{
	u8 file_contents[1024]={0};
	u8 newtemp_to_file[1024]={0};
	s32 date1;s32 date;
	
	
		
	s32 file_len = 1024;
	u8 temp;
	Trace("xgd","sendData.asEDCserial=%s\r\n",sendData.asEDCserial);
	
	u8 time[128]={0};
	u32 len =6; 
	u8 temp_to_file[1024];
	u8 trxtime[128]={0};
	
	if(trxtype.TRX==today)
	{
		date1=strncpy(date,timer,6);
		Trace("xgd","date=%s\r\n",date);
		trxtype.TRX=yesterday;
	}

	Trace("xgd","EDCtime=%s\r\n",timer);
	sprintf(time,"%s\r\n",timer);
	sdkWriteFile(transacton_history,time,12);
	sdkReadFile(transacton_history,trxtime,0,&len);
	Trace("xgd","transacton_history=%s\r\n",trxtime);
	//if(strcmp(trxtime,date1)==0)
	if(sdkAccessFile(transacton_history))
	{
	
	
		
		sdkReadFile(savetrx,file_contents,0,&file_len);
		Trace("xgd","savetrx=%s\r\n",file_contents);
		
		sprintf(temp_to_file,"%s\r\n%s",sendData.asEDCserial,file_contents);
		temp=sdkWriteFile(savetrx,temp_to_file,strlen(temp_to_file));
		
		
		
		
	}
	else
	{
		
		Trace("xgd","EDCtime=%s\r\n",timer);
	
		
		sprintf(time,"%s\r\n",timer);
		sdkWriteFile(newtransacton_history,time,12);
		sdkReadFile(newtransacton_history,trxtime,0,&len);
		Trace("xgd","newtransacton_history=%s\r\n",trxtime);
		if(sdkAccessFile(newtransacton_history))
		{
			
			sdkReadFile(newsavetrx,file_contents,0,&file_len);
			Trace("xgd","savetrx=%s\r\n",file_contents);
		
			sprintf(newtemp_to_file,"%s\r\n%s",sendData.asEDCserial,file_contents);
			temp=sdkWriteFile(newsavetrx,newtemp_to_file,strlen(temp_to_file));
		}
		
		
	}
	
	
	if(temp == SDK_FILE_OK)
	
		{
							 
					  
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SAVE TRX SUCCESS.", SDK_DISP_DEFAULT);
			
			sdkDispBrushScreen();
			sdkmSleep(1500);
				
		
			
		}
		else
		{
			sdkDispClearScreen();	  
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SAVE TRX FAIL.", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
		}

}*/
/*void versionread(void)
{
	 u8 readversion[1024]={0};
	 u8 SendServerVersion[32];
	 int len = 0, spaces = 0;
	 u32 length=10;
	 s32 ret=0;
	 memset(AppVersion,0,sizeof(AppVersion));
	 memset(PrintBLine3,0,sizeof(PrintBLine3));
	 memset(ver,0,sizeof(ver));
	 memset(accessVesion,0,sizeof(accessVesion));
	 memset(readversion,0,sizeof(readversion));
	 memset(SendServerVersion,0,sizeof(SendServerVersion));
	 ret=sdkReadFile("/mtd0/res/version.ini",readversion,0,&length);
	 strcpy(PrintBLine3,"Version ");
	 strncat(PrintBLine3,readversion,10);
	 strncat(accessVesion,readversion,10);
	 strcpy(ver,"v ");
	 strncat(ver,readversion,10);
	 strncat(ver,"d",1); 
	 strcpy(verlife,"v ");
	 strncat(verlife,readversion,10);
	 Trace("xgd","PrintBline3=%s\r\n",PrintBLine3); 
	 Trace("xgd","ver=%s\r\n",ver); 
	 Trace("xgd","AppVersion=%s\r\n",accessVesion); 
	 Trace("xgd","verlife=%s\r\n",verlife); 
	 memcpy(SendServerVersion,accessVesion,strlen(accessVesion));
	 Trace("xgd","SendServerVersion=%s\r\n",SendServerVersion);
	 
	 
	        
	    
	        // Scan through src counting spaces and length at the same time //
	        while (SendServerVersion[len]) {
	         if (SendServerVersion[len] == ' ')
	           ++spaces;
	         ++len;
	        }
	    
	        // Figure out how much space the new string needs (including 0-term) and allocate it //
	        int newLen = len + spaces*2 + 1;
	        char * dst = malloc(newLen);
	        // Scan through src and either copy chars or insert %20 in dst //
	        int srcIndex=0,dstIndex=0;
	        while (SendServerVersion[srcIndex]) {
	        if (SendServerVersion[srcIndex] == '.') {
	          dst[dstIndex++]='-';
	          
	          ++srcIndex;
	        } else {
	          dst[dstIndex++] = SendServerVersion[srcIndex++];
	        }
	        }
	        dst[dstIndex] = '\0';
	    
	        Trace("SendServerVersion_dst","dst=%s\r\n",dst);
	     memcpy(AppVersion,dst,strlen(dst)+1);
	     Trace("Appversion","appversion=%s\r\n",AppVersion);
	   
	/// Trace("xgd","AppVersion=%s\r\n",AppVersion); 
	   
	 
}*/

void FPdelete(void)
{
	if(sdkAccessFile(delFp1))
	{
		sdkDelFile("/mtd0/app_sk.lic");
	}
	else 
	{
			
	}
	if(sdkAccessFile(delFp2))
	{
		sdkDelFile("mtd0/dll/libFPC.so");
	}
	else 
	{
		
	}
}

/*void VersionImagedownload(void)
{
	//u8 out[128] = {0};
	u8 recvbuf[300*1024]={0};
	u8 sendbuf[1024]={0};
	int ret = 0;
	int len = 0;
	u8 buf[128] = {0};
	u8 buf1[128]={0};
	memset(buf,0,sizeof(buf));
	sprintf(buf, "http://122.248.120.162/edcdev/Versionimagedownload/%s",receivedData.imageforVersion);
	Trace("VersionImagedownload","image path for download=%s\r\n",buf);	
		
    memset(recvbuf, 0, sizeof(recvbuf));
	
	SDK_EXT_CURL_DATA curl_data = {
            .bMethod = true,
 
 	                 // GET method
			
			.psURL = buf,
			
            
            .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
            .nPort = 9988,                                 // HTTP default port
            .psSend = sendbuf,                           // POST data
            .nSendLen = strlen(sendbuf),
            .psRecv = recvbuf,                            // receive buffer
            .nRecvLen = sizeof(recvbuf),
            .nTimeout =  120 * 1000 ,                         // time out 2 mins
    };
    
	
    ret = sdkExtCurl(&curl_data);
	
	len= sizeof(recvbuf);
	Trace("VersionImagedownload","recBufSize = %d\r\n",len);
	
	if(SDK_OK == ret)
    {   
		//Trace("DownloadUpdate","recvbuf = %s\r\n",recvbuf);
		sdkDispClearScreen();    
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, " IMAGE DOWNLOAD", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "OK.", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "DOWNLOAG FILE.", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkmSleep(4000);
		Trace("VersionImagedownload","Writing file...\r\n");
		
		
		memset(buf1,0,sizeof(buf1));
		sprintf(buf1,"/mtd0/res/%s",receivedData.imageforVersion);
		Trace("VersionImagedownload","image path for download=%s\r\n",buf1);

        int temp = sdkWriteFile(buf1,recvbuf,sizeof(recvbuf));
		
		
        if(temp == SDK_FILE_OK){
			
			sdkDispClearScreen(); 
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "IMAGE WRITE OK.", SDK_DISP_DEFAULT);   
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "WRITE COMPLETE", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
			sdkmSleep(4000);
				Trace("DownloadUpdate","Write complete.\r\n");
			
				sdkPrintInit();
				sdkPrintBitMap(buf1, SDK_PRINT_RIGHTALIGN, 0);
				sdkPrintStart(); 
				 
        }
		else
		{
			sdkDispClearScreen();    
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "IMAGE WRITE FAIL.", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "WRITE FAIL!", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
			sdkmSleep(4000);
			Trace("VersionImagedownload","Write fail!");
		}
		
		
    }
    else if(SDK_ERR == ret || SDK_TIME_OUT == ret)
    {
        sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "DOWNLOAD FAIL!", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "[ENTER] 	DOWNLOAD AGAIN", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE5, 0, "[CANCEL] 	SKIP", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkmSleep(4000);
		
		Trace("VersionImagedownload","TIMEOUT1!");
        
		
       
    }
    sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,buf1);
	sdkDispBrushScreen();
	sdkmSleep(4000);
	


	if(sdkAccessFile(buf1))
	{
		sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "DOWNLOAD FAIL!", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "EXIT", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkmSleep(4000);
		sdkDelFile(buf1);
		
		Trace("VersionImagedownload","Delete image file =%s\r\n",buf1);
		
		
		
		
		
	}
	else
	{
		
	}
	if(sdkAccessFile(buf1))
	{
		sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "DOWNLOAD FAIL!", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "EXIT", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkmSleep(4000);
	}
	else 
	{
		sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "DOWNLOAD FAIL!", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, " NO EXIT", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkmSleep(4000);
	}

}*/




		


