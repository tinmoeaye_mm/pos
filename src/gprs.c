/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : gprs.c
   Version       : Initial Draft
   Author        : Cody
   Created       : 2014/7/24
   Last Modified :
   Description   : This file shows how to communicate with GPRS
   History       :
   1.Date        : 2014/7/24
    Author      : Cody
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"
int ModuleGprs(void);

/*******************************************************************************
** Description :  Send and receive data through GPRS/WCDMA
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-24
** Remark      :  1. configure GPRS/WCDMA
                  2. create a link
                  3. send data
                  4. receive data
                  5. destroy link

** Modify By     :  Thu Kha  2015-01-14
 
*******************************************************************************/
//void dominname();
int ModuleGprs(void);
char ip_addr_list[10][16] = {{0}};
	  s32 i, ip_num;
int ModuleGprs(void)
{
    SDK_COMM_STCOMMPARAM st_comm_param;
    s32 res;
	DispClearContent();    
    //sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CREATING PPP...", SDK_DISP_DEFAULT);
    sdkDispShowBmp(0,0,240,320,"/mtd0/res/connect.bmp");
    sdkDispBrushScreen();
    
    memset(&st_comm_param, 0, sizeof(st_comm_param));
    st_comm_param.eMode = SDK_COMM_GPRS;
    strcpy(st_comm_param.stCommInfo.stPppWireLessInfo.asGprsApn, APN);
    strcpy(st_comm_param.stServerInfo.asServerIP, IPAddress1);        //can be any valid IP
   // strcpy(st_comm_param.stServerInfo.asServerIP, "truemoney.com.mm");
   Trace("modulegprs","Connect IPAddress1");
    strcpy(st_comm_param.stServerInfo.asPort, "80");
    sdkCommConfig(&st_comm_param);
    memset(&res,0,sizeof(res));
    res = sdkCommCreatePPP(20*1000);
    Trace("xgd", "sdkCommCreatePPP() = %d\r\n", res);

    if (res != SDK_OK)
    {
    	Trace("modulegprs","Connect IPAddress1 not ok");
        DispClearContent();    
        
         memset(&st_comm_param, 0,sizeof(st_comm_param));
    	st_comm_param.eMode = SDK_COMM_GPRS;
	    strcpy(st_comm_param.stCommInfo.stPppWireLessInfo.asGprsApn, APN);
	    strcpy(st_comm_param.stServerInfo.asServerIP, IPAddress1);        //can be any valid IP
	   // strcpy(st_comm_param.stServerInfo.asServerIP, "truemoney.com.mm");
	   Trace("modulegprs","Connect IPAddress2");
	    strcpy(st_comm_param.stServerInfo.asPort, "80");
	    sdkCommConfig(&st_comm_param);
	    memset(&res,0,sizeof(res));
	    res = sdkCommCreatePPP(20*1000);
	    Trace("xgd", "sdkCommCreatePPP() = %d\r\n", res);
	    if (res != SDK_OK)
	    {
	    	Trace("modulegprs","Connect IPAddress2 not ok");
	    	DispClearContent();    
        	sdkDispShowBmp(0,0,240,320,"/mtd0/res/againconnect.bmp");
	        sdkDispBrushScreen();
	        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
	        return 0;
		}
		else if(res==SDK_OK)
		{
			Trace("modulegprs","Connect IPAddress2 ok");
			return SDK_OK;
		}
    }
	/*else if (res==SDK_OK)
	{
		Trace("modulegprs","Connect IPAddress 1 ok");
		return SDK_OK;

	}*/
	return SDK_OK;	
}


