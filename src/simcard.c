/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : simcard.c
  Version       : Initial Draft
  Author        : Cody
  Created       : 2015/9/30
  Last Modified :
  Description   : A demo on how to get SIM card info
  Function List :
  History       :
  1.Date        : 2015/9/30
    Author      : Cody
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"


/*******************************************************************************
** Description :  Get IMSI & ICCID from SIM card
** Parameter   :  void
** Return      :  
** Author      :  Cody   2015-09-30 
** Remark      :  1. call sdkCommCreatePPP() to create a PPP link
                  2. call sdkCommGetSimIMSI() to get IMSI
*******************************************************************************/
void ModuleSimCard(void)
{
    u8 str_imsi[32] = {0};
    u8 str_iccid[32] = {0};
    s32 res;
    SDK_COMM_STCOMMPARAM st_comm_param;

    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "SIM CARD", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
    sdkDispBrushScreen();

    // Must create a PPP link first before you can get IMSI
    DispClearContent();    
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CREATING PPP...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    memset(&st_comm_param, 0, sizeof(st_comm_param));
    st_comm_param.eMode = SDK_COMM_GPRS;
    strcpy(st_comm_param.stCommInfo.stPppWireLessInfo.asGprsApn, "CMNET");
    strcpy(st_comm_param.stServerInfo.asServerIP, "219.133.170.86");        //can be any valid IP
    strcpy(st_comm_param.stServerInfo.asPort, "8616");
    sdkCommConfig(&st_comm_param);
    res = sdkCommCreatePPP(20*1000);
    Trace("xgd", "sdkCommCreatePPP() = %d\r\n", res);
    if (res != SDK_OK)
    {
        DispClearContent();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CREATE PPP FAILED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3*1000);
        return;
    }
    
    DispClearContent();  
    sdkDispFillRowRam(SDK_DISP_LINE2, 0, "IMSI:", SDK_DISP_LEFT_DEFAULT);
    sdkDispFillRowRam(SDK_DISP_LINE4, 0, "ICCID:", SDK_DISP_LEFT_DEFAULT);
    sdkDispBrushScreen();

    #if 0
    // Get and display IMEI
    res = sdkCommGetWirelessModuleID(str_imei);
    if (res > 0)
    {
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, str_imei, SDK_DISP_RIGHT_DEFAULT);
        sdkDispBrushScreen();
    }
    Trace("cody", "sdkCommGetWirelessModuleID()=%d\r\n", res);
    #endif

    // Get and display IMSI
    res = sdkCommGetSimIMSI(str_imsi, 10*1000);
    if (res > 0)
    {
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, str_imsi, SDK_DISP_RIGHT_DEFAULT);
        sdkDispBrushScreen();
    }
    Trace("cody", "sdkCommGetSimIMSI()=%d\r\n", res);
    Trace("cody", "IMSI: [%s]\r\n", str_imsi);

    // Get and display ICCID
    res = sdkCommGetSimID(str_iccid, 10*1000);
    if (res > 0)
    {
        sdkDispFillRowRam(SDK_DISP_LINE5, 0, str_iccid, SDK_DISP_RIGHT_DEFAULT);
        sdkDispBrushScreen();
    }
    Trace("cody", "sdkCommGetSimID()=%d\r\n", res);
    Trace("cody", "ICCID: [%s]\r\n", str_iccid);

    sdkKbWaitKey(SDK_KEY_MASK_ESC | SDK_KEY_MASK_ENTER, 30000);
}
