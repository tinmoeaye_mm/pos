/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : dns.c
   Version       : Initial Draft
   Author        : Steven
   Created       : 2014/11/4
   Last Modified :
   Description   : This file shows user how to get ip of a domain name.
   History       :
   1.Date        : 2014/11/4
    Author      : Steven
    Modification: Created file

******************************************************************************/
#include "../inc/global.h"


static bool ConnectPpp(const u8 *pasApn, u32 uiTimeOut);

/*******************************************************************************
** Description :  Get ip of a specified domain name
** Parameter   :  void
** Return      :
** Author      :  Steven   2014-11-4
** Remark      :
*******************************************************************************/
void ModuleDns(void)
{
    s32 i, ip_num, key;
    char ip_addr_list[10][16] = {{0}};
    bool dhcp_flag = false;
    static bool open_eth0_flag = false;

    while(1)
    {
        sdkDispClearScreen();
        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "DNS", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SELECT A COMM:", SDK_DISP_LEFT_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "1.ETHERNET", SDK_DISP_LEFT_DEFAULT);
        sdkDispFillRowRam(SDK_DISP_LINE4, 0, "2.GPRS/WCDMA", SDK_DISP_LEFT_DEFAULT);
        sdkDispBrushScreen();

        key = sdkKbWaitKey(SDK_KEY_MASK_12 | SDK_KEY_MASK_ESC, 5000);

        /*ethernet*/
        if (key == '1')
        {
            DispClearContent();
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, "OPEN ETH0...", SDK_DISP_DEFAULT);
            sdkDispBrushScreen();

            /*open network device eth0*/
            dhcp_flag = true;

            if (!open_eth0_flag)
            {
                OpenEth0(dhcp_flag);
                open_eth0_flag = true;
            }
            DispClearContent();
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, "www.baidu.com", SDK_DISP_DEFAULT);
            sdkDispFillRowRam(SDK_DISP_LINE4, 0, "GETTING IP...", SDK_DISP_DEFAULT);
            sdkDispBrushScreen();
            sdkmSleep(1000);

            if (!dhcp_flag)
            {
                //if DHCP is not supported, nameserver can NOT be obtained automatically,
                //you set the nameserver in "/etc/resolv.conf" manually, herein, "172.18.0.1" & "172.18.0.3" are only for XGD's intranet nameserver
                sdkExtSetDNS("172.18.0.1", "172.18.0.3");
            }
        }
        /*GPRS/WCDMA*/
        else if (key == '2')
        {
            DispClearContent();
            sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CONNECT PPP...", SDK_DISP_DEFAULT);
            sdkDispBrushScreen();

            /* connect PPP to get the exact nameserver automatically*/
            if (false == ConnectPpp("cmnet", 30 * 1000)) //timeout: 30s
            {
                DispClearContent();
                sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CONNECT PPP FAIL!", SDK_DISP_DEFAULT);
                sdkDispBrushScreen();
                sdkKbWaitKey(SDK_KEY_MASK_ALL, 5000);
                continue;
            }
        }

        if (key == '1' || key == '2')
        {
            /*get IP of  the domain name*/
            ip_num = sdkExtGetHostByName("www.baidu.com", 2, ip_addr_list, 10);
            DispClearContent();

            if (ip_num > 0)
            {
                Trace("xgd", "total ip num = %d\r\n", ip_num);

                for (i = 0; i < ip_num; i++)
                {
                    Trace("xgd", "ip%d  = %s\r\n", i + 1, ip_addr_list[i]);
                }

                sdkDispFillRowRam(SDK_DISP_LINE3, 0, "GET IP SUCCESS", SDK_DISP_DEFAULT);
                sdkDispFillRowRam(SDK_DISP_LINE4, 0, ip_addr_list[0], SDK_DISP_DEFAULT);
            }
            else
            {
                sdkDispFillRowRam(SDK_DISP_LINE3, 0, "GET IP FAILED!", SDK_DISP_DEFAULT);
            }
            sdkDispBrushScreen();
            sdkKbWaitKey(SDK_KEY_MASK_ALL, 5000);
        }
        else if (key == SDK_KEY_ESC)
        {
            return;
        }
    }
}

/*******************************************************************************
** Description :  open network device eth0
** Parameter   :  void
** Return      :
** Author      :  Steven   2014-11-4
** Remark      :
*******************************************************************************/
bool OpenEth0(bool bisDhcp)
{
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CONNECTING...", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
    SDK_COMM_STLANPARAM st_lan_param;

    memset(&st_lan_param, 0, sizeof(st_lan_param));
    st_lan_param.bIsDhcp = bisDhcp;

    if (!bisDhcp)
    {
        strcpy(st_lan_param.asNetClientIP, "192.168.1.12");
        strcpy(st_lan_param.asNetMask, "255.255.255.0");
        strcpy(st_lan_param.asNetGate, "192.168.1.254");
        strcpy(st_lan_param.asDnsIP, "8.8.8.8");
    }
    return (SDK_OK == sdkExtOpenEthernet(&st_lan_param)) ? true : false;
}

/*******************************************************************************
** Description :  Connect PPP
** Parameter   :  void
** Return      :
** Author      :  Steven   2014-11-4
** Remark      :
*******************************************************************************/
static bool ConnectPpp(const u8 *pasApn, u32 uiTimeOut)
{
    SDK_COMM_STCOMMPARAM st_comm_param;
    s32 res;

    memset(&st_comm_param, 0, sizeof(st_comm_param));
    st_comm_param.eMode = SDK_COMM_GPRS;
    strcpy(st_comm_param.stCommInfo.stPppWireLessInfo.asGprsApn, pasApn);
    strcpy(st_comm_param.stServerInfo.asServerIP, "219.133.170.86");        //can be any valid IP
    strcpy(st_comm_param.stServerInfo.asPort, "8616");
    sdkCommConfig(&st_comm_param);
    res = sdkCommCreatePPP(uiTimeOut);
    Trace("xgd", "sdkCommCreatePPP() = %d\r\n", res);
    return (SDK_OK == res) ? true : false;
}

