/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : ethernet.c
   Version       : Initial Draft
   Author        : Cody
   Created       : 2014/7/24
   Last Modified :
   Description   : This file shows how to communicate with ethernet
   History       :
   1.Date        : 2014/7/24
    Author      : Cody
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"


/*******************************************************************************
** Description :  Send and receive data through ethernet
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-24
** Remark      :  1. configure network
                  2. create a link
                  3. send data
                  4. receive data
                  5. destroy link
*******************************************************************************/
void ModuleEthernet(void)
{
    SDK_COMM_STCOMMPARAM st_commparam;
    u8 sbuf[128] = "GET /index.html HTTP/1.0\r\n\r\n";
    u8 rbuf[1024] = {0};
    s32 len;
    u8 dispbuf[32+1] = {0};
    u8 tracebuf[128+1] = {0};
    s32 len1;
    s32 i;

    // Intialize communication parameters
    memset(&st_commparam, 0, sizeof(st_commparam));
    st_commparam.ucDialTime = 30 * 1000;                                       // Maximum period of time for dialing before timeout
    st_commparam.ucReDialTimes = 2;                                          // Times to try for redial
    st_commparam.eMode = SDK_COMM_ETHERNET;
    st_commparam.stCommInfo.stLanInfo.bIsDhcp = 0;                            // Disable DHCP
    st_commparam.stCommInfo.stLanInfo.bIsPPPoe = 0;                           // Not PPPoe
    strcpy(st_commparam.stCommInfo.stLanInfo.asNetClientIP, "172.23.1.211");  // Local IP
    strcpy(st_commparam.stCommInfo.stLanInfo.asNetGate, "172.23.0.254");      // Local gateway
    strcpy(st_commparam.stCommInfo.stLanInfo.asNetMask, "255.255.0.0");       // Local Subnet mask
    strcat(st_commparam.stServerInfo.asServerIP, "93.184.216.34");            // Server IP: example.com
    strcat(st_commparam.stServerInfo.asPort, "80");                           // Server Port

    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "ETHERNET", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CONFIGURING...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);

    // Configure communication device
    if (sdkCommConfig(&st_commparam) != SDK_OK)
    {
        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CONFIGURATION ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return;
    }
    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "LINKING...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);

    // Create a link
    if (sdkCommCreateLink() != SDK_OK)
    {
        sdkCommDestoryLink();

        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "LINK ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return;
    }
    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SENDING DATA...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);

    // Send data
    if (sdkCommSendData(sbuf, strlen(sbuf), SDK_COMM_TRANSPARENT) != SDK_OK)
    {
        sdkCommDestoryLink();

        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SEND DATA ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return;
    }
    DispClearContent();
    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "RECEIVING DATA...", SDK_DISP_DEFAULT);
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);

    // Receive data
    len = sdkCommRecvData(rbuf, sizeof(rbuf), 30000, NULL);

    if (len <= 0)
    {
        sdkCommDestoryLink();

        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "RECEIVE DATA ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
        return;
    }

    
    // Output debug info
    len = len > 128 ? 128 : len;
    memcpy(tracebuf, rbuf, len);
    Trace("xgd", ">>>> Content (first 128 bytes):\r\n%s\r\n", tracebuf);

    // Display response message
    sdkDispClearScreen();
    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESPONSE:", SDK_DISP_LEFT_DEFAULT);
    for (i = 0; i < 4 && len > 0; i++)
    {
        len1 = len > 20 ? 20 : len;
        len -= len1;
        memcpy(dispbuf, &tracebuf[20*i], len1);
        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
    }
    sdkDispBrushScreen();
    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30*1000);
    
    // Destroy the established link
    sdkCommDestoryLink();
}

