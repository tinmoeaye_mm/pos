#include "string.h"
#include "../inc/global.h"


#define MAXTOK 256
jsmn_parser jsmn_p;
jsmntok_t jsmn_t[MAXTOK];
unsigned char recbuf[MAXREC];
s32 ke;


int json_parse(char * recbuf) {
	if (recbuf[0]=='[')
	{
		recbuf[0]=' ';
	}	
	jsmn_init(&jsmn_p);
//	MLOG("json parse %.30s\n", recbuf);
	if (strlen((char *)recbuf) == 0)
		return 0;
	return jsmn_parse(&jsmn_p, (char *)recbuf, (size_t)strlen((char *)recbuf), jsmn_t, MAXTOK);
}

int js_tok(char * dst, char * name, int obj) {
	int i, j, object_tokens = 0;
	jsmntok_t * t;
	typedef enum { START, KEY, FOUND, SKIP } parse_state;
	parse_state state = START;

	for (i = obj, j = 1; j > 0; i++, j--) {
		t = &jsmn_t[i];
		printf("%c i %d j %d type %d size %d start %d end %d\n", recbuf[t->start], i, j, t->type, t->size, t->start, t->end);
		if (t->type == JSMN_ARRAY || t->type == JSMN_OBJECT)
			j += t->size;
		switch (state) {
		case START:
			if (t->type != JSMN_OBJECT)
				return 0;
			object_tokens = t->size;
			if (object_tokens == 0) {
				printf("Error - no tokens at start\n");
				return 0;
			}
			if (object_tokens % 2 != 0) {
				printf("Error - odd number of tokens at start\n");
				return 0;
			}

			state = KEY;
			break;
		case KEY:
			object_tokens--;
			if (t->type == JSMN_OBJECT)
				break;
			if (t->type != JSMN_STRING) {
			//	MLOG("key is not a string");
				return 0;
			}
			state = SKIP;
			if (strncmp((const char *)(recbuf + t->start), name, t->end - t->start) == 0)
				state = FOUND;
			break;
		case SKIP:
			if (t->type != JSMN_STRING && t->type != JSMN_PRIMITIVE && t->type != JSMN_ARRAY) {
				printf("type is not a string, primitive or array in skip\n");
				return 0;
			}
			state = KEY;
			break;
		case FOUND:
			if (t->type != JSMN_STRING && t->type != JSMN_PRIMITIVE)
				return 0;
			strncpy(dst, (const char *)(recbuf + t->start), t->end - t->start);
			dst[t->end - t->start] = '\0';
		//	MLOG("Found %s\n", name);
			return 1;
		}
	}
//	MLOG("Not found: %s\n", name);
	return 0;
}

int js_array(int * arraydx, char * name) {
	int i, j, object_tokens = 0;
	jsmntok_t * t;
	typedef enum { START, KEY, SKIP, FOUND } parse_state;
	parse_state state = START;

	for (i = 0, j = 1; j > 0; i++, j--) {
		t = &jsmn_t[i];
		if (t->type == JSMN_ARRAY || t->type == JSMN_OBJECT)
			j += t->size;
		switch (state) {
		case START:
			if (t->type != JSMN_OBJECT)
				return 0;
			state = KEY;
			object_tokens = t->size;
			if (object_tokens == 0)
				return 0;
			if (object_tokens % 2 != 0)
				return 0;
			break;
		case KEY:
			object_tokens--;
			if (t->type == JSMN_OBJECT)
				break;
			if (t->type != JSMN_STRING)
				return 0;
			state = SKIP;
			if (strncmp((const char *)(recbuf + t->start), name, t->end - t->start) == 0) {
				state = FOUND;
			}
			break;
		case SKIP:
			if (t->type != JSMN_STRING && t->type != JSMN_PRIMITIVE && t->type != JSMN_ARRAY)
				return 0;
			object_tokens--;
			state = KEY;
			if (object_tokens == 0)
				return 0;
			break;
		case FOUND:
			if (t->type != JSMN_ARRAY)
				return 0;
			*arraydx = i;
			return t->size;
		}
	}
	return 0;
}

int js_get_array_element(char * dst, int arraydx, int index) {
	jsmntok_t * t;
	t = &jsmn_t[arraydx + index + 1];
	if (t->type != JSMN_STRING && t->type != JSMN_PRIMITIVE)
		return 0;
	strncpy(dst, (const char *)(recbuf + t->start), t->end - t->start);
	dst[t->end - t->start] = '\0';
	return 1;
}

int js_array_index(int arraydx, int index) {
	jsmntok_t * t;
	arraydx++;
	while (index >0) {
		t = &jsmn_t[arraydx];
		if (t->type != JSMN_OBJECT)
			return 0;
		arraydx += t->size + 1;
		index--;
	}
	return arraydx;
}

void js_dump() {
	int i, j, object_tokens = 0;
	char str[256];
	jsmntok_t * t;
	typedef enum { START, KEY, FOUND, SKIP } parse_state;
	parse_state state = START;

	for (i = 0, j = 1; j > 0; i++, j--) {
		t = &jsmn_t[i];
		memset(str, 0, 256);
		strncpy(str, (const char *)(recbuf + t->start), t->end - t->start);
		printf("%s %c i %d j %d type %d size %d start %d end %d\n", str, recbuf[t->start], i, j, t->type, t->size, t->start, t->end);
		if (t->type == JSMN_ARRAY || t->type == JSMN_OBJECT) {
			j += t->size;
			printf("j is now %d\n", j);
		}

		switch (state) {
		case START:
			if (t->type != JSMN_OBJECT)
				return;
			state = KEY;
			object_tokens = t->size;
			if (object_tokens == 0) {
				printf("Error - no tokens at start\n");
				return;
			}
			if (object_tokens % 2 != 0) {
				printf("Error - odd number of tokens at start\n");
				return;
			}
			break;
		case KEY:
			object_tokens--;
			if (t->type == JSMN_OBJECT)
				break;
			if (t->type != JSMN_STRING) {
				printf("Error - key is not a string\n");
				return;
			}
			state = SKIP;
			break;
		case SKIP:
			if (t->type != JSMN_STRING && t->type != JSMN_PRIMITIVE && t->type != JSMN_ARRAY) {
				printf("type is not a string, primitive or array in skip\n");
				return;
			}
			object_tokens--;
			state = KEY;
			/*
			if (object_tokens == 0) {
			printf("no tokens left in skip\n");
			return;
			}
			*/
			break;
		case FOUND:
			if (t->type != JSMN_STRING && t->type != JSMN_PRIMITIVE)
				return;
		}
	}
}
int json_scrub(char * BUG,int len)  
{
	int jo = 0;
	if (BUG[0]!='\0')
	{	
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE1,0,"Slip Fail!",SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE2,0,"U Want ReSlip?",SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4,0,"Yes[ENTER]",SDK_DISP_LEFT_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4,0,"No[CANCEL]",SDK_DISP_RIGHT_DEFAULT);
		sdkDispBrushScreen();
		ke = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
		switch(ke)
		{
			case SDK_KEY_ENTER:
		//	MLOG("json parse %.10s\n", BUG);
			sprintf(recbuf,"%s",BUG);
			memset(BUG,0,sizeof(BUG));
			jsmn_init(&jsmn_p);
			jo = jsmn_parse(&jsmn_p,(char *)recbuf,(size_t)strlen((char *)recbuf),jsmn_t,MAXTOK);
			return jo;
			break;
			case SDK_KEY_ESC:
			return jo;
			break;
			default:
			return jo;
		}
			
	}
	return jo;
}

