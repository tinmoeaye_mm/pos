/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : mifarecard.c
   Version       : Initial Draft
   Author        : Cody
   Created       : 2014/7/23
   Last Modified :
   Description   : This file shows user how to write and read S50/S70 mifare
                  card
   History       :
   1.Date        : 2014/7/23
    Author      : Cody
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"




aes256_context ctx;

#define DATA_KEY_DEFAULT "\xFF\xFF\xFF\xFF\xFF\xFF"
#define DATA_KEY_AB "\x62\x61\x6B\x77\x61\x6E\xFF\x07\x80\x69\x6D\x61\x6C\x61\x6E\x67"
#define DATA_KEY_RESET "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x07\x80\x69\xFF\xFF\xFF\xFF\xFF\xFF"


/*******************************************************************************
** Description :  Query and access S50/S70 mifare card
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-07-23
** Remark      :  Only type S50 or S70 mifare card are supported so far
                  Follow these steps to access mifare card:
                  1. open RF device
                  2. query card
                  3. verify key
                  4. read or write data to card block
                  5. power down mifare card and close RF device
*******************************************************************************/
void ModuleReadCard(void)
{

	//Trace("modulereadcard", "Start ModuleReadCard= \r\n");
	s32 gsCloseRfTimer;
   	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	//u8 buf5[256] = {0};
	gsCloseRfTimer= sdkTimerGetId();

   	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	Trace("Module Read Card"," Beep Ok");
	 }
	 else if(sdkTimerIsEnd(gsCloseRfTimer,30000))
	 {
	 	sdkIccCloseRfDev();
	 	Trace("Module Read Card"," IccPowerDown");
	 	IconFace();
		
	 }
	 else
	 {
        sdkPEDCancel();
        ModuleReadCard();
        Trace("Module Read Card","PED cancel");
	 }

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	memset(sendData.asAgentCardNo, 0, sizeof(sendData.asAgentCardNo));
	memset(sendData.asSalesCardNo, 0, sizeof(sendData.asSalesCardNo));
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));

	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("xgd", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	if(strcmp(sendData.asFirstDigit,"0")==0)
	{
		memcpy(sendData.asSalesCardNo, &buf1[0], 9);
	}
	if(strcmp(sendData.asFirstDigit,"1")==0)
	{
		memcpy(sendData.asAgentCardNo, &buf1[0], 9);
	}
	else if(strcmp(sendData.asFirstDigit,"2")==0)
	{
		memcpy(sendData.asCustomerCardNo,&buf1[0],9);
	}	

	Trace("xgd", "Agent Card No = %s\r\n", sendData.asAgentCardNo);
	Trace("xgd", "Sales Card No = %s\r\n", sendData.asSalesCardNo);
	Trace("xgd","Customer Card No=%s\r\n",sendData.asCustomerCardNo);
	
	
	// Read PIN from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(PIN_BLOCK, buf, &len);

	if (res != SDK_OK || buf[0] != 0)
	{
		 sdkSysBeep(SDK_SYS_BEEP_ERR);
		 sdkPEDCancel();
		  AppMain();
	}

	if(strcmp(sendData.asFirstDigit,"0")==0)
	{
		memset(sendData.asSalesCardPin, 0, sizeof(sendData.asSalesCardPin));
		sdkBcdToAsc(sendData.asSalesCardPin, &buf[1], 3);
	}
	if(strcmp(sendData.asFirstDigit,"1")==0)
	{
		memset(sendData.asAgentCardPin, 0, sizeof(sendData.asAgentCardPin));
		sdkBcdToAsc(sendData.asAgentCardPin, &buf[1], 3);
	}	

	sdkIccCloseRfDev(); 
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispSetBackColor(0xFFFF); 
	sdkDispSetFontColor(0x0000);
	
	if(strcmp(sendData.asFirstDigit,"1")==0||strcmp(sendData.asFirstDigit,"0")==0)
	{
		LoginFace();
	}
	else
	{
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "AGENT OR SALE CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		AppMain();
	}
	
		

}
void ModuleWriteCard(void)
{
	
	unsigned char buf4[MAXREC];
	
	u8 buf[128] = {0}, buf2[256] = {0};
	s32 len, res;
	u8 disptype = SDK_DISP_FDISP | SDK_DISP_LDISP | SDK_DISP_INCOL;		
	s32 key = 0;
	u8 temp[128]={0};
	
	if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 20000)) >= 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_OK);
	}
	
	memset(buf, 0, sizeof(buf));
	res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len);
	if (res != SDK_OK || buf[0] != 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "VERIFY KEY ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		return;
	}
	
	sdkDispClearScreen();
	memset(buf2, 0, sizeof(buf2));
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "[ENTER PINCODE]  :", disptype);
	sdkDispFillRowRam(SDK_DISP_LINE5, 0, "IF ERR,PRESS [CLR]", disptype);
	sdkDispFillRowRam(SDK_DISP_LINE4, 0, "_ _ _ _ _ _ _ _",SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	printf("timeout is stgAppSetting.stAutoRetTime.uiOptAutoRet%d\r\n", 60000);
	key = sdkKbGetScanf(60000, temp, 6, 6, SDK_MMI_PWD ,SDK_DISP_LINE3);
		
	if(key == SDK_KEY_ENTER)
	{

		Trace("xgd", "plain= %s\r\n", temp);

		sdkAscToBcd(buf2,&temp[1],temp[0]);

		aes256_init(&ctx, DEFAULT_KEY);
		aes256_encrypt_ecb(&ctx, buf2);

		Trace("xgd", "encrypt= %s\r\n", buf2);
		
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SETTING PIN CODE......", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkmSleep(1000);
		memset(buf, 0, sizeof(buf));

		res = sdkIccMifareWriteBlock(PIN_BLOCK, buf2, 6, buf, &len);
		if (res != SDK_OK || buf[0] != 0)
		{
			sdkSysBeep(SDK_SYS_BEEP_ERR);
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SETTIN PIN ERROR!", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
			sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
			return;
		}
		
		sdkDispClearScreen();
	    sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READING DATA........", SDK_DISP_DEFAULT);
	    sdkDispBrushScreen();
	    sdkmSleep(1000);

	    memset(buf4, 0, sizeof(buf4));
	    res = sdkIccMifareReadBlock(PIN_BLOCK, buf4, &len);

	    if (res != SDK_OK || buf4[0] != 0)
	    {
	       sdkSysBeep(SDK_SYS_BEEP_ERR);   
	        sdkDispClearScreen();
	        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
	        sdkDispBrushScreen();
	        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
	        return;
	    }

		SalesMenu();
	
	}
	
}

s32 ModuleActivateAgentCard()
{
	//Trace("xgd","ModuleActivateAgentCard Start");
	
   	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;

   	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 20000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	 }
	 else
	 {
        sdkPEDCancel();
        return ModuleActivateAgentCard();
	 }

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
        sdkSysBeep(SDK_SYS_BEEP_ERR);

		DispClearContent();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
        return 0;
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	if (res != SDK_OK || buf[0] != 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_ERR);

        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
        sdkPEDCancel();
        return 0;
	}

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	memset(sendData.asAgentCardNo, 0, sizeof(sendData.asAgentCardNo));
	
	memcpy(sendData.asAgentCardNo, &buf1[0], 9);

	Trace("xgd", "Agent Card No = %s\r\n", sendData.asAgentCardNo);

	/*sdkAscToBcd(buf2,sendData.asAgentEnterPin,strlen(sendData.asAgentEnterPin));

	aes256_init(&ctx, DEFAULT_KEY);
	aes256_encrypt_ecb(&ctx, buf2);

	//Trace("xgd", "encrypt= %s\r\n", buf2);
	
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SETTING PIN CODE......", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	sdkmSleep(1000);
	memset(buf, 0, sizeof(buf));

	res = sdkIccMifareWriteBlock(PIN_BLOCK, buf2, 6, buf, &len);
	if (res != SDK_OK || buf[0] != 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_ERR);

        DispClearContent();
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SETTING PIN ERROR!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
        sdkPEDCancel();
        return 0;
		
	}

	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SETTING PIN CODE", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4, 0, "DONE", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	sdkIccCloseRfDev(); 
	*/
	return res;
}

s32 ControlBlock()
{
	u8 buf[128] = {0},temp[128]={0};
	s32 len,res,data_block=7;
	
	Top: 
	len=0;
	res=0;
	memset(buf,0,sizeof(buf));
	memset(temp,0,sizeof(temp));

	sdkIccOpenRfDev();

	if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 20000)) >= 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_OK);
	} 
	else
	{
		sdkPEDCancel();
        return ControlBlock();
	}
	
	memset(buf, 0, sizeof(buf));
	res = sdkIccMifareVerifyKey(data_block, SDK_RF_KEYA, DATA_KEY_DEFAULT, 6, buf, &len);

	if (res != SDK_OK || buf[0] != 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_ERR);  
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		return 0;
	}
	
	memset(buf, 0, sizeof(buf));
	res = sdkIccMifareWriteBlock(data_block,DATA_KEY_AB, 16, buf, &len);

	if (res != SDK_OK || buf[0] != 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "WRIE CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		return 0;
	}

	if(data_block<15){  
		data_block+=4;
		goto Top;
	}
	
	sdkIccCloseRfDev(); 
	return res;

}

void ResetCard(void)
{
	char buf[128] = {0},buf2[256] = {0},temp[128]={0};
	int len,res,data_block=7;

	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1, 0, "RESET CARD", SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TAP YOUR CARD", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5, 0, "[ESC] TO QUIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	Top: 
	len=0;
	res=0;
	memset(buf,0,sizeof(buf));
	memset(buf2,0,sizeof(buf2));
	memset(temp,0,sizeof(temp));
	sdkIccOpenRfDev();
	if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 20000)) >= 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_OK);
	} 
	memset(buf, 0, sizeof(buf));
	res = sdkIccMifareVerifyKey(data_block, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len);
	if (res != SDK_OK || buf[0] != 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_ERR);  
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "VERIFY KEY ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		return ;
	}

	memset(buf2, 0, sizeof(buf2)); 
	memset(buf, 0, sizeof(buf));
	res = sdkIccMifareWriteBlock(data_block,DATA_KEY_RESET, 16, buf, &len);

	if (res != SDK_OK || buf[0] != 0)
	{
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR IN WRITING!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
		return ;
	}

	if(data_block<15){  
		data_block+=4;
		goto Top;
	}

	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "RESET CARD", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE4, 0, "DONE", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
	
	sdkIccCloseRfDev(); 

	SalesMenu();
}


