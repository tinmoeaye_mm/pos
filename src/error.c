/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : error.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/4/4
   Last Modified :
   Description   : agent module-related functions are defined
                  here
   History       :
   1.Date        : 2016/4/4
    Author      : Thu Kha, Tin Moe Aye
    Modification: Created file

******************************************************************************/
#include "../inc/global.h"

s32 len, len1, i;
u8 dispbuf[32+1] = {0};

void NewMainMenu(void);
void DisplayErrorMessage(u8 *header ,u8 *msg,bool def)
{
	if(def)
	{
		sdkDispClearScreen();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PLEASE TRY AGAIN", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
	}
	else
	{
		len = 128;
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE1, 0, header, SDK_DISP_DEFAULT);
	    sdkDispFillRowRam(SDK_DISP_LINE2, 0, "TRANSACTION FAIL", SDK_DISP_DEFAULT);
	    for (i = 0; i < 4 && len > 0; i++)
	    {
	        len1 = len > 20 ? 20 : len;
	        len -= len1;
	        
	        memcpy(dispbuf, &receivedData.ErrorMessage[20*i], len1);
	        
	    	if (strcmp(receivedData.Status,"Success")!=0)
			{
				memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
				memset(receivedData.Message,0,sizeof(receivedData.Message));
				js_tok(receivedData.ErrorMessage,"errorMessage",0);
				js_tok(receivedData.Message,"message",0);
				if(strlen(receivedData.ErrorMessage)!=0)
				{
					memcpy(dispbuf,&receivedData.ErrorMessage[20*i],len1);
				}
				else 
				{
					memcpy(dispbuf,&receivedData.Message[20*i],len1);
				}
			}
			sdkDispFillRowRam(SDK_DISP_LINE3+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
	    }
	    sdkDispBrushScreen();
	    sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 10000);
	}
}

void FunUndDev(s32 rtmp)
{
	s32 key;

	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"FUNCTION",SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"UNDER",SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
	sdkDispFillRowRam(SDK_DISP_LINE4,0,"DEVELOPMENT",SDK_DISP_NOFDISPLINE | SDK_DISP_CDISP | SDK_DISP_INCOL);
	sdkDispBrushScreen();

	key = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,30000);
	
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				if(rtmp==1)
				{	
					MainMenu();
				}
				else if(rtmp==2)
				{
					
					if(topup.MAINMenu==New_Menu)
	        		{
	        		 
	            		NewMainMenu();
					}
					else
					{
					
				
						ChooseOperatorMenu();
					}
				}
				else if(rtmp==3)
				{
					AgentMenu();
				}
				else if(rtmp==4)
				{
					SalesMenu();
				}
			}
			break;
		case SDK_KEY_ESC:
			{
				
				if(rtmp==4)
				{
					SalesMenu();	
				}
			
				
				else if(rtmp==2)
				{
					
					if(topup.MAINMenu==New_Menu)
	        		{
	        		 
	            		NewMainMenu();
					}
					else
					{
					
				
						MainMenu();	
					}
				}
				else
				{
					MainMenu();	
				}
			}
			break;
		default:
	    	{
	    		
	    	}
	    	break;
	}

	AppMain();
				
}


