
/******************************************************************************

   Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
   File Name     : main.c
   Version       : Initial Draft
   Author        : Cody
   Created       : 2014/6/26
   Last Modified :
   Description   : main and other module-unrelated functions are defined
                  here
   History       :
   1.Date        : 2014/6/26
    Author      : Cody,Thu Kha,Tin Moe Aye
    Modification: Created file

******************************************************************************/
#include "../inc/global.h"


//main functon  images
const u8 asPathLogin[] = "/mtd0/res/auth.bmp";
const u8 asPathPin[] = "/mtd0/res/enterpincode.bmp";
const u8 asPathFP[] = "/mtd0/res/fingerprint.bmp";
const u8 asPathAgent[] = "/mtd0/res/agentmenu.bmp";
const u8 asPathSales[] = "/mtd0/res/salesmenu.bmp";
const u8 asPathMain[] = "/mtd0/res/mainmenu.bmp";

#define CTRL_BLOCK    11
#define CARDNO_BLOCK  9
#define PINNO_BLOCK   10
#define DATA_BLOCK    10


bool isPower;
s32 isSystemStart = 0;
s32 idealTimer;

// functions declaration 
void AppMain(void);
void CheckPin(void);
void ModuleCard(void);
void ModuleCommunication(void);
void LoginCheck(int i);
void PosHelth(void);
void CustomerRegisterName(void);
void TransferMainMenu(void);
void MemToMemCashInMainMenu(void);
void MemberCheckBalance(void);
void MasterAgentCashIn(void);
void MasterAgentWithdraw(void);
void FPdelete(void);
void versionread(void);
void DirectMessage(void);
void NewMainMenu(void);
void CashOutModuleForNewMenu(void);
void SelectAmountMenu(void);
void AeonAgreement(void);
void YESCMainMenu(void);
void Select_Eload_Epin(void);
void DispDateTime(u8 ucLine);
void BatteryMessage(void);
char *ForSendData();
void MasterAgentSettlement(void);

const u8 delwarning[]="/mtd0/res/HLcashout.bmp";
//-----------------------------------------------------------------------------

#if 1
#ifdef XGD_SDK_DEBUG

DBG_FILTER DbgFilter =
{
    false,                              // whether in testing state
    DBG_BLOCK_BLACKLIST,        		// only block the tags of black list
    DBG_MODE_RS232,                     // mode for Trace(), TraceHex(), Assert(), Verify().
    DBG_ENABLE_ALL,                     // enable all trace modes
};

// white list
char const *pWhiteList[] =
{
    "xgd",
    "cody",
    "sdkExtLib",
    "lsl_debug",
    "sdkMultiLangChange",
    
};


// black list
char const *pBlackList[] =
{
    "dbg"
};


#endif


#else

//白名单//white list
static char const *  pWhiteList[] =
{
     "xgd",
    "cody",
    "sdkExtLib",
    "lsl_debug",
    "sdkMultiLangChange",
};


//黑名单//black list
static char const *  pBlackst[] =
{
    "d"
};

static const SDK_DEBUG_FILTER DebugFilter =
{
    false,                                                   //是否在测试状态下//whether in testing state
    false,                                                // 是否输出函数名//whether output function name
    SDK_DEBUG_TARGET_COM,             // 调试信息输出到串口//debug massage output to serial port
    SDK_DEUBG_OUT_ALL,                     //trace过滤模式//trace filtering mode
};
#endif


/*******************************************************************************
** Description :  Program entery point.
** Parameter   :  s32 argc
                  char const *argv[]
** Return      :
** Author      :  Cody   2014-06-26
** Remark      :

        [ PLEASE DO NOT MODIFY ANY OF CONTENTS IN THIS FUNCTION !!!]

*******************************************************************************/
s32 main(s32 argc, char const *argv[])
{
     Trace("pxp","Enter main\r\n");

	sdkSysLoadDll(NULL, 0);						 // Load libraries
	sdkExtLoadDll(NULL, 0); 						// Load extended libraries 
	sdkSysLoad3rdDll(NULL, 0);
	sdkSysMain(argc, (const char **)argv); 		// Initialize system

#ifdef XGD_SDK_DEBUG                                    // for app running in debug mode
    AppDbgInit(&DbgFilter,
               pWhiteList, sizeof(pWhiteList) / sizeof(char*),
               pBlackList, sizeof(pBlackList) / sizeof(char*));
#endif

    AppMain();

	sdkSysUnLoad3rdDll();
    sdkExtUnLoadDll();           // Unload extended libraries. it must be called before exiting main()
    sdkSysUnLoadDll();           // Unload libraries. it must be called before exiting main()
    sdkSysAppQuit();             // Do some clean-up before exiting main()

    return 0;

}

/*******************************************************************************
** Description :
** Parameter   :  void
** Return      :
** Author      :  wushilin   2014-08-14
** Remark      :  the initialization of application should be configured here
*******************************************************************************/
void AppMain(void)
{
	s32 ret= 0;
	//s32 key=0;

	
	SDK_SYS_INITIAL_INFO st_initial;

	// Enable beep when pressing key
	sdkSysEnableBeep(true);
	sdkSysBeep(SDK_SYS_BEEP_OK);

    // Initialize system
    st_initial.bIsHavePinPad = false;   		// PINPAD: true->external, false->internal
    st_initial.bIsINRf = true;                  // RF-READER: true->internal, false->external
    sdkSysInitParam(&st_initial);
    
  //Setup local connection
   
	FPdelete();//  To Solve for finger print Error
	//Setup 3G connection
	if(isSystemStart ==0)
	{
		
		while (ModuleGprs()==0)
		{
			ModuleGprs();
		}

		memset(sendData.asTerminalSN,0,sizeof(sendData.asTerminalSN));
		sdkReadPosSn(sendData.asTerminalSN);
			
	  memset(sendData.asSimICCID,0,sizeof(sendData.asSimICCID));
	  ret = sdkCommGetSimID(sendData.asSimICCID,10000);
	
	  memset(sendData.asSimIMSI,0,sizeof(sendData.asSimIMSI));
	  ret = sdkCommGetSimIMSI(sendData.asSimIMSI,10000);
		
		Trace("xgd", "sendData.asTerminalSN= %s\r\n", sendData.asTerminalSN);
		Trace("xgd", "sendData.asSimICCID= %s\r\n", sendData.asSimICCID);
		Trace("xgd", "sendData.asSimIMSI= %s\r\n", sendData.asSimIMSI);			
			
		transaction=false; //For PageAgentCheckBlance
		
		secondtransactionstart=false;//For PageAgentCheckBalance
		
		poweronforNewMenu=true;// For PageAgentCheckBalance
	
		timerflage=true;//For BatteryMeesage
		batterymessage=true;
		VersionChangeV2();
		CheckPaperRollStatus();
		DirectMessage();
		
				
	}

	isSystemStart=1;
	
	
	forappmainbalance=true;
	Forappmaintransaction=true;
	IconFace();

	
}

void LoginFace(void)
{
	s32 res;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, asPathLogin);
	sdkDispBrushScreen();
	
    res = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, KbWaitTime);

	if (res == SDK_KEY_1)
    {
		LoginCheck(1);//Check Login status , Fingerprint

    }
    else if (res == SDK_KEY_2)
    {
    	LoginCheck(2);//Checl Login status, passward
    }
    else if (res == SDK_KEY_ESC)
    {
        
        	IconFace();
		
    }

	IconFace();
}

void LoginCheck(int i)
{
	u32 Temp_timer;
	Temp_timer = sdkTimerGetId();
 	//u8 readPINBCD[256] = {0};
	u8 buf[64] = {0};
//	u8 buf4[256] = {0};
 	s32 key = 0;
	s32 rslt;
	fingerprint.Error=AGENTMENUCHECK;//For fingerprint
	if(i==1)
	{	
		memset(sendData.asAgentEnterPin, 0, sizeof(sendData.asAgentEnterPin));
		sprintf(sendData.asAgentEnterPin,"0");
		while(1)
		{	
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT...", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();

			if (sdkTimerIsEnd(Temp_timer, 120 * 1000))     //Timer-out 60s
			{
				sdkSysBeep(SDK_SYS_BEEP_OK);
				return AppMain();
			}

			rslt = AgentFPCheckV2();

			if (rslt == SDK_OK)                                                               
			{
				rslt = VerifyFPC(receivedData.asFingerTemplate,3);

				if (rslt == SDK_OK)                                                               
				{
					
						NewMainMenu();
				}
				else			
				{
					LoginFace();
				}
			}
			else
			{
				LoginFace();
			}
		}
	}
	else
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, asPathPin);
		sdkDispBrushScreen();
		memset(sendData.asAgentEnterPin, 0, sizeof(sendData.asAgentEnterPin));
		memset(sendData.asSalesEnterPin, 0, sizeof(sendData.asSalesEnterPin));
		
  		key = sdkKbGetScanf(60000, buf, 6, 6, SDK_MMI_PWD ,SDK_DISP_LINE4);

		if (key == SDK_KEY_ENTER)
	  	{
	  		/*sdkAscToBcd(readPINBCD,&buf[1],buf[0]);

			TraceHex("logincheck", "readPINBCD Hex", readPINBCD, 6);
			
			aes256_init(&ctx, DEFAULT_KEY);
			aes256_encrypt_ecb(&ctx, readPINBCD);

			TraceHex("logincheck", "readPINBCD Enc Hex", readPINBCD, 6);
	
			memset(buf4, 0, sizeof(buf4));
			sdkBcdToAsc(buf4, &readPINBCD[0], 3);*/

			if(strcmp(sendData.asFirstDigit,"0")==0)
			{
				//aes256_decrypt_ecb(&ctx, buf4);
				//aes256_decrypt_ecb(&ctx, sendData.asSalesCardPin);

				//Trace("logincheck", "Sales Type PIN = %s\r\n", buf4);
				//Trace("logincheck", "Sales Card PIN = %s\r\n", sendData.asSalesCardPin);
				memset(sendData.asSalesEnterPin,0,sizeof(sendData.asSalesEnterPin));
				memcpy(sendData.asSalesEnterPin,&buf[1],6);
				Trace("logincheck", "Sales Enter PIN = %s\r\n", sendData.asSalesEnterPin);
				rslt=CheckSALEPINModule();
				if(rslt==SDK_OK)
				{
				
					SalesMenu();
				}
				else
				{
					LoginFace();
				}
				
				/*else
				{
					sdkSysBeep(SDK_SYS_BEEP_ERR);
					sdkDispClearScreen();
					sdkDispFillRowRam(SDK_DISP_LINE3, 0, "WRONG PIN!", SDK_DISP_DEFAULT);
					sdkDispBrushScreen();
					sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 3000);
					LoginFace();
				}*/
			
				
			}
			if(strcmp(sendData.asFirstDigit,"1")==0)
			{

				
				memset(sendData.asAgentEnterPin,0,sizeof(sendData.asAgentEnterPin));
				memcpy(sendData.asAgentEnterPin,&buf[1],6);
				rslt = CheckAgentPINModule();

				if(rslt==SDK_OK)
				{
				
					NewMainMenu();
				}
				else
				{
					LoginFace();
				}
				
			}	
			
	  	}
		else if(key==SDK_KEY_ESC)
		{
			LoginFace();
		}
		
	}

	LoginFace();
	
}


/*******************************************************************************
** Description :  Display and handles main menu item.
** Parameter   :  void
** Return      :
** Author      :  Cody   2014-06-30
** Remark      :
*******************************************************************************/
	
void IconFace(void)
{	

	//Trace("xgd", "Start IconFace= \r\n");
	u8 tmp[128] = {0};
	//u8 temp1[128] = {0};
	u32 tmr_disp;
	bool refresh_time = true;
	bool refresh_batterytime=true;
	s32 key;
	//u32 timerforideal;
	idealTimer=0;
	idealTimer = sdkTimerGetId();
 	
 	
 	
	sdkDispSetBackColor(0xFFFF);
	sdkDispSetFontColor(0x0000); 

	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320,"/mtd0/res/Wellcome.bmp");
	//sdkDispShowBmp(30,30, 240, 320,"/mtd0/res/Wellcome.bmp");
	

	memset(tmp,0,sizeof(tmp));
	if(IPAddress=="api-dev.truemoney.com.mm/trueapi")
	{
		sprintf(tmp,"%s",ver);
		
	}
	
	else if(IPAddress=="api.truemoney.com.mm/trueapi")
	{
		sprintf(tmp,"%s",verlive);
	}
	sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
	sdkDispAt(55,25, tmp);
	sdkDispBrushScreen();
	tmr_disp = sdkTimerGetId();
	while(1)
	
	{
		if(refresh_time)//For display time 
		{
			DispDateTime(SDK_DISP_LINE5);
			sdkDispBrushScreen();
			refresh_time = false;	
		}
		
		if(refresh_batterytime)//For Battery Icon
			{
				BatteryMessage();
				refresh_batterytime=false;
			}
		
		
		 key = sdkKbWaitKey(SDK_KEY_MASK_ENTER,1000);
		switch(key)
		{
			case SDK_KEY_ENTER:
				{
					sdkDispClearScreen();
					sdkDispShowBmp(0,0,240,320,"/mtd0/res/agentcardread.bmp");
					sdkDispBrushScreen();
					ModuleReadCard();
				}
				break;
				default:
				{
					
				}	
				break;
		}
		refresh_time =true;
		if(sdkTimerIsEnd(tmr_disp, 60000))
        {
        	refresh_batterytime=true;
            tmr_disp = sdkTimerGetId();
            
        }
						
				
	}		
	
}
void BatteryMessage(void)
{
	s32 ret,e_battery,signalLevel;
	 bool isPower;

   
    e_battery = sdkSysGetBatteryStateAndControl();
    isPower = sdkSysIsExPower();
    signalLevel = sdkCommGetSignalLevel();
    Trace("battery Message","batterylevel=%d\r\n",e_battery);
    	if(isPower)
			{
				
				sdkDispIcon(SDK_DISP_PLUG, 1, 35, 2);//if plug in , display plug in icon
				if(powerfirstbeep==true)
				{	
					Trace("battery message","first time EDC open when EDC has plug in \r\n");
					 sdkSysBeep(SDK_SYS_BEEP_OK);//plug in for ok voice
					 powerfirstbeep=false;
				 
				//Trace("Icon face"," is  power plug in \r\n");
				}
			}	
			else
			{
				
				sdkDispIcon(SDK_DISP_PLUG,0, 35, 2);//for unplug icon
				powerfirstbeep=true;// For beep soung while power plug in
				 
				 
				Trace("battery message"," is  power not  plug in \r\n");
		
			
			}
    
    switch (e_battery)// for battery status
	{
		case -2: 
			sdkDispIcon(SDK_DISP_BATTERY, 98, 1, 2);
			break;
		case -1:
			sdkDispIcon(SDK_DISP_BATTERY, 98, 1, 2);
			break;
		case 0:
			sdkDispIcon(SDK_DISP_BATTERY, 0, 1, 2);
			break;
		case 1:
			sdkDispIcon(SDK_DISP_BATTERY, 1, 1, 2);
			break;
		case 2:
			sdkDispIcon(SDK_DISP_BATTERY, 2, 1, 2);
	    	break;
		case 3:
			sdkDispIcon(SDK_DISP_BATTERY, 3, 1, 2);
			break;
		case 99:
			sdkDispIcon(SDK_DISP_BATTERY, 99, 1, 2);
			break;
		default:
			sdkDispIcon(SDK_DISP_BATTERY, 98, 1, 2);
			break;
				
	}
		if (signalLevel >= 0x28)//for signal strength
		{
			sdkDispIcon(SDK_DISP_SIGNAL,5, 210, 2);
		}
		else if (signalLevel >= 0x20)
		{
			sdkDispIcon(SDK_DISP_SIGNAL, 4, 210, 2);
		}
		else if (signalLevel >= 0x15)
		{
			sdkDispIcon(SDK_DISP_SIGNAL, 3, 210, 2);
		}
		else if (signalLevel >= 0x10)
		{
			sdkDispIcon(SDK_DISP_SIGNAL, 2, 210, 2);
		}
		else if (signalLevel == 0x05)
		{
			sdkDispIcon(SDK_DISP_SIGNAL, 1,210, 2);
		}
		else
		{
			sdkDispIcon(SDK_DISP_SIGNAL, 0, 210, 2);
		}
		
				
			if(!isPower)
				{
					 if (sdkTimerIsEnd(idealTimer, 60* 1000 )) 
				     {
				     	//Trace("Icon face","is not power \r\n");
				         sdkSysBeep(SDK_SYS_BEEP_OK);
						 ret =sdkSysEnterIdle(1, 1);
						 idealTimer = sdkTimerGetId();//Restart the timer
					}
						     
				}
				else
					{
						//Trace("Icon face"," is  power \r\n");
					}
	
}



void MainMenu(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key = 0;
	u8 fpData[32] ="0";
	s32 ret;
	u8 prebalance[128]={0};
	u8 buf6[128]={0};
	memset(buf6,0,sizeof(buf6));
		
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0,238,318,asPathMain);
	
	u8 buf[128]={0};
	u8 buf1[128]={0};
	u8 buf2[128]={0};
	u8 buf5[128]={0};
	
	memset(buf, 0, sizeof(buf));
	memset(buf1,0,sizeof(buf1));
	memset(buf2,0,sizeof(buf2));
	
	if(poweronforNewMenu==true && secondtransactionstart==false) //first time poweron , call this url one time.
	{
		ForSendData();               //checktrx
		Trace("MainMenu","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
		sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial);

		sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

		memset(sendData.asHashData, 0, 128);
		sdkBcdToAsc(sendData.asHashData, out, 32);

		Trace("mainmenu","tmp = %s\r\n",sendData.asHashData);

		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf, "%s%s/pageAgentChkBalance/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);
	
	
		ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
					poweronforNewMenu=false;//For PageAgentCheck Balance
					Forappmaintransaction=false;//For PageAgentCheck Balance
					
					memset(receivedDatanew.NewMainMenuAmount,0,sizeof(receivedDatanew.NewMainMenuAmount));
					js_tok(receivedDatanew.NewMainMenuAmount,"agentBalanceAmount",0);
					
					Trace("MainMenu","receivedData.NewMainMenuAmount=%s\r\n",receivedDatanew.NewMainMenuAmount);
					
					strcpy(prebalance,receivedDatanew.NewMainMenuAmount);
						
					Trace("MainMenu","prebalance= %s\r\n",prebalance);
					sprintf(buf1,"KS %s",receivedDatanew.NewMainMenuAmount);
					
					sprintf(buf6, "BALANCE:\n");
					sdkDispSetFontSize( SDK_DISP_FONT_BIG);
					sdkDispAt(11,260,buf6);
					sdkDispBrushScreen();
			
					
					sdkDispSetFontSize( SDK_DISP_FONT_BIG);
					sdkDispAt(11,290,buf1);
					sdkDispBrushScreen();
			
			}
			else
			{
				memset(buf5,0,sizeof(buf5));
				sprintf(buf5, "YOUR BALANCE IS NOT AVAILABLE.");
			}
	
		
		}

	}
	
	else if(transaction==true && secondtransactionstart==true)//EDC has transation , transation success,
	{
		ForSendData();               //checktrx
		Trace("mainmenu","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
		sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial);

		sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

		memset(sendData.asHashData, 0, 128);
		sdkBcdToAsc(sendData.asHashData, out, 32);

		Trace("mainmenu","tmp = %s\r\n",sendData.asHashData);

		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf, "%s%s/pageAgentChkBalance/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);
	
	
		ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
				if(strcmp(receivedData.ServiceStatus,"Success")==0)
				{
					memset(receivedDatanew.NewMainMenuAmount,0,sizeof(receivedDatanew.NewMainMenuAmount));
					js_tok(receivedDatanew.NewMainMenuAmount,"agentBalanceAmount",0);
					Trace("MainMenu","receivedDatanew.NewMainMenuAmount= %s\r\n",receivedDatanew.NewMainMenuAmount);
					strcpy(prebalance,receivedDatanew.NewMainMenuAmount);
						
					Trace("MainMenu","prebalance= %s\r\n",prebalance);
					sprintf(buf1,"KS %s",receivedDatanew.NewMainMenuAmount);
						
					sprintf(buf6, "BALANCE:\n");
					sdkDispSetFontSize( SDK_DISP_FONT_BIG);
					sdkDispAt(11,260,buf6);
					sdkDispBrushScreen();
				
					sdkDispSetFontSize( SDK_DISP_FONT_BIG);
					sdkDispAt(11,290,buf1);
					sdkDispBrushScreen();
					
				}
				else
				{
					memset(buf5,0,sizeof(buf5));
					sprintf(buf5, "YOUR BALANCE IS NOT AVAILABLE.");
				}
				
		}
	}
		
	else if(forappmainbalance==true&&poweronforNewMenu==false &&Forappmaintransaction==true)// reach for AppMain
	{
		ForSendData();               //checktrx
		Trace("mainmenu","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
		sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial);

		sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

		memset(sendData.asHashData, 0, 128);
		sdkBcdToAsc(sendData.asHashData, out, 32);

		Trace("mainmenu","tmp = %s\r\n",sendData.asHashData);

		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf, "%s%s/pageAgentChkBalance/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);
	
	
		ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				forappmainbalance=false;//For Pageagent Check Balance	
				memset(receivedDatanew.NewMainMenuAmount,0,sizeof(receivedDatanew.NewMainMenuAmount));
				js_tok(receivedDatanew.NewMainMenuAmount,"agentBalanceAmount",0);
				Trace("MainMenu","receivedDatanew.NewMainMenuAmount= %s\r\n",receivedDatanew.NewMainMenuAmount);
				strcpy(prebalance,receivedDatanew.NewMainMenuAmount);
				
				Trace("MainMenu","prebalance= %s\r\n",prebalance);
				sprintf(buf1,"KS %s",receivedDatanew.NewMainMenuAmount);
				
				sprintf(buf6, "BALANCE:\n");
  				sdkDispSetFontSize( SDK_DISP_FONT_BIG);
  				sdkDispAt(11,260,buf6);
  				sdkDispBrushScreen();
	
				
  				sdkDispSetFontSize( SDK_DISP_FONT_BIG);
  				sdkDispAt(11,290,buf1);
  				sdkDispBrushScreen();
  			
			}	
			else
			{
				memset(buf5,0,sizeof(buf5));
				sprintf(buf5, "YOUR BALANCE IS NOT AVAILABLE.");
			}
	
		
		}

	}
	
	else // transaction faill , not call url 
	{
		Trace("MainMenu","receivedData.NewMainMenuAmount=%s\r\n",receivedDatanew.NewMainMenuAmount);
		strcpy(prebalance,receivedDatanew.NewMainMenuAmount);
				
		Trace("MainMenu","prebalance= %s\r\n",prebalance);
		sprintf(buf1,"KS %s",receivedDatanew.NewMainMenuAmount);
		
		sprintf(buf6, "BALANCE:\n");
		sdkDispSetFontSize( SDK_DISP_FONT_BIG);
		sdkDispAt(11,260,buf6);
		sdkDispBrushScreen();

		
		sdkDispSetFontSize( SDK_DISP_FONT_BIG);
		sdkDispAt(11,290,buf1);
		sdkDispBrushScreen();
  			
	}

	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | 
                               SDK_KEY_MASK_5 | SDK_KEY_MASK_6 | SDK_KEY_MASK_7 | SDK_KEY_MASK_ESC, KbWaitTime);
     switch (key)
    {
         case SDK_KEY_1:
            {
                
                 transaction=false;//For Pageagent check balance
                MemToMemCashInMainMenu();
            }
            break;

         case SDK_KEY_2:
            {
            	 transaction=false;//For Pageagent check balance
                CashOutModule();
            }
            break;

         case SDK_KEY_3:
            {
            	 transaction=false;//For Pageagent check balance
                TransferMainMenu();
                
            }
            break;

         case SDK_KEY_4:
            {
                transaction=false;//For Pageagent check balance
                MemberCheckBalance();
            }
            break;

         case SDK_KEY_5:
            {
            	 transaction=false;//For Pageagent check balance
               ChooseOperatorMenu();
            }
            break;

        case SDK_KEY_6:
            {
            	 transaction=false;//For Pageagent check balance
             	BillPayMainMenu();
            }
            break;
        case SDK_KEY_7:
            {
            	transaction=false;//For Pageagent check balance
				AgentMenu();
				//ModuleTouchScreen();
            }
            break;
        
		 case SDK_KEY_ESC:
		 	{
		 		transaction=false;//For Pageagent check balance
		 		NewMainMenu();//Shortcut menu
		 	}
		 	break;
         default:
            {
            	
            }
             break;
             
             
             
    }
	transaction=false;//For Pageagent check balance
	NewMainMenu();		
	

}


void NewMainMenu(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 key = 0;
	u8 fpData[32] ="0";
	s32 ret;
	u8 prebalance[128]={0};
	

	sdkDispClearScreen();
	sdkDispShowBmp(0,0,239,318,"/mtd0/res/mainpage.bmp");
	//sdkDispBrushScreen();
	
	u8 buf[128]={0};
	u8 buf1[128]={0};
	u8 buf2[128]={0};
	u8 buf5[128]={0};
	
	memset(buf, 0, sizeof(buf));
	memset(buf1,0,sizeof(buf1));
	memset(buf2,0,sizeof(buf2));
	
	
	if(poweronforNewMenu==true && secondtransactionstart==false) //first time poweron , call this one time
	{
	
		ForSendData();               //checktrx
		Trace("newmainmenu","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
		sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial);

		sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

		memset(sendData.asHashData, 0, 128);
		sdkBcdToAsc(sendData.asHashData, out, 32);

		Trace("newmainmenu","tmp = %s\r\n",sendData.asHashData);

		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf, "%s%s/pageAgentChkBalance/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);
	
	
		ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				poweronforNewMenu=false;//For PageAgentCheck Balance
				Forappmaintransaction=false;//For PageAgentCheck Balance
				
				memset(receivedDatanew.NewMainMenuAmount,0,sizeof(receivedDatanew.NewMainMenuAmount));
				js_tok(receivedDatanew.NewMainMenuAmount,"agentBalanceAmount",0);
				
				Trace("NewMainMenu","receivedData.NewMainMenuAmount=%s\r\n",receivedDatanew.NewMainMenuAmount);
				
				memset(prebalance,0,sizeof(prebalance));
				strcpy(prebalance,receivedDatanew.NewMainMenuAmount);
					
				Trace("NewMainMenu","prebalance= %s\r\n",prebalance);
				sprintf(buf1,"KS %s",receivedDatanew.NewMainMenuAmount);
				
				sdkDispSetFontSize( SDK_DISP_FONT_BIG);
				sdkDispAt(11,290,buf1);
				sdkDispBrushScreen();
			
  			}
			else
			{
				memset(buf5,0,sizeof(buf5));
				sprintf(buf5, "YOUR BALANCE IS NOT AVAILABLE.");
			}
	
		}

	
	}
	
	else if(transaction==true && secondtransactionstart==true)//EDC has transation , transation success,
	{
		ForSendData();               //checktrx
		Trace("newmainmenu","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
		sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial);

		sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

		memset(sendData.asHashData, 0, 128);
		sdkBcdToAsc(sendData.asHashData, out, 32);

		Trace("newmainmenu","tmp = %s\r\n",sendData.asHashData);

		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf, "%s%s/pageAgentChkBalance/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);
	
		ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				memset(receivedDatanew.NewMainMenuAmount,0,sizeof(receivedDatanew.NewMainMenuAmount));
				js_tok(receivedDatanew.NewMainMenuAmount,"agentBalanceAmount",0);
				Trace("NewMainMenu","receivedDatanew.NewMainMenuAmount= %s\r\n",receivedDatanew.NewMainMenuAmount);
				strcpy(prebalance,receivedDatanew.NewMainMenuAmount);
					
				Trace("NewMainMenu","prebalance= %s\r\n",prebalance);
				sprintf(buf1,"KS %s",receivedDatanew.NewMainMenuAmount);
				
				sdkDispSetFontSize( SDK_DISP_FONT_BIG);
				sdkDispAt(11,290,buf1);
				sdkDispBrushScreen();
  		
			}
			else
			{
				memset(buf5,0,sizeof(buf5));
				sprintf(buf5, "YOUR BALANCE IS NOT AVAILABLE.");
			}
		}
	}
		
	else if(forappmainbalance==true&&poweronforNewMenu==false&&	Forappmaintransaction==true)// reach in AppMain menu
	{
		ForSendData();               //checktrx
		Trace("pageagentcheckbalance","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
		sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial);

		sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

		memset(sendData.asHashData, 0, 128);
		sdkBcdToAsc(sendData.asHashData, out, 32);

		Trace("newmainmenu","tmp = %s\r\n",sendData.asHashData);

		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf, "%s%s/pageAgentChkBalance/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,fpData,sendData.asAgentEnterPin,sendData.asEDCserial,sendData.asHashData);
	
		ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				forappmainbalance=false;//For PageAgentCheck Balance	
				memset(receivedDatanew.NewMainMenuAmount,0,sizeof(receivedDatanew.NewMainMenuAmount));
				js_tok(receivedDatanew.NewMainMenuAmount,"agentBalanceAmount",0);
				Trace("NewMainMenu","receivedDatanew.NewMainMenuAmount= %s\r\n",receivedDatanew.NewMainMenuAmount);
				strcpy(prebalance,receivedDatanew.NewMainMenuAmount);
				
				Trace("NewMainMenu","prebalance= %s\r\n",prebalance);
				sprintf(buf1,"KS %s",receivedDatanew.NewMainMenuAmount);
				
  				sdkDispSetFontSize( SDK_DISP_FONT_BIG);
  				sdkDispAt(11,290,buf1);
  				sdkDispBrushScreen();
  			
			}	
			else
			{
				memset(buf5,0,sizeof(buf5));
				sprintf(buf5, "YOUR BALANCE IS NOT AVAILABLE.");
			}
		
		
		}

	}
	else // transaction faill , not call url 
	{
		Trace("NewMainMenu","receivedData.NewMainMenuAmount=%s\r\n",receivedDatanew.NewMainMenuAmount);
		strcpy(prebalance,receivedDatanew.NewMainMenuAmount);
				
		Trace("NewMainMenu","prebalance= %s\r\n",prebalance);
		sprintf(buf1,"KS %s",receivedDatanew.NewMainMenuAmount);
		
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispAt(11,290,buf1);
		sdkDispBrushScreen();
  			
	}
	while(1)
	{
		BatteryMessage();
		  
		  
			key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | 
		                               SDK_KEY_MASK_5 | SDK_KEY_MASK_6 |SDK_KEY_MASK_7 |SDK_KEY_MASK_8| SDK_KEY_MASK_ESC,60000);
		    switch (key)
		    {
		         case SDK_KEY_1:
		            {
		            	
		            	fingerprint.Error=NEWMAINMENU;//IF  fingerprint error, return page
		            	cashout.CashOutMAINMenu=CashOut_New_Menu;//for shortcut menu & old menu
		               	CashOutModuleForNewMenu();
		            }
		            break;
		
		         case SDK_KEY_2:
		            {
		               
		               	
		                topup.MAINMenu=New_Menu;//for shortcut menu & old menu
		                topup.Operator = MPT;
						sdkDispShowBmp(0,0,240,320,"/mtd0/res/mptmenu.bmp");
						sdkDispBrushScreen();
						SelectAmountMenu();
		            }
		            break;
		
		         case SDK_KEY_3:
		            {
		               
		                topup.MAINMenu=New_Menu;//For NewMainMenu
		                topup.Operator=OOREDOO;
						sdkDispShowBmp(0,0,240,320,"/mtd0/res/odomenu.bmp");
						sdkDispBrushScreen();
						SelectAmountMenu();
		                
		            }
		            break;
		
		         case SDK_KEY_4:
		            {
		             
		              topup.MAINMenu=New_Menu;//For NewMainMenu
		               	topup.Operator=TELENOR;
						Select_Eload_Epin();
		            }
		            break;
		
		         case SDK_KEY_5:
		            {
		            	
		               topup.MAINMenu=New_Menu;//For NewMainMenu
		               topup.Operator=MECTEL;
						sdkDispShowBmp(0,0,240,320,"/mtd0/res/mecmenu.bmp");
						sdkDispBrushScreen();
						SelectAmountMenu();
		            }
		            break;
		
		        case SDK_KEY_6:
		            {
		            	
		             	printtype.BillMainMenu=Bill_New_Menu;//For NewMainMenu
		             	AeonAgreement();
		            }
		            break;
		         case SDK_KEY_7:
				 {
				 	
				 	printtype.BillMainMenu=Bill_New_Menu;//For NewMainMenu
				 	YESCMainMenu();
				 	
				}  
				break;
				case SDK_KEY_8:
				{
				
					transaction=false;//For return menu
					MainMenu();
				}	 
				break;
		        case SDK_KEY_ESC:
				{
					
				 	transaction=false;//For return menu
				 	AppMain();
				}
				break;
		         default:
		            {
		            	
		            	
		            }
		             break;
		             
		             
		             
		    }
		    transaction=false;//For return
	

	}
	
}



void AgentMenu(void)
{
    s32 key = 0;
	u32 Temp_timer;
	Temp_timer = sdkTimerGetId();

	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, asPathAgent);
	sdkDispBrushScreen();

    key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | 
                               SDK_KEY_MASK_5 | SDK_KEY_MASK_6 | SDK_KEY_MASK_7 |SDK_KEY_MASK_8| SDK_KEY_MASK_ESC, KbWaitTime);
    			
    switch (key)
    {
         case SDK_KEY_1:
            {
                MasterAgentCashIn();
            }
            break;
		case SDK_KEY_2:
            {
               
                MasterAgentWithdraw();
            }
            break;
       case SDK_KEY_3:
	  		 {
	   			MasterAgentSettlement();
			}  
			break;   
		case SDK_KEY_4:
            {
				
				CustomerRegisterName();
		 	}
            break;

         case SDK_KEY_5:
            {
                AgentCheckBalanceV2();
            }
            break;

         case SDK_KEY_6:
            {
				AgentSummReport();
            }
            break;

        case SDK_KEY_7:
            {
                BankInfo();
            }
            break;
        case SDK_KEY_8:
            {
				AgentRqeuestMenu();
            }
            break;

		 case SDK_KEY_ESC:
		 	{
				MainMenu();
		 	}
		 	break;
         default:
            {
            }
            break;
    }
	MainMenu();
}



void SalesMenu(void)
{
    s32 key = 0;
    u8 page;
    step1:
	page= 0;
	while(1)
	{  
		step2:
		if(page==0)
		{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, asPathSales);
				sdkDispBrushScreen();
		}
		else if(page==1)
		{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,"/mtd0/res/salesmenusecond.bmp");
				sdkDispBrushScreen();
		}
		key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | SDK_KEY_MASK_5 |SDK_KEY_MASK_6|
                              SDK_KEY_MASK_7| SDK_KEY_MASK_8 |SDK_KEY_MASK_UP | SDK_KEY_MASK_DOWN| SDK_KEY_MASK_ESC,KbWaitTime);
    			
		    switch (key)
		    {
		    	
		         case SDK_KEY_1:
		            {	
		            	if(page==0)
		            	{
		            		AgentWithdrawV2();
						}
						else if(page==1)
						
						{
							forsale.servicetype=reactivateAgentRequest;
							Salesreadagentcard();
						}
		            }
		            break;
					case SDK_KEY_2:
		            {
		            	if(page==0)
		            	{
		            		AgentRefillV2();
						}
		            	else if(page==1)
		            	{
		            		SalesPersonFPMenu();
						}
		            }
		            
		            break;
		
		         case SDK_KEY_3:
		            {	
		            	if(page==0)
		            	{
		            		
							SalesPersonChkBalance();
						}
		            
		            }
		            break;
		
		         case SDK_KEY_4:
		            {
		            	if(page==0)
		            	{
		            		forsale.servicetype=posHandoverRequest;
							ReadSaleCard();
						}
		               
		            }
		            break;
		
				 case SDK_KEY_5:
					{		
					
						if(page==0)
						{
							forsale.servicetype=Salesagentdeposit;
							AgentActivationV2();
						}
					 	
					 	
					 	
					}
					break;
				 case SDK_KEY_6:
					 {
					 	if(page==0)
					 	{
					 		
					 		forsale.servicetype=agentContractEndRequest;
					 		Salesreadagentcard();
						 
					 		
						}
					 	
					 }	
				 break;
				 case SDK_KEY_7:
				 	{
				 		if (page==0)
				 		{
				 			
				 			forsale.servicetype=terminalChangeRequest;
				 			Salesreadagentcard();
				 			
				 			
						 }
				 		
					 }
					 break;
				case SDK_KEY_8:
					{
						if(page==0)
						{
							forsale.servicetype=terminalSwitchRequest;
				 			Salesreadagentcard();
						
						}
						
					}
					break;
			
				case SDK_KEY_UP:
                {
                	
                    if (page == 1)
                    {
                        page=page-1;
                    }
                    goto step2;
                }
                break;

             case SDK_KEY_DOWN:
                {
                	formenu.saleservice=pageflagon;
                    if (page == 0)
                    {
                        page=page+1;
                    }
                }
                break;
		
		
				 case SDK_KEY_ESC:
				 	{
						if(page==0)
						{
							AppMain();
						}
						else if(page==1)
						{
							goto step1;
						}
						
				 	}
				 	break;
				
		         default:
		            {
		            	
		            }
		            break;
		            
		    }
		    
		    if(formenu.saleservice==pageflagon)
		    {
		    	formenu.saleservice=pageflagoff;
		    	goto step2;
			}
			else
			{
				break; 	
			}
		   
	}
	AppMain();
}
void DispDateTime(u8 ucLine)
{
    u8 tmp[64];
    u8 disp[128];

    memset(tmp, 0, sizeof(tmp));
    memset(disp, 0, sizeof(disp));

    if (SDK_OK == sdkGetRtc(tmp));
     Trace("dispDateTime", "time=%s",tmp);
    {
        if (tmp[0] >= 0x50)
        {
            sprintf(disp, "19%02X-%02X-%02X %02X:%02X:%02X", 
                         // YY        MM        DD        HH        MM        SS
                          tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5]);
                          
        }
        
        else
        {
            sprintf(disp, "20%02X-%02X-%02X %02X:%02X:%02X", 
                         // YY        MM        DD        HH        MM        SS
                          tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5]);
        }

        sdkDispClearRowRam(ucLine);
        sdkDispFillRowRam(ucLine, 0, disp, SDK_DISP_DEFAULT);
     
        
    }
}


