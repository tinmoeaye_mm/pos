/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : DirectMessage.c
   Version       : Initial Draft
   Author        : Tin Moe Aye
   Created       : 2016/3/17
   Last Modified :
   Description   : agent module-related functions are defined
                  here
******************************************************************************/


#include "../inc/global.h"

void DirectMessage(void)
{
	//downlaod max 300KB
    u8 recvbuf[300*1024]={0};
	
    u8 sendbuf[1024]={0};
	int ret = 0;
	int len = 0;
	u8 message[128] ={0};
	u8 out[128] = {0};
	u8 buf[128] = {0};
	u8 buf1[128]={0};
	
	memset(message,0,sizeof(message));
	sprintf(message,"%s",sendData.asTerminalSN);
	Trace("directmessage","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("directmessage","tmp = %s\r\n",sendData.asHashData);

	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/getPrintMessage/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asTerminalSN,sendData.asHashData);
	Trace("directmessage","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
	{
		memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();	 
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT FOR", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE4, 0, "DIRECT MESSAGE SLIP", SDK_DISP_DEFAULT);
			sdkDispSetFontSize(SDK_DISP_FONT_BIG);
			sdkDispBrushScreen();
			
			memset(receivedData.imageforDM,0,sizeof(receivedData.imageforDM));
			js_tok(receivedData.imageforDM,"imageURL",0);
			
			Trace("DirectMessage","image url=%s\r\n",receivedData.imageforDM);

				
		}
		
		
	}		
	memset(buf,0,sizeof(buf));
	sprintf(buf, "http://122.248.120.162/edc/DirectMessage/%s",receivedData.imageforDM);
	Trace("DirectMessage","image path for download=%s\r\n",buf);	
		
    memset(recvbuf, 0, sizeof(recvbuf));
	
	SDK_EXT_CURL_DATA curl_data = {
            .bMethod = true,
	                 // GET method
			
			.psURL = buf,
			
            
            .psCaFile = NULL,                            // Path to CA certificate. NULL if no CA is needed
            .nPort = 9988,                                 // HTTP default port
            .psSend = sendbuf,                           // POST data
            .nSendLen = strlen(sendbuf),
            .psRecv = recvbuf,                            // receive buffer
            .nRecvLen = sizeof(recvbuf),
            .nTimeout =  120 * 1000 ,                         // time out 2 mins
    };
    
	
    ret = sdkExtCurl(&curl_data);
	
	len= sizeof(recvbuf);
	Trace("DownloadUpdate","recBufSize = %d\r\n",len);
	
	if(SDK_OK == ret)
    {   
		
		Trace("DownloadUpdate","Writing file...\r\n");
		
		memset(buf1,0,sizeof(buf1));
		sprintf(buf1,"/mtd0/res/%s",receivedData.imageforDM);
		Trace("DirectMessage","image path for download=%s\r\n",buf1);

        int temp = sdkWriteFile(buf1,recvbuf,sizeof(recvbuf));
		
		
        if(temp == SDK_FILE_OK)
		{
			Trace("DownloadUpdate","Write complete.\r\n");
			
			sdkPrintInit();
			sdkPrintBitMap(buf1, SDK_PRINT_RIGHTALIGN, 0);
			sdkPrintStart(); 
				 
        }
		else
		{
			
			Trace("DownloadUpdate","Write fail!");
		}
		
		
    }
    else if(SDK_ERR == ret || SDK_TIME_OUT == ret)
    {
       
		Trace("DownloadUpdate","TIMEOUT1!");
        
    }
    
	if(sdkAccessFile(buf1))
	{
	
		sdkDelFile(buf1);
		
		Trace("DirectMessage","Delete image file =%s\r\n",buf1);
			
	}
	else
	{
		
	}


	
}

