/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : agent.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/2/10
   Last Modified :
   Description   : agent module-related functions are defined
                  here
   History       :
   1.Date        : 2016/2/10
    Author      : Thu Kha,Tin Moe Aye
    Modification: Created file

******************************************************************************/

#include "../inc/global.h"

#define CTRL_BLOCK    	11
#define CARDNO_BLOCK  	9

const u8 asPathSelectFinger2[] = "/mtd0/res/fpselect.bmp";


void SalesPersonFPMenu(void)
{
	s32 key=0;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/salevisit.bmp");
	sdkDispBrushScreen();
	
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 |SDK_KEY_MASK_3| SDK_KEY_MASK_ESC, 60000);
	switch(key)
	{
		case SDK_KEY_1:
						SalesPersonFPRegister();
						break;
		case SDK_KEY_2:
						SalesPersonVisit();
						break;
		case SDK_KEY_3:
						SalesPersonFPReset();
						break;
		case SDK_KEY_ESC:
						SalesMenu();
						break;
		default:
						SalesMenu();
						break;
	}
	
}
void SalesPersonFPRegister(void)
{
	s32 key=0;
	s32 ret = 0;
	u8 temp[1024];
	fingerprint.Error=SALE;
	sdkDispClearScreen();
	sdkDispShowBmp(0,5,240,150,"/mtd0/res/Hdcustomer.bmp");
	sdkDispShowBmp(0, 0, 240, 320, asPathSelectFinger2);
	sdkDispBrushScreen();
	
	memset(sendData.asFingerImage,0,sizeof(sendData.asFingerImage));
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);

	if (key == SDK_KEY_1)
	{
		strcpy(sendData.asFingerImage, "lindex");
	}
	else if (key == SDK_KEY_2)
	{
		strcpy(sendData.asFingerImage, "rthumb");
	}
	else if(key==SDK_KEY_ESC)
	{
		SalesPersonFPMenu();
	}

	Trace("xgd","sendData.asFingerimage = %s\r\n",sendData.asFingerImage);
	
	ret = EnrollFPC(2, true, temp);

	s32 len2;
	u8 *pBufLen = &temp[8];
	len2 = ((pBufLen[0] << 24) 
		  | (pBufLen[1] << 16)
		  | (pBufLen[2] << 8)
		  | pBufLen[3]);
	
	u8 buf2[1024] = {0};
	sdkBcdToAsc(buf2, temp, len2);
	TraceHex("xgd", "Fingerprint Template", temp, len2);
	
	Trace("xgd","buf2 = %s\r\n",buf2);

	if (ret != SDK_OK)
	{
		AgentMenu();
	}
	memset(sendData.asFingerTemplate,0,sizeof(sendData.asFingerTemplate));

	memcpy(sendData.asFingerTemplate,buf2,len2*2); //cody for pst data
	
	//memcpy(sendData.asFingerTemplate,buf2,512);
	ret = VerifyFPC(temp, SafeLEVEL_3);
	if (ret == SDK_OK)									  //cancel key to exit
	{
		SalesPersonRegisterSendData();
	}
	else
	{
		SalesPersonFPMenu();
	}
}
void SalesPersonRegisterSendData(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
					
	sprintf(message,"%s%s%s",sendData.asSalesCardNo,sendData.asFingerTemplate,sendData.asTerminalSN);

	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
			
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
			
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
			
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/fpregister/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asSalesCardNo,sendData.asFingerTemplate,sendData.asTerminalSN,sendData.asHashData);
			
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
			
	ret = SendReceivedData(sendData.asSendBuf,false);
			
			
	if(ret!=0)
	{
		memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
		Trace("xgd","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
					
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				sdkDispClearScreen();    
				sdkDispFillRowRam(SDK_DISP_LINE1, 0, "FINGER PRINT REGISTER", SDK_DISP_DEFAULT);		
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);	
				sdkDispBrushScreen();
				sdkmSleep(1500);
				AppMain();
						
							
			}
			else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
				js_tok(receivedData.ErrorMessage,"message",0);						
				DisplayErrorMessage("FINGER PRINT REGISTER",receivedData.ErrorMessage,false);
				SalesPersonFPMenu();
			}
				
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	AppMain();			
			
}
void SalesPersonVisit(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};

	
	s32 rslt;

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	memset(message,0,sizeof(message));				
	sprintf(message,"%s",sendData.asSalesCardNo);

	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
			
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
			
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
			
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/fpcheck/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asSalesCardNo,sendData.asHashData);
			
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
			
	ret = SendReceivedData(sendData.asSendBuf,false);
			
			
	if(ret!=0)
	{
		memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
		Trace("xgd","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
					
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				memset(receivedData.AccessFinger,0,sizeof(receivedData.AccessFinger));
				js_tok(receivedData.AccessFinger,"fingerPrint",0);
				
				Trace("xgd","receivedData.AccessFinger= %s\r\n",receivedData.AccessFinger);
				Trace("xgd","receivedData.ServiceStatus = %s\r\n",sendData.asFingerTemplate);	
			
			memset(receivedData.asFingerTemplate,0,sizeof(receivedData.asFingerTemplate));
			sdkAscToBcd(receivedData.asFingerTemplate, receivedData.AccessFinger, 512);
			
			ret = SDK_OK;
				
			if(ret == SDK_OK)
			
			rslt = VerifyFPC(receivedData.asFingerTemplate,1);		
					
											
			{							
				sprintf(message,"%s%s",sendData.asSalesCardNo,sendData.asTerminalSN);
			
				sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
										
				memset(sendData.asHashData, 0, 128);
				sdkBcdToAsc(sendData.asHashData, out, 32);
										
				Trace("xgd","tmp = %s\r\n",sendData.asHashData);
										
				memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
				sprintf(sendData.asSendBuf, "%s%s/spvisit/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asSalesCardNo,sendData.asTerminalSN,sendData.asHashData);
										
				Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
										
				ret = SendReceivedData(sendData.asSendBuf,false);						
										
				if(ret!=0)
				{
					memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
					js_tok(receivedData.ServiceStatus,"serviceStatus",0);
										
					Trace("xgd","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
												
					if(strcmp(receivedData.ServiceStatus,"Success")==0)
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							sdkDispClearScreen();    
							sdkDispFillRowRam(SDK_DISP_LINE1, 0, "SALE VISIT", SDK_DISP_DEFAULT);		
							sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
							sdkDispBrushScreen();
							sdkmSleep(1500);
							AppMain();
						}
					else
						{
							sdkDispClearScreen();
							sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
							sdkDispBrushScreen();
							sdkmSleep(1500);
							memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
							js_tok(receivedData.ErrorMessage,"message",0);						
							DisplayErrorMessage("SALE VISIT",receivedData.ErrorMessage,false);
							SalesPersonFPMenu();
						}
							
				}
						
			}
				
			}
			else
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
				sdkDispBrushScreen();
				sdkmSleep(1500);
				sdkDispClearScreen();    
				sdkDispFillRowRam(SDK_DISP_LINE1, 0, "PLS REGISTER", SDK_DISP_DEFAULT);
				js_tok(receivedData.ErrorMessage,"message",0);			
				DisplayErrorMessage("SALE VISIT",receivedData.ErrorMessage,false);		
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "", SDK_DISP_DEFAULT);		
				sdkDispBrushScreen();
			}
			
	}
	else
	{
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
		sdkDispBrushScreen();
		sdkmSleep(1500);
		memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
		js_tok(receivedData.ErrorMessage,"message",0);						
		DisplayErrorMessage("SALE VISIT",receivedData.ErrorMessage,false);
		SalesPersonFPMenu();
	}				
	
	AppMain();	
}
void SalesPersonFPReset(void)
{
	u8 message[128] ={0};
	s32 ret,key;
	u8 out[128] = {0};
	u8 temp[1024] = {0};
	//fingerprint.Error=SALE;
	
	sdkDispClearScreen(); 
		
	sdkDispShowBmp(0,5,240,150,"/mtd0/res/Hdcustomer.bmp");
	sdkDispShowBmp(0, 0, 240, 320, asPathSelectFinger2);
	sdkDispBrushScreen();
	
	memset(sendData.asFingerImage,0,sizeof(sendData.asFingerImage));
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);

	if (key == SDK_KEY_1)
	{
		strcpy(sendData.asFingerImage, "lindex");
	}
	else if (key == SDK_KEY_2)
	{
		strcpy(sendData.asFingerImage, "rthumb");
	}
	else if(key==SDK_KEY_ESC)
	{
		SalesPersonFPMenu();
	}

	Trace("xgd","sendData.asFingerimage = %s\r\n",sendData.asFingerImage);
	
	ret = EnrollFPC(2, true, temp);

	s32 len2;
	u8 *pBufLen = &temp[8];
	len2 = ((pBufLen[0] << 24) 
		  | (pBufLen[1] << 16)
		  | (pBufLen[2] << 8)
		  | pBufLen[3]);
	
	u8 buf2[1024] = {0};
	sdkBcdToAsc(buf2, temp, len2);
	TraceHex("xgd", "Fingerprint Template", temp, len2);
	
	Trace("xgd","buf2 = %s\r\n",buf2);


	memset(sendData.asFingerTemplate,0,sizeof(sendData.asFingerTemplate));

	memcpy(sendData.asFingerTemplate,buf2,len2*2);
	
	sdkDispBrushScreen();
					
	sprintf(message,"%s%s%s",sendData.asSalesCardNo,sendData.asFingerTemplate,sendData.asTerminalSN);

	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
			
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
			
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
			
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/fpreset/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asSalesCardNo,sendData.asFingerTemplate,sendData.asTerminalSN,sendData.asHashData);
			
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
			
	ret = SendReceivedData(sendData.asSendBuf,false);
			
			
	if(ret!=0)
	{
		memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
		Trace("xgd","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
					
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				sdkDispClearScreen();    
				sdkDispFillRowRam(SDK_DISP_LINE1, 0, "FINGER PRINT RESET", SDK_DISP_DEFAULT);		
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
				sdkDispBrushScreen();
				sdkmSleep(1500);
				AppMain();
						
							
			}
			else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
				js_tok(receivedData.ErrorMessage,"message",0);						
				DisplayErrorMessage("FINGER PRINT RESET",receivedData.ErrorMessage,false);
				SalesPersonFPMenu();
			}
				
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	AppMain();				
}
void SalesPersonChkBalance(void)
{
 
	u8 tsk[64] = {0};
	u8 out[128] = {0};
	u8 hashData[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	s32 key = 0;
	u8 buf[128] = {0};
	 
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	memcpy(tsk,TSK,strlen(TSK));

 	sprintf(message,"%s%s%s",sendData.asSalesCardNo,sendData.asSalesEnterPin,sendData.asTerminalSN);

	sdkCalcHmacSha256(tsk, strlen(tsk), message, strlen(message), out);

	memset(hashData, 0, 128);
	sdkBcdToAsc(hashData, out, 32);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/salesPersonChkBalance/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asSalesCardNo,sendData.asSalesEnterPin,sendData.asTerminalSN,hashData);
	Trace("xgd","sendata=%s\r\n",sendData.asSendBuf);
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
			memset(receivedData.BalanceAmount,0,sizeof(receivedData.BalanceAmount));
			js_tok(receivedData.BalanceAmount,"salesPersonBalanceAmount",0);
			
			memset(receivedData.CardNo,0,sizeof(receivedData.CardNo));
			js_tok(receivedData.CardNo,"salesPersonCardNo",0);

			memset(receivedData.SalesCardRef,0,sizeof(receivedData.SalesCardRef));
			js_tok(receivedData.SalesCardRef,"salesPersonCardRef",0);

			memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			memset(receivedData.TRX,0,sizeof(receivedData.TRX));
			js_tok(receivedData.TRX,"trx",0);

			memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);

//			sdkDispClearScreen();    
//	        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "Sale CHECK BALANCE", SDK_DISP_DEFAULT);
//			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "Balance :", SDK_DISP_LEFT_DEFAULT);
//			sdkDispFillRowRam(SDK_DISP_LINE4, 0, receivedData.BalanceAmount, SDK_DISP_RIGHT_DEFAULT);
	
			
			sdkDispClearScreen();
			
			memset(buf,0,sizeof(buf));
			sprintf(buf, "%s", "SALE CHECK BALANCE");
			sdkDispAt(12, 20, buf);
			memset(buf,0,sizeof(buf));
			sprintf(buf,"%s","BALANCE:");
			sdkDispAt(12,80,buf);
			memset(buf,0,sizeof(buf));
			sprintf(buf,"KS %s",receivedData.BalanceAmount);
			sdkDispAt(12,120,buf);
			sdkDispSetFontSize(SDK_DISP_FONT_NORMAL);
			sdkDispFillRowRam(SDK_DISP_LINE5, 0, "PRESS [ENTER] TO PRINT SLIP", SDK_DISP_DEFAULT);
			sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	        sdkDispBrushScreen();

			key = sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);

			switch (key)
	        {
	             case SDK_KEY_ENTER:
	                {
	                    PrintSaleCheckBalance();
	                }
	                break;
	             case SDK_KEY_ESC:
	                {
	                    SalesMenu();
	                }
	             default:
	                {
	                }
	                break;
	        }
						
		}
		else
		{
			sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
			memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
			js_tok(receivedData.ErrorMessage,"message",0);

			DisplayErrorMessage("SALE CHECK BALANCE",receivedData.ErrorMessage,false);
		}
	}
	else
	{
		DisplayErrorMessage("","",true);
	}

	SalesMenu();
}
void AgentRefillV2(void)
{
 	u8 tsk[64] = {0};
	u8 out[128] = {0};
	u8 hashData[128] = {0};
	u8 message[128] ={0};
	u8 buf1[128] = {0};
	u8 buf[16] = {0};
	u8 tmp[32] = {0};
	s32 len, res,key;
	u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
    
    sdkDispClearScreen();
	sdkDispShowBmp(0,0,240, 320, "/mtd0/res/salerefill.bmp");
	sdkDispBrushScreen();
    
    key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | SDK_KEY_MASK_5 | SDK_KEY_MASK_ESC , KbWaitTime);
    
	memset(sendData.asWithdrawAmount,0,sizeof(sendData.asWithdrawAmount));			
    
	switch (key)
    {
         case SDK_KEY_1:
            {
                sprintf(sendData.asRefillAmount,"50000");
            }
            break;

         case SDK_KEY_2:
            {
				sprintf(sendData.asRefillAmount,"100000");
            }
            break;

         case SDK_KEY_3:
            {
				sprintf(sendData.asRefillAmount,"200000");
            }
            break;

         case SDK_KEY_4:
            {
                sprintf(sendData.asRefillAmount,"300000");
            }
            break;

		 case SDK_KEY_5:
			{
			 	sdkDispClearScreen();
				sdkDispFillRowRam(SDK_DISP_LINE1, 0, "AGENT REFILL", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "AMOUNT", SDK_DISP_DEFAULT);
				sdkDispBrushScreen();
				
				memset(buf,0,sizeof(buf));
				res = GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,4,8, NULL, " KS");
	
				if (res == SDK_KEY_ENTER)
				{
					Trace("xgd", "buf= %s\r\n", buf);
					memset(sendData.asRefillAmount,0,sizeof(sendData.asRefillAmount));
					memcpy(sendData.asRefillAmount,&buf[1],8);
					
					Trace("xgd","sendData.asRefillAmountforbuf= %s\r\n",&buf[1]);
					Trace("xgd","sendData.asRefillAmount= %s\r\n",sendData.asRefillAmount);

					
				}
				else
				{
					AgentRefillV2();
				}
			}
			break;
		 case SDK_KEY_ESC:
		 	{
				SalesMenu();
		 	}
		 	break;
         default:
            {
				SalesMenu();
            }
            break;
    }
    Trace("xgd", "sendData.asWithdrawAmount= %s\r\n", sendData.asRefillAmount);
    
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE1, 0, "REFILL", SDK_DISP_DEFAULT);
		memset(tmp,0,sizeof(tmp));
		
		
		sprintf(tmp,"%s KS",sendData.asRefillAmount);
		Trace("xgd","sendData.asRefillAmount= %s\r\n",sendData.asRefillAmount);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, tmp , SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TAP AGENT CARD" , SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "TO CONTINUE", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		
		sdkIccOpenRfDev();
		 // Query mifare card
		 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
		 {
			// Play a beep on successful query card
			sdkSysBeep(SDK_SYS_BEEP_OK);
		 }
		 else
		 {
			sdkPEDCancel();
			memset(tmp,0,sizeof(tmp));
  			 memset(sendData.asRefillAmount,0,sizeof(sendData.asRefillAmount));
  			 memset(buf,0,sizeof(buf));
   			memset(buf1,0,sizeof(buf1));
			SalesMenu();
		 }
		 // Verify original key
		 memset(buf, 0, sizeof(buf));
		 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
		 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
		 
		 if (res != SDK_OK || buf[0] != 0)
		 {
			sdkSysBeep(SDK_SYS_BEEP_ERR);
			sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
			sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
			sdkPEDCancel();
			SalesMenu();
		 }
		
		// Read card no from mifare card
		memset(buf, 0, sizeof(buf));
		res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);
		if (res != SDK_OK || buf[0] != 0)
		{
			sdkSysBeep(SDK_SYS_BEEP_ERR);
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
			sdkPEDCancel();
			SalesMenu();
		}
		
		memset(buf1, 0, sizeof(buf1));
		sdkBcdToAsc(buf1, &buf[1], datalen);
		memset(sendData.asAgentCardNo, 0, sizeof(sendData.asAgentCardNo));
		memcpy(sendData.asAgentCardNo, &buf1[0], 9);
		Trace("xgd", "Agent Card No = %s\r\n", sendData.asAgentCardNo);
		sdkIccCloseRfDev(); 
		
		sdkDispClearScreen();	 
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();

		memset(tsk, 0, sizeof(tsk));
		memcpy(tsk,TSK,strlen(TSK));

		sprintf(message,"%s%s0%s%s%s",sendData.asAgentCardNo,sendData.asSalesCardNo,sendData.asSalesEnterPin,sendData.asTerminalSN,sendData.asRefillAmount);

		sdkCalcHmacSha256(tsk, strlen(tsk), message, strlen(message), out);

		memset(hashData, 0, 128);
		sdkBcdToAsc(hashData, out, 32);
		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf, "%s%s/agentRefill/%s/%s/%s/0/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asSalesCardNo,sendData.asSalesEnterPin,sendData.asTerminalSN,sendData.asRefillAmount,hashData);
		Trace("xgd","sendata=%s\r\n",sendData.asSendBuf);
		
		res = SendReceivedData(sendData.asSendBuf,false);
		
		if(res!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				memset(receivedData.TRX,0,sizeof(receivedData.TRX));
				memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));
				memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
				memset(receivedData.CardNo,0,sizeof(receivedData.CardNo));
				memset(receivedData.RefillAmount,0,sizeof(receivedData.RefillAmount));
				memset(receivedData.SalesCardRef,0,sizeof(receivedData.SalesCardRef));
				memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
				memset(receivedData.BalanceAmount,0,sizeof(receivedData.BalanceAmount));
				
				js_tok(receivedData.TRX,"trx",0);
				js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
				js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
				js_tok(receivedData.CardNo,"agentCardNo",0);
				js_tok(receivedData.RefillAmount,"refillAmount",0);
				js_tok(receivedData.SalesCardRef,"salesPersonCardRef",0);
				js_tok(receivedData.CardRef,"agentCardRef",0);
				js_tok(receivedData.BalanceAmount,"agentBalanceAmount",0);
			
				PrintAgentRefill();

			}
			else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
				js_tok(receivedData.ErrorMessage,"message",0);

				DisplayErrorMessage("AGENT REFILL",receivedData.ErrorMessage,false);
			}
		}
		else
		{
			DisplayErrorMessage("","",true);
		}

	SalesMenu();
}
void AgentWithdrawV2(void)
{
 	u8 tsk[64] = {0};
	u8 out[128] = {0};
	u8 hashData[128] = {0};
	u8 message[128] ={0};
	u8 buf1[128] = {0};
	u8 buf[16] = {0};
	u8 tmp[32] = {0};
	s32 len, res, key;
	u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	u8 asPathPin[] = "/mtd0/res/enterpincode.bmp";
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240, 320, "/mtd0/res/salewithdraw.bmp");
	sdkDispBrushScreen();

    key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_3 | SDK_KEY_MASK_4 | SDK_KEY_MASK_5 | SDK_KEY_MASK_ESC , KbWaitTime);
    
	memset(sendData.asWithdrawAmount,0,sizeof(sendData.asWithdrawAmount));			
    
	switch (key)
    {
         case SDK_KEY_1:
            {
                sprintf(sendData.asWithdrawAmount,"50000");
            }
            break;

         case SDK_KEY_2:
            {
				sprintf(sendData.asWithdrawAmount,"100000");
            }
            break;

         case SDK_KEY_3:
            {
				sprintf(sendData.asWithdrawAmount,"200000");
            }
            break;

         case SDK_KEY_4:
            {
                sprintf(sendData.asWithdrawAmount,"300000");
            }
            break;

		 case SDK_KEY_5:
			{
			 	sdkDispClearScreen();
				sdkDispFillRowRam(SDK_DISP_LINE1, 0, "AGENT WITHDRAW", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "AMOUNT", SDK_DISP_DEFAULT);
				sdkDispBrushScreen();
				
				res = GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,4,8, NULL, " KS");
	
				if (res == SDK_KEY_ENTER)
				{
					Trace("xgd", "buf= %s\r\n", buf);
					memset(sendData.asWithdrawAmount,0,sizeof(sendData.asWithdrawAmount));
					memcpy(sendData.asWithdrawAmount,&buf[1],buf[0]);
					
				}
				else
				{
					AgentWithdrawV2();
				}
			}
			break;
		 case SDK_KEY_ESC:
		 	{
				SalesMenu();
		 	}
		 	break;
         default:
            {
				SalesMenu();
            }
            break;
    }

	Trace("xgd", "sendData.asWithdrawAmount= %s\r\n", sendData.asWithdrawAmount);
	
	//res = GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,4,8, NULL, " KS");
	
	//if (res == SDK_KEY_ENTER)
	//{
	//	Trace("xgd", "buf= %s\r\n", buf);
	//	memset(sendData.asWithdrawAmount,0,sizeof(sendData.asWithdrawAmount));
	//	memcpy(sendData.asWithdrawAmount,&buf[1],buf[0]);
	//	Trace("xgd", "sendData.asWithdrawAmount= %s\r\n", sendData.asWithdrawAmount);

		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE1, 0, "WITHDRAW", SDK_DISP_DEFAULT);
		memset(tmp,0,sizeof(tmp));
		sprintf(tmp,"%s KS", sendData.asWithdrawAmount);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, tmp , SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TAP AGENT CARD" , SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "TO CONTINUE", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		
		sdkIccOpenRfDev();
		 // Query mifare card
		 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
		 {
			// Play a beep on successful query card
			sdkSysBeep(SDK_SYS_BEEP_OK);
		 }
		 else
		 {
			sdkPEDCancel();
			SalesMenu();
		 }
		 // Verify original key
		 memset(buf, 0, sizeof(buf));
		 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
		 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
		 
		 if (res != SDK_OK || buf[0] != 0)
		 {
			sdkSysBeep(SDK_SYS_BEEP_ERR);
			sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
			sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
			sdkPEDCancel();
			SalesMenu();
		 }
		
		// Read card no from mifare card
		memset(buf, 0, sizeof(buf));
		res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);
		if (res != SDK_OK || buf[0] != 0)
		{
			sdkSysBeep(SDK_SYS_BEEP_ERR);
			sdkDispClearScreen();
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
			sdkDispBrushScreen();
			sdkPEDCancel();
			SalesMenu();
		}
		
		memset(buf1, 0, sizeof(buf1));
		sdkBcdToAsc(buf1, &buf[1], datalen);
		memset(sendData.asAgentCardNo, 0, sizeof(sendData.asAgentCardNo));
		memcpy(sendData.asAgentCardNo, &buf1[0], 9);
		Trace("xgd", "Agent Card No = %s\r\n", sendData.asAgentCardNo);
		sdkIccCloseRfDev(); 
		
		sdkDispClearScreen();
		sdkDispShowBmp(0, 0, 240, 320, asPathPin);
		sdkDispBrushScreen();
		memset(sendData.asAgentEnterPin, 0, sizeof(sendData.asAgentEnterPin));
		
		key = sdkKbGetScanf(60000, buf, 6, 6, SDK_MMI_PWD ,SDK_DISP_LINE4);
		
		if (key == SDK_KEY_ENTER)
	  	{
	  		//sdkAscToBcd(readPINBCD,&buf[1],buf[0]);
			memset(sendData.asAgentEnterPin,0,sizeof(sendData.asAgentEnterPin));
			memcpy(sendData.asAgentEnterPin,&buf[1],6);
			
	  	}
		else
		{
			SalesMenu();
		}
		
		sdkDispClearScreen();	 
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();

		memset(tsk, 0, sizeof(tsk));
		memcpy(tsk,TSK,strlen(TSK));

		sprintf(message,"%s%s0%s%s%s",sendData.asAgentCardNo,sendData.asSalesCardNo,sendData.asAgentEnterPin,sendData.asTerminalSN,sendData.asWithdrawAmount);

		sdkCalcHmacSha256(tsk, strlen(tsk), message, strlen(message), out);

		memset(hashData, 0, 128);
		sdkBcdToAsc(hashData, out, 32);
		memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
		sprintf(sendData.asSendBuf, "%s%s/agentWithdraw/%s/%s/%s/0/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asSalesCardNo,sendData.asAgentEnterPin,sendData.asTerminalSN,sendData.asWithdrawAmount,hashData);
		Trace("xgd","sendata=%s\r\n",sendData.asSendBuf);
		
		res = SendReceivedData(sendData.asSendBuf,false);
		
		if(res!=0)
		{
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				memset(receivedData.TRX,0,sizeof(receivedData.TRX));
				memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));
				memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
				memset(receivedData.CardNo,0,sizeof(receivedData.CardNo));
				memset(receivedData.WithdrawAmount,0,sizeof(receivedData.WithdrawAmount));
				memset(receivedData.SalesCardRef,0,sizeof(receivedData.SalesCardRef));
				memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
				
				js_tok(receivedData.TRX,"trx",0);
				js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
				js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
				js_tok(receivedData.CardNo,"agentCardNo",0);
				js_tok(receivedData.WithdrawAmount,"withdrawAmount",0);
				js_tok(receivedData.SalesCardRef,"salesPersonCardRef",0);
				js_tok(receivedData.CardRef,"agentCardRef",0);
				js_tok(receivedData.BalanceAmount,"remainingBalance",0);
			
				PrintAgentWithdraw();

			}
			else
			{
				sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
				memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
				js_tok(receivedData.ErrorMessage,"message",0);

				DisplayErrorMessage("AGENT WITHDRAW",receivedData.ErrorMessage,false);
			}
		}
		else
		{
			DisplayErrorMessage("","",true);
		}
	//}
	//else if(res==SDK_KEY_ESC)
	//{
	//	SalesMenu();
	//}
	
	SalesMenu();
}

void AgentActivationV2(void)
{
	u8 temp[1024];
	u8 temp2[128];
	s32 key = 0;
	s32 ret = 0;
	
    fingerprint.Error=SALE;
    
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, asPathSelectFinger2);
	sdkDispBrushScreen();
	u8 flag = 0;
	u32 Temp_timer;
//	Temp_timer = sdkTimerGetId();
//	Trace("xgd", " Agent Activation V2 Temp_Timer= %s\r\n",Temp_timer);
	
	sendData.asRefillAmount[0] = '0';

	Temp_timer = sdkTimerGetId();
	
    key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);

	if (key == SDK_KEY_1)
	{
		strcpy(sendData.asFingerImage, "lindex");
	}
	else if (key == SDK_KEY_2)
	{
		strcpy(sendData.asFingerImage, "rthumb");
	}
	else
	{
		SalesMenu();
	}

	Trace("xgd","sendData.asFingerimage = %s\r\n",sendData.asFingerImage);
	
	ret = EnrollFPC(2, true, temp);

	s32 len2;
	u8 *pBufLen = &temp[8];
	len2 = ((pBufLen[0] << 24) 
		  | (pBufLen[1] << 16)
		  | (pBufLen[2] << 8)
		  | pBufLen[3]);
	
	u8 buf2[1024] = {0};
	sdkBcdToAsc(buf2, temp, len2);
	TraceHex("xgd", "Fingerprint Template", temp, len2);
	
	Trace("xgd","buf2 = %s\r\n",buf2);

	if (ret != SDK_OK)
	{
		SalesMenu();
	}
	memset(sendData.asFingerTemplate,0,sizeof(sendData.asFingerTemplate));

	memcpy(sendData.asFingerTemplate,buf2,len2*2); //cody for pst data
	
	//memcpy(sendData.asFingerTemplate,buf2,512);
	ret = VerifyFPC(temp, SafeLEVEL_3);
	if (ret != SDK_OK)									  //cancel key to exit
	{
		SalesMenu();
	}
	
	memset(sendData.asAgentEnterPin, 0, sizeof(sendData.asAgentEnterPin));
	memset(temp2, 0, sizeof(temp2));

	sdkDispClearScreen();
	sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/sxipin.bmp");
	sdkDispBrushScreen();				  

	key = sdkKbGetScanf(30000, temp2, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE5);
	
	if (key != SDK_KEY_ENTER)
	{
		SalesMenu();
	}

	memcpy(sendData.asAgentEnterPin, &temp2[1], 6);
	Trace("xgd","SendData.asAgentEnterPin-1 = %s\r\n",sendData.asAgentEnterPin);
	memset(temp2, 0, sizeof(temp2));
	while (1)
	{
		sdkDispClearScreen();
		sdkDispShowBmp(30, 155, 200, 100,"/mtd0/res/rentersixpin.bmp");
		sdkDispBrushScreen();

		key = sdkKbGetScanf(30000, temp2, 6, 6, SDK_MMI_PWD, SDK_DISP_LINE5);
		if (key == SDK_KEY_ESC || key == SDK_TIME_OUT)
		{
			SalesMenu();
		}
		
		ret = memcmp(sendData.asAgentEnterPin, &temp2[1], sizeof(sendData.asAgentEnterPin));
		Trace("xgd","SendData.asAgentEnterPin-2 = %s\r\n",sendData.asAgentEnterPin);
		Salesreadagentcard();
			if (SDK_EQU != ret)
			{
				sdkDispClearRow(SDK_DISP_LINE3);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PIN NOT MATCH", SDK_DISP_NOFDISP | SDK_DISP_CDISP | SDK_DISP_INCOL);
				sdkDispBrushScreen();
				sdkmSleep(1000);
				flag++;
	
				if (flag == 3)
				{
					SalesMenu();
				}
			}
			else
			{
				break;
			}
		Salesreadagentcard();
	}
}
void Salesreadagentcard(void)
{
	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	//u8 buf5[256] = {0};
	
//	u8 out[128] = {0};
//	u8 message[128] ={0};
//	
//	s32 key = 0;
	//s32 ret;
	
	step1:
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/trueagent.bmp");
	sdkDispBrushScreen();
	//sdkmSleep(1500); 	

   	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	
	  	
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asAgentCardNo,0,sizeof(sendData.asAgentCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("xgd", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	
	if(strcmp(sendData.asFirstDigit,"0")==0||strcmp(sendData.asFirstDigit,"2")==0)
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "AGENT CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		goto step1;
	}
	if(strcmp(sendData.asFirstDigit,"1")==0)
	{
		memcpy(sendData.asAgentCardNo,&buf1[0],9);
		
	}	
	Trace("xgd","Customer Card No=%s\r\n",sendData.asAgentCardNo);
	if (forsale.servicetype==Salesagentdeposit)
	{
		Salesagentdepositmenu();
	}
	else if(forsale.servicetype==agentContractEndRequest|| forsale.servicetype==reactivateAgentRequest||forsale.servicetype==terminalChangeRequest||forsale.servicetype==terminalSwitchRequest||forsale.servicetype==posHandoverRequest)
	{
		SaleRequest();
	}
	
}
void Salesagentdepositmenu(void)
{
	s32 len, len1, i,key,ret;
	u8 message[128] ={0};
	u8 dispbuf[32+1] = {0};	
	u8 out[128] = {0};
	sdkDispClearScreen();
	sdkDispFillRowRam(SDK_DISP_LINE1,0,"AGENT DEPOSIT",SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE2,0,"1 :      0 Ks",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3,0,"2 :  75000 Ks",SDK_DISP_LEFT_DEFAULT);
	sdkDispBrushScreen();

	key = sdkKbWaitKey(SDK_KEY_MASK_1|SDK_KEY_MASK_2,30000);
	
	switch(key)
	{
		case SDK_KEY_1:
			sendData.asPOSDeposite=0;
			break;
		case SDK_KEY_2:
		    sendData.asPOSDeposite= 75000;
			break;
		default:
			SalesMenu();
			break;					
	}
	
	Trace("xgd","sendData.asPOSDeposite = %d\r\n",sendData.asPOSDeposite);
	Trace("xgd","sendData.asFingerTemplate = %s\r\n",sendData.asFingerTemplate);
	Trace("xgd","sendData.asAgentEnterPin = %s\r\n",sendData.asAgentEnterPin);

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

 	sprintf(message,"%s%s%s%s%s%d0",sendData.asSalesCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asFingerTemplate,sendData.asAgentEnterPin,sendData.asPOSDeposite);

	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("xgd","tmp = %s\r\n",sendData.asHashData);

	//en_message(sendData.asAgentEnterPin);

	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/agentActivation/%s/%s/%s/%s/%s/%s/%d/0/%s",uri,IPAddress,SvcVersion,sendData.asSalesCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asFingerTemplate,sendData.asAgentEnterPin,sendData.asPOSDeposite,sendData.asHashData);

	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);

	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);

		Trace("xgd","receivedData.ServiceStatus = %s\r\n",receivedData.ServiceStatus);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
			js_tok(receivedData.Amount,"depositAmount",0);
			
			js_tok(receivedData.AgentCardNo,"agentCardNo",0);
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
					
			js_tok(receivedData.TRX,"trx",0);
					
			/*sdkDispClearScreen();    
	        sdkDispFillRowRam(SDK_DISP_LINE1, 0, "AGENT ACTIVATION", SDK_DISP_DEFAULT);		
	        sdkDispFillRowRam(SDK_DISP_LINE2, 0, "SUCCESS.", SDK_DISP_DEFAULT);		
			sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PRINTING...", SDK_DISP_DEFAULT);		
			sdkDispBrushScreen();*/
			PrintAgentActivation();
			sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
		}
		else
		{	
			sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
		
			js_tok(receivedData.ErrorMessage,"message",0);
			len = 128;
			sdkDispClearScreen();
		    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "ACTIVATION FAIL", SDK_DISP_DEFAULT);
		    for (i = 0; i < 4 && len > 0; i++)
		    {
		        len1 = len > 20 ? 20 : len;
		        len -= len1;
		        memcpy(dispbuf, &receivedData.ErrorMessage[20*i], len1);
		        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
		    }
		    sdkDispBrushScreen();
	        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		}
	
	}
	else
	{
		sdkDispClearScreen();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE4, 0, "PLEASE TRY AGAIN", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
	}

	SalesMenu();
	
}
void SaleRequest(void)
{
	u8 out[128] = {0};
	//u8 buf[128] = {0};
	u8 message[128] ={0};
	s32 ret;
		
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	 if(forsale.servicetype==agentContractEndRequest|| forsale.servicetype==reactivateAgentRequest||forsale.servicetype==terminalChangeRequest||forsale.servicetype==terminalSwitchRequest)
	 {
	 	memset(message,0,sizeof(message));
			memset(out,0,sizeof(out));
			sprintf(message,"%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSalesCardNo);
			Trace("agentcontrctendrequest","message=%s\r\n",message);
			sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
			memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
			sdkBcdToAsc(sendData.asSignature, out, 32);
			Trace("agentcontractendrequest","signature = %s\r\n",sendData.asSignature);
			
			memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
			sprintf(sendData.asSendBuf,"--data &version=%s&agentCardNo=%s&terminalRef=%s&salesPersonCardNo=%s&signature=%s",SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asSalesCardNo,sendData.asSignature);
			Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	 }
	 else if(forsale.servicetype==posHandoverRequest)
	 {
	 		memset(message,0,sizeof(message));
			memset(out,0,sizeof(out));
			sprintf(message,"%s%s",sendData.asRequestSaleNum,sendData.asTerminalSN);
			Trace("agentcontrctendrequest","message=%s\r\n",message);
			sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
		
			memset(sendData.asSignature, 0,sizeof(sendData.asSignature) );
			sdkBcdToAsc(sendData.asSignature, out, 32);
			Trace("agentcontractendrequest","signature = %s\r\n",sendData.asSignature);
			
			memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
			sprintf(sendData.asSendBuf,"--data &version=%s&salesPersonCardNo=%s&terminalRef=%s&signature=%s",SvcVersion,sendData.asRequestSaleNum,sendData.asTerminalSN,sendData.asSignature);
			Trace("modulepaycash","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	 }
	
	
	if(forsale.servicetype==agentContractEndRequest)
	{
		memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
		sprintf(sendData.asSendURL, "%s%s/agentContractEndRequest/",uri,IPAddress);
		Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
	}
	else if(forsale.servicetype==terminalChangeRequest)
	{
		memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
		sprintf(sendData.asSendURL, "%s%s/terminalChangeRequest/",uri,IPAddress);
		Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
	}
	else if (forsale.servicetype==terminalSwitchRequest)
	{
		memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
		sprintf(sendData.asSendURL, "%s%s/terminalSwitchRequest/",uri,IPAddress);
		Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
	}
	else if(forsale.servicetype==reactivateAgentRequest)
	{
		memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
		sprintf(sendData.asSendURL, "%s%s/reactivateAgentRequest/",uri,IPAddress);
		Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
	}
	else if(forsale.servicetype==posHandoverRequest)
	{
		memset(sendData.asSendURL,0,sizeof(sendData.asSendURL));
		sprintf(sendData.asSendURL, "%s%s/posHandoverRequest/",uri,IPAddress);
		Trace("modulepaycash","sendData.asSendURL=%s\r\n",sendData.asSendURL);
	}
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData_POST(sendData.asSendURL,sendData.asSendBuf,false);
	if(ret==SDK_OK)
	{
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			
			transaction=true;              //  for pageagentcheckbalance
			secondtransactionstart=true;   //  for pageagentcheckbalance
			
	
		
			js_tok(receivedData.CardRef,"agentCardRef",0);

			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);

			
			js_tok(receivedData.TRX,"trx",0);
			
			js_tok (receivedData.AgentCardNo,"agentCardNo",0);
			
		
			
			js_tok(receivedData.SaleCardRef,"salesPersonCardNo",0);
			PrintSaleRequest();
		}
 			
		else
		{
			//PrintAgentRequest();
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			//transaction=false;     //for pageagentcheckbalance
			memset(receivedData.ErrorMessage,0,sizeof(receivedData.ErrorMessage));
			js_tok(receivedData.ErrorMessage,"message",0);
		
			DisplayErrorMessage("SALE REQUEST",receivedData.ErrorMessage,false);
			
			
		}
					
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	
	SalesMenu();	
}
s32 CheckSALEPINModule()
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	s32 ret;
	u8 dispbuf[32+1] = {0};
	s32 len, len1, i;

	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	
	memset(out,0,sizeof(out));
	sdkMD5(out,sendData.asSalesEnterPin,6);	
	memset(sendData.asHashEnterPin,0,sizeof(sendData.asHashEnterPin));
	sdkBcdToAsc(sendData.asHashEnterPin,out,16);
	Trace("modulepaycash","sendData.asHashEnterPin = %s\r\n",sendData.asHashEnterPin);
	memset(sendData.asPostSaleEnterPin,0,sizeof(sendData.asPostSaleEnterPin));
	strcpy(sendData.asPostSaleEnterPin,sendData.asHashEnterPin);
	Trace("modulepaycash ","Post Agent Enter pin  = %s\r\n",sendData.asPostSaleEnterPin);
	
	sprintf(message,"%s%s",sendData.asSalesCardNo,sendData.asPostSaleEnterPin);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);

	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);

	Trace("CheckSalePINModule","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf, "%s%s/salePersonLogin/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asSalesCardNo,sendData.asPostSaleEnterPin,sendData.asHashData);
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			ret = SDK_OK;
		}
		else
		{
			
			js_tok(receivedData.ErrorMessage,"message",0);
			js_tok(receivedData.ErrorCode,"code",0);
			if(strcmp(receivedData.ErrorCode,"Err00142")==0)
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/wrongpin.bmp");
				sdkDispBrushScreen();	
			}
			else
			{
				len = 128;
				sdkDispClearScreen();
			    sdkDispFillRowRam(SDK_DISP_LINE1, 0, "LOGIN FAIL", SDK_DISP_DEFAULT);
			    for (i = 0; i < 4 && len > 0; i++)
			    {
			        len1 = len > 20 ? 20 : len;
			        len -= len1;
			        memcpy(dispbuf, &receivedData.ErrorMessage[20*i], len1);
			        sdkDispFillRowRam(SDK_DISP_LINE2+i, 0, dispbuf, SDK_DISP_LEFT_DEFAULT);
			    }
				sdkDispBrushScreen();
		        	
			}
			sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
			ret =0;
		    
		}
	}
	else
	{
		sdkDispClearScreen();    
        sdkDispFillRowRam(SDK_DISP_LINE3, 0, "ERROR OCCURRED!", SDK_DISP_DEFAULT);
        sdkDispBrushScreen();
        sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
		ret = 0;
	}
	return ret;
}


void ReadSaleCard(void)
{
	s32 len, res;
 	u8 buf[128] = {0}, buf1[128] = {0};
    u8 databuf[] = "\xDA\xDA\xDA\xDA\xDA\xDA\xDA\xDA";
    s32 datalen = sizeof(databuf) - 1;
	//u8 buf5[256] = {0};
	
//	u8 out[128] = {0};
//	u8 message[128] ={0};
//	
//	s32 key = 0;
	//s32 ret;
	
	step1:
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/requestsalecardno.bmp");
	sdkDispBrushScreen();
	//sdkmSleep(1500); 	

   	//open RF device
 	sdkIccOpenRfDev();

	 // Query mifare card
	 if ((res = sdkIccRFQuery(SDK_ICC_RFCARD_A, buf, 60000)) >= 0)
	 {
	  	// Play a beep on successful query card
	  	sdkSysBeep(SDK_SYS_BEEP_OK);
	  	
	  	
	 }
	 

	 // Verify original key
	 memset(buf, 0, sizeof(buf));
	 res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\xFF\xFF\xFF\xFF\xFF\xFF", 6, buf, &len); //for normal card
	 //res = sdkIccMifareVerifyKey(CTRL_BLOCK, SDK_RF_KEYA, "\x62\x61\x6B\x77\x61\x6E", 6, buf, &len); //for protect card
	 
	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispSetFontSize(SDK_DISP_FONT_LARGE);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "TRUE MONEY CARD", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);
		sdkPEDCancel();
		AppMain();
	 }
	
	// Read card no from mifare card
    memset(buf, 0, sizeof(buf));
    res = sdkIccMifareReadBlock(CARDNO_BLOCK, buf, &len);

	 if (res != SDK_OK || buf[0] != 0)
	 {
		sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
	 }

	// Convert data from BCD to ASCII
	
    memset(buf1, 0, sizeof(buf1));
    sdkBcdToAsc(buf1, &buf[1], datalen);
	
	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
	memset(sendData.asAgentCardNo,0,sizeof(sendData.asAgentCardNo));
	memcpy(sendData.asFirstDigit, &buf1[0], 1);
	Trace("xgd", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	
	
	if(strcmp(sendData.asFirstDigit,"1")==0||strcmp(sendData.asFirstDigit,"2")==0)
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "SALE CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		goto step1;
	}
	if(strcmp(sendData.asFirstDigit,"0")==0)
	{
		memset(sendData.asRequestSaleNum,0,sizeof(sendData.asRequestSaleNum));
		memcpy(sendData.asRequestSaleNum,&buf1[0],9);
		
	}	
	Trace("xgd","Customer Card No=%s\r\n",sendData.asRequestSaleNum);
	SaleRequest();
	
	
}
