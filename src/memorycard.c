#include "../inc/global.h"

#define CARD_ADDR    10


SDK_ICC_PARAM st_param;
 SDK_ICC_CARDTYPE eCardType;
 
   
  
  
 void MemToMemCashInConfirm(void);
 void ReadMemoryCard(void) 
{
	
	SDK_ICC_RMEMIN st_rmemin;//* parameters for reading memory card */ 
    s32 timid, len, res;
    
    u8 buf[128] = {0};
    u8 strbuf[128] = {0};
    
   
    u8 wbuf[128]={0};
    s32 wlen = sizeof(wbuf) - 1;
    int pass = 5;
    //int readpass=4.5;
    
   
   
    
   

    // Configure ICC parameters
    memset(&st_param, 0, sizeof(st_param));
    st_param.ucCardMode = SDK_ICC_ICC;
    st_param.eCardType = SDK_ICC_SLE4442;
    st_param.ucSlotNo = 3;

    // Wait for user to insert card
    timid = sdkTimerGetId();
    sdkIccOpenIcDev();

    while (sdkIccGetCardStatus(&st_param, 50) != SDK_OK || st_param.ucCardMode != SDK_ICC_ICC)
    {
        if (sdkTimerIsEnd(timid, 30000))
        {
            return;
        }
    }


    // Configure ICC parameters
    memset(&st_param, 0, sizeof(st_param));
    st_param.ucCardMode = SDK_ICC_ICC;
    st_param.eCardType = SDK_ICC_SLE4442; 
    st_param.ucSlotNo = 3;

    // Configure struct SDK_ICC_RMEMIN
    memset(&st_rmemin, 0, sizeof(st_rmemin));
    st_rmemin.ucAddress = CARD_ADDR;
    st_rmemin.ucReadLen =wlen;

    // Read original card data before write
    memset(buf, 0, sizeof(buf));
    len = 0;
    res = sdkIccMemReadData(&st_param, &st_rmemin, buf, &len);
    Trace("memorycardread","read data=%d\r\n",&st_rmemin);
    Trace("memorycardread","read data befor write=%d\r\n",buf);
    //TraceHex("memorycardread","readdata",&st_rmemin,4);
	Trace("memorycardread","res=%d\r\n,res",res);
    // Check read access result
    
    if (res != SDK_OK || buf[0] != 0)
    {
        sdkSysBeep(SDK_SYS_BEEP_ERR);
		sdkDispClearScreen();
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "READ CARD ERROR!", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		sdkPEDCancel();
		AppMain();
    }
    
		TraceHex("cody", "sdkIccMemReadData", &buf[1], len);
        memset(strbuf, 0, sizeof(strbuf));
        sdkBcdToAsc(strbuf, &buf[1], pass);
        Trace("read","read display data befor write=%s\r\n",strbuf);
		
      	memcpy(sendData.asMemberCardNo, &strbuf[0],9);
    	memset(sendData.asMemberCardNo,0,sizeof(sendData.asMemberCardNo));
    	memset(sendData.asFirstDigit, 0, sizeof(sendData.asFirstDigit));
    	memcpy(sendData.asFirstDigit, &strbuf[0], 1);
	Trace("xgd", "Card Start Digit = %s\r\n", sendData.asFirstDigit);
	if(strcmp(sendData.asFirstDigit,"2")==0)
	{
		memcpy(sendData.asCustomerCardNo,&strbuf[0],9);
		
		Trace("xgd","Customer Card No=%s\r\n",sendData.asCustomerCardNo);
		
		if(printtype.CashPrint==CASHIN)
		{
			MemToMemCashInConfirm();
		}
	}
		else 
	{	
		sdkDispClearScreen();
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE TAP", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "MEMBER CARD.", SDK_DISP_DEFAULT);
		sdkDispSetFontSize(SDK_DISP_FONT_BIG);
		sdkDispBrushScreen();
		sdkmSleep(3000);
		//goto step1;
	}	
 } 
   
