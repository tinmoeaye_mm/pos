/******************************************************************************

   Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
   File Name     : CashOut.c
   Version       : Initial Draft
   Author        : Thu Kha Aung
   Created       : 2016/3/17
   Last Modified :
   Description   : CashOut module-related functions are defined
                  here
   History       :
   1.Date        : 2016/3/17
    Author      :  Tin Moe Aye
    Modification: Created file

******************************************************************************/
#include "../inc/global.h"
const u8 asPathCashOut[] = "/mtd0/res/cashout.bmp";

void IntNrcNum(void);
void  IntCashOut(void);
void IntCashOutPh(void);
void DomesticCashOut(void);
void ReceiverPhNum(void);
void SendDomesticCashOut(void);
void DomesticCashOutShow(void);
void SendIntCashOut(void);
void ModuleTransferCustomerReadCard(void);
void CashOutMemToMem(void);
void DomesticMemToMemCashOut(void);
void IntCashOutConfirm(void);
void SendIntCashOutConfirm(void);
void SendIntNotifyCashOut(void);
void CashOutMemToMemConfirm(void);
void SecondSendDomesticCashOut(void);
void CashOutModuleForNewMenu(void);
void NewMainMenu(void);
void ForSendData(void);
char copypasskey [10] ;
char passkey[10];
char NrcNumCopy [6] ;

void CashOutModule(void)  // CashOutMainMenu Start
{
    s32 key = 0;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, asPathCashOut);
	sdkDispBrushScreen();

	secondtransactionstart=false;     // for pageagentcheckbalance
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);
    			
    switch (key)
    {
         case SDK_KEY_1:
            {
             	
				 cashout.CashOutMAINMenu=CashOut_Old_Menu; // for old menu
             	DomesticMemToMemCashOut();
            }
            break;

         case SDK_KEY_2:
            {
            	cashout.CashOutMAINMenu=CashOut_Old_Menu;  //for old menu
                IntCashOut();
            }
            break;
         case SDK_KEY_ESC:
		 	{
		 		MainMenu();
			}   
         default:
            {
            	
            }
            
    }
	return NewMainMenu();
}
void CashOutModuleForNewMenu(void)  // for shortcut menu for cashoutmodule 
{
    s32 key = 0;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, asPathCashOut);
	sdkDispBrushScreen();
	secondtransactionstart=false;
	key = sdkKbWaitKey(SDK_KEY_MASK_1 | SDK_KEY_MASK_2 | SDK_KEY_MASK_ESC, 60000);
    			
    switch (key)
    {
         case SDK_KEY_1:
            {
             	
             	DomesticMemToMemCashOut();
            }
            break;

         case SDK_KEY_2:
            {
            
                IntCashOut();
            }
            break;
         case SDK_KEY_ESC:
		 	{
		 		
		 		NewMainMenu();
			}   
         default:
            {
           
            	
            }
            
    }

	NewMainMenu();
}



void DomesticCashOut(void)  // Domestic CashOut
{
	s32 key = 0;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/dcashoutpin.bmp");
	sdkDispBrushScreen();
	
	memset(passkey,0,sizeof(passkey));
	key = sdkKbGetScanf(30000,passkey,8,8,SDK_MMI_PWD,SDK_DISP_LINE3);
	Trace("domesticcashout","Intpasskey=%s\r\n",passkey);
	memset(copypasskey,0,sizeof(copypasskey));
	memcpy(copypasskey,&passkey[1],10);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				ReceiverPhNum();
			
			}
			break;
		case SDK_KEY_ESC:
			{
			
			
				if(cashout.CashOutMAINMenu==CashOut_New_Menu)  //for shortcutmenu & old menu
				{
					NewMainMenu();
				}
				else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
						CashOutModule();
				}		
			}
			break;
			default:
			{
				
			}
		
	}
      if(cashout.CashOutMAINMenu==CashOut_New_Menu)  //for shortcutmenu & old menu
				{
					NewMainMenu();
				}
				else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
						CashOutModule();
				}		
	
}
void DomesticMemToMemCashOut(void) // Domestic member To member CashOut
{
	s32 res;
	u8 buf[16] = {0};
	step1:
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320,"/mtd0/res/DosmecticCashOutMenu.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res= GetAmountNumber(30*1000, buf, SDK_DISP_LINE3,4,8, NULL, " KS");
	
	Trace("domesticmentomencashout","length=%d\r\n",strlen(buf));

		
			if(res==SDK_KEY_ENTER&&strlen(buf)>=5 ) 
			{
				
				printtype.CashPrint=CASHOUT; // for print slip
				Trace("domesticmemtomemcashout", "buf= %s\r\n",buf);
				memset(sendData.asTransferAmount,0,sizeof(sendData.asTransferAmount));
				memcpy(sendData.asTransferAmount,&buf[1],buf[0]);
				Trace("domesticmemtomemcashout", "sendData.asCashOutAmount= %s\r\n",sendData.asTransferAmount);
				ModuleTransferCustomerReadCard();
			}
			else if(res==15 && strlen(buf)>=5)
			{
				
				Trace("domesticmemtomemcashout","15");
				Trace("domesticmemtomemcashout", "buf= %s\r\n", buf);
				memset(sendData.asTransferAmount,0,sizeof(sendData.asTransferAmount));
				memcpy(sendData.asTransferAmount,&buf[1],buf[0]);
				Trace("domesticmemtomemcashout", "sendData.asTransferAmount= %s\r\n",sendData.asTransferAmount);
				DomesticCashOut();
				
				
			}
			else if(res==SDK_KEY_F1&&strlen(buf)>=5)
			{
					Trace("domesticmemtomemcashout","F1");
					Trace("domesticmemtomemcashout", "buf= %s\r\n", buf);
					memset(sendData.asTransferAmount,0,sizeof(sendData.asTransferAmount));
					memcpy(sendData.asTransferAmount,&buf[1],buf[0]);
					Trace("domesticmemtomemcashout", "sendData.asTransferAmount= %s\r\n",sendData.asTransferAmount);
					DomesticCashOut();
			}
			else if(res==SDK_KEY_ESC)
			{
					//MainMenu();
				if(cashout.CashOutMAINMenu==CashOut_New_Menu)
				{
					NewMainMenu();
				}
				else if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
						MainMenu();
				}	
			}
			
			else if(res==SDK_KEY_F1&&strlen(buf)<5)
			{
				Trace("domesticmemtomemcashout","3 digit F1 amount");
					sdkDispClearScreen();
				sdkDispSetFontSize(SDK_DISP_FONT_BIG);
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE ENTER", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CASH OUT AMOUNT.", SDK_DISP_DEFAULT);
				sdkDispSetFontSize(SDK_DISP_FONT_BIG);
				sdkDispBrushScreen();
				sdkmSleep(3000);
				goto step1;
			}
				else if(res==15&&strlen(buf)<5)
			{
				Trace("domesticmemtomemcashout","3 digit F1 amount");
					sdkDispClearScreen();
				sdkDispSetFontSize(SDK_DISP_FONT_BIG);
				sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PLEASE ENTER", SDK_DISP_DEFAULT);
				sdkDispFillRowRam(SDK_DISP_LINE3, 0, "CASH OUT AMOUNT.", SDK_DISP_DEFAULT);
				sdkDispSetFontSize(SDK_DISP_FONT_BIG);
				sdkDispBrushScreen();
				sdkmSleep(3000);
				goto step1;
			}
			
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)
		{
				NewMainMenu();
		}
	else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
		{
						MainMenu();
		}	
	
	
}
void CashOutMemToMemConfirm(void)
{
	s32 key;
	
	u8 buf[128]={0};
	
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	sdkDispClearScreen();
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "MEMBER CASHOUT");
	sdkDispAt(60, 20, buf);

	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","MEMBER CARDNo:");
	sdkDispAt(12,60,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",sendData.asCustomerCardNo);
	sdkDispAt(12,100,buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s","AMOUNT:");
	sdkDispAt(12,140,buf);
	memset(buf,0,sizeof(buf));
	sprintf(buf,"KS %s",sendData.asTransferAmount);
	sdkDispAt(12,180,buf);
	

	
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);

	sdkDispBrushScreen();
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,60000);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				CashOutMemToMem();
			}
			break;
		case SDK_KEY_ESC:
			{
				DomesticMemToMemCashOut();
			}
			break;
		default:
			{
				
			}		
	}
	DomesticMemToMemCashOut();
		
}
void CashOutMemToMem(void)
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	
	s32 ret;
	
	ForSendData();
	Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sprintf(message,"%s%s%s%d%s%s%s",sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,0,sendData.asCustomerEnterPin,sendData.asTransferAmount,sendData.asEDCserial);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/cusCashOut/%s/%s/%s/%s/%d/%s/%s/%s/%s",uri,IPAddress,SvcVersion,sendData.asCustomerCardNo,sendData.asAgentCardNo,sendData.asTerminalSN,0,sendData.asCustomerEnterPin,sendData.asTransferAmount,sendData.asEDCserial,sendData.asHashData);
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
	
			transaction=true;              //  for pageagentcheckbalance
			secondtransactionstart=true;   //  for pageagentcheckbalance
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
			
			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			js_tok(receivedData.CustomerCardNum,"cusCardNo",0);
			
			js_tok(receivedData.Amount,"cashOutAmount",0);
			
			js_tok(receivedData.MemberRef,"customerCardRef",0);
			
			js_tok(receivedData.commission,"cashOutFee",0);
			
			js_tok(receivedData.TotalAmount,"totalAmount",0);
			
			js_tok(receivedData.BalanceAmount,"cusBalanceAmount",0);
			printMemtoMemCashOut();
			
			
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;              ////  for pageagentcheckbalance
			js_tok(receivedData.ErrorMessage,"errorMessage",0);
			
			js_tok(receivedData.ErrorCode,"code",0);
			
		if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}	
			else
			{
				DisplayErrorMessage("Member CANSHOUT",receivedData.ErrorMessage,false);
			}
			
			
		}
		
	}
	else
	{
		DisplayErrorMessage("","",true);
	}
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)
		{
			NewMainMenu();
		}
		else  if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
		{
			NewMainMenu();
		}			
}

void ReceiverPhNum(void)
{	
	u8 buf[16] = {0};
	s32 res;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/13.bmp");
	sdkDispBrushScreen();
	
	memset(buf,0,sizeof(buf));
	res = GetPhoneNumber(30*1000, buf, SDK_DISP_LINE3,7,13, "09", NULL,"ALL");
	switch(res)
	{
		case SDK_KEY_ENTER: 
		{
			Trace("xgd","buf=%s\r\n",buf);
			memset(sendData.asReceiverPhone,0,sizeof(sendData.asReceiverPhone));
			memcpy(sendData.asReceiverPhone,&buf[1],buf[0]);
			Trace("xgd","sendData.asReceiverPhone=%s\r\n",sendData.asReceiverPhone);
			SendDomesticCashOut();
		}
		break;
		case SDK_KEY_ESC:
		{
			
			if(cashout.CashOutMAINMenu==CashOut_New_Menu)// for shortcut menu & old menu
				{
					NewMainMenu();
				}
				else  if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					CashOutModule();
				}				
		}
		break;
		default:
		{
	
		}
		
	}

	if(cashout.CashOutMAINMenu==CashOut_New_Menu)// for shortcut menu & old menu
				{
					NewMainMenu();
				}
				else  if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					CashOutModule();
				}			
	
}
void SendDomesticCashOut(void) //Domestic CashOut first url 
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	
	s32 ret;
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	sprintf(message,"%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTransferAmount,sendData.asReceiverPhone,copypasskey);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/TcashOutConfirm/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersionV3,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTransferAmount,sendData.asReceiverPhone,copypasskey,sendData.asHashData);
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
	if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			js_tok(receivedData.RePassCodeFee,"regenerateCost",0);
			
			js_tok(receivedData.RegenerateCount,"regenerateCount",0);
			
			DomesticCashOutShow();
			
		}
		else
		{
			
			js_tok(receivedData.ErrorMessage,"message",0);
			js_tok(receivedData.ErrorCode,"code",0);
			if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
			}	
			else
			{
				DisplayErrorMessage("TCASH OUT-FirstURL",receivedData.ErrorMessage,false);	
			}
			
		}
	}
	else
	{	
		DisplayErrorMessage("","",true);
	}
	
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)// for shortcut menu & old menu
				{
					NewMainMenu();
				}
				else  if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					CashOutModule();
				}			
}
void DomesticCashOutShow(void)
{
	char buf [10]={0};
	s32 key=0;
	printtype.CashPrint=CASHOUT;
	CashOutRecall=true;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 150, "/mtd0/res/HLcashout.bmp");
	sdkDispShowBmp(0, 45, 240, 150, "/mtd0/res/cashoutamount.bmp");
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "KS %s", sendData.asTransferAmount);
	sdkDispAt(90, 75, buf);
	
	sdkDispShowBmp(0, 110, 240, 150, "/mtd0/res/receiverPhoneNo.bmp");
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"09%s",sendData.asReceiverPhone);
	sdkDispAt(90, 150,buf);
	
	if(strcmp(receivedData.RegenerateCount,"0") != 0)  //  for pass code fee
	{
			sdkDispShowBmp(0, 180, 240, 100, "/mtd0/res/pcodefee.bmp");
	
			memset(buf,0,sizeof(buf));
			sprintf(buf,"KS %s",receivedData.RePassCodeFee);
			sdkDispAt(90, 210,buf);	
	}

	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ENTER]YES",SDK_DISP_LEFT_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE5,0,"[ESC]NO",SDK_DISP_RIGHT_DEFAULT);
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);

	sdkDispBrushScreen();
	key = sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC ,30000);
	switch (key)
	{
		case SDK_KEY_ENTER:
			{
				SecondSendDomesticCashOut();		
			}
		break;
		case SDK_KEY_ESC:  // for shortcut menu & old menu
			{
				if(cashout.CashOutMAINMenu==CashOut_New_Menu)
				{
					NewMainMenu();
				}
				else  if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					CashOutModule();
				}			
			}
		break;
		default:
			{
				
			}
	}
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)// for shortcut menu & old menu
				{
					NewMainMenu();
				}
				else  if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					MainMenu();
				}			
		
}
void SecondSendDomesticCashOut(void) //Domestic cashout second url
{
	u8 out[128] = {0};
	u8 message[128] ={0};
	
	s32 ret;
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();
	
	ForSendData();  // checktrx
	Trace("xgd","sendData.asEDCserial&time=%\r\n",sendData.asEDCserial);
	sprintf(message,"%s%s%s%s%s%s",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTransferAmount,sendData.asReceiverPhone,copypasskey,sendData.asEDCserial);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/TcashOutReconfirm/%s/%s/%s/%s/%s/%s/%s/%s",uri,IPAddress,SvcVersionV3,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asTransferAmount,sendData.asReceiverPhone,copypasskey,sendData.asEDCserial,sendData.asHashData);
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	
		if(ret!=0)
	{
		
		js_tok(receivedData.ServiceStatus,"serviceStatus",0);
		
		if(strcmp(receivedData.ServiceStatus,"Success")==0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=true;   // for pageagentcheckbalance
			secondtransactionstart=true;     // for pageagentcheckbalance
			
			js_tok(receivedData.CardRef,"agentCardRef",0);

			js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);

			js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
			
			js_tok(receivedData.Amount,"cashOutAmount",0);
			
			js_tok(receivedData.CashOutFee,"cashOutFee",0);
			
			js_tok(receivedData.PhoneNo,"mobileNo",0);
			
			js_tok(receivedData.Passcode,"passcode",0);
			
			js_tok(receivedData.TransferAmount,"transferAmount",0);
			
			js_tok(receivedData.TRX,"trx",0);
			
			js_tok(receivedData.RegenerateCount,"regenerateCount",0);
			
			js_tok(receivedData.RePassCodeFee,"regenerateCost",0);
			
			PrintDomesticCashOut();
			CheckPaperRollStatus();
			if(cashout.CashOutMAINMenu==CashOut_New_Menu)  // for shortcut menu & old meun
				{
					NewMainMenu();
				}
				else  if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					NewMainMenu();
				}		
			
		}
		else
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			transaction=false;  //for pageagentcheck balance
			js_tok(receivedData.ErrorMessage,"message",0);
			
			DisplayErrorMessage("CASH OUT-SECONDURL",receivedData.ErrorMessage,false);
		}
		
	} 
	else
	{
		DisplayErrorMessage("","",true);
	}
	
	if(cashout.CashOutMAINMenu==CashOut_New_Menu) //  for shortcut menu & old meun
				{
					NewMainMenu();
				}
				else  if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
					MainMenu();
				}		
}
void  IntCashOut(void)   // IRS start
{
	s32 key = 0;
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/icashout.bmp");
	sdkDispBrushScreen();
	
	memset(passkey,0,sizeof(passkey));
	key = sdkKbGetScanf(30000,passkey,8,8,SDK_MMI_PWD,SDK_DISP_LINE3);
	Trace("xgd","Intpasskey=%s\r\n",passkey);
	
	memset(copypasskey,0,sizeof(copypasskey));
	memcpy(copypasskey,&passkey[1],8);
	Trace("xgd","copypasskey=%s\r\n",copypasskey);
	switch(key)
	{
		case SDK_KEY_ENTER : 
		{
			
			IntCashOutPh();
		}
		break;
		case SDK_KEY_ESC:            // for shortcut menu & old meun
		{
			
			if(cashout.CashOutMAINMenu==CashOut_New_Menu)
						{
							NewMainMenu();
						}
						else if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
		}
		break;
		default:
		{
				
		}
	
	
	}
	
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)   // for shortcut menu & old meun
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
	
}
void IntCashOutPh(void)
{	
	u8 buf[16] = {0};
	s32 res;
	
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/16.bmp");
	sdkDispBrushScreen();
	memset(buf,0,sizeof(buf));

	res= sdkKbGetScanf(60000,buf,4,25,SDK_MMI_NUMBER,SDK_DISP_LINE3);
	switch(res)
	{
		case SDK_KEY_ENTER:
			{
				Trace("xgd","buf= %s\r\n",buf);
				memset(sendData.asPhoneNum,0,sizeof(sendData.asPhoneNum));
				memcpy(sendData.asPhoneNum,&buf[1],buf[0]);
				Trace("xgd","sendData.asReceiverPhone=%s\r\n",sendData.asPhoneNum);
				IntNrcNum();
			}
			break;
		case SDK_KEY_ESC:                // for shortcut menu & old meun
			{
			
				if(cashout.CashOutMAINMenu==CashOut_New_Menu)
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
			} 
			break;
			default:
			{
				CashOutModule();
			}
			break;	
	}

	if(cashout.CashOutMAINMenu==CashOut_New_Menu)    // for shortcut menu & old meun
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
	
}
void IntNrcNum(void)
{	
	s32 key = 0;
	char NrcNum[64];
	
	sdkDispClearScreen();
	sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/17.bmp");
	sdkDispBrushScreen();
	
	memset(NrcNum,0,sizeof(NrcNum));
	key = sdkKbGetScanf(30000,NrcNum,1,12,SDK_MMI_NUMBER,SDK_DISP_LINE3); 
	
	memset(NrcNumCopy,0,sizeof(NrcNumCopy));
	memcpy(NrcNumCopy,&NrcNum[1],12);
	
	Trace("xgd","NrcNumCopy=%s\r\n",NrcNumCopy);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				SendIntCashOut();
			}
			break;
		case SDK_KEY_ESC:
			{
				IntCashOutPh();
			}
			break;
			default:
			{
				
			}	
			break;
	}

	if(cashout.CashOutMAINMenu==CashOut_New_Menu)   // for shortcut menu & old meun
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
}
void SendIntCashOut(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();

	memset(message,0,sizeof(message));
	sprintf(message,"%s%s%s%s%s%d",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,copypasskey,NrcNumCopy,0);
	
	Trace("xgd","message=%s\r\n",message);
	sdkCalcHmacSha256(TSK,strlen(TSK),message,strlen(message),out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/irsConfirmCashout/%s/%s/%s/%s/%s/%s/%d/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,copypasskey,NrcNumCopy,0,sendData.asHashData);
	
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	memset(&receivedData,0,sizeof(receivedData));
	ret = SendReceivedData(sendData.asSendBuf,false);
	if(ret!=0)
		{
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			{
				
				js_tok(receivedData.Amount,"amount",0);
				
				IntCashOutConfirm();
			}
			
			else
			{
				
				js_tok(receivedData.ErrorMessage,"message",0);
				js_tok(receivedData.ErrorCode,"code",0);
				if((strcmp(receivedData.ErrorCode,"Err00077")==0)||(strcmp(receivedData.ErrorCode,"Err00023")==0)||(strcmp(receivedData.ErrorCode,"Err00086")==0)||(strcmp(receivedData.ErrorCode,"Err00110")==0)||(strcmp(receivedData.ErrorCode,"Err00136")==0)||(strcmp(receivedData.ErrorCode,"Err00167")==0)||(strcmp(receivedData.ErrorCode,"Err00245")==0))
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320,  "/mtd0/res/notenoughbalance.bmp");
				sdkDispBrushScreen();
				sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 5000);	
				}
				else
				{
					DisplayErrorMessage("CASHOUT",receivedData.ErrorMessage,false);
				}
					
				
			}
		}
	else
	{
		DisplayErrorMessage("","",true);
	}
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)   // for shortcut menu & old meun
				{
					NewMainMenu();
				}
				else  if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
				{
						CashOutModule();
				}		
}
void IntCashOutConfirm(void)
{
	s32 key=0;
	u8 buf[128]={0};
	
	sdkDispClearScreen();
	sdkDispShowBmp(0,0,240,320,"/mtd0/res/IRSConfirm.bmp");
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf, "%s", "CASHOUT AMOUNT");
	sdkDispAt(55, 15, buf);
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"%s",receivedData.Amount);
	sdkDispAt(55,40,buf);
	
	sdkDispBrushScreen();
	sdkDispSetFontSize(SDK_DISP_FONT_BIG);
	
	key  = sdkKbWaitKey(SDK_KEY_MASK_ENTER|SDK_KEY_MASK_ESC,120000);
	switch(key)
	{
		case SDK_KEY_ENTER:
			{
				SendIntCashOutConfirm();
			}
			break;
		case SDK_KEY_ESC:    // for shortcut menu & old meun
			{
				if(cashout.CashOutMAINMenu==CashOut_New_Menu)
						{
							NewMainMenu();
						}
						else if(cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}		
			
			}
			break;
		default:
			{
				
			}		
	}
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)   // for shortcut menu & old meun
						{
							NewMainMenu();
						}
	else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
}
void SendIntCashOutConfirm(void)
{
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};
	u8 buf[128]={0};
	
	sdkDispClearScreen();	 
	sdkDispFillRowRam(SDK_DISP_LINE2, 0, "PROCESSING......", SDK_DISP_DEFAULT);
	sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE WAIT", SDK_DISP_DEFAULT);
	sdkDispBrushScreen();


	memset(message,0,sizeof(buf));
	sprintf(message,"%s%s%s%s%s%d",sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,copypasskey,NrcNumCopy,0);

	Trace("xgd","message = %s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message), out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData, out, 32);
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/irsReconfirmCashout/%s/%s/%s/%s/%s/%s/%d/%s",uri,IPAddress,SvcVersion,sendData.asAgentCardNo,sendData.asTerminalSN,sendData.asPhoneNum,copypasskey,NrcNumCopy,0,sendData.asHashData);
	
	
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	
	
	ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
			if(strcmp(receivedData.ServiceStatus,"Success")==0)
			
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
				sdkDispBrushScreen();
				sdkmSleep(1500);
				
				transaction=true;              //  for pageagentcheckbalance
				secondtransactionstart=true;   //  for pageagentcheckbalance
			
				memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
				js_tok(receivedData.ServiceStatus,"serviceStatus",0);
				
				
				memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
				js_tok(receivedData.CardRef,"agentCardRef",0);
	
				memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));	
				js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
	
				memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
				js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
					
				memset(receivedData.TRX,0,sizeof(receivedData.TRX));
				js_tok(receivedData.TRX,"trx",0);
				
				memset(receivedData.CardRef,0,sizeof(receivedData.CardRef));
				js_tok(receivedData.CardRef,"agentCardRef",0);
	
				memset(receivedData.TerminalSerialNo,0,sizeof(receivedData.TerminalSerialNo));	
				js_tok(receivedData.TerminalSerialNo,"terminalSerialNo",0);
	
				memset(receivedData.TransactionLogDate,0,sizeof(receivedData.TransactionLogDate));
				js_tok(receivedData.TransactionLogDate,"transactionLogDate",0);
					
				memset(receivedData.TRX,0,sizeof(receivedData.TRX));
				js_tok(receivedData.TRX,"trx",0);
				
				memset(receivedData.Amount,0,sizeof(receivedData.Amount));
				js_tok(receivedData.Amount,"amount",0);
				
				memset(receivedData.PhoneNo,0,sizeof(receivedData.PhoneNo));
				js_tok(receivedData.PhoneNo,"receiver_mobile_number",0);
				
				memset(receivedData.BookingId,0,sizeof(receivedData.BookingId));
				js_tok(receivedData.BookingId,"receiver_id",0);
				
				memset(receivedData.SenderCountryCode,0,sizeof(receivedData.SenderCountryCode));
				js_tok(receivedData.SenderCountryCode,"sender_country_code",0);
				
				memset(receivedData.ReceiverCountryCode,0,sizeof(receivedData.ReceiverCountryCode));
				js_tok(receivedData.ReceiverCountryCode,"receiver_country_code",0);
				
				memset(receivedData.Activity,0,sizeof(receivedData.Activity));
				js_tok(receivedData.Activity,"transaction_id",0);
				
				SendIntNotifyCashOut();
				
		
			}
			
			else
			{
				sdkDispClearScreen();
				sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
				sdkDispBrushScreen();
				sdkmSleep(1500);
				transaction=false;    //for pageagentcheckbalance
				js_tok(receivedData.ErrorMessage,"message",0);
				DisplayErrorMessage("CASHOUT",receivedData.ErrorMessage,false);
			}
		}
	else
	{
		sdkDispClearScreen();	 
		sdkDispFillRowRam(SDK_DISP_LINE2, 0, "TRANSACTION FAIL.", SDK_DISP_DEFAULT);
		sdkDispFillRowRam(SDK_DISP_LINE3, 0, "PLEASE TRY AGAIN.", SDK_DISP_DEFAULT);
		sdkDispBrushScreen();
		 s32 key = sdkKbWaitKey(SDK_KEY_MASK_ENTER | SDK_KEY_MASK_ESC, 30000);
	
		 	switch (key)
     	   {
             case SDK_KEY_ENTER:
                {
                   if(cashout.CashOutMAINMenu==CashOut_New_Menu)   //for shortcut menu & old menu
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
                }
                break;

        	case SDK_KEY_ESC:
                {
                    if(cashout.CashOutMAINMenu==CashOut_New_Menu) //for shortcut menu & old menu
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
                }
				break;
             default:
                {
                
                }
                
                break;
		 }
		
	}
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)  //for shortcut menu & old menu
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}	
	
	
}
void SendIntNotifyCashOut(void)
{
	
	u8 message[128] ={0};
	s32 ret;
	u8 out[128] = {0};

	memset(message,0,sizeof(message));
	sprintf(message,"%s%s",receivedData.Activity,"true");
	
	Trace("xgd","message = %s\r\n",message);
	sdkCalcHmacSha256(TSK, strlen(TSK), message, strlen(message),out);
	
	memset(sendData.asHashData, 0, 128);
	sdkBcdToAsc(sendData.asHashData,out, 32);
	Trace("xgd","tmp = %s\r\n",sendData.asHashData);
	
	memset(sendData.asSendBuf,0,sizeof(sendData.asSendBuf));
	sprintf(sendData.asSendBuf,"%s%s/irsNotifyCashOut/%s/%s/%s/%s",uri,IPAddress,SvcVersion,receivedData.Activity,"true",sendData.asHashData);
	
	Trace("xgd","sendData.asSendBuf = %s\r\n",sendData.asSendBuf);
	

	ret = SendReceivedData(sendData.asSendBuf,false);
		if(ret!=0)
		{
			sdkDispClearScreen();
			sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionsuccess.bmp");
			sdkDispBrushScreen();
			sdkmSleep(1500);
			memset(receivedData.ServiceStatus,0,sizeof(receivedData.ServiceStatus));
			js_tok(receivedData.ServiceStatus,"serviceStatus",0);
			
			foragent = false;
			PrintIntCashout(); 
			foragent = true;									
			PrintIntCashout();
			CheckPaperRollStatus();
			if(cashout.CashOutMAINMenu==CashOut_New_Menu)  //for shortcut menu & old menu
						{
							NewMainMenu();
						}
						else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
						{
							CashOutModule();
						}		
			
		}
	else
	{
		sdkDispClearScreen();
					sdkDispShowBmp(0, 0, 240, 320, "/mtd0/res/transactionfail.bmp");
					sdkDispBrushScreen();
					sdkmSleep(1500);
		DisplayErrorMessage("","",true);
	}
	if(cashout.CashOutMAINMenu==CashOut_New_Menu)  //for shortcut menu & old menu
	{
	NewMainMenu();
	}
	else if (cashout.CashOutMAINMenu==CashOut_Old_Menu)
	{
	CashOutModule();
	}	

}



