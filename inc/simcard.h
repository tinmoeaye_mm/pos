/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : simcard.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2015/9/30
  Last Modified :
  Description   : This file contains prototypes for SIM card module
  Function List :
  History       :
  1.Date        : 2015/9/30
    Author      : Cody
    Modification: Created file

******************************************************************************/

#ifndef _SIM_CARD_H_
#define _SIM_CARD_H_

extern void ModuleSimCard(void);

#endif

