/******************************************************************************

     Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
  File Name     : agent.h
  Version       : Initial Draft
  Author		: Thu Kha Aung
  Created		: 2016/2/10
  Last Modified :
  Description   : This file contains prototypes for Agent module
  History       :
  1.Date        :  2016/2/10
    Author      : Thu Kha
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_SALES_H_
#define _MODULE_SALES_H_

typedef struct{
	 s32 servicetype;
}Forsale;
Forsale forsale;

typedef enum{
	Salesagentdeposit=0,
	agentContractEndRequest,//5(menu number)
	reactivateAgentRequest,//8
	terminalChangeRequest,//6
	terminalSwitchRequest,//7
	posHandoverRequest,
	
}Saleservice;


extern void SalesPersonChkBalance(void);
extern void SalesPersonFPMenu(void);
extern void SalesPersonFPRegister(void);
extern void SalesPersonVisit(void);
extern void SalesPersonFPReset(void);
extern void SalesPersonRegisterSendData(void);
extern void Salesreadagentcard(void);
extern void Salesagentdepositmenu(void);
extern void SalesModuleActivationCard(void);
extern void SaleRequest(void);
extern int SendReceivedData_POST(char * sendurl,char * sendbuf,bool tcp);
extern s32 CheckSALEPINModule();
extern void ReadSaleCard(void);

#endif

