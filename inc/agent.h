/******************************************************************************

     Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
  File Name     : agent.h
  Version       : Initial Draft
  Author		: Thu Kha Aung
  Created		: 2016/2/10
  Last Modified :
  Description   : This file contains prototypes for Agent module
  History       :
  1.Date        :  2016/2/10
    Author      : Thu Kha
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_AGENT_H_
#define _MODULE_AGENT_H_

extern void AgentCheckBalanceV2(void);
extern void VersionChangeV2(void);
extern void AgentActivationV2(void);
extern s32 AgentFPCheckV2(void);
extern void BankInfo(void);
extern void AgentSummReport(void);
extern s32 CheckAgentPINModule();
extern void AgentRqeuestMenu(void);
extern void AgentPinChange(void);
extern void SalesPersonChkBalance(void);
extern void AgentRefillV2(void);
extern void AgentWithdrawV2(void);
extern void PinChange(void);
extern void MemberPinChange(void);
extern void showMemberCard(void);

typedef enum{
	today,
	yesterday,

}day;

typedef struct{
	s32 TRX;
	
	
}TRXtype;
TRXtype trxtype; 

typedef struct{
	
	int AgentMainMenu;
	
}Agent;
Agent agent;

typedef enum{
	Agent_New_Menu=0,
	Agent_Old_Menu,
	
}AgentMainMenu;
#endif

