/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
  File Name     : modem.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2014/7/24
  Last Modified :
  Description   : This file contains prototypes for GPRS module
  History       :
  1.Date        : 2014/7/24
    Author      : Cody
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_GPRS_H_
#define _MODULE_GPRS_H_

extern int ModuleGprs(void);


#endif



