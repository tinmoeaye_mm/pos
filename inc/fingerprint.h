/******************************************************************************

  Copyright (C), 2001-2015, Xinguodu Co., Ltd.

******************************************************************************
  File Name     : fpc.h
  Version       : Initial Draft
  Author        : pxp
  Created       : 2015/10/08
  Last Modified :
  Description   :  This file contains fpc related function prototypes
  History       :
  1.Date        : 2015/10/08
    Author      : pxp
    Modification: Created file

******************************************************************************/

#ifndef  _MODULE_FPC_H_
#define  _MODULE_FPC_H_

extern void FPC_Test();
extern s32 EnrollFPC(u8 iCount, bool bisMix, u8 *lpOut);
extern s32 VerifyFPC(u8 *FPCdata, s32 iSafeLevel);
extern u32 FingerPrintEigen(u8 *allfingers, u8 *FPCdata);


typedef struct{
	int Error;
}FingerPrint;
FingerPrint fingerprint;

typedef enum{
	AGENT = 0,
	SALE,
	AGENTMENUCHECK,
	SALEMENUCHECK,
	MEMCASHOUTCASHIN,
	TOPUP,
	NEWMAINMENU,
	

	
}Menu;
#endif

