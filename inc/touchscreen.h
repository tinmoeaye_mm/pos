/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : touchscreen.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2015/07/13
  Last Modified :
  Description   : 
  History       :
  1.Date        : 2015/07/13
    Author      : Cody
    Modification: Created file

******************************************************************************/
#ifndef __TOUCHSCREEN_H__
#define __TOUCHSCREEN_H__

extern void ModuleTouchScreen(void);
extern bool IsTouchScreenModel(void);


#endif

