/******************************************************************************

  Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

 ******************************************************************************
  File Name     : transfer.h
  Version       : Initial Draft
  Author        : Thu Kha Aung
  Created       : 2014/11/4
  Last Modified :
  Description   : This file contains prototypes for Transfer Money
  Function List :
  History       :
  1.Date        : 2016/11/3
    Author      : Thu Kha Aung
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_TRANSFER_H_
#define _MODULE_TRANSFER_H_

typedef struct{
	
	int Transfer_MainMenu;
	
}Transfer;
Transfer transfer;

typedef enum{
	Transfer_New_Menu=0,
	Transfer_Old_Menu,
	
}Transfer_MainMenu;

void TransferModule(void);
void CashOutModule(void);
extern void ModuleTransferCustomerReadCard(void);

#endif

