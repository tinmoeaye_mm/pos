/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : printer.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2014/6/26
  Last Modified :
  Description   : This file contains printer module related function protot-
                  ypes.
  History       :
  1.Date        : 2014/6/26
    Author      : Cody
    Modification: Created file

******************************************************************************/
    
#ifndef _MODULE_PRINTER_H_
#define _MODULE_PRINTER_H_
        
extern void ModulePrinter(void);
extern void PrintAgentCheckBalance(void);
extern void PrintAgentReport(void);
extern void PrintBankInfo(void);
extern void PrintSaleCheckBalance(void);
extern void BillPrint(void);
extern void PrintCashTransfer(void);
char * valuestar(char * phonenum);
extern void PrintDomesticCashOut(void);
extern void PrintTopUp(void);
extern void PrintSlipConfirm(s32 rtmp);
char * date(char *datenum);
extern void PrintRefund(void);
extern void PrintCustomerRegister(void);
extern void PrintRecall(s32 rtmp);
extern void PrintAgentRefill(void);
extern void PrintAgentWithdraw(void);
extern void PrintMemToMemTransfer(void);
extern void printMemtoMemCashOut(void);
extern void PrintMemToMemCashIn(void);
extern void PrintMemCheckBalance(void);
extern void PrintIntCashout(void);
extern void PrintMasterAgentRefill(void);
extern void PrintMasterAgentWithDraw(void);
extern void PrintAgentActivation(void);
extern void CheckPaperRollStatus(void);
extern void PrintSaleRequest(void);
extern void PrintMasterSettlement(void);

#endif

