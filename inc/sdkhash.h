#ifndef _SDK_HASH_H_
#define _SDK_HASH_H_



/*******************************************************************
**  CORP.    :	ShenZhen Xinguodu Technology Co.,Ltd.
**  AUTHOR   :	Ashur
**  NAME     :  sdkTSKCalcHash
**  FUNCTION :  Use SHA256 Algorithm to calculate Hash value with key
**  REFERENCE:  pTSK: Terminal Session Key ; uiTSK: key length;
                pSrc: Source data          ; uiSrc: data length;
                lpOut:Hash Value
**  RETURN   :  SDK_OK
**  DATE/TIME:	2015.07.15  15:43:06
**  REMARK   :
********************************************************************/
s32 sdkTSKCalcHash(u8 *pTSK, u16 uiTSK, u8 *pSrc, u16 uiSrc, u8 *lpOut);
/*******************************************************************
**  CORP.    :	ShenZhen Xinguodu Technology Co.,Ltd.
**  AUTHOR   :	Ashur
**  NAME     :  sdkCalcHash
**  FUNCTION :  SHA256 Algorithm to calculate Hash value
**  REFERENCE:  pSrc: Source data          ; uiSrc: data length;
                lpOut:Hash Value
**  RETURN   :
**  DATE/TIME:	2015.07.15  16:14:25
**  REMARK   :
********************************************************************/
s32 sdkCalcHash(u8 *pSrc, u16 uiSrc, u8 *lpOut);

#endif
