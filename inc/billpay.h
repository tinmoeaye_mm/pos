
/******************************************************************************

     Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
  File Name     : agent.h
  Version       : Initial Draft
  Author		: Thu Kha Aung
  Created		: 2016/3/17
  Last Modified :
  Description   : This file contains prototypes for Agent module
  History       :
  1.Date        :  2016/3/17
    Author      : Thu Kha
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_BILLPAY_H_
#define _MODULE_BILLPAY_H_

extern void BillPayMainMenu(void);



typedef enum{
	AEON=0,
	GOBUS,
	BNF,
	HELLOCAB,
	CP,
	YESC,
	YCDC,
	MPTBILL,
	TICKETBO,
	TITAN,
	MESC,
	AIRDOMESTIC,
	AIROVERSEA,
	BUSTICKET,
	IHOME,
}BillType;

typedef struct{
	s32 BillPrint;
	s32 CashPrint ;
	s32 BillPayment;
	s32 BillMainMenu;
	
}PrintType;
PrintType printtype; 

typedef enum{
	Bill_New_Menu=0,
	Bill_Old_Menu,
	
}BillMainMenu;

typedef enum{
	CASHOUT=0,
	CASHTRANSFER,
	CASHIN,
	CASHBALANCE,
	MASTERREFILL,
	MASTERWITHDRAW,
	MASTERSETTLEMENT,
	
	
	
}CashType;

typedef enum{
	CASH=0,
	MEMCARD,
	
	
	
	
}BIllCashType;


bool foragent;
bool Balanceflage;

extern void MemBillPayLoginCheck(int i);
 extern void MemBillPayLoginFace(void);
 extern void IHomeSentData(void);
 extern void IHomeref(void);
 extern void IhomeLastData(void);
 extern void IhomeConfirm(void);



#endif

