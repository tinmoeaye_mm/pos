/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : dns.h
  Version       : Initial Draft
  Author        : Steven
  Created       : 2014/11/4
  Last Modified :
  Description   : This file contains prototypes for DNS
  Function List :
  History       :
  1.Date        : 2014/11/4
    Author      : Steven
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_DNS_H_
#define _MODULE_DNS_H_

extern bool OpenEth0(bool bisDhcp);
extern void ModuleDns(void);

#endif

