/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : display.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2014/6/26
  Last Modified :
  Description   : This file contains display module related function protot-
                  ypes.
  History       :
  1.Date        : 2014/6/26
    Author      : Cody
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_DISPLAY_H_
#define _MODULE_DISPLAY_H_
    
extern void ModuleDisplay(void);
extern void DispClearContent(void);

#endif

