/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : http.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2015/9/29
  Last Modified :
  Description   : This file contains prototypes for HTTP module
  History       :
  1.Date        : 2015/9/29
    Author      : Cody
    Modification: Created file

******************************************************************************/

#ifndef _HTTP_H_
#define _HTTP_H_

extern void ModuleHttpGreetingGET(void);
extern void ModuleHttpGreetingPOST(void);
extern void ModuleTerminalVersionChange(void);
extern void ModuleGetToken(void);
extern void ModuleAgentCheckBalanceV1(void);
extern void ModuleAgentCheckBalanceV2(void);
extern int SendReceivedData(char * buf,bool tcp);


#endif

