/******************************************************************************

  Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

 ******************************************************************************
  File Name     : cybpher.h
  Version       : Initial Draft
  Author        : Thu Kha Aung
  Created       : 2014/11/4
  Last Modified :
  Description   : This file contains prototypes for Cypher
  Function List :
  History       :
  1.Date        : 2016/11/3
    Author      : Thu Kha Aung
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_CYPHER_H_
#define _MODULE_CYPHER_H_

void en_message(unsigned char *msg);
void dc_message(unsigned char *msg);
char * SpaceFormat(char * pBuf);


#endif
