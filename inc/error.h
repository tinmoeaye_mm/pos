/******************************************************************************

     Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

******************************************************************************
  File Name     : error.h
  Version       : Initial Draft
  Author		: Thu Kha Aung
  Created		: 2016/4/4
  Last Modified :
  Description   : This file contains prototypes for Agent module
  History       :
  1.Date        :  2016/4/4
    Author      : Thu Kha
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_ERROR_H_
#define _MODULE_ERROR_H_

extern void DisplayErrorMessage(u8 *header ,u8 *msg,bool def);
extern void FunUndDev(s32 rtmp);

#endif

