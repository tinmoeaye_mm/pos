/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : key.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2014/6/26
  Last Modified :
  Description   : This file contains key module related function prototypes
  History       :
  1.Date        : 2014/6/26
    Author      : Cody
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_KEY_H_
#define _MODULE_KEY_H_

extern void ModuleKey(void);
extern int Getkey();
s32 GetPhoneNumber(s32 nTimeout, u8 *pBuf, s32 nLine, s32 nMinLen, s32 nMaxLen, const u8 *pPrompt, const u8 *pUnit, const u8 *pOperator);
s32 GetAmountNumber(s32 nTimeout, u8 *pBuf, s32 nLine, s32 nMinLen, s32 nMaxLen, const u8 *pPrompt, const u8 *pUnit);
s32 GetFormatNumber(s32 nTimeout, u8 *pBuf, s32 nLine, s32 nMinLen, s32 nMaxLen);

char * AgreementNumFormat(char * pBuf);
char *Decrypt(char *pBuf);

#endif

