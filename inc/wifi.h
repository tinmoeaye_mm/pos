/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : wifi.h
  Version       : Initial Draft
  Author        : Steven
  Created       : 2014/11/4
  Last Modified :
  Description   : This file contains prototypes for WIFI
  Function List :
  History       :
  1.Date        : 2014/11/4
    Author      : Steven
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_WIFI_H_
#define _MODULE_WIFI_H_

extern void ModuleWifi(void);

#endif
