/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

******************************************************************************
  File Name     : ethernet.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2014/7/24
  Last Modified :
  Description   : This file contains prototypes for ethernet module
  History       :
  1.Date        : 2014/7/24
    Author      : Cody
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_ETHERNET_H_
#define _MODULE_ETHERNET_H_

extern void ModuleEthernet(void);


#endif


