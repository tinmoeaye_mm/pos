/******************************************************************************

  Copyright (C), 2015-2016, True Money Myanmar Co., Ltd.

 ******************************************************************************
  File Name     : cybpher.h
  Version       : Initial Draft
  Author        : Thu Kha Aung
  Created       : 2014/11/4
  Last Modified :
  Description   : This file contains prototypes for Json
  Function List :
  History       :
  1.Date        : 2016/11/3
    Author      : Thu Kha Aung
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_JSON_H_
#define _MODULE_JSON_H_


int js_tok(char * dst, char * name, int obj); //finds a key/value in an object
int json_parse(char * recbuf);
int js_get_array_element(char * dst, int arraydx, int index);
int js_array(int * arraydx, char * name);


#endif

