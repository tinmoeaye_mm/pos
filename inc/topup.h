typedef struct{
	int TopUpType;
	int PaymentType;
	int Operator;
	int MAINMenu;
	int TeleMenu;
	
}Topup;
Topup topup;

typedef enum{
	New_Menu=0,
	Old_Menu,
	
}MAINMenu;

typedef enum{
	PAY_CASH = 0,
	PAY_TCARD,
	PAY_TCARDF,
}PaymentType;

typedef enum{
	TELENOR = 0,
	MECTEL,
	OOREDOO,
	MPT,
	
	
}Operator;
typedef enum{
	TelenorMenu,
}TeleMenu;

typedef enum{
	TOPUP_EPIN = 0,
	TOPUP_ELOAD,
}TopUpType;

char mobileNo[64];


extern void ChooseOperatorMenu(void);
void TopUpLoginCheck(int i);
void TopUpLoginFace(void);
s32 MemberFPCheck(void);


