/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : https.h
  Version       : Initial Draft
  Author        : xiaokai
  Created       : 2015/7/24
  Last Modified :
  Description   : This file contains prototypes for modem module
  History       :
  1.Date        : 2015/7/24
    Author      : xiaokai
    Modification: Created file

******************************************************************************/

#ifndef __HTTPS_H__
#define __HTTPS_H__


extern void ModuleHttps(void);

#endif 

