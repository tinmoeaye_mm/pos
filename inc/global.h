/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : global.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2014/6/26
  Last Modified :
  Description   : This file contains module configuration options and library
                  includes.
  Function List :
  History       :
  1.Date        : 2014/6/26
    Author      : Cody,Thu Kha Aung, Tin Moe Aye
    Modification: Created file

******************************************************************************/

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include "sdkGlobal.h"
#include "sdkexGlobal.h"
// modules header files
#include "key.h"
#include "display.h"
#include "printer.h"
#include "mifarecard.h"
#include "gprs.h"
#include "ethernet.h"
#include "dns.h"
#include "wifi.h"
#include "https.h"
#include "sdkhash.h"
#include "aes256.h"
#include "http.h"
#include "simcard.h"
#include "jsmn.h"
#include "sdkHmacSha256.h"
#include "agent.h"
#include "sales.h"
#include "fingerprint.h"
#include "topup.h"
#include "cypher.h"
#include "json.h"
#include "transfer.h"
#include "billpay.h"
#include "error.h"
#include "reprint.h"
#include "Cashout.h"


#include "../lib/sdk3rdload.h"

#define SvcVersion "v2"
#define SvcVersionV3 "v3"
#define MAXREC 2500

	//For Dev
#define IPAddress2 "203.81.80.208"
#define IPAddress "api-dev.truemoney.com.mm/trueapi"
#define IPAddress1 "122.248.120.162"
#define Port 80

	//Live
	
/*#define IPAddress2 "203.81.80.208"
#define IPAddress "api.truemoney.com.mm/trueapi"
#define IPAddress1 "122.248.120.162"
#define Port 80*/




#define uri "http://"
#define APN "mptnet"
#define ISO_OR_MX_1  11   1  // then length pointer FPC buffer is 256  Normal use
#define ISO_OR_MX_2     2  // then length pointer FPC buffer is 128
#define ERR_FINGER          0xA6                                       //手指不符
#define ERR_ID          	0xA7                                       //ID不符合
//#define DEFAULT_KEY "095True&Succe$$878366639@TruePIN"
#define DEFAULT_KEY "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
 

#define PrintBLine1 "True Money Call Center 012305360"
#define PrintBLine2 "www.truemoney.com.mm"
#define PrintBLine3 "Version 0.8.1"	
		//Print Slip
#define AppVersion "0-8-1"					//Send to server
#define ver "Version 0.8.1d"						//Show on dev POS screen
#define verlive "Version 0.8.1"

#define SafeLEVEL_1     1
#define SafeLEVEL_2     2
#define SafeLEVEL_3     3       //Normal use
#define SafeLEVEL_4     4
#define SafeLEVEL_5     5

#define KbWaitTime     60000
#define KbWaitPaperRoll 650000

#define CTRL_BLOCK    	11
#define CARDNO_BLOCK  	9
#define PIN_BLOCK   	10

bool batterymessage;

/*u8 PrintBLine3 [32];
u8 accessVesion[32];
u8 AppVersion [64];
u8 ver [32];
u8 verlife[32];*/

u8 TSK[64];


			
struct _Sys                                                                            //the important parameters of system
{
    u16 usYear;                                                                         //range(1949-2050),when >=0x50, means 19xx, <0x50 means 20xx
    u8 ucMonth;                                                                         //range(1-12)
    u8 ucDay;                                                                           //range(1-31)
    u8 ucHour;                                                                          //range(0-23)
    u8 ucMinute;                                                                        //range(0-59)
    u8 ucSecond;                                                                        //range(0-59)
};
struct _Sys stgSys;

struct _SendData{
	u8 asAuthType[10];
	u8 asPhoneNum[16];
	u8 asSenderPhone[16];
	u8 asReceiverPhone[16];
	u8 asNRC[19];
	u8 asName[30];
	u8 asFingerImage[10];
	u8 asFingerTemplate[1024];
	u8 asAgentCardPin[64];
	u8 asSalesCardPin[64];
	u8 asAgentEnterPin[64];
	u8 asAgentEnterNewPin[64];
	u8 asSalesEnterPin[64];
	u8 asAmount[12];
	u8 asTransferAmount[12];
	u8 asCashInAmount[12];
	u8 asCusCardNo[64];
	u8 asAgentCardNo[64];
	u8 asSalesCardNo[64];
	u8 asFirstDigit[2];
	u8 asTerminalSN[32];
	u8 asSimICCID[64];
	u8 asSimIMSI[64];
	u8 asSendBuf[1024];
	u8 asHashData[128];
	s32 asPOSDeposite;
	u8 asRefillAmount[10];
	int asTopUpAmount;
	u8 asAeonAgreeNo[16];
	u8 asGoBusNo[16];
	u8 asBnfBusNo[16];
	u8 asHelloCabDriverID[20];
	u8 asHelloCabVehicalNo[64];
	u8 asCPNum[10];
	u8 asBill[64];
	u8 asBillCharge[64];
	u8 asBillAmount[64];
	u8 asPowerRange[32];
	u8 asSignalRange[32];
	u8 asBatteryRange[32];
	u8 asCustomerCardNo[64];
	u8 asMasterAgentCardNo[64];
	u8 asOtherAgentCardNo[64];
	u8 asCustomerEnterPin[64];
	u8 asTicketName[32];
	u8 asTicketBookingID[32];
	u8 asWithdrawAmount[16];
	u8 asReceiverCardNo[64];
	u8 asMemberCardNo[64];
	u8 asMemberEnterNewPin[64];
	u8 asMemberEnterPin[64];
	char asEDCtime[64];
	u8 asOtherPin[64];
	char asEDCserial[64];
	 u8 asAeonName[32];
	u8 InvoiceNumber[32];
	u8 OrderNumber[32];
	u8 asSignature[125];
	u8 asSendURL[125];
	u8 asPostAgentEnterPin[125];
	u8 asHashEnterPin[64];
	u8 asPostSaleEnterPin[125];
	 u8 asRequestSaleNum[64];
};
struct _SendData sendData;

struct _ReceivedData{
	
	u8 AccessFinger[1024];
	u8 ServiceStatus[32];
	u8 BalanceAmount[32];
	u8 CardRef[32];
	u8 CardNo[32];
	u8 ShopName[128];
	u8 ShopAddress[256];
	u8 ShopNameEx[32];
	u8 TerminalSerialNo[32];
	u8 TRX[32];
	u8 TransactionLogDate[32];
	u8 key[32];
	u8 ErrorMessage[128];
	u8 asFingerTemplate[512];
	u8 openingBalance[128];
	u8 commission[128];
	u8 closingBalance[128];
	u8 mpt[15];
	u8 cp[15];
	u8 ooredoo[15];
	u8 mectel[15];
	u8 gobus[15];
	u8 bnf[15];
	u8 remit[15];
	u8 PhoneNo[25];
	u8 Amount[12];
	u8 BankName[64];
	u8 AccNum[64];
	u8 AccType[64];
	u8 AccHolderName[64];
	u8 SwiftCode[64];
	u8 TRUEREF[64];
	u8 BookingDate[32];
	u8 BookingId[32];
	u8 Trip[32];
	u8 TripDate[32];
	u8 TripTime[32];
	u8 Bus[12];
	u8 BusClass[12];
	u8 Passanger[12];
	u8 DriverId[24];
	u8 DriverName[24];
	u8 AeonName[64];
	u8 AeonAgreeNo[32];
	u8 GobusSeat[16];
	u8 TransferAmount[16];
	u8 TransferFee[16];
	u8 Passcode[32];
	u8 ExpDate[32];
	u8 SenderPhone[16];
	u8 ReceiverPhone[16];
	u8 CashOutFee[16];
	u8 PhPinCode[32];
	u8 SerialNo[32];
	u8 HelloCab[24];
	u8 BillRefNo[32];
	u8 Company[32];
	u8 InvoiceNo[32];
	u8 TotalAmount[16];
	u8 SaleCardRef[32];
	u8 AeonPhoneNo[32];
	u8 BillAmount[16];
	u8 Status[32];
	u8 CustRef[32];
	u8 CopyBillRefNo[32];
	u8 CopyCustRef[32];
	u8 DueDate[64];
	u8 TelenorEpin[32];
	u8 CnpAmount[50];
	u8 CopyDueDate[64];
	u8 Message[64];
	u8 BillType[16];
	u8 MemberRef[32];
	u8 CustomerCardNum[32];
	u8 SalesCardRef[64];
	u8 RefillAmount[16];
	u8 WithdrawAmount[16];
	u8 departcity[45];
	u8 arrivalcity[45];
	u8 Activity[45];
	u8 ReprintType[45];
	u8 SenderCountryCode[32];
	u8 ReceiverCountryCode[32];
	u8 AgentCardNo[16];
	u8 AgentCardRef[32];
	u8 MasterCardRef[32];
	u8 AccessPinCode[32];
	u8 imageforDM[64];
	u8 RegenerateCount[8];
	u8 RePassCodeFee[16];
	u8 imageforVersion[64];
	u8 InvoiceNumber[32];
	u8 OrderNumber[32];
	u8 RefNo[32];
	u8 ErrorCode[32];
	u8 setamount[32];
	u8 setopeningbalnce[32];
	u8	setclosingbalance[32];
	u8 settotalamt[32];
	
		
		
};
struct _ReceivedData receivedData;


struct _ReceivedDataNew{
	u8 NewMainMenuAmount[64];
	
};
struct _ReceivedDataNew receivedDatanew;

typedef struct{
	 s32 saleservice;
	 s32 billservice;
	 
}ForMenu;
ForMenu formenu;

typedef enum{
	pageflagon=0,
	pageflagoff,//5(menu number)

	
}Pageflag;


aes256_context ctx;
bool ForRecall;
bool CashOutRecall;
s32 printresult;
s32 PrintCashOut;
bool ForBalance;
bool transaction;
bool MecNewPin;

bool ForOTA;
bool oldmenu; 
bool poweronforNewMenu;
bool secondtransactionstart;
bool forappmainbalance; 
bool Forappmaintransaction;
bool powerfirstbeep;
bool batterytimerflage;
bool batteryzeroflage;
bool timerflage;




extern void LoginFace(void);
extern void AppMain(void);
extern void IconFace(void);
extern void SalesMenu(void);
extern void MainMenu(void);
extern void AgentMenu(void);
extern void TrueMoneyInitialize(void);




#endif

