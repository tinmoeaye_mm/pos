#ifndef _SDK_HMAC_SHA256_H_
#define _SDK_HMAC_SHA256_H_

/*******************************************************************
**  CORP.    :	ShenZhen Xinguodu Technology Co.,Ltd.
**  AUTHOR   :	Jerry
**  NAME     :  sdkCalcHmacSha256
**  FUNCTION :  Use SHA256 Algorithm to calculate Hash Mac value with key
**  REFERENCE:  pKey: Key; uiKeyLen: key length;
                pData: data; uiDataLen: data length;
                lpOut:Hash Mac Value
**  RETURN   :  SDK_OK
**  DATE/TIME:	2015.12.15  20:19:06
**  REMARK   :
********************************************************************/
s32 sdkCalcHmacSha256(u8 *pKey, u16 uiKeyLen, u8 *pData, u16 uiDataLen, u8 *lpOut);

#endif
