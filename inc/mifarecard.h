/******************************************************************************

  Copyright (C), 2001-2014, Xinguodu Co., Ltd.

 ******************************************************************************
  File Name     : mifarecard.h
  Version       : Initial Draft
  Author        : Cody
  Created       : 2014/7/23
  Last Modified :
  Description   : This file contains prototypes for mifare module
  Function List :
  History       :
  1.Date        : 2014/7/23
    Author      : Cody
    Modification: Created file

******************************************************************************/

#ifndef _MODULE_MIFARECARD_H_
#define _MODULE_MIFARECARD_H_

//extern void ModuleReadCard(void);
extern void ModuleWriteCard(void);

extern void WritePin(void);

extern void ModuleWriteCard(void);

extern void CheckPin(void);

extern s32 ModuleActivateAgentCard(void);

extern s32 ControlBlock(void);

extern void ResetCard(void);
extern void ModuleReadCard(void);

#endif

